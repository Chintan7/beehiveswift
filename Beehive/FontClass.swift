//
//  FontClass.swift
//  Beehive
//
//  Created by SoluLab on 02/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

let CenturyGothicBold9 = UIFont(name: "Gothic725 Bd BT", size: CGFloat(9.0))!
let CenturyGothicBold10 = UIFont(name: "Gothic725 Bd BT", size: CGFloat(10.0))!
let CenturyGothicBold11 = UIFont(name: "Gothic725 Bd BT", size: CGFloat(11.0))!
let CenturyGothicBold12 = UIFont(name: "Gothic725 Bd BT", size: CGFloat(12.0))!
let CenturyGothicBold15 = UIFont(name: "Gothic725 Bd BT", size: CGFloat(15.0))!
let CenturyGothicBold18 = UIFont(name: "Gothic725 Bd BT", size: CGFloat(18.0))!
let CenturyGothiCRegular11 = UIFont(name: "Century Gothic", size: CGFloat(11.0))!
let CenturyGothiCRegular12 = UIFont(name: "Century Gothic", size: CGFloat(12.0))!
let CenturyGothiCRegular13 = UIFont(name: "Century Gothic", size: CGFloat(13.0))!
let CenturyGothiCRegular14 = UIFont(name: "Century Gothic", size: CGFloat(14.0))!
let CenturyGothiCRegular15 = UIFont(name: "Century Gothic", size: CGFloat(15.0))!
let CenturyGothiCRegular16 = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
let CenturyGothiCRegular18 = UIFont(name: "Century Gothic", size: CGFloat(18.0))!

let IS_IPHONE5 = (UIScreen.main.bounds.size.width >= 568 || UIScreen.main.bounds.size.height >= 568) ? true : false
let IS_IPHONE = (UI_USER_INTERFACE_IDIOM() == .phone) ? true : false
let IS_IPAD = (UI_USER_INTERFACE_IDIOM() == .pad) ? true : false
let DeviceType = ((IS_IPAD) ? "IPAD" : (IS_IPHONE5) ? "IPHONE 5" : "IPHONE")


let WHITECOLOR = UIColor.white
let BLACKCOLOR =  UIColor.black
let GRAYCOLOR = UIColor.lightGray
let DARKGRAYCOLOR = UIColor.darkGray
let CALENDARBACKGROUNDCOLOR = UIColor.init(red: CGFloat(245 / 255.0), green: CGFloat(245 / 255.0), blue: CGFloat(245 / 255.0), alpha: CGFloat(1.0))
