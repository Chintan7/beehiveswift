//
//  AppDelegate.swift
//  Beehive
//
//  Created by SoluLab on 30/11/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import Fabric
import Crashlytics
import GooglePlaces
//import SlackKit

let APPDELEGATE = UIApplication.shared.delegate! as! AppDelegate
let STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let FONTTYPE = "Helvetica"
let screenSize = UIScreen.main.bounds.size

var LOGSTRING = ""

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,UIAlertViewDelegate,CLLocationManagerDelegate, ODTableViewDelegate{

    var window: UIWindow?

    // Variable Declaration

    var result: CGSize?
    
    var navController: UINavigationController?
    var theStatsButton: UIButton!
    var theStatsODTableView: ODTableView!
    var aAnimationView: UIView!
    var alodingLbl: UILabel!
    
    var oldLocation: CLLocation?
    var currentLocation: CLLocation?
    var locationManager: CLLocationManager?
    var circularRegion: CLCircularRegion?
    
    var monitoringRegions: NSMutableSet?
    
    var timer: Timer?
    
    var theMainReachability: Reachability?
    var theStatsArray = [OldRequest]()
    var userNotificationInfo: NSDictionary!
    var geofenceMonitor: GeofenceMonitor?

    var timeBasedPingFromServer: Int?
    var distanceFilterFromServer: Int? = 0
    var dataObjectIndex: Int?
    
    var geofences = NSMutableArray()
    var allProjectArray = NSMutableArray()
    var allNotificationArray = NSMutableArray()
    var trackerDetailArray = NSMutableArray()
    
    var plistPath: NSString?
    var commitString: String = ""
    var unreadNotificationCount: String? = ""
    var checkedInProjectID: String?
    var checkedInProjectName: String?
    var checkedInProjectLocation: String?
    
    var checkInDataDict = NSMutableDictionary()
    var projectDictionary = NSMutableDictionary()
    
    var checkInButtonSelected: Bool!
    var isInBackground: Bool? = false
    var isAbleTOCommit: Bool? = false
    var ShareingCodeWithClient: Bool? = false
    var isInternetAvailable: Bool? = false
    var isRequestInProgress: Bool? = false
    var notificationCounterFlag: Bool? = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        print("didFinishLaunchingWithOptions")
        sleep(2)
        Fabric.with([Crashlytics.self])
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        
        LocationClass.sharedInstance.setupLogFile()
        LocationClass.sharedInstance.setLog(str: "Launch Application")
        LocationClass.sharedInstance.initLocationManager()
        
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        self.setupNavigation()
        
        GMSServices.provideAPIKey(kGOOGLE_MAP_API_KEY)
        GMSPlacesClient.provideAPIKey(kGOOGLE_MAP_API_KEY)
        
        aAnimationView = UIView(frame: self.window!.frame)
        result = self.window!.screen.bounds.size
        
        alodingLbl = UILabel(frame: CGRect(x: CGFloat(((result?.width)! / 2) - 50), y: CGFloat((result?.height)! / 2), width: CGFloat(100), height: CGFloat(40)))
        alodingLbl.textAlignment = .center
        
        self.prepareChargementLoader()
        self.setupAppDelegate()
        self.registerPushNotification()

        //let bot = SlackKit.init()
        //bot.webAPI?.sendMeMessage(channel: "https://hooks.slack.com/services/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", text: "World", success: nil, failure: nil)
        
        //Timer.scheduledTimer(timeInterval: TimeInterval.init(120), target: self, selector: #selector(self.getSignalStrength), userInfo: nil, repeats: true)
        
        print("App launched.")
        
        return true
    }
    
    func getSignalStrength() {
        
        let application = UIApplication.shared
        let statusBarView = application.value(forKey: "statusBar") as! UIView
        let foregroundView = statusBarView.value(forKey: "foregroundView") as! UIView
        let foregroundViewSubviews = foregroundView.subviews
        
        var dataNetworkItemView:UIView!
        
        for subview in foregroundViewSubviews {
            if subview.isKind(of: NSClassFromString("UIStatusBarSignalStrengthItemView")!) {
                dataNetworkItemView = subview
                break
            } else {
//                print("\nSignal: 0. No Service")
                LocationClass.sharedInstance.setLog(str: "\nSignal: 0. No Service")
                return
//                return 0 //NO SERVICE
            }
        }
        
//        print("\nSignal: \(dataNetworkItemView.value(forKey: "signalStrengthBars") as! Int)")
        LocationClass.sharedInstance.setLog(str: "\nSignal: \(dataNetworkItemView.value(forKey: "signalStrengthBars") as! Int)")
//        return dataNetworkItemView.value(forKey: "signalStrengthBars") as! Int
        
    }

    
    func setupNavigation() {
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
    }

    func setRootView(_ string: String) {
        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: string)
        navController = UINavigationController(rootViewController: homeScreen)
        self.window!.rootViewController! = navController!
    }

    func openHomeScreen() {
        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
        let menuView = MenuEventViewController(nibName: "MenuEventViewController", bundle: nil)
        APPDELEGATE.navController = UINavigationController(rootViewController: homeScreen)
        let container = MFSideMenuContainerViewController.container(withCenter: APPDELEGATE.navController, leftMenuViewController: menuView, rightMenuViewController: nil)
        APPDELEGATE.window!.rootViewController = container!
    }
    
    func addStatButton() {
        ODKit.methodDispatch(afterSeconds: 1) {
            
            let theStatsButton = UIButton.init()
            self.theStatsButton = theStatsButton;
            self.window?.addSubview(theStatsButton)
            
            theStatsButton.theMinY = ODKit.getStatusBarHeight()
            theStatsButton.theWidth = (theStatsButton.superview?.theWidth)!/4;
            theStatsButton.theHeight = ODKit.getStatusBarHeight()
            theStatsButton.theCenterX = (theStatsButton.superview?.theWidth)!/4;
            theStatsButton.backgroundColor = UIColor.red
            theStatsButton.setTitle("Stats", for: theStatsButton.state)
            
            theStatsButton.methodHandleAction(block: {
                self.theStatsODTableView.isHidden = !self.theStatsODTableView.isHidden
                UIApplication.shared.keyWindow?.rootViewController?.view.endEditing(true)
                self.methodReloadStatsArray()
            })
            
            let theStatsODTableView = ODTableView.init()
            
            self.theStatsODTableView = theStatsODTableView
            theStatsButton.superview?.addSubview(theStatsODTableView)
            theStatsODTableView.theDelegate = self;        
            theStatsODTableView.theMinY = theStatsButton.theMaxY;
            theStatsODTableView.theWidth = (theStatsODTableView.superview?.theWidth)!/2;
            theStatsODTableView.theHeight = Double.init((self.window?.frame.size.height)!) - Double.init(40);
            theStatsODTableView.backgroundColor = UIColor.white
            theStatsODTableView.isHidden = true;
            self.methodReloadStatsArray()
        }
    }

    func setupAppDelegate() {

        self.timeBasedPingFromServer = 20
     
        checkInButtonSelected = UserDefaultClass.sharedInstance().theCurrentProjectId != nil ? true : false
        
        if UserDefaults.standard.bool(forKey: hasLogin) {
            self.addStatButton()
        }

        self.isRequestInProgress = false

        if UserDefaults.standard.object(forKey: "timeIntervalPing") == nil {
            self.setTimeIntervalPing(300)
        }

        self.window! = UIWindow(frame: UIScreen.main.bounds)
        // Override point for customization after application launch.
        isInBackground = false

        monitoringRegions = NSMutableSet()
        _ = UserDefaults.standard
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let basePath = ((paths.count) > 0) ? "\(paths[0])" : ""
        plistPath = (basePath as NSString).appendingPathComponent("BeehiveLocalDB.plist") as NSString?

        if notificationCounterFlag == false {
            allNotificationArray = NSMutableArray()
            self.viewNavigation()
            self.getNotificationCountValue()
        }
        
        Timer.scheduledTimer(timeInterval: TimeInterval(10), target: self, selector: #selector(self.timerFetch), userInfo: nil, repeats: true)
        
        self.window!.makeKeyAndVisible()
    }

    func registerPushNotification() {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: ([.sound, .alert, .badge]), categories: nil))
        UIApplication.shared.registerForRemoteNotifications()
    }

    func tableView(_ tableView: ODTableView, numberOfCellsInSection section: Int) -> Int {
        return self.theStatsArray.count
    }

    func tableViewAbstractCell(_ tableView: ODTableView) -> UITableViewCell {
        return StatsCell.init()
    }


    func tableView(_ tableView: ODTableView, setupAbstractCell cell: UITableViewCell) {

        let theCell = cell as! StatsCell


        theCell.theOldRequest = self.theStatsArray[cell.theIndexPath.row]
    }

    func methodReloadStatsArray() {

//        self.theStatsArray = OldRequest.getObjectsArray(with: nil, propertyToFetch: nil, sortDescriptorArray: [NSSortDescriptor.init(key: "theDate", ascending: false)]) as! [OldRequest]
        
        let fetchRequest = NSFetchRequest<OldRequest>(entityName: "OldRequest")
        
        do {
            self.theStatsArray = try APPDELEGATE.managedObjectContext.fetch(fetchRequest)
        } catch {}
        self.theStatsArray = self.theStatsArray.reversed()
        self.theStatsODTableView.reloadData()
    }

    func getProjectArrayFromLocal() -> NSMutableArray {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let basePath = (paths.count > 0) ? "\(paths.object(at: 0))" : ""
        let plistPath1 = (basePath as NSString).appendingPathComponent("BeehiveLocalDB.plist") as NSString?
        let projectDict = NSMutableDictionary(contentsOfFile: plistPath1 as! String)
        var storeProjectDetailArray = NSMutableArray()
        
        if (projectDict?["ProjectDetails"] as! NSArray).count > 0 {
            storeProjectDetailArray = NSMutableArray()
            for i in 0..<(projectDict?["ProjectDetails"] as! NSArray).count {
                storeProjectDetailArray.add((projectDict?["ProjectDetails"] as! NSArray).object(at: i))
            }
        }
        return storeProjectDetailArray
    }

    func markAsReadAllNotification() {
        var notificationStr = String()
        for i in 0..<(allNotificationArray.count) {
            let notificationTypeString = ((allNotificationArray[i] as! NSDictionary).value(forKey: "type") as! String)
            if (notificationTypeString == "checked_in") || (notificationTypeString == "checked_out") || (notificationTypeString == "invitation_accepted") || (notificationTypeString == "invitation_rejected") || (notificationTypeString == "tentative_checked_in_confirmed") || (notificationTypeString == "tentative_checked_in_declined") {

                if notificationStr.characters.count > 0 {
                    notificationStr += "|"
                }

                let str = ((allNotificationArray[i] as! NSDictionary).value(forKey: "id") as! String)
                notificationStr += str
            }
        }
        if notificationStr.characters.count > 0 {
            self.callMarkAsReadAllNotification(postString: notificationStr)
        }
    }

    func createGeofenceOfProject(_ geofenceData: NSMutableDictionary, isWebAcceptedProject webAcceptedProject: Bool) {
       
        if webAcceptedProject {
            projectDictionary = NSMutableDictionary(contentsOfFile: plistPath! as String)!
            if projectDictionary.allKeys.count == 0 {
                projectDictionary = NSMutableDictionary() /* capacity: 0 */
            }
            let projectsArray = self.getProjectArrayFromLocal()
            projectsArray.insert(geofenceData, at: 0)
            projectDictionary["ProjectDetails"] = projectsArray
            projectDictionary.write(toFile: plistPath! as String, atomically: true)
            _ = projectDictionary.write(toFile: plistPath! as String, atomically: true)
        }
        var regionCenter = CLLocationCoordinate2D()
        regionCenter.latitude = CLLocationDegrees.abs(geofenceData.value(forKey: "project_lat") as! Double)
        regionCenter.longitude = CLLocationDegrees.abs(geofenceData.value(forKey: "project_lng") as! Double)

        let newGeofenceRegion = CLCircularRegion.init(center: regionCenter, radius: CLLocationDistance.init(geofenceData.value(forKey: "project_radius") as! Double), identifier: (geofenceData.value(forKey: "id") as! String))
        
        self.locationManager?.startMonitoring(for: (newGeofenceRegion as CLRegion))
        self.updateGeofences()
    }

    func updateOldGeofence(_ geofenceData: NSMutableDictionary) {

        let allProjectsArray = self.getProjectArrayFromLocal()
        for i in 0..<allProjectsArray.count {
            if (((allProjectsArray[i] as! NSDictionary).value(forKey: "id") as! String) == (geofenceData.value(forKey: "id") as! String)) {

                (allProjectsArray.object(at: i) as! NSMutableDictionary).setValue((geofenceData.value(forKey: "project_radius") as! String), forKey: "project_radius")
                (allProjectsArray.object(at: i) as! NSMutableDictionary).setValue((geofenceData.value(forKey: "project_lat") as! String), forKey: "project_lat")
                (allProjectsArray.object(at: i) as! NSMutableDictionary).setValue((geofenceData.value(forKey: "project_lng") as! String), forKey: "project_lng")
            }
        }
        projectDictionary["ProjectDetails"] = allProjectsArray
        projectDictionary.write(toFile: plistPath! as String, atomically: true)
        self.updateGeofences()
    }

    func removeGeofence(_ geofenceData: NSMutableDictionary) {
        let allProjectsArray = self.getProjectArrayFromLocal()

        for i in 0..<allProjectsArray.count {

            if (((allProjectsArray[i] as! NSDictionary).value(forKey: "id") as! String) == (geofenceData.value(forKey: "id") as! String)) {
                allProjectsArray.remove(at: i)
            }
        }

        projectDictionary["ProjectDetails"] = allProjectsArray
        projectDictionary.write(toFile: plistPath! as String, atomically: true)
        let saveStatus = projectDictionary.write(toFile: plistPath! as String, atomically: true)
//        var centre: CLLocationCoordinate2D
//        centre.latitude = Double(geofences.value(forKey: ""))
//            = (geofenceData.value(forKey: "project_lat") as! String).doubleValue
//        centre.longitude = (geofenceData.value(forKey: "project_lng") as! String).doubleValue
        //    CLCircularRegion *deleteGeofence = [[CLCircularRegion alloc] initWithCenter:centre radius:[[geofenceData valueForKey:@"project_radius"] doubleValue] identifier:[geofenceData valueForKey:@"id"]];
        //[locationManager stopMonitoringForRegion:(CLRegion *)deleteGeofence];
        self.updateGeofences()
        //    [locationManager stopMonitoringForRegion:(CLRegion *)deleteGeofence];
    }


    func applicationWillResignActive(_ application: UIApplication) {
        print("applicationWillResignActive")
    
    }


    func getDate() -> String {
        
        let format = DateFormatter.init()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        return "\(format.string(from: Date.init()))"
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        print("applicationDidEnterBackground.")

        
        notificationCounterFlag = true
        isInBackground = true

        /*
         __block UIBackgroundTaskIdentifier background_task; //Create a task object

         background_task = [application beginBackgroundTaskWithExpirationHandler: ^ {
         //Register background_task
         [application endBackgroundTask: background_task]; //Tell the system that we are done with the tasks
         background_task = UIBackgroundTaskInvalid; //Set the task to be invalid
         //Above code called when endBackgroundTask is called
         }];

         [self.timer invalidate];
         self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeBasedPingFromServer target:self selector:@selector(_turnOnLocationManager) userInfo:nil repeats:NO];
         */
    
        if let count = unreadNotificationCount {
            
            if count.characters.count != 0 {
                    application.applicationIconBadgeNumber = Int(count)!
            } else {
                application.applicationIconBadgeNumber = 0
            }
        } else {
            application.applicationIconBadgeNumber = 0
        }
        
        //Jaimesh
//        application.beginBackgroundTask {
//            LocationClass.sharedInstance.startMonitoring()
//        }
        //
        
        LocationClass.sharedInstance.setLog(str: "Entered Background")
        
//        self.timer?.invalidate()
//        self.timer = Timer.scheduledTimer(timeInterval: Double(self.timeBasedPingFromServer!), target: self, selector: #selector(self._turnOnLocationManager), userInfo: nil, repeats: false)
//        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")
        LocationClass.sharedInstance.setLog(str: "Entered Forground")
        isInBackground = false
    }


    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")

        LocationClass.sharedInstance.setLog(str: "DidBecomeActive")
        if UserDefaults.standard.bool(forKey: hasLogin) {
            LocationClass.sharedInstance.getAllCurrentProjects()
        }
    }
    
    func setupDidBecomeActive() {

        self.theMainReachability = Reachability.init(hostname: "https://www.google.com")
        
        distanceFilterFromServer = 40
        self.timeBasedPingFromServer = self.getTimeIntervalPing()
        if distanceFilterFromServer! <= 0 && self.timeBasedPingFromServer! <= 0 {
            distanceFilterFromServer = DISTANCE_FILTER
            self.timeBasedPingFromServer = TIMEBASED_LOCATION_CHECK
        }
        
        geofenceMonitor = GeofenceMonitor()
        geofences = NSMutableArray()

        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: Double(timeBasedPingFromServer!), target: self, selector: #selector(self._turnOnLocationManager), userInfo: nil, repeats: false)
        
        self.updateGeofences()
        if deviceVersion8() {}

        let checkReachability = Reachability.init(hostname: "http://google.com")
        if (checkReachability?.isReachable)! {self.updateInterface(with: checkReachability!)}

        if notificationCounterFlag! {
            if UserDefaults.standard.bool(forKey: hasLogin) {
                self.getNotificationCountValue()
            }
            notificationCounterFlag = false
        }

    }

    func deviceVersion8() -> Bool {
//        if Double(UIDevice.current.systemVersion)! >= 8.0 {
//            return true
//        }
        return true
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        print("applicationWillTerminate")
       
        LocationClass.sharedInstance.setLog(str: "Application Terminate")
        UserDefaults.standard.set(LOGSTRING, forKey: "LOGSTRING")
        LocationClass.sharedInstance.saveTextFile()
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        UserDefaults.standard.set(false, forKey: hasCheckedIn)
        UserDefaults.standard.synchronize()
        self.saveContext()
    }
    
    func initializeLocalNotification() {
        UIApplication.shared.cancelAllLocalNotifications()
        let localNotification = UILocalNotification()
        let calendar = Calendar.current
        // gets default calendar
        var components = calendar.dateComponents([.month,.year,.day,.weekday,.second,.minute], from: Date())
        components.hour = 23
        components.minute = 59
        components.second = 59
        localNotification.timeZone! = NSTimeZone.local
        localNotification.fireDate! = calendar.date(from: components)!
        localNotification.userInfo! = ["localNotificationID" : "setNotificationForEveryMidNight"]
        localNotification.alertBody! = "Mid Night Check Out From \(UserDefaults.standard.value(forKey: "Project_Name") as! String)"
        localNotification.soundName! = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }

    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        if ((notification.userInfo!["localNotificationID"] as! String) == "setNotificationForEveryMidNight") {
            let currentDateTime = notification.fireDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let midNightTime = dateFormatter.string(from: currentDateTime!)
            if UserDefaults.standard.bool(forKey: hasCheckedIn) {
                //            Local Notification Reachability
                let checkReachability = Reachability.init(hostname: "http://www.google.com")

                if checkReachability?.isReachable == false {
                    self.storeDataLocally("\(UserDefaults.standard.value(forKey: "Project_Id") as! String)", checkOutTime: midNightTime)
                }
                else {self.checkOut((UserDefaults.standard.value(forKey: "Project_Id") as! String), checkOutTime: midNightTime)}
            }
        }
        else if ((notification.userInfo!["localNotificationID"] as! String) == "TestNotification") {
            let myAlert = UIAlertView(title: "Beehive", message: "Test Local Notificaiton", delegate: nil, cancelButtonTitle: "OK....", otherButtonTitles: "")
            myAlert.show()
        }
    }

    func addChargementLoader() {if let tmpWindow = self.window {tmpWindow.addSubview(self.aAnimationView)}}

    func prepareChargementLoader() {
        var progressInd: UIActivityIndicatorView?
        aAnimationView.backgroundColor = UIColor(red: CGFloat(0 / 255.0), green: CGFloat(0 / 255.0), blue: CGFloat(0 / 255.0), alpha: CGFloat(0.50))
        progressInd = UIActivityIndicatorView(activityIndicatorStyle: .white)
        progressInd?.hidesWhenStopped = true
        progressInd!.frame = CGRect(x: CGFloat(((result?.width)! / 2) - 20), y: CGFloat(((result?.height)! / 2) - 20), width: CGFloat(progressInd!.frame.size.width), height: CGFloat(progressInd!.frame.size.height))
        progressInd!.startAnimating()
        progressInd!.tag = 10
        alodingLbl.text = "Loading..."
        alodingLbl.textAlignment = .left
        //alodingLbl.font = UIFont(name: "Knockout-HTF31-JuniorMiddlewt", size: CGFloat(16.0))!
        //alodingLbl.backgroundColor = UIColor.clear
        alodingLbl.textColor = UIColor.white
        alodingLbl.tag = 11
        self.aAnimationView.addSubview(alodingLbl)
        self.aAnimationView.addSubview(progressInd!)
    }

    func setChargementFramesForViewMode(_ interfaceOrientation: UIInterfaceOrientation) {
        let progressInd = (aAnimationView.viewWithTag(10)! as! UIActivityIndicatorView)
        if interfaceOrientation == .portrait || (interfaceOrientation == .portraitUpsideDown) {
            progressInd.frame = CGRect(x: CGFloat(40), y: CGFloat(230), width: CGFloat(progressInd.frame.size.width), height: CGFloat(progressInd.frame.size.height))
            alodingLbl.frame = CGRect(x: CGFloat(70), y: CGFloat(280), width: CGFloat(250), height: CGFloat(20))
        }
        else if (interfaceOrientation == .landscapeLeft) || (interfaceOrientation == .landscapeRight) {
            progressInd.frame = CGRect(x: CGFloat(120), y: CGFloat(120), width: CGFloat(progressInd.frame.size.width), height: CGFloat(progressInd.frame.size.height))
            alodingLbl.frame = CGRect(x: CGFloat(150), y: CGFloat(120), width: CGFloat(250), height: CGFloat(20))
        }
    }

    func removeChargementLoader() { self.aAnimationView.removeFromSuperview() }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenStr = deviceToken.description
        var pushToken = tokenStr.replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
        if pushToken.characters.count == 0 {
            pushToken = "sakjhasdji1239812bjasd87324"
        }
        UserDefaults.standard.setValue(pushToken, forKey: "deviceTocken")
        UserDefaults.standard.synchronize()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {}

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

        
        let info = (userInfo as AnyObject) as! NSDictionary
        
        if ((info.object(forKey: "aps") as! NSDictionary).object(forKey: "content-available") as! String) == "1" {
            
            if ((info.object(forKey: "0") as! NSDictionary).object(forKey: "type") as! String) == "silent_project_updated" {
                
                self.updateOldGeofence((info.object(forKey: "0") as! NSDictionary).object(forKey: "data") as! NSMutableDictionary)
            } else if ((info.object(forKey: "0") as! NSDictionary).object(forKey: "type") as! String) == "silent_project_deleted" {
                self.removeGeofence((info.object(forKey: "0") as! NSDictionary).object(forKey: "data") as! NSMutableDictionary)
            } else if ((info.object(forKey: "0") as! NSDictionary).object(forKey: "type") as! String) == "silent_project_accepted" {
                self.createGeofenceOfProject(((info.object(forKey: "0") as! NSDictionary).object(forKey: "data") as! NSMutableDictionary), isWebAcceptedProject: true)
            }
            completionHandler(.newData)
            return
        } else {
            self.getNotificationCountValue()
        }
    }

    func saveData(inDocumentDirectory returnDataArray: NSArray) {

        if let dict = NSMutableDictionary(contentsOfFile: plistPath! as String) {
            projectDictionary = dict
        } else if projectDictionary.count == 0 {
            projectDictionary = NSMutableDictionary()
        } else if projectDictionary.allKeys.count == 0 {
            projectDictionary = NSMutableDictionary() /* capacity: 0 */
        }

        projectDictionary.setValue(returnDataArray, forKey: "ProjectDetails")
//      projectDictionary["ProjectDetails"] = returnDataArray
   
        projectDictionary.write(toFile: plistPath! as String, atomically: true)
        
        let saveStatus = projectDictionary.write(toFile: plistPath! as String, atomically: true)
        self.updateGeofences()
    }

    func storeDataLocally(_ projectid: String, checkOutTime time: String) {
        if projectDictionary.allKeys.count == 0 {
            projectDictionary = NSMutableDictionary() /* capacity: 0 */
        }
        var trackerArray = projectDictionary["CheckInCheckOutTracker"] as? NSMutableArray
        if trackerArray == nil {
            trackerArray = NSMutableArray() /* capacity: 0 */
        }
        let timeTrackerDict = NSMutableDictionary()
        
        timeTrackerDict.setValue(UserDefaults.standard.value(forKey: "Project_Id"), forKey: "id")
        timeTrackerDict.setValue(time, forKey: "checkout_time")

        trackerArray?.insert(timeTrackerDict, at: (trackerArray?.count)!)

        //        timeTrackerDict["id"] = (UserDefaults.standard.value(forKey: "Project_Id") as! String)
        //        timeTrackerDict["checkout_time"] = time
        //        trackerArray.insert(timeTrackerDict, at: trackerArray.count)
        projectDictionary["CheckInCheckOutTracker"] = trackerArray
        projectDictionary.write(toFile: plistPath! as String, atomically: true)
        let saveStatus = projectDictionary.write(toFile: plistPath! as String, atomically: true)
        self.checkInButtonSelected = false
        UserDefaults.standard.set(false, forKey: hasCheckedIn)
        UserDefaults.standard.removeObject(forKey: "Project_Name")
        UserDefaults.standard.removeObject(forKey: "Project_Location")
        UserDefaults.standard.removeObject(forKey: "Project_Id")
        UserDefaults.standard.synchronize()
    }

    func updateGeofences() {

        let storeProjectDetailArray = NSMutableArray()
        geofences.removeAllObjects()


        if let dict = NSMutableDictionary(contentsOfFile: plistPath! as String) {
            projectDictionary = dict
        }

        if ((projectDictionary.value(forKey: "ProjectDetails")) != nil) {

            let arr = projectDictionary.object(forKey: "ProjectDetails") as? NSArray

            for i in 0..<arr!.count {
                let projectArr = projectDictionary.object(forKey: "ProjectDetails") as? NSArray
                let project = projectArr?.object(at: i) as? NSDictionary
                storeProjectDetailArray.add(project)
                let geofence = geofenceMonitor?.getGeofence(project as! [AnyHashable : Any]!)
                if (geofence != nil) {
                    geofences.add(geofence!)
                }
            }

        }

        for i in 0..<storeProjectDetailArray.count {
            var centre = CLLocationCoordinate2D()

            let lati = (storeProjectDetailArray.object(at: i) as! NSDictionary).value(forKey: "project_lat")
            let long = (storeProjectDetailArray.object(at: i) as! NSDictionary).value(forKey: "project_lng")
            let radius = (storeProjectDetailArray.object(at: i) as! NSDictionary).value(forKey: "project_radius")
            let ide = (storeProjectDetailArray.object(at: i) as! NSDictionary).value(forKey: "id")

            centre.latitude = (lati as! NSString).doubleValue
            centre.longitude = (long as! NSString).doubleValue

            let region = CLCircularRegion.init(center: centre, radius: CLLocationDistance.init(radius as! String)!, identifier: ide as! String)

//            let region = CLCircularRegion(center: centre, radius: radius as! CLLocationDistance, identifier: ide as! String)
            if deviceVersion8() {
//                self.locationManager?.requestAlwaysAuthorization()
//                self.locationManager?.requestWhenInUseAuthorization()
            }
            locationManager?.startMonitoring(for: (region as CLRegion))
        }
    }
    


    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
    
        let theRegion = (region as Any) as! CLCircularRegion
        theRegion.notifyOnEntry = true
        theRegion.notifyOnExit = true

        if currentLocation?.coordinate != nil {
            if theRegion.contains(currentLocation!.coordinate) {
                self.locationManager(manager, didEnterRegion: theRegion)
            }
        }
    }

    func _turnOnLocationManager() {
        self.timer?.invalidate()
//        self.locationFilters()
    }

    func checkForLocationTime() {
        if self.timeBasedPingFromServer! > 0 {
            self.timer = Timer.scheduledTimer(timeInterval: Double(self.timeBasedPingFromServer!), target: self, selector: #selector(self._turnOnLocationManager), userInfo: nil, repeats: false)
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        self.timer?.invalidate()

        if UserDefaults.standard.bool(forKey: hasLogin) {
            let location = locations.last
            let currentLat = location!.coordinate.latitude
            let currentLong = location!.coordinate.longitude
            let currentLocationHorizontalAcc = location!.horizontalAccuracy
            let currentLocationTimeStemp = location!.timestamp
            currentLocation = locations.first
        }

        if geofences.count > 0 {
            let arr = geofences
           _ = geofenceMonitor?.monitor(with: locations.last!, geoList: arr as [AnyObject])
            
    }
        self.checkForLocationTime()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {

        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
        if !UserDefaults.standard.bool(forKey: hasCheckedIn) {
            UserDefaults.standard.set(true, forKey: hasCheckedIn)
            UserDefaults.standard.synchronize()
            self.check(in: "\(region.identifier)", checkInTime: dateTime)
        }
        
        SLAlert.showAlert(str: "Entered within region \(region.identifier)")
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
        if UserDefaults.standard.bool(forKey: hasCheckedIn) {
            self.checkOut("\(region.identifier)", checkOutTime: dateTime)
        }
        SLAlert.showAlert(str: "Exited from region \(region.identifier)")

    }

    func distanceBetweenCoordinate(_ originCoordinate: CLLocationCoordinate2D, andCoordinate destinationCoordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        let originLocation = CLLocation(latitude: originCoordinate.latitude, longitude: originCoordinate.longitude)
        let destinationLocation = CLLocation(latitude: destinationCoordinate.latitude, longitude: destinationCoordinate.longitude)
        let distance = originLocation.distance(from: destinationLocation)
        return distance
    }

    func reachabilityChanged(_ note: Notification) {
        let curReach = note.object!
        assert((curReach is Reachability), "Invalid parameter not satisfying: (curReach is Reachability)")
        self.updateInterface(with: curReach as! Reachability)
    }


    func updateInterface(with curReach: Reachability) {
        let netStatus = curReach.currentReachabilityStatus
        if netStatus != .notReachable {
            isInternetAvailable = true
            var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let basePath = (paths.count > 0) ? "\(paths[0])" : ""

            plistPath = (basePath as NSString).appendingPathComponent("BeehiveLocalDB.plist") as NSString?
            if let dict = NSMutableDictionary.init(contentsOfFile: plistPath! as String) {
                projectDictionary = dict
            }

            if projectDictionary.allKeys.count == 0 {
                projectDictionary = NSMutableDictionary() /* capacity: 0 */
            }

            if let arr = projectDictionary.object(forKey: "CheckInCheckOutTracker") as? NSMutableArray {
                trackerDetailArray = arr
            }

            if trackerDetailArray.count > 0 {
                self.postLocalData(toServer: trackerDetailArray, isFirstTime: true)
            }
        }
    }


    func postLocalData(toServer dataArray: NSMutableArray, isFirstTime firstTime: Bool) {
        if firstTime {
            dataObjectIndex = 0
        }
        else {

        }
        if ((trackerDetailArray.count) != nil) {

            if (((dataArray.object(at: dataObjectIndex!) as! NSDictionary).object(forKey:"checkin_time") as? String) != nil) {

                //                self.check(in: (dataArray[dataObjectIndex!]["id"] as! String), offlineCheckInTime: (dataArray[dataObjectIndex]["checkin_time"] as! String))



                let checkInID = (dataArray.object(at: dataObjectIndex!) as! NSDictionary).value(forKey: "id") as! String

                let checkInTime = (dataArray.object(at: dataObjectIndex!) as! NSDictionary).value(forKey: "checkin_time") as! String


                self.check(in: checkInID, offlineCheckInTime: checkInTime)

            }
            else if (((dataArray.object(at: dataObjectIndex!) as! NSDictionary).object(forKey:"checkout_time") as? String) != nil) {
                let checkOutID = (dataArray.object(at: dataObjectIndex!) as! NSDictionary).value(forKey: "id") as! String

                let checkOutTime = (dataArray.object(at: dataObjectIndex!) as! NSDictionary).value(forKey: "checkout_time") as! String

                let checkOutIntervel = (dataArray.object(at: dataObjectIndex!) as! NSDictionary).value(forKey: "checkout_interval") as! String
                
                
                self.checkOut(checkOutID, offlineCheckOutTime: checkOutTime, checkOutIntervalType: checkOutIntervel)
            }
        }
    }
    
    

    func check(in projectId: String, offlineCheckInTime checkinTime: String) {
        
        self.addChargementLoader()
        checkedInProjectID = projectId

        var dictTemp: JSONDictionary = [:]

        dictTemp["project_id"] = projectId as AnyObject?
        dictTemp["check_in_time"] = checkinTime as AnyObject?
        dictTemp["auto"] = "\(0)" as AnyObject?
    
        
        APIManager.sharedInstance.checkInProject(dict: dictTemp) { (result:JSONDictionary?, error:NSError?) in
            
            self.removeChargementLoader()
            
            if error == nil {

                if ((result?["status"] as! String) == "OK") {
                    self.trackerDetailArray.remove(at: self.dataObjectIndex)
                    self.projectDictionary["CheckInCheckOutTracker"] = self.trackerDetailArray
                    self.projectDictionary.write(toFile: self.plistPath! as String, atomically: true)
                    let saveStatus = self.projectDictionary.write(toFile: self.plistPath! as String, atomically: true)
                    self.postLocalData(toServer: self.trackerDetailArray, isFirstTime: false)
                }
            }
        }
    }


    func checkOut(_ projectId: String, offlineCheckOutTime checkOutTime: String, checkOutIntervalType type: String) {

        self.addChargementLoader()
        var dictTemp: JSONDictionary = [:]

        dictTemp["project_id"] = projectId as AnyObject?
        dictTemp["check_out_time"] = checkOutTime as AnyObject?
        dictTemp["checkout_interval"] = type as AnyObject?
        dictTemp["auto"] = "\(0)" as AnyObject?

        APIManager.sharedInstance.checkOutProject(dict: dictTemp) { (result:JSONDictionary?, error:NSError?) in

            self.removeChargementLoader()
            if error == nil {
                if ((result?["status"] as! String) == "OK") {
                    self.trackerDetailArray.remove(at: self.dataObjectIndex)
                    self.projectDictionary["CheckInCheckOutTracker"] = self.trackerDetailArray
                    self.projectDictionary.write(toFile: self.plistPath! as String, atomically: true)
                    let saveStatus = self.projectDictionary.write(toFile: self.plistPath! as String, atomically: true)
                    self.postLocalData(toServer: self.trackerDetailArray, isFirstTime: false)
                }
            }
        }
    }

    
    func timerFetch() {
        
        return
        if UserDefaults.standard.bool(forKey: hasLogin) {
            APIManager.sharedInstance.getNotificationCountValue { (dict:JSONDictionary?, error:NSError?) in
                if error == nil {
                    self.unreadNotificationCount = "\(dict?["unread_push_notification"] as! String)"
                    if let count = self.unreadNotificationCount {
                        UIApplication.shared.applicationIconBadgeNumber = Int(count)!
                    } else {
                        UIApplication.shared.applicationIconBadgeNumber = 0
                    }
                    UserDefaults.standard.setValue(self.unreadNotificationCount, forKey: "NotificationCountValue")
                    UserDefaults.standard.synchronize()
                    NotificationCenter.default.post(name: Notification.Name.init(rawValue: "NotificationCountValue"), object: self.unreadNotificationCount)
                }
            }
        }
        
        
    }

    func getNotificationCountValue() {
                self.addChargementLoader()

        //        APIManager.sharedInstance.getNotificationCountValue()

          APIManager.sharedInstance.getNotificationCountValue { (dict:JSONDictionary?, error:NSError?) in
            self.removeChargementLoader()
            if error == nil {
                self.unreadNotificationCount = "\(dict?["unread_push_notification"] as! String)"
                
                UserDefaults.standard.setValue(self.unreadNotificationCount, forKey: "NotificationCountValue")
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: Notification.Name.init(rawValue: "NotificationCountValue"), object: self.unreadNotificationCount)
            }
        }
    }

    func getAllNotificationData() {

        
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.getAllNotificationData { (dict:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            if error == nil {
                let notificationView = NotificationView()
//                self.allNotificationArray = nil
                self.allNotificationArray = NSMutableArray()
                for i in 0..<(dict?["push_notifications"] as! NSArray).count {
                    self.allNotificationArray.insert((dict?["push_notifications"] as! NSArray)[i], at: i)
                }
                //            notificationView.delegate = self
                notificationView.customView()
                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "GetAllNotificationData"), object: self)
            }
        }
    }

    func requestGetMyCurrentProject() {

        self.addChargementLoader()
        APIManager.sharedInstance.requestGetMyCurrentProject { (dict:JSONDictionary?, error:NSError?) in

            self.removeChargementLoader()
            if error == nil {

                    let arr = NSMutableArray()
                    for i in 0..<(dict?["projects"] as! NSArray).count {
                        arr.add(((dict?["projects"] as! NSArray).object(at: i) as! NSDictionary).object(forKey: "Project")!)
                    }
                    if arr.count > 0 {
                        self.saveData(inDocumentDirectory: arr)
                    }
            }
        }
    }

    func notificationReadWebService(notificationID: String) {
        APIManager.sharedInstance.notificationReadWebService(id: notificationID) { (result:JSONDictionary?, error:NSError?) in
            if error == nil {
                if "\(result?[kstatus]!)" == kOK {
                    self.getNotificationCountValue()
                }
            }
        }
        
    }

    func check(in projectId: String, checkInTime checkinTime: String) {
        let def = UserDefaults.standard
        def.set(Int(true), forKey: "project-\(projectId)")
        def.synchronize()
        checkedInProjectID = projectId
    }

    func checkOut(_ projectId: String, checkOutTime: String) {
        let def = UserDefaults.standard
        def.set(Int(false), forKey: "project-\(projectId)")
        def.synchronize()
    }

    func callMarkAsReadAllNotification(postString: String) {

        self.addChargementLoader()
        APIManager.sharedInstance.callMarkAsReadAllNotification(id: postString) { (result:JSONDictionary?, error:NSError?) in
        self.removeChargementLoader()
            if error == nil {
                if "\(result?[kstatus]!)" == kOK {
                    self.getNotificationCountValue()
                }
            }
            
        }
    }

    func viewNavigation() {

        if !UserDefaults.standard.bool(forKey: hasLogin) {
            allNotificationArray = NSMutableArray()
            var loginViewController =  LoginViewController()
            loginViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            navController = UINavigationController(rootViewController: loginViewController)
            self.window!.rootViewController = self.navController!
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        else {
            self.openHomeScreen()

        }
    }

    func navigate(toProfileView viewController: UIViewController) {

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "UserProfileViewController")
        let navigateController = UINavigationController(rootViewController: newView)
        viewController.menuContainerViewController.centerViewController = navigateController
    }

    func setTimeIntervalPing(_ timeInterval: Int) {
        let def = UserDefaults.standard
        def.set(timeInterval, forKey: "timeIntervalPing")
        def.synchronize()
        self.timeBasedPingFromServer = self.getTimeIntervalPing()
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(Int(timeBasedPingFromServer!)), target: self, selector: #selector(self._turnOnLocationManager), userInfo: nil, repeats: false)
    }

    func getTimeIntervalPing() -> Int {
        let ping = UserDefaults.standard.integer(forKey: "timeIntervalPing")
        return ping
    }

    // MARK: - Core Data stack

//    @available(iOS 10.0, *)
//    lazy var persistentContainer: NSPersistentContainer = {
//        /*
//         The persistent container for the application. This implementation
//         creates and returns a container, having loaded the store for the
//         application to it. This property is optional since there are legitimate
//         error conditions that could cause the creation of the store to fail.
//        */
//        let container = NSPersistentContainer(name: "Beehive")
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                 
//                /*
//                 Typical reasons for an error here include:
//                 * The parent directory does not exist, cannot be created, or disallows writing.
//                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                 * The device is out of space.
//                 * The store could not be migrated to the current model version.
//                 Check the error message to determine what the actual problem was.
//                 */
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        return container
//    }()
//
//    // MARK: - Core Data Saving support
//
//    func saveContext () {
//
//        var context = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)
//
//        if #available(iOS 10.0, *) {
//            context = persistentContainer.viewContext
//        } else {
//            // Fallback on earlier versions
//        }
//        if context.hasChanges {
//            do {
//                try context.save()
//            } catch {
//                // Replace this implementation with code to handle the error appropriately.
//                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                let nserror = error as NSError
//                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//            }
//        }
//    }
    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.solulab.coreDBdemo" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "BeehiveSwift", withExtension: "momd")
        return NSManagedObjectModel(contentsOf: modelURL!)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }

        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    
}

