//
//  ProjectsTable.swift
//  Beehive
//
//  Created by Ankit on 2/14/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit

class ProjectsTable: NSManagedObject {
    @NSManaged var theCreated: String?
    @NSManaged var theId: String?
    @NSManaged var theModified: String?
    @NSManaged var theCity: String?
    @NSManaged var theCountry: String?
    @NSManaged var theLatitude: String?
    @NSManaged var theLongitude: String?
    @NSManaged var theLocation: String?
    @NSManaged var theName: String?
    @NSManaged var theNote: String?
    @NSManaged var thePostCode: String?
    @NSManaged var theRadius: String?
    @NSManaged var theState: String?
    @NSManaged var theStatus: String?
    @NSManaged var theStreet: String?
    @NSManaged var theStreetNumber: String?
    @NSManaged var theTimeZone: String?
    @NSManaged var theUserId: String?    
}
