//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CrewMemberPaywallViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 03/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class CrewMemberPaywallViewController: BaseViewController {

    @IBOutlet var fieldsContainerView: UIScrollView!
    @IBOutlet var awesomeTextLabel: UILabel!
    @IBOutlet var termslabel: UILabel!
    @IBOutlet var addCreditCardButton: UIButton!
    @IBOutlet var nothanksButton: UIButton!
    @IBOutlet var termConditionButton: UIButton!
    @IBOutlet var backGroundScreen: UIView!
    @IBOutlet var termBackground: UIView!
    @IBOutlet var termConditionTextView: UITextView!
    @IBOutlet var termConditionClose: UIButton!
    var isAcceptCreditCards = false
    var resultText = ""

    @IBAction func termConditionButtonPressed(_ sender: Any) {
        backGroundScreen.isHidden = false
        if screenSize.height == 568 {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.termBackground.frame = CGRect(x: CGFloat(self.termBackground.frame.origin.x), y: CGFloat(218), width: CGFloat(self.termBackground.frame.size.width), height: CGFloat(350))
                var frame = self.termConditionTextView.frame
                frame.size.height = 260
                self.termConditionTextView.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
            })
        }
        else {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.termBackground.frame = CGRect(x: CGFloat(self.termBackground.frame.origin.x), y: CGFloat(130), width: CGFloat(self.termBackground.frame.size.width), height: CGFloat(300))
                var frame = self.termConditionTextView.frame
                frame.size.height = 260
                self.termConditionTextView.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
            })
        }
    }

    @IBAction func nothanksButtonPressed(_ sender: Any) {

        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController") as? HomeScreenController
        self.navigationController!.pushViewController(homeScreen!, animated: true)
    }

    @IBAction func termConditionCloseClick(_ sender: Any) {
        backGroundScreen.isHidden = true
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            let frame = self.termBackground.frame
            self.termBackground.frame = frame
        }, completion: {(_ finished: Bool) -> Void in
            //        NSLog(@"Closed");
        })
    }

    @IBAction func pay() {

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "CreditCardViewController")
        self.navigationController!.pushViewController(newView, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.customizeView()
    }

    override func viewWillAppear(_ animated: Bool) {
        print("View Will Appear")
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func customizeView() {
        fieldsContainerView.contentSize = CGSize(width: CGFloat(320), height: CGFloat(650))
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let backBtn = UIButton(type: .custom)
        backBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        backBtn.setImage(image, for: .normal)
        backBtn.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let bckBarButton = UIBarButtonItem(customView: backBtn)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        var barItems = [UIBarButtonItem]()
        barItems.append(bckBarButton)
        barItems.append(flexSpace)
        viewToolbar.setItems(barItems, animated: true)
        fieldsContainerView.addSubview(viewToolbar)
            // Awesome Header Label
        let firstLabel = "Awesome!"
        let secondLabel = "You've added crew members to a project."
        let thirdLabel = "Tracking crew members requires a Crew Member subscription. Sign up now and start tracking!"
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)\n\(thirdLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Gothic725 Bd BT", size: CGFloat(18.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: firstLabel.characters.count, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(14.0))!, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1), length: thirdLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (firstLabel.characters.count) + (secondLabel.characters.count), length: thirdLabel.characters.count))
        awesomeTextLabel.attributedText = customTextLabelAttribute
            //  Terms label
        let labelOne = "Only $5 per crew member"
        let labelTwo = "Projects are free. Active crew members are only 5$ per month."
        let labelThree = "Don't use it, don't pay"
        let labelFour = "You will not be charged for inactive crew members."
        let labelFive = "Free Projects"
        let labelSix = "Create as many projects as you like, free of charge."
        let customTermLabelAttribute = NSMutableAttributedString(string: "\(labelOne)\n\(labelTwo)\n\n\(labelThree)\n\(labelFour)\n\n\(labelFive)\n\(labelSix)")
        //First 1
        customTermLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(15.0))!, range: NSRange(location: 0, length: labelOne.characters.count))
        customTermLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: labelOne.characters.count))
        // 2
        customTermLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: labelOne.characters.count + 1, length: labelTwo.characters.count))
        customTermLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: labelOne.characters.count + 1, length: labelTwo.characters.count))
        //second 1
        customTermLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(14.0))!, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2), length: labelThree.characters.count))
        customTermLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2), length: labelThree.characters.count))
        //2
        customTermLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(11.0))!, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2) + (labelThree.characters.count + 1), length: labelFour.characters.count))
        customTermLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 2) + (thirdLabel.characters.count + 1), length: labelFour.characters.count))
        //Third 1
        customTermLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(14.0))!, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2) + (labelThree.characters.count + 1) + (labelFour.characters.count + 2), length: labelFive.characters.count))
        customTermLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2) + (labelThree.characters.count + 2) + (labelFour.characters.count + 2), length: labelFive.characters.count))
        // 2
        customTermLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(11.0))!, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2) + (labelThree.characters.count + 1) + (labelFour.characters.count + 2) + (labelFive.characters.count + 1), length: labelSix.characters.count))
        customTermLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (labelOne.characters.count + 1) + (labelTwo.characters.count + 2) + (labelThree.characters.count + 1) + (labelFour.characters.count + 2) + (labelFive.characters.count + 1), length: labelSix.characters.count))
        termslabel.attributedText = customTermLabelAttribute
        let termButtonLabel = "Legal | Terms & Conditions"
        termConditionButton.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        termConditionButton.setTitle(termButtonLabel, for: .normal)
        if screenSize.height == 568 {
            termBackground.frame = CGRect(x: CGFloat(termBackground.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(termBackground.frame.size.width), height: CGFloat(350))
        }
        else {
            termBackground.frame = CGRect(x: CGFloat(termBackground.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(termBackground.frame.size.width), height: CGFloat(300))
        }
        termBackground.layer.cornerRadius = 5.0
        termBackground.layer.borderWidth = 1.0
        termBackground.layer.borderColor = UIColor.darkGray.cgColor
        termConditionTextView.font = CenturyGothiCRegular15
        backGroundScreen.isHidden = true
    }




    func backBtnClick() {
        popTo()
    }
}
//
//  CrewMemberPaywallViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 03/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
