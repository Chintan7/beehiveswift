//
//  Profile.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation


class Profile {

    var firstName: String?
    var lastName: String?
    var copmpany: String?
    var contactNoWork: String?

//    var payment: String?
//    var email: String?
//    var id: String?

    var mobile: String?
    var streetNumber: String?

    var street: String?
    var city: String?
    var state: String?
    var country: String?
    var postalCode: String?
    var lat: String?
    var lng: String?
    var serviceID: String?
    var userimg: String?
    var extensi: String?

    func toJasonDict() -> JSONDictionary {
        var dict: JSONDictionary = [:]
        if let firstName = firstName { dict[kfirstName] = firstName as AnyObject? }
        if let lastName = lastName { dict[klastName] = lastName as AnyObject? }
        if let copmpany = copmpany { dict["user_company"] = copmpany as AnyObject? }
        if let firstName = contactNoWork { dict["user_contact_no_work"] = firstName as AnyObject? }
        if let firstName = mobile { dict["user_mobile"] = firstName as AnyObject? }
        if let firstName = streetNumber { dict["user_street_number"] = firstName as AnyObject? }
        if let firstName = street { dict["user_street"] = firstName as AnyObject? }
        if let firstName = city { dict["user_city"] = firstName as AnyObject? }
        if let firstName = state { dict["user_state"] = firstName as AnyObject? }
        if let firstName = country { dict["user_country"] = firstName as AnyObject? }
        if let firstName = postalCode { dict["user_postal_code"] = firstName as AnyObject? }
        if let firstName = lat { dict["user_lat"] = firstName as AnyObject? }
        if let firstName = lng { dict["user_lng"] = firstName as AnyObject? }
        if let firstName = serviceID { dict["service_id"] = firstName as AnyObject? }
        if let firstName = userimg { dict["user_avatar"] = firstName as AnyObject? }
        if let firstName = extensi { dict["extension"] = firstName as AnyObject? }
        return dict

    }

/*
     ["payment_status": 1, "result": {
     status = OK;
     }, "data": {
     User =     {
     email = "chintan@solulab.com";
     "first_name" = Chintan;
     id = 296;
     "last_name" = Thakkar;
     "user_avatar" = "/users/5832ebf1-88e4-4484-b2e5-5ab0ac1f2daa.jpg";
     "user_city" = Ahmedabad;
     "user_company" = Solulab;
     "user_contact_no_work" = 469;
     "user_country" = India;
     "user_mobile" = 1234567890;
     "user_postal_code" = 380059;
     "user_state" = Gujarat;
     "user_street" = Thaltej;
     "user_street_number" = 3p;
     };
     UserService =     (
     {
     id = 69;
     service = staff;
     }
     );
     }]
     */
}

