//
//  Register.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

class Register {

    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var cPassword: String?
    var terms: String?

    func toJsonDict() -> JSONDictionary {

        var dict: JSONDictionary = [:]

        if let firstName = firstName { dict[kfirstName] = firstName as AnyObject? }
        if let lastName = lastName { dict[klastName] = lastName as AnyObject? }
        if let email = email { dict[kEmail] = email as AnyObject? }
        if let password = password { dict[ktpassword] = password as AnyObject? }
        if let cPassword = cPassword { dict[kcpassword] = cPassword as AnyObject? }
        if let terms = terms { dict[kterms_conditions] = terms as AnyObject? }

        return dict

    }

}
