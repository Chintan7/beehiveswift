//
//  Projects.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

class Projects {

    var roalID: String?
    var pageNo: String?

    func tojsonDict() -> JSONDictionary {

        var dic: JSONDictionary = [:]

        if let roalID = roalID { dic[krole_id] = roalID as AnyObject? }
        if let pageNo = pageNo { dic[kpage] = pageNo as AnyObject? }

        return dic
    }

}

class ProjectsResponse {

    var status: String?
    var projects: [ProjectList]?
    var count: String?

    func populateWithJson(dict: JSONDictionary) {

        if let status = dict[kstatus] as? String{
            self.status = status
        }

        if let count = dict[kcount] as? String {
            self.count = count
        }

        if let project = dict[kprojects] as? [JSONDictionary] {

            var projectsArrTemp = [ProjectList]()

            for projectTemp in project {
                let projectTempObj = ProjectList()
                projectTempObj.populateWithJson(dict: projectTemp)
                projectsArrTemp.append(projectTempObj)
            }
            self.projects = projectsArrTemp
        }
    }
}

class ProjectList {
    var countReview: CountReview?
    var checkeOutCrew: CheckeOutCrew?
    var checkedInCrew: CheckedInCrew?
    var project: Project?

    func populateWithJson(dict: JSONDictionary) {

        if let countReview = dict[k0] as? JSONDictionary {
            let countReviewTemp = CountReview()
            countReviewTemp.populateWithJson(dict: countReview)
            self.countReview = countReviewTemp
        }

        if let checkeOutCrew = dict[kCheckeOutCrew] as? JSONDictionary {
            let checkeOutCrewTemp = CheckeOutCrew()
            checkeOutCrewTemp.populateWithJson(dict: checkeOutCrew)
            self.checkeOutCrew = checkeOutCrewTemp
        }

        if let checkedInCrew = dict[kCheckedInCrew] as? JSONDictionary {
            let checkedInCrewTemp = CheckedInCrew()
            checkedInCrewTemp.populateWithJson(dict: checkedInCrew)
            self.checkedInCrew = checkedInCrewTemp
        }

        if let project = dict[kProject] as? JSONDictionary {
            let projectTemp = Project()
            projectTemp.populateWithJson(dict: project)
            self.project = projectTemp
        }


    }

}

class CountReview {

    var count_crews: String?

    func populateWithJson(dict: JSONDictionary)  {
        if let count_crews = dict[kcount_crews] as? String {
            self.count_crews = count_crews
        }
    }

}
class CheckeOutCrew {
    var total: String?

    func populateWithJson(dict: JSONDictionary)  {
        if let checkeOutCrew = dict[ktotal] as? String {
            self.total = checkeOutCrew
        }
    }
}

class CheckedInCrew {
    var total: String?

    func populateWithJson(dict: JSONDictionary)  {

        if let checkedInCrew = dict[ktotal] as? String {
            self.total = checkedInCrew
        }

    }
}

class Project {

    var created: String?
    var id: String?
    var modifide: String?
    var city: String?
    var country: String?
    var lat: String?
    var lng: String?
    var location: String?
    var name: String?
    var note: String?
    var postalCode: String?
    var radius: String?
    var state: String?
    var status: String?
    var street: String?
    var streetNumber: String?
    var timeZone: String?
    var userID: String?

    func populateWithJson(dict: JSONDictionary) {

        if let created = dict[kCreated] as? String {
            self.created = created
        }

        if let id = dict[kID] as? String {
            self.id = id
        }

        if let modifide = dict[kmodified] as? String {
            self.modifide = modifide
        }

        if let city = dict[kproject_city] as? String {
            self.city = city
        }

        if let country = dict[kproject_country] as? String {
            self.country = country
        }

        if let lat = dict[kproject_lat] as? String {
            self.lat = lat
        }

        if let lng = dict[kproject_lng] as? String {
            self.lng = lng
        }

        if let location = dict[kproject_location] as? String {
            self.location = location
        }

        if let name = dict[kproject_name] as? String {
            self.name = name
        }

        if let note = dict[kproject_note] as? String {
            self.note = note
        }

        if let postalCode = dict[kproject_postal_code] as? String {
            self.postalCode = postalCode
        }

        if let postalCode = dict[kproject_postal_code] as? String {
            self.postalCode = postalCode
        }

        if let radius = dict[kproject_radius] as? String {
            self.radius = radius
        }

        if let state = dict[kproject_state] as? String {
            self.state = state
        }

        if let street = dict[kproject_street] as? String {
            self.street = street
        }

        if let streetNumber = dict[kproject_street_number] as? String {
            self.streetNumber = streetNumber
        }

        if let timeZone = dict[kproject_timezone] as? String {
            self.timeZone = timeZone
        }

        if let userID = dict[kuser_id] as? String {
            self.userID = userID
        }

    }
    /*
     created = "2016-11-25 18:35:35";
     id = 537;
     modified = "2016-11-25 18:35:35";
     "project_city" = "San Francisco County";
     "project_country" = "United States";
     "project_lat" = "37.78735900";
     "project_lng" = "-122.40822700";
     "project_location" = "246-298 Powell St, San Francisco, CA 94102, USA";
     "project_name" = USProject;
     "project_note" = "";
     "project_postal_code" = 94102;
     "project_radius" = "2000.00";
     "project_state" = California;
     "project_status" = 1;
     "project_street" = "San Francisco";
     "project_street_number" = "246-298";
     "project_timezone" = "America/Los_Angeles";
     "user_id" = 300;
     */

}
