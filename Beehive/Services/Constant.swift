//
//  Constant.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation



let kMAIN_URL = "http://54.68.29.83/mobile/v1/"

let BaseWebRequestUrl = "http://54.68.29.83/mobile/v1/"
let ImageBaseUrl = "http://54.68.29.83"
let LoginRequestUrl = "signIn.json"
let GetMyProjects = "getMyProjects.json"
let SignupRequestUrl = "signUp.json"
let ForgotPasswordUrl = "getPassword.json"
let CheckEmailVerificationUrl = "checkEmailVerification.json"
let ResendEmailVerificationLink = "resendEmailVerification.json"
let GetMyProfileDetails = "getMyProfile.json"
let SaveMyProfileURL = "saveProfile.json"
let DeleteCrewMemberURL = "deleteCrewMember.json"
let CreateProjectUrl = "createProject.json"
let GetProjectDetails = "getProjectDetails/"
let GetAllArchivedProject = "getAllMyArchivedProjects.json?"
let GetProjectNotesURL = "getNotes/"
let UploadImageUrl = "imageURL"
let GetNoteDetailsURL = "getNoteDetail/"
let SaveNotesURL = "saveNote.json"
let DeleteProjectURL = "deleteProject/"
let MakeProjectArchiveURL = "makeArchive/"
let DisableCrewNotificationsUrl = "settingPushNotification.json"
let GetUnreadNotificationCount = "countUnreadNotifications.json"
let GetAllNotifications = "getNotifications.json"
let MarkNotificationAsReadURL = "markReadAction.json"
let MarkAllNotificationAsReadUrl = "markReadAllNotifications.json"
let GetSearchedCrewForProject = "searchCrews.json"
let AddCrewBySelection = "sendInvite.json"
let InviteCrewViaEmail = "sendInviteEmail.json"

//MARK: Login --
let kAPPNAME        = "Beehive"
let kAPPUser        = "LoggedInUser"
let kDeviceToken    = "deviceTocken"
let kResult         = "result"
let kEmail          = "email"
let kPassword       = "password"
let k_token         = "_token"
let k_username      = "_username"
let kmessage        = "message"
let kstatus         = "status"
let kINVALID        = "INVALID"
let kFAILED         = "FAILED"
let kOK             = "OK"
let kERROR          = "ERROR"


//MARK: Register - 

let kfirstName          = "first_name"
let klastName           = "last_name"
let ktpassword          = "tpassword"
let kcpassword          = "cpassword"
let kterms_conditions   = "terms_conditions"

let krole_id        = "role_id"
let kpage           = "page"

let kcount          = "count"
let kprojects       = "projects"
let k0              = "0"
let kcount_crews        = "count_crews"
let kCheckeOutCrew      = "CheckeOutCrew"
let ktotal              = "total"
let kCheckedInCrew      = "CheckedInCrew"
let kProject            = "Project"
let kCreated            = "created"
let kID                 = "id"
let kmodified           = "modified"
let kproject_city       = "project_city"
let kproject_country    = "project_country"
let kproject_lat        = "project_lat"
let kproject_lng        = "project_lng"
let kproject_location   = "project_location"
let kproject_name       = "project_name"
let kproject_note       = "project_note"
let kproject_postal_code    = "project_postal_code"
let kproject_radius     = "project_radius"
let kproject_state      = "project_state"
let kproject_status     = "project_status"
let kproject_street     = "project_street"
let kproject_street_number  = "project_street_number"
let kproject_timezone    = "project_timezone"
let kuser_id            = "user_id"


