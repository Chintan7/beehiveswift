//
//  Login.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

public typealias JSONDictionary = [String: AnyObject]

class Login {

    var deviceToken: String?
    var email: String?
    var password: String?

    func toJsonDict() -> JSONDictionary {
        var dict: JSONDictionary = [:]

        if let deviceToken = deviceToken { dict[kDeviceToken] = deviceToken as AnyObject? }

        if let email = email { dict[kEmail] = email as AnyObject? }

        if let password = password { dict[kPassword] = password as AnyObject? }

        return dict
    }
}


public class LoggedInUser {


    var token: String?
    var userName: String?
    var status: String?
    var message: String?

    public class var sharedInstance: LoggedInUser {
        struct Singleton {
            static let instance: LoggedInUser = LoggedInUser()
        }
        return Singleton.instance
    }
        
    init() {

        if let user = UserDefaults.standard.object(forKey: kAPPUser) as? JSONDictionary {
            self.populateWithJson(dict: user)
        }

    }

    func populateWithJson(dict: JSONDictionary) {

        if let token = dict[k_token] as? String {
            self.token = token
        }

        if let userName = dict[k_username] as? String {
            self.userName = userName

            
        }

        if let status = dict[kstatus] as? String {
            self.status = status
        }
        if let message = dict[kmessage] as? String {
            self.message = message
        }

    }

    func toJsonDict() -> JSONDictionary {

        var dic: JSONDictionary = [:]

        if let token = token { dic[k_token] = token as AnyObject? }
        if let userName = userName { dic[k_username] = userName as AnyObject? }
        if let status = status { dic[kstatus] = status as AnyObject? }
        if let message = message { dic[kmessage] = message as AnyObject? }
        return dic
    }

    func getAuth() -> [String:String] {

        if self.userName == nil {
            return ["":""]
        }
        if self.token == nil {
            return ["":""]
        }

        let authStr = "\(self.userName!):\(self.token!)".data(using: .utf8)
        print(authStr)
        let authValue = "Basic \(authStr!.base64EncodedString(options: .endLineWithCarriageReturn))"

        print(authValue)
        return ["Authorization": "\(authValue)"]
    }

    func authValue() -> String {
        if self.userName == nil {
            return ""
        }
        if self.token == nil {
            return ""
        }

        let authStr = "\(self.userName!):\(self.token!)".data(using: .utf8)
        print(authStr)
        return "Basic \(authStr!.base64EncodedString(options: .endLineWithCarriageReturn))"
    }

}
