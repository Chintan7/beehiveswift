//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  ProjectManager.h
//  Beehive
//
//  Created by OlDor on 4/21/16.
//  Copyright © 2016 SDNMACMINI13. All rights reserved.
//
import Foundation

let keyProjectManagerDidCheckInOutNotification = "keyProjectManagerDidCheckInOutNotification"

class ProjectManager: NSObject {

    private(set) var isInProgress = false

    public class var sharedInstance: ProjectManager {
        struct Singleton {
            static let instance: ProjectManager = ProjectManager()
        }
        return Singleton.instance
    }

    override init() {
        super.init()
        self.methodInitProjectManager()
    }

    func methodInitProjectManager() {
        isInProgress = false
    }

    func setIsInProgressMethod(_ isInProgress: Bool) {
        if isInProgress == isInProgress {
            return
        }
        self.isInProgress = isInProgress
    }


    func methodProcessLocation(_ theLocation: CLLocation) {
//        methodAssert(theLocation)

        let theDateFormatter = DateFormatter()
        theDateFormatter.locale = NSLocale.getDefault() as Locale!
        theDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let theDateString = theDateFormatter.string(from: Date())

        var theArray = [AnyObject]()

        for i in 0..<Int((UserDefaultClass.sharedInstance().theRequestArray?.count)!) {
            print(UserDefaultClass.sharedInstance().theRequestArray?[i])
            theArray.append(UserDefaultClass.sharedInstance().theRequestArray?[i] as AnyObject)
        }

        var arrTemp = [String]()
        arrTemp.append("\(theLocation.coordinate.latitude)")
        arrTemp.append("\(theLocation.coordinate.longitude)")
        arrTemp.append("\(theDateString)")
        theArray.append(arrTemp as AnyObject)

//        theArray.append(UserDefaultClass.sharedInstance().theRequestArray as! [String])
//        theArray.addObjects(from: UserDefaultClass.sharedInstance().theRequestArray!)
        //theArray.add([theLocation.coordinate.latitude,theLocation.coordinate.longitude,theDateString])
//        theArray.addObjects(from: [theLocation.coordinate.latitude,theLocation.coordinate.longitude,theDateString])
        //theArray += UserDefaults.sharedInstance().theRequestArray
//        theArray.append([(theLocation.coordinate.latitude), (theLocation.coordinate.longitude), theDateString])

        UserDefaultClass.sharedInstance().theRequestArray = theArray as? [[String]]

//        UserDefaults.sharedInstance().theRequestArray = theArray
        if !self.isInProgress {
            self.methodSendAllInfo()
        }
    }

    func methodSendAllInfo() {
        self.isInProgress = true

        BeehiveProject.methodDownloadAllProjectsWithNoCopyBlock { (theProjectArr:[BeehiveProject]?, error:Error?) in


            if (theProjectArr?.count)! > 0 {

                var theRequestArray = UserDefaultClass.sharedInstance.theRequestArray
                if theRequestArray!.count == 0 {
                    print("no coordinates to send")
                    self.isInProgress = false
                    return
                }
                
                var theObject = theRequestArray.first!
                var theLatitude: Double = CDouble((Int(theObject[.indexLatitude])))
                var theLongitude: Double = CDouble((Int(theObject[.indexLongitude])))
                var theCoordinateLocation = CLLocation(latitude: theLatitude, longitude: theLongitude)
                var theTimeStampString = theObject[.indexDate]
                var theLocationCoordinate2D = CLLocationCoordinate2DMake(theLatitude, theLongitude)
                var theClosestDistance: Double = -1
                var theClosestProject: BeehiveProject?
                for j in 0..<Int(theProjectArr!.count!) {
                    var theProject = theProjectArray[j]
                    var theCircularRegion = CLCircularRegion(center: CLLocationCoordinate2DMake(CDouble(theProject.theLatitude), CDouble(theProject.theLongitude)), radius: CDouble(theProject.theRadius), identifier: theProject.theId)
                    if theCircularRegion.containsCoordinate(theLocationCoordinate2D) {
                        var theRegionLocation = CLLocation(latitude: CDouble(theProject.theLatitude), longitude: CDouble(theProject.theLongitude))
                        var theDistance: Double = theCoordinateLocation.distance(from: theRegionLocation)
                        if theClosestDistance < 0 || theDistance < theClosestDistance {
                            theClosestDistance = theDistance
                            theClosestProject = theProject
                        }
                    }
                }
                var isTest = false
                if isTest {
                    if UserDefaults.sharedInstance().theCurrentProjectId {
                        self.methodCheck(in: false, projectId: UserDefaults.sharedInstance().theCurrentProjectId, timeStamp: theTimeStampString, noCopyBlock: {() -> Void in
                            var theArray = [Any]()
                            theArray += UserDefaults.sharedInstance().theRequestArray
                            theArray.remove(at: 0)
                            UserDefaults.sharedInstance().theRequestArray = theArray
                            UserDefaults.sharedInstance().theCurrentProjectId = nil
                            self.methodSendAllInfo()
                            NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                        })
                        return
                    }
                    else {
                        theClosestProject = BeehiveProject.getObjectsArray(withPredicate: nil, propertyToFetchArray: nil, sortDescriptorArray: nil).first!
                        self.methodCheck(in: true, projectId: theClosestProject!.theId, timeStamp: theTimeStampString, noCopyBlock: {() -> Void in
                            var theArray = [Any]()
                            theArray += UserDefaults.sharedInstance().theRequestArray
                            theArray.remove(at: 0)
                            UserDefaults.sharedInstance().theRequestArray = theArray
                            UserDefaults.sharedInstance().theCurrentProjectId = theClosestProject!.theId
                            self.methodSendAllInfo()
                            NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                        })
                        return
                    }
                }
                else {
                    if isEqual(UserDefaults.sharedInstance().theCurrentProjectId, theClosestProject!.theId) {
                        var theArray = [Any]()
                        theArray += UserDefaults.sharedInstance().theRequestArray
                        theArray.remove(at: 0)
                        UserDefaults.sharedInstance().theRequestArray = theArray
                        self.methodSendAllInfo()
                        NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                        print("coordinate deleted")
                        return
                    }
                    else {
                        if UserDefaults.sharedInstance().theCurrentProjectId {
                            self.methodCheck(in: false, projectId: UserDefaults.sharedInstance().theCurrentProjectId, timeStamp: theTimeStampString, noCopyBlock: {() -> Void in
                                var theArray = [Any]()
                                theArray += UserDefaults.sharedInstance().theRequestArray
                                theArray.remove(at: 0)
                                UserDefaults.sharedInstance().theRequestArray = theArray
                                UserDefaults.sharedInstance().theCurrentProjectId = nil
                                self.methodSendAllInfo()
                                NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                            })
                            return
                        }
                        else {
                            self.methodCheck(in: true, projectId: theClosestProject!.theId, timeStamp: theTimeStampString, noCopyBlock: {() -> Void in
                                var theArray = [Any]()
                                theArray += UserDefaults.sharedInstance().theRequestArray
                                theArray.remove(at: 0)
                                UserDefaults.sharedInstance().theRequestArray = theArray
                                UserDefaults.sharedInstance().theCurrentProjectId = theClosestProject!.theId
                                self.methodSendAllInfo()
                                NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                            })
                            return
                        }
                    }
                    if UserDefaults.sharedInstance().theCurrentProjectId {
                        self.methodCheck(in: false, projectId: UserDefaults.sharedInstance().theCurrentProjectId, timeStamp: theTimeStampString, noCopyBlock: {() -> Void in
                            var theArray = [Any]()
                            theArray += UserDefaults.sharedInstance().theRequestArray
                            theArray.remove(at: 0)
                            UserDefaults.sharedInstance().theRequestArray = theArray
                            UserDefaults.sharedInstance().theCurrentProjectId = nil
                            self.methodSendAllInfo()
                            NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                        })
                        return
                    }
                    else {
                        var theArray = [Any]()
                        theArray += UserDefaults.sharedInstance().theRequestArray
                        theArray.remove(at: 0)
                        UserDefaults.sharedInstance().theRequestArray = theArray
                        self.methodSendAllInfo()
                        NotificationCenter.default.post(name: keyProjectManagerDidCheckInOutNotification, object: nil)
                        print("coordinate deleted in the end")
                        return
                    }
                }
                print("coordinate ignored")
                self.isInProgress = false
            }
            else {
                self.isInProgress = false
            }
        } as! ([BeehiveProject]?, Error?) -> ())
    }


    func methodCheck(in isCheckIn: Bool, projectId theProjectId: String, timeStamp theTimeStampString: String, noCopyBlock theBlock: @escaping () -> Void) {
        var prefs = UserDefaults.standard
        var emailId = prefs.object(forKey: "UserId")!
        var userToken = prefs.object(forKey: "User_Token")!
        var authStr = "\(emailId):\(userToken)"
        var authData = authStr.data(using: String.Encoding.utf8)
        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
        var strURL = "\(BaseWebRequestUrl)\(isCheckIn ? CheckInUrl : CheckOutUrl)"
        strURL = strURL.replacingOccurrencesOf(" ", withString: "")
        var url = URL(string: strURL)!
        var request = ASIFormDataRequest(URL: url)
        request.delegate = self
        request.addHeader("Authorization", value: authValue)
        request.setPostValue(theProjectId, forKey: "project_id")
        request.setPostValue(theTimeStampString, forKey: (isCheckIn ? "check_in_time" : "check_out_time"))
        request.setPostValue("0", forKey: "auto")
        request.requestMethod = "POST"
        request.setCompletionBlock({() -> Void in
            print("request success for isCheckIn:\(isCheckIn), projectId:\(theProjectId),\nDATE theTimeStampString:\(theTimeStampString)")
            if theBlock {
                theBlock()
            }
            var theOldRequest = OldRequest.methodInit(withJSONDictionary: [:])
            theOldRequest.theProjectId = theProjectId
            theOldRequest.theDate = Date()
            theOldRequest.theCheckINOUT = isCheckIn ? "checkIN" : "checkOUT"
            theOldRequest.managedObjectContext!.save(nil)
            (UIApplication.shared.delegate! as! BeehiveAppDelegate).methodReloadStatsArray()
        })
        request.failedBlock = {() -> Void in
            print("request failed for isCheckIn:\(isCheckIn), projectId:\(theProjectId),\nDATE theTimeStampString:\(theTimeStampString)")
            if theBlock {
                theBlock()
            }
            var theOldRequest = OldRequest.methodInit(withJSONDictionary: [:])
            theOldRequest.theProjectId = "request failed!"
            theOldRequest.theDate = Date()
            theOldRequest.theCheckINOUT = isCheckIn ? "checkIN" : "checkOUT"
            theOldRequest.managedObjectContext!.save(nil)
            (UIApplication.shared.delegate! as! BeehiveAppDelegate).methodReloadStatsArray()
        }
        request.startAsynchronous()
    }
}
