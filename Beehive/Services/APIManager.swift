 //
//  APIManager.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation
import Alamofire

public class APIManager {

    public class var sharedInstance: APIManager {
        struct Singleton {
            static let instance: APIManager = APIManager()
        }
        return Singleton.instance
    }

    init() {}

    //MARK: Login

    func LoginUser(login: Login, completion:@escaping (_ error: NSError?) -> ())  {


        print(login.toJsonDict())
        print(BaseWebRequestUrl + LoginRequestUrl)
        Alamofire.request(BaseWebRequestUrl + LoginRequestUrl, method: .post, parameters: login.toJsonDict()).responseJSON { response in

            if let json = response.result.value as? JSONDictionary {
                print(json)

                if let result = json[kResult] as? JSONDictionary {
                    LoggedInUser.sharedInstance.populateWithJson(dict: result)

                    let status = LoggedInUser.sharedInstance.status

                    if status == kOK {

                        UserDefaults.standard.set(result, forKey: kAPPUser)
                        UserDefaults.standard.set(true, forKey: hasLogin)
                        UserDefaults.standard.set(result, forKey: "_token")
                        UserDefaults.standard.set(result[k_token]!, forKey: "User_Token")
                        self.getNotificationCountValue(completion: { (error) in
                                    completion(nil)
                        })
                    } else if status == kINVALID {
                        completion(SLError.EmptyResultError)
                        SLAlert.showAlert(str: "\(result["message"]!)")
                    } else if kstatus == kFAILED {
                        SLAlert.showAlert(str: "\(result["message"]!)")
                        completion(SLError.EmptyResultError)
                    }
                }
            } else {
                print(response)
            }
        }
    }


    //MARK: Sign Up

    func RegisterNewUser(register: Register, completion:@escaping (_ error: NSError?) -> ()) {


        APPDELEGATE.addChargementLoader()
        print(register.toJsonDict())

        Alamofire.request(BaseWebRequestUrl + SignupRequestUrl, method: .post, parameters: register.toJsonDict()).responseJSON { response in
            APPDELEGATE.removeChargementLoader()

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {
                    
                    if let mesage = result[kmessage] as? [String] {
                        if mesage.count > 0 {
                            SLAlert.showAlert(str: mesage[0])
                        }
                    }
                    
                    let status = result[kstatus] as? String
                    if status == kOK {
                        completion(nil)
                    } else if status == kINVALID {
                        completion(SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            }
        }
    }


    func forgotPasswordUser(email: String, completion:@escaping (_ error: NSError?) -> ()) {

        print([kEmail:email])
        Alamofire.request(BaseWebRequestUrl + ForgotPasswordUrl, method: .post, parameters:[kEmail:email]).responseJSON { response in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {
                    
                    let status = result[kstatus] as? String
                    
                    if let message = result[kmessage] as? String {
                        SLAlert.showAlert(str: message)
                    }
                    
                    if status == kOK {
                        completion(nil)
                    } else if status == kINVALID {
                        completion(SLError.EmptyResultError)
                    } else if kstatus == kFAILED {
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
                
            } else {
                completion(SLError.EmptyResultError)
            }
        }

    }

    func getNotificationCountValue(completion:@escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ())  {

        
        
        completion(nil,SLError.EmptyResultError)
        return
        print(LoggedInUser.sharedInstance.getAuth())
    

        Alamofire.request(BaseWebRequestUrl + GetUnreadNotificationCount, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in
            
            completion(nil,SLError.EmptyResultError)
            
            return
            if let json = response.result.value as? JSONDictionary {
                print(json)

                if let result = json[kResult] as? JSONDictionary {
                    print(result)

                    if "\(result[kstatus]!)" == kOK {
                        completion(result,nil)
                    } else if "\(result[kstatus]!)" == kERROR {
                        completion(nil,SLError.EmptyResultError)
//                        SLAlert.showAlert(str: "\(result[kmessage]!)")
                    } else if "\(result[kstatus]!)" == kINVALID {
                        completion(nil,SLError.EmptyResultError)
//                        SLAlert.showAlert(str: "\(result[kmessage]!)")
                    }

                } else {
                    completion(nil,SLError.EmptyResultError)
                }

            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    //MARK: - requestGetMyCurrentProject

    func requestGetMyCurrentProject(project: Projects ,completion:@escaping (_ projects:ProjectsResponse?, _ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetMyProjects, method: .get, parameters: project.tojsonDict(), headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        let projectResponse = ProjectsResponse()
                        projectResponse.populateWithJson(dict: json)
                        completion(projectResponse, nil)
                    } else {

                    }


                } else {
                    completion(nil, SLError.EmptyResultError)
                    print(kFAILED)
                }
            } else {
                completion(nil, SLError.EmptyResultError)
                print(kFAILED)
            }
        }
    }

    //MARK: - GetAllArchivedProject

    func getAllArchivedProject(project: Projects ,completion:@escaping (_ projects:ProjectsResponse?, _ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetAllArchivedProject, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                print(json)

                if let result = json[kResult] as? JSONDictionary {
                    print(result)
                    if "\(result[kstatus]!)" == kOK {

                        let projectResponse = ProjectsResponse()
                        projectResponse.populateWithJson(dict: json)
                        completion(projectResponse, nil)
                    } else {
                        completion(nil, SLError.EmptyResultError)
                    }

                } else {
                    print(kFAILED)
                    completion(nil, SLError.EmptyResultError)
                }

            } else {
                completion(nil, SLError.EmptyResultError)
                print(kFAILED)
            }
        }
    }

    //MARK: Profile

    func getUserProfileDetails(completion: @escaping(_ dict: JSONDictionary?, _ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetMyProfileDetails, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(json, nil)
                    } else {
                        completion(nil, SLError.EmptyResultError)
                    }
                } else {
                    completion(nil, SLError.EmptyResultError)
                }

            } else {
                completion(nil, SLError.EmptyResultError)
                print(kFAILED)
            }
        }
    }


    //MARK: Update Profile


    func updateUserProfile(profile: Profile, completion: @escaping(_ error:NSError?) -> ()) {

        
//        Alamofire.upload(multipartFormData:{ multipartFormData in
//            
//            
//            if image != nil {
//                if let imageData = UIImageJPEGRepresentation(image!, 0.6) {
//                    multipartFormData.append(imageData, withName: "user_avatar", fileName: "file.png", mimeType: "image/png")
//                }
//            }
//            
//            for (key, value) in profile.toJasonDict() {
//                
//                multipartFormData.append(String.init(value as! NSString).data(using: String.Encoding.utf8)!, withName: key)
//                
//            }
//            
//        },
//                         usingThreshold:UInt64.init(),
//                         to:BaseWebRequestUrl + SaveMyProfileURL,
//                         method:.post,
//                         headers:LoggedInUser.sharedInstance.getAuth(),
//                         encodingCompletion: { encodingResult in
//                            switch encodingResult {
//                            case .success(let upload, _, _):
//                                upload.responseJSON { response in
//                                    if let json = response.result.value as? JSONDictionary {
//                                        if let result = json[kResult] as? JSONDictionary {
//                                            
//                                            if "\(result[kstatus]!)" == kOK {
//                                                completion(nil)
//                                            } else {
//                                                completion(SLError.EmptyResultError)
//                                            }
//                                        } else {
//                                            completion(SLError.EmptyResultError)
//                                        }
//                                    } else {
//                                        completion(SLError.EmptyResultError)
//                                    }
//
//                                }
//                            case .failure(let encodingError):
//                                completion(SLError.EmptyResultError)
//                            }
//        })
        
//        Alamofire.upload(.POST, BaseWebRequestUrl + SaveMyProfileURL, multipartFormData: {
//            multipartFormData in
//            
//            if image != nil {
//                if let imageData = UIImageJPEGRepresentation(image!, 0.6) {
//                    multipartFormData.appendBodyPart(data: imageData, name: "profilePicture", fileName: "file.png", mimeType: "image/png")
//                }
//            }
//            
//            for (key, value) in register.toJsonDictionary() {
//                multipartFormData.appendBodyPart(data: value.data(using: String.Encoding.utf8)!, name: key)
//                
//            }
//        }, encodingCompletion: {
//            encodingResult in
//            
//            switch encodingResult {
//            case .success(let upload, _, _):
//                
//                upload.responseJSON {
//                    response in
//                    
//                    print(response.result.value)
//                    if let dict = (response.result.value as? JSONDictionary), let statusCode = dict[kSP_StatusCode] as? Int {
//                        
//                        if statusCode == 200 {
//                            completion()
//                            ShowAlert(dict[kSP_Success]![kSP_Message]! as! String)
//                            
//                        } else {
//                            ShowAlert(dict[kSP_Fail]![kSP_Message]! as! String)
//                            failure(error: SPErrors.EmptyResultError)
//                        }
//                        
//                    } else {
//                        failure(error: SPErrors.EmptyResultError)
//                    }
//                    
//                }
//                
//            case .failure(let encodingError):
//                print(encodingError)
//                failure(error: SPErrors.EmptyResultError)
//            }
//            
//            
//        })

        
        
        
        Alamofire.request(BaseWebRequestUrl + SaveMyProfileURL, method: .post, parameters: profile.toJasonDict(), headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(nil)
                    } else {
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            } else {
                completion(SLError.EmptyResultError)
            }
        }
    }

    func getAccountsBillingInfo(completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetAccountBillingInfo, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                print(json)
                if let result = json[kResult] as? JSONDictionary {
                    print(result)
                    completion(json,nil)
                } else {
                    completion(nil,SLError.EmptyResultError)
                }

            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }

    }

    //MARK:- GetProjectReportListURL

    func fetchAllProjectsReportList(completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetProjectReportListURL, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                
                
                completion(json,nil)


            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func fetchAllCrewReportList(completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetCrewReportListURL, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {


                    completion(json,nil)


            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }


    func fetchgoogleData(text: String,completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ())  {
        //[NSString stringWithFormat:@"%@input=%@&types=geocode&sensor=false&key=%@",GOOGLE_AUTOCOMPLETE_SEARCH_URL,textFieldText,kGOOGLE_MAP_SERVER_API_KEY_19JUNE]
        let strURL = "\(GOOGLE_AUTOCOMPLETE_SEARCH_URL)input=\(text)&types=geocode&sensor=false&key=\(kGOOGLE_MAP_SERVER_API_KEY_19JUNE)"

        Alamofire.request(strURL, method: .get).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func getCompleteData(text: String,completion: @escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ())  {

        let strURL = "\(kGOOGLE_GEOCODE_URL)=\(text)&sensor=false"

        Alamofire.request(strURL, method: .get).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }        }
    }

    func createNewProject(proje: [String:String],completion:@escaping (_ dict:JSONDictionary?, _ error: NSError?) -> ()) {

        print(LoggedInUser.sharedInstance.getAuth())
        print(proje)
        Alamofire.request(BaseWebRequestUrl + CreateProjectUrl, method: .post, parameters: proje, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            print(response.result.value)
            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(json,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func getGoogleData(url:String, completion:@escaping(_ dict:JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(url, method: .get).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                print(json)
                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }

    }


    func getProjectDetails(projectID: String,completion:@escaping (_ dict:JSONDictionary?, _ error: NSError?) -> ()) {

        
        print(BaseWebRequestUrl + GetProjectDetails + "\(projectID).json")
        
        Alamofire.request(BaseWebRequestUrl + GetProjectDetails + "\(projectID).json", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            print(response.result.value)
            
            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(json,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func getProjectNotes(projectId: String, completion:@escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetProjectNotesURL + "\(projectId).json", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(json,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }


    func uploadAlbumImages(dict: JSONDictionary, completion:@escaping(_ dict:JSONDictionary?,_ error:NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + SaveNotesURL, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        print(json)
                        completion(json,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }


    func deleteProjectWebSerrviceCalling(projectID: String,completion: @escaping (_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + DeleteProjectURL + "\(projectID).json", method: .delete, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        print(json)
                        completion(nil)
                    } else {
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            } else {
                completion(SLError.EmptyResultError)
            }
        }
    }

    func makeProjectArchiveWebSirvice(projectID: String,completion: @escaping (_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + MakeProjectArchiveURL + "\(projectID).json", method: .put, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        print(json)
                        completion(nil)
                    } else {
                        completion(SLError.EmptyResultError)
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            } else {
                completion(SLError.EmptyResultError)
            }
        }
    }

    func disableCrewNotificationWebService(dict: JSONDictionary, completion: @escaping(_ dict: JSONDictionary?,_ eror: NSError?) -> ()) {

        print(dict)

        Alamofire.request(BaseWebRequestUrl + DisableCrewNotificationsUrl, method: .put, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        print(json)
                        completion(result,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func searchCrewForAddingToProject(projectID: String, completion: @escaping(_ dict: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetSearchedCrewForProject, method: .post, parameters: ["project_id":"\(projectID)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        print(json)
                        completion(result,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func addCrewIntoProject(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + GetSearchedCrewForProject, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        print(json)
                        completion(result,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func addCrew(dict:[String: String], completion: @escaping (_ dict: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + AddCrewBySelection, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(result,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func deleteCrewMemberWeservice(dict: JSONDictionary, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {
        Alamofire.request(BaseWebRequestUrl + DeleteCrewMemberURL, method: .post, parameters: dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {
                    print(json)
                    completion(result,nil)
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func getPayPalRequiredAttributes(completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {


        Alamofire.request(BaseWebRequestUrl + GetPayPalAttributes, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {
                    print(json)
                    completion(result,nil)
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func storeCreditCard(cardDict: JSONDictionary, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + StoreCreditCard, method: .post, parameters:cardDict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in


            print(response.result.value)

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {
                    print(json)

                    if "\(result[kstatus]!)" == kOK {
                        completion(result,nil)
                    } else if "\(result[kstatus]!)" == kERROR {
                        completion(nil,SLError.EmptyResultError)
                        SLAlert.showAlert(str: "\(result[kmessage]!)")
                    } else if "\(result[kstatus]!)" == kINVALID {
                        completion(nil,SLError.EmptyResultError)
                        SLAlert.showAlert(str: "\(result[kmessage]!)")
                    }


                } else {

                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func sendInvitationViaEmail(email: String, projectID: String, completion: @escaping(_ error: NSError?) -> ()) {
        Alamofire.request(BaseWebRequestUrl + InviteCrewViaEmail, method: .post, parameters:["project_id":"\(projectID)", "email":"\(email)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            print(response.result.value)
            if let json = response.result.value as? JSONDictionary {
                if let result = json[kResult] as? JSONDictionary {
                    print(json)

                    if "\(result[kstatus]!)" == kOK {
                        completion(nil)
                    } else if "\(result[kstatus]!)" == kERROR {
                        completion(SLError.EmptyResultError)
                        SLAlert.showAlert(str: "\(result[kmessage]!)")
                    } else if "\(result[kstatus]!)" == kINVALID {
                        completion(SLError.EmptyResultError)
                        SLAlert.showAlert(str: "\(result[kmessage]!)")
                    }
                } else {
                    completion(SLError.EmptyResultError)
                }
            } else {
                completion(SLError.EmptyResultError)
            }
        }

    }

    //MARK: - Appdelege methods
    //MARK: - ------------

    func requestGetMyCurrentProject(completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {


        Alamofire.request(BaseWebRequestUrl + "getMyProjects.json?role_id=4&page=1", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(json,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)	
                    }


                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func getManualCheckInProjectsArray() {

        
    }

    func getAllNotificationData(completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {


        Alamofire.request(BaseWebRequestUrl + GetAllNotifications, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        completion(result,nil)
                    } else if "\(result[kstatus]!)" == kERROR {
                        completion(nil,SLError.EmptyResultError)
                    } else if "\(result[kstatus]!)" == kINVALID {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func checkInProject(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?, _ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + CheckInUrl, method: .get, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {


                if let result = json[kResult] as? JSONDictionary {
                    print(result)
                    completion(result,nil)
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }


    func acceptProjectWebService(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?, _ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + AcceptProjectURL, method: .post, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                    print(json)
                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func checkOutProject(dict: JSONDictionary, completion: @escaping(_ result: JSONDictionary?, _ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + CheckOutUrl, method: .get, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {

                    completion(result,nil)
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }


    func notificationReadWebService(id: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + MarkNotificationAsReadURL, method: .post, parameters:["push_notification_id":"\(id)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                if let result = json[kResult] as? JSONDictionary {
                    if "\(result[kstatus]!)" == kOK {
                        completion(result,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func callMarkAsReadAllNotification(id: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + MarkAllNotificationAsReadUrl, method: .post, parameters:["push_notification_id":"\(id)"], headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                if let result = json[kResult] as? JSONDictionary {
                    if "\(result[kstatus]!)" == kOK {
                        completion(result,nil)
                    } else {
                        completion(nil,SLError.EmptyResultError)
                    }
                    
                } else {
                    completion(nil,SLError.EmptyResultError)
                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func fetchNotificationData(id: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(BaseWebRequestUrl + TentetativeCheckInNotificationUrl + "/\(id).json", method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                print(json)

//                if let result = json[kResult] as? JSONDictionary {
//
//                    if "\(result[kstatus]!)" == kOK {
                        completion(json,nil)
//                    } else {
//                        completion(nil,SLError.EmptyResultError)
//                    }
//
//
//                } else {
//                    completion(nil,SLError.EmptyResultError)
//                }
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }

    }
    func callAcceptTentativeCheckInWebService(url: String, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(url, method: .get, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                print(json)
                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func callWebService(url: String, method:HTTPMethod, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(url, method: method, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {
                print(json)
                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }

    func callWebServiceWithParameter(url: String, dict: [String:String]?,method:HTTPMethod, completion: @escaping (_ result: JSONDictionary?,_ error: NSError?) -> ()) {

        Alamofire.request(url, method: method, parameters:dict, headers: LoggedInUser.sharedInstance.getAuth()).responseJSON { (response) in

            if let json = response.result.value as? JSONDictionary {

                completion(json,nil)
            } else {
                completion(nil,SLError.EmptyResultError)
            }
        }
    }
}
    

