

import Foundation
import CoreData

class OldRequest: NSManagedObject {


    var theProjectId: String?
    var theCheckINOUT: String?
    var theDate: Date?

    class func methodInit(withJSONDictionary theDictionary: JSONDictionary) -> OldRequest {
//        methodAssert(theDictionary)

        return OldRequest.init(jsonDictionary: theDictionary)
    }

    convenience init(jsonDictionary theDictionary: JSONDictionary) {
        //        methodAssert(theDictionary)
        self.init(entity: NSEntityDescription.entity(forEntityName: "OldRequest", in: APPDELEGATE.managedObjectContext)!, insertInto: APPDELEGATE.managedObjectContext)
    }

    class func getObjectsArray(withPredicate thePredicate: NSPredicate?, propertyToFetchArray thePropertyToFetchArray: [String]?, sortDescriptorArray theSortDescriptorArray: [NSSortDescriptor]?) -> [AnyObject]? {


        let theFetchRequest = NSFetchRequest<OldRequest>(entityName: "OldRequest")

        if thePredicate != nil {
            theFetchRequest.predicate = thePredicate
        }

        theFetchRequest.includesSubentities = false
        if thePropertyToFetchArray!.count > 0 {
            theFetchRequest.propertiesToFetch = thePropertyToFetchArray
        }
        if theSortDescriptorArray!.count > 0 {
            theFetchRequest.sortDescriptors = theSortDescriptorArray
        }
        do {
            return try APPDELEGATE.managedObjectContext.fetch(theFetchRequest)
        }
        catch {
            return nil
        }
    }


    func methodFill(withDictionary theDictionary: JSONDictionary) {
    }
}

