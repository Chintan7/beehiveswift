//
//  Created by od1914@my.bristol.ac.uk.
//
//
import Foundation
import CoreData

class BeehiveProject: NSManagedObject {

    var theCreated: String?
    var theId: String?
    var theModified: String?
    var theCity: String?
    var theCountry: String?
    var theLatitude: String?
    var theLongitude: String?
    var theLocation: String?
    var theName: String?
    var theNote: String?
    var thePostCode: String?
    var theRadius: String?
    var theState: String?
    var theStatus: String?
    var theStreet: String?
    var theStreetNumber: String?
    var theTimeZone: String?
    var theUserId: String?


    class func methodInit(withJSONDictionary theDictionary: JSONDictionary) -> BeehiveProject {
        //        methodAssert(theDictionary)

        return BeehiveProject.init(jsonDictionary: theDictionary)
    }

    convenience init(jsonDictionary theDictionary: JSONDictionary) {
        //        methodAssert(theDictionary)
        self.init(entity: NSEntityDescription.entity(forEntityName: "BeehiveProject", in: APPDELEGATE.managedObjectContext)!, insertInto: APPDELEGATE.managedObjectContext)
    }


    class func getObjectsArray(withPredicate thePredicate: NSPredicate?, propertyToFetchArray thePropertyToFetchArray: [String]?, sortDescriptorArray theSortDescriptorArray: [NSSortDescriptor]?) -> [BeehiveProject]? {

        let theFetchRequest = NSFetchRequest<BeehiveProject>(entityName: "BeehiveProject")

        if thePredicate != nil {
            theFetchRequest.predicate = thePredicate
        }
        theFetchRequest.includesSubentities = false
        if thePropertyToFetchArray!.count > 0 {
            theFetchRequest.propertiesToFetch = thePropertyToFetchArray
        }
        if theSortDescriptorArray!.count > 0 {
            theFetchRequest.sortDescriptors = theSortDescriptorArray
        }
        do {
            return try APPDELEGATE.managedObjectContext.fetch(theFetchRequest)
        }
        catch {
            return nil
        }
    }

    class func methodDownloadAllProjectsWithNoCopyBlock(_ theBlock: @escaping (_ theProjectArray: [BeehiveProject]?, _ theError: Error?) -> ()) {

        let thePageCounter = 1
        let requestString = "getMyProjects.json?role_id=4&page=\(thePageCounter)"
        var strURL = "\(BaseWebRequestUrl)\(requestString)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")

        // Create NSURL Ibject
        let myUrl = URL(string: requestString);

        // Creaste URL Request
        var request = URLRequest(url:myUrl!);

        request.setValue(LoggedInUser.sharedInstance.authValue(), forHTTPHeaderField: "Authorization")

        // Set request HTTP method to GET. It could be POST as well
        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in

            // Check for error
            if error != nil
            {
                print("error=\(error)")
                return
            }

            // Print out response string
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")


            // Convert server json response to NSDictionary
            do {
                if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {

                    // Print out dictionary
                    print(convertedJsonIntoDict)

                    var theArray = BeehiveProject.getObjectsArray(withPredicate: nil, propertyToFetchArray: nil, sortDescriptorArray: nil)

                    print("BeforeDelete \(theArray)")
                    for i in 0..<Int(theArray!.count) {
                        let theObject = theArray?[i]
                        APPDELEGATE.managedObjectContext.delete(theObject as! NSManagedObject)
                    }
                    print("after Deleget \(theArray)")
                    var theFinalArray = [BeehiveProject]()
                    let theProjectsArray = convertedJsonIntoDict["projects"] as? NSArray
                    print("COUNT: \(theProjectsArray?.count)")

                    for i in 0..<Int((theProjectsArray?.count)!) {

                        if let dict = (theProjectsArray?.object(at: i) as! JSONDictionary)["Project"] as? JSONDictionary {
let theProject = BeehiveProject.methodInit(withJSONDictionary: dict)
                            theFinalArray.append(theProject)
                        }
                    }
                    do {
                        try APPDELEGATE.managedObjectContext.save()
                    } catch {

                    }
                    theBlock(theFinalArray, nil)
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }

        }

        task.resume()
        /*
        var url = URL(string: strURL)!
        var request = ASIFormDataRequest(URL: url)
        var pref = UserDefaults.standard
        var emailString = pref.object(forKey: "UserId")!
        var userTokenString = pref.object(forKey: "User_Token")!
        var authStr = "\(emailString):\(userTokenString)"
        var authData = authStr.data(using: String.Encoding.utf8)
        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
        request.addHeader("Authorization", value: authValue)
        request.tag = 1
        request.requestMethod = "GET"
        weakify(request)
        request.setCompletionBlock({() -> Void in
            strongify(request)
            var theJSONDictionary = request.responseString.jsonValue
            if isEqual(theJSONDictionary["result"]["status"], "OK") {
                var theArray = BeehiveProject.getObjectsArray(withPredicate: nil, propertyToFetchArray: nil, sortDescriptorArray: nil)
                for i in 0..<theArray.count {
                    var theObject = theArray[i]
                    theMOC.deleteObject(theObject)
                }
                var theFinalArray = [Any]()
                var theProjectsArray = theJSONDictionary["projects"]
                print("COUNT: \(theProjectsArray.count)")
                for i in 0..<theProjectsArray.count {
                    var theProject = BeehiveProject.methodInit(withJSONDictionary: theProjectsArray[i]["Project"])
                    theFinalArray.append(theProject)
                }
                theMOC.save(nil)
                if theBlock {
                    theBlock(theFinalArray, nil)
                }
            }
            else {
                if theBlock {
                    theBlock(nil, nil)
                }
            }
        })
        request.failedBlock = {() -> Void in
            if theBlock {
                theBlock(nil, nil)
            }
        }
        request.startAsynchronous()
 */
    }



    //@property (nonatomic, retain, nullable) NSSet<<#SomeClassName#> *> *<#TheList#>;

    func methodFill(withDictionary theDictionary: JSONDictionary) {
        
//       methodAssert(theDictionary)

        if let theValue = theDictionary["created"] as? String {
            self.theCreated = theValue
        }

        if let theValue = theDictionary["id"] as? String
        {
            self.theId = theValue
        }

        if let theValue = theDictionary["modified"] as? String {
            self.theModified = theValue
        }

        if let theValue = theDictionary["project_city"] as? String {
            self.theCity = theValue
        }

        if let theValue = theDictionary["project_country"] as? String {
            self.theCountry = theValue
        }

        if let theValue = theDictionary["project_lat"] as? String {
            self.theLatitude = theValue
        }

        if let theValue = theDictionary["project_lng"] as? String {
            self.theLongitude = theValue
        }

        if let theValue = theDictionary["project_location"] as? String {
            self.theLocation = theValue
        }

        if let theValue = theDictionary["project_name"] as? String {
            self.theName = theValue
        }

        if let theValue = theDictionary["project_note"] as? String {
            self.theNote = theValue
        }

        if let theValue = theDictionary["project_postal_code"] as? String {
            self.thePostCode = theValue
        }

        if let theValue = theDictionary["project_radius"] as? String {
            self.theRadius = theValue
        }

        if let theValue = theDictionary["project_state"] as? String {
            self.theState = theValue
        }

        if let theValue = theDictionary["project_status"] as? String {
            self.theStatus = theValue
        }

        if let theValue = theDictionary["project_street"] as? String {
            self.theStreet = theValue
        }

        if let theValue = theDictionary["project_street_number"] as? String {
            self.theStreetNumber = theValue
        }

        if let theValue = theDictionary["project_timezone"] as? String {
            self.theTimeZone = theValue
        }

        if let theValue = theDictionary["user_id"] as? String {
            self.theUserId = theValue
        }
    }

}

