
//
//  AddCrewListViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 30/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit

class AddCrewListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    var screenSize = CGSize.zero
    var crewListTable: UITableView!
    var crewSearchBar: UISearchBar!
    var searchView: UIView!
    var allCrewArray = [JSONDictionary]()
    var searchCrewsArray = [JSONDictionary]()
    var isCrewSelected = false
    var selectedCrewArray = [JSONDictionary]()
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    //     *********
    var filterCrewlist = [JSONDictionary]()

    var projectId: String!
    var isfilterList = false
    var crewNames = NSMutableArray()
    var selectedCrewButton: UIButton!
    var crewSelectButton: UIButton!
    var allCrewSelectionArray = [JSONDictionary]()
    var allcrewMembersArray = [JSONDictionary]()
    var crewCounter: Int! = 0
    var postArr = [[String: String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("Project ID : \(self.projectId)")
        allcrewMembersArray = [JSONDictionary]()
        searchCrewsArray = [JSONDictionary]()
        postArr = [[String: String]]()
        searchCrewsArray = allcrewMembersArray
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        self.searchCrewForAddingToProject()
        crewCounter = 0
        self.viewCustomization()
    }

    func viewCustomization() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton

            //    Toolbar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let img = UIImage(named: "backBtn.png")!
        let btn = UIButton(type: .custom)
        btn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(img.size.width), height: CGFloat(img.size.height))
        btn.setImage(img, for: .normal)
        btn.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: btn)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let inviteCrewBtnImage = UIImage(named: "smallOrngBtn.png")!
        let inviteCrewButton = UIButton(type: .custom)
        inviteCrewButton.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(60), height: CGFloat(25))
        inviteCrewButton.setBackgroundImage(inviteCrewBtnImage, for: .normal)
        inviteCrewButton.setTitle("Invite", for: .normal)
        inviteCrewButton.setTitleColor(UIColor.white, for: .normal)
        inviteCrewButton.titleLabel!.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(13.0))!
        inviteCrewButton.addTarget(self, action: #selector(self.inviteViaEmail), for: .touchUpInside)
        let prefBarButton = UIBarButtonItem(customView: inviteCrewButton)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(44)))
        titleLabel.text = "Add Crew"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold15
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        let flexSpace1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        barItems.append(flexSpace)
        barItems.append(toolbarTitleLbl)
        barItems.append(flexSpace1)
        barItems.append(prefBarButton)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        screenSize = UIScreen.main.bounds.size
        if screenSize.height == 568 {
            crewListTable = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(300)))
        }
        else {
            crewListTable = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(250)))
        }
        crewListTable.dataSource = self
        crewListTable.delegate = self
        self.view.addSubview(crewListTable)
        searchView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(50)))
        searchView.layer.shadowColor! = UIColor.gray.cgColor
        searchView.layer.shadowOffset = CGSize(width: CGFloat(2), height: CGFloat(2))
        searchView.layer.shadowOpacity = 1
        searchView.layer.shadowRadius = 2.0
        searchView.clipsToBounds = false
        let btnImage = UIImage(named: "plainOrangeBtn.png")!
        let addSelectedBtn = UIButton(type: .custom)
        let cantFindBtn = UIButton(type: .custom)
        let sendInviteEmailBtn = UIButton(type: .custom)
        if screenSize.height == 568 {
            addSelectedBtn.frame = CGRect(x: CGFloat(30), y: CGFloat(370), width: CGFloat(260), height: CGFloat(35))
            cantFindBtn.frame = CGRect(x: CGFloat(80), y: CGFloat(420), width: CGFloat(160), height: CGFloat(15))
            sendInviteEmailBtn.frame = CGRect(x: CGFloat(80), y: CGFloat(445), width: CGFloat(160), height: CGFloat(15))
        }
        else {
            addSelectedBtn.frame = CGRect(x: CGFloat(30), y: CGFloat(315), width: CGFloat(260), height: CGFloat(35))
            cantFindBtn.frame = CGRect(x: CGFloat(80), y: CGFloat(360), width: CGFloat(160), height: CGFloat(15))
            sendInviteEmailBtn.frame = CGRect(x: CGFloat(80), y: CGFloat(375), width: CGFloat(160), height: CGFloat(15))
        }
        addSelectedBtn.setBackgroundImage(btnImage, for: .normal)
        addSelectedBtn.setTitle("Add Selected", for: .normal)
        addSelectedBtn.setTitleColor(UIColor(red: CGFloat(253.0), green: CGFloat(211.0), blue: CGFloat(178.0), alpha: CGFloat(0.6)), for: .normal)
        addSelectedBtn.titleLabel!.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(18.0))!
        addSelectedBtn.addTarget(self, action: #selector(self.addSelectedCrew), for: .touchUpInside)
        self.view.addSubview(addSelectedBtn)
        cantFindBtn.setTitle("Cant Find Someone ?", for: .normal)
        cantFindBtn.setTitleColor(UIColor.gray, for: .normal)
        cantFindBtn.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(13.0))!
        cantFindBtn.addTarget(self, action: #selector(self.inviteViaEmail), for: .touchUpInside)
        self.view.addSubview(cantFindBtn)
        sendInviteEmailBtn.setTitle("send an invite via email", for: .normal)
        sendInviteEmailBtn.setTitleColor(UIColor.darkGray, for: .normal)
        sendInviteEmailBtn.titleLabel!.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(15.0))!
        sendInviteEmailBtn.addTarget(self, action: #selector(self.inviteViaEmail), for: .touchUpInside)
        self.view.addSubview(sendInviteEmailBtn)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func backBtnClick() {
        popTo()
    }

    func addSelectedCrew() {
        print("Add Selected")

        let feeInfoView = STORYBOARD.instantiateViewController(withIdentifier: "CrewFeeInfoViewController") as! CrewFeeInfoViewController
        feeInfoView.crewCount = "\(UInt(postArr.count))"
        feeInfoView.selectedCrewArray = postArr
        feeInfoView.projectId = self.projectId
        var addCrewString = String()
        if postArr.count > 1 {
            addCrewString = postArr[0]["Crew_Id"]!
            for j in 1..<postArr.count {
                addCrewString = "\(addCrewString)|\(postArr[j]["Crew_Id"]!)"
            }
        }
        else if postArr.count == 1 {
            addCrewString = postArr[0]["Crew_Id"]!
        }

        print("String \(addCrewString)")
        if (addCrewString == "") {
            let myAlert = UIAlertView(title: "Error", message: "Please select atleast one crew", delegate: nil, cancelButtonTitle: "OK")
            myAlert.show()
            return
        }
        //    [self addCrewIntoProject:addCrewString];
        self.navigationController!.pushViewController(feeInfoView, animated: true)
    }

    func inviteViaEmail() {
        print("Invite....!!")

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "InviteViaEmailViewController") as! InviteViaEmailViewController
        newView.projectId = self.projectId
        self.navigationController!.pushViewController(newView, animated: true)
    }
// MARK: Table Datasource & Delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //    return [searchCrewsArray count];
        return isfilterList ? filterCrewlist.count : allcrewMembersArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0.0)
        let CellIdentifier = "\(indexPath.section),\(indexPath.row)"
        var cell: UITableViewCell?
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
        }
        cell!.selectionStyle = .none
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(55), y: CGFloat(10), width: CGFloat(220), height: CGFloat(15)))

        titleLabel.font = CenturyGothicBold15
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.darkGray
        let crewCityLabel = UILabel(frame: CGRect(x: CGFloat(55), y: CGFloat(15), width: CGFloat(220), height: CGFloat(30)))

        let crewImageView = UIImageView(frame: CGRect(x: CGFloat(5), y: CGFloat(5), width: CGFloat(45), height: CGFloat(45)))
        crewCityLabel.font = CenturyGothiCRegular11
        crewCityLabel.backgroundColor = UIColor.clear
        crewCityLabel.textAlignment = .left
        crewCityLabel.textColor = UIColor.gray





        titleLabel.text = isfilterList ? "\(filterCrewlist[indexPath.row]["Crew_Name"]!)" : "\(allcrewMembersArray[indexPath.row]["Crew_Name"]!)"

        //titleLabel.text = isfilterList ? (filterCrewlist[indexPath.row]["Crew_Name"]) : ((allcrewMembersArray[indexPath.row] as AnyObject).value(forKey: "Crew_Name") as! String)

//        crewCityLabel.text! = isfilterList ? ((filterCrewlist[indexPath.row] as AnyObject).value(forKey: "City") as! String) : ((allcrewMembersArray[indexPath.row] as AnyObject).value(forKey: "City") as! String)

        crewCityLabel.text = isfilterList ? "\(filterCrewlist[indexPath.row]["City"]!)" : "\(allcrewMembersArray[indexPath.row]["City"]!)"


        let imageStr = isfilterList ? "\(filterCrewlist[indexPath.row]["user_avatar"]!)" : "\(allcrewMembersArray[indexPath.row]["user_avatar"]!)"

        if (imageStr == "") {
            crewImageView.image = #imageLiteral(resourceName: "ImageIcon.png")
        }
        else {
           let url = URL(string: "\(ImageBaseUrl)\(imageStr)")!
            crewImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
        }
        crewImageView.layer.borderColor = UIColor.black.cgColor
        crewImageView.layer.borderWidth = 1.0
        let ckeckButton = UIButton(frame: CGRect(x: CGFloat(275), y: CGFloat(10), width: CGFloat(30), height: CGFloat(30)))
        ckeckButton.setImage(UIImage(named: "unselectBtn.png")!, for: .normal)
        ckeckButton.setImage(UIImage(named: "selectGreenBtn.png")!, for: .selected)
        ckeckButton.isUserInteractionEnabled = false
        print(self.allcrewMembersArray[indexPath.row]["Crew_Id"])
//        print(self.filterCrewlist[indexPath.row]["Crew_Id"])


        let btnTag = isfilterList ? self.filterCrewlist[indexPath.row]["Crew_Id"] as? String : self.allcrewMembersArray[indexPath.row]["Crew_Id"] as? String

        ckeckButton.tag = Int(btnTag!)!
        cell!.contentView.addSubview(crewCityLabel)
        cell!.contentView.addSubview(crewImageView)
        cell!.contentView.addSubview(titleLabel)
        cell!.contentView.addSubview(ckeckButton)
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell


        let btnTag = isfilterList ? self.filterCrewlist[indexPath.row]["Crew_Id"] as? String : self.allcrewMembersArray[indexPath.row]["Crew_Id"] as? String

        print(btnTag)
        if let ckeckButton = cell.viewWithTag(Int(btnTag!)!) as? UIButton {


            var dataDict = [String: String]()
            if isfilterList {

                if let name = filterCrewlist[indexPath.row]["Crew_Name"] as? String {
                    dataDict["Name"] = name
                }

                //            dataDict["Name"] = filterCrewlist[indexPath.row]["Crew_Name"] as AnyObject?
                if let state = filterCrewlist[indexPath.row]["State"] as? String {
                    dataDict["State"] = state
                }

                //            dataDict["State"] = filterCrewlist[indexPath.row]["State"]!
                if let city = filterCrewlist[indexPath.row]["City"] as? String {
                    dataDict["City"] = city
                }

                //            dataDict["City"] = filterCrewlist[indexPath.row]["City"]!
                if let id = filterCrewlist[indexPath.row]["Crew_Id"] as? String {
                    dataDict["Crew_Id"] = id
                }

                //            dataDict["Crew_Id"] = filterCrewlist[indexPath.row]["Crew_Id"]!
                if let image = filterCrewlist[indexPath.row]["user_avatar"] as? String {
                    dataDict["user_avatar"] = image
                }

                //            dataDict["user_avatar"] = filterCrewlist[indexPath.row]["user_avatar"]!
            }
            else {

                if let name = allcrewMembersArray[indexPath.row]["Crew_Name"] as? String {
                    dataDict["Name"] = name
                }
                if let state = allcrewMembersArray[indexPath.row]["State"] as? String {
                    dataDict["State"] = state
                }
                if let city = allcrewMembersArray[indexPath.row]["City"] as? String {
                    dataDict["City"] = city
                }
                if let id = allcrewMembersArray[indexPath.row]["Crew_Id"] as? String {
                    dataDict["Crew_Id"] = id
                }
                if let image = allcrewMembersArray[indexPath.row]["user_avatar"] as? String {
                    dataDict["user_avatar"] = image
                }


                //            dataDict["Name"] = (allcrewMembersArray[indexPath.row].value(forKey: "Crew_Name") as! String)
                //            dataDict["City"] = (allcrewMembersArray[indexPath.row].value(forKey: "City") as! String)
                //            dataDict["State"] = (allcrewMembersArray[indexPath.row].value(forKey: "State") as! String)
                //            dataDict["Crew_Id"] = (allcrewMembersArray[indexPath.row].value(forKey: "Crew_Id") as! String)
                //            dataDict["user_avatar"] = (allcrewMembersArray[indexPath.row].value(forKey: "user_avatar") as! String)
            }
            if ckeckButton.isSelected == true {
                ckeckButton.isSelected = false
                
                
                let index = postArr.index(where: { (dataDict) -> Bool in
                    return true
                })
                postArr.remove(at: index!)
                
            }
            else {
                
                ckeckButton.isSelected = true
                postArr.append(dataDict)
            }

        }

//        var ckeckButton = (cell.viewWithTag(Int(((IsfilterList ? filterCrewlist : allcrewMembersArray[indexPath.row].value(forKey: "Crew_Id") as! String) as NSString ?? "0").intValue))! as! UIButton)
                print("newWorkoutViewController.ExerciseArray..\(postArr)")
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentYoffset: CGFloat = scrollView.contentOffset.y
        if contentYoffset < -55 {
            if screenSize.height == 568 {
                crewListTable.frame = CGRect(x: CGFloat(0), y: CGFloat(88), width: CGFloat(320), height: CGFloat(256))
            }
            else {
                crewListTable.frame = CGRect(x: CGFloat(0), y: CGFloat(88), width: CGFloat(320), height: CGFloat(206))
            }
            self.view.addSubview(searchView)
            crewSearchBar = UISearchBar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
            crewSearchBar.backgroundColor = UIColor.clear
            crewSearchBar.placeholder = "Search"
            crewSearchBar.delegate = self
            searchView.addSubview(crewSearchBar)
        }
        else if contentYoffset > 50 {
            if screenSize.height == 568 {
                crewListTable.frame = CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(300))
            }
            else {
                crewListTable.frame = CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(250))
            }
            searchView.removeFromSuperview()
        }

    }

    func crewSelection(_ sender: UIButton) {
        let t = sender.tag
        print("Selected Row : \(t)")
        //    if (sender.isSelected)
        //    {
        //        crewCounter --;
        //        [[searchCrewsArray objectAtIndex:sender.tag] setValue:@"0" forKey:@"SelectionMode"];
        //        [sender setSelected:NO];
        //    }
        //    else
        //    {
        //        crewCounter++;
        //        [[searchCrewsArray objectAtIndex:sender.tag] setValue:@"1" forKey:@"SelectionMode"];
        //        [sender setSelected:YES];
        //    }
        //    NSLog(@"New Selected Array : %@",searchCrewsArray);
        //    NSLog(@"Crew Count : %i",crewCounter);
        crewListTable.reloadData()
    }
// MARK: - SearchBar Delegates

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count == 0 {
            isfilterList = false
        }
        else {
            isfilterList = true
//            filterCrewlist = NSMutableArray()
            let predicate = NSPredicate(format: "Crew_Name contains[c] %@", searchText)
//            _ = searchCrewsArray.filtered(using: predicate)
//            filterCrewlist += tempArray
            //        for (int count=0; count<[filterCrewlist count]; count++) {
            //            [[filterCrewlist objectAtIndex:count] setObject:@"" forKey:@"settings"];
            //            [[filterCrewlist objectAtIndex:count] setObject:@"" forKey:@"repetitions"];
            //        }
            //        filterCrewlist = [NSMutableArray arrayWithArray:tempArray];
            crewListTable.reloadData()
            if filterCrewlist.count == 0 {
                let alert = UIAlertView(title: "", message: "Search not found", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            }
        }
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        crewSearchBar.resignFirstResponder()
        crewListTable.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isfilterList = false
        crewListTable.reloadData()
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }

        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }

//            else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//            {
//                UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//                CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//                [self.navigationController pushViewController:newView animated:YES];
//            }
        tableView.deselectRow(at: indexPath, animated: true)
    }
// MARK: - ASIHTTP Requests

    func searchCrewForAddingToProject() {
        APPDELEGATE.addChargementLoader()
        var urlString = "\(BaseWebRequestUrl)\(GetSearchedCrewForProject)"
        urlString = urlString.replacingOccurrences(of: " ", with: "")
        _ = URL(string: urlString)!


        APIManager.sharedInstance.searchCrewForAddingToProject(projectID: self.projectId) { (result:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {

                if let crewArr = result!["Crews"] as? [JSONDictionary] {
                    self.allCrewArray = crewArr


                    for crew in self.allCrewArray {

                        if let user = crew["User"] as? JSONDictionary {
                            var tempDict: JSONDictionary = [:]

                            if let id = user["id"] as? String{
                                tempDict["Crew_Id"] = id as AnyObject?
                            }
                            var name = ""
                            if let fstName = user["first_name"] as? String {
                                name = fstName
                            }
                            if let lstName = user["last_name"] as? String {
                                name = name + " \(lstName)"
                            }
                            tempDict["Crew_Name"] = "\(name)" as AnyObject?
                            if let city = user["user_city"] as? String {
                                tempDict["City"] = "\(city)" as AnyObject?
                            }
                            if let state = user["user_state"] as? String {
                                tempDict["State"] = "\(state)" as AnyObject?
                            }
                            if let avtar = user["user_avatar"] as? String {
                                tempDict["user_avatar"] = "\(avtar)" as AnyObject?
                            }
                            self.allcrewMembersArray.append(tempDict)

                        }
                    }
                    self.searchCrewsArray = self.allcrewMembersArray;
                    self.crewListTable.reloadData()


//                    for (int i = 0; i < [allCrewArray count]; i++)
//                    {
//                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//                        [dict setObject:[[[allCrewArray objectAtIndex:i] valueForKey:@"User"] valueForKey:@"id"] forKey:@"Crew_Id"];
//                        NSString *name = [NSString stringWithFormat:@"%@ %@",[[[allCrewArray objectAtIndex:i] valueForKey:@"User"] valueForKey:@"first_name"],[[[allCrewArray objectAtIndex:i] valueForKey:@"User"] valueForKey:@"last_name"]];
//
//                        [dict setObject:name forKey:@"Crew_Name"];
//
//                        [dict setObject:[[[allCrewArray objectAtIndex:i] valueForKey:@"User"] valueForKey:@"user_city"] forKey:@"City"];
//                        [dict setObject:[[[allCrewArray objectAtIndex:i] valueForKey:@"User"] valueForKey:@"user_state"] forKey:@"State"];
//                        [dict setObject:[[[allCrewArray objectAtIndex:i] valueForKey:@"User"] valueForKey:@"user_avatar"] forKey:@"user_avatar"];
//                        [allcrewMembersArray insertObject:dict atIndex:i];
                    }
                    //            NSLog(@"Array  Content : %@",allcrewMembersArray);


                }

            }

        }


//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.de
        
//        request.setPostValue(self.projectId, forKey: "project_id")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

    func addCrew(intoProject crewIdString: String) {
//        APPDELEGATE.addChargementLoader()
//        var urlString = "\(BaseWebRequestUrl)\(AddCrewBySelection)"
//        var serviceURL = URL(string: urlString)!
//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.delegate = self
//        request.setPostValue(self.projectId, forKey: "project_id")
//        request.setPostValue(crewIdString, forKey: "user_id")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 2
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                allCrewArray = ((returnDict["result"] as! String)["Crews"] as! String)
//                //            NSLog(@"All crew : %@",allCrewArray);
//                for i in 0..<allCrewArray.count {
//                    var dict = [AnyHashable: Any]()
//                    dict["Crew_Id"] = ((allCrewArray[i].value(forKey: "User") as! String).value(forKey: "id") as! String)
//                    var name = "\((allCrewArray[i].value(forKey: "User") as! String).value(forKey: "first_name") as! String) \((allCrewArray[i].value(forKey: "User") as! String).value(forKey: "last_name") as! String)"
//                    dict["Crew_Name"] = name
//                    dict["City"] = ((allCrewArray[i].value(forKey: "User") as! String).value(forKey: "user_city") as! String)
//                    dict["State"] = ((allCrewArray[i].value(forKey: "User") as! String).value(forKey: "user_state") as! String)
//                    dict["user_avatar"] = ((allCrewArray[i].value(forKey: "User") as! String).value(forKey: "user_avatar") as! String)
//                    allcrewMembersArray.insert(dict, at: i)
//                }
//                //            NSLog(@"Array  Content : %@",allcrewMembersArray);
//                searchCrewsArray = allcrewMembersArray
//                crewListTable.reloadData()
//            }
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Add Crew Return Data = \(returnDict)")
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var .error = request.error
//        print("Error : \(.error)")
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }



//
//  AddCrewListViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 30/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
