//
//  Global.m
//  Beehive
//
//  Created by Deepak Dixit on 02/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import "Global.h"
#import "UserDefaultClass.h"
#import "ODKit.h"

@implementation Global


static Global *shareInstance = nil;
+(Global *)sharedInstance
{
    @synchronized(self)
    {
        if (shareInstance == nil)
        {
            shareInstance = [[Global alloc] init];
        }
    }
    return shareInstance;
}

- (BOOL) validateName:(NSString *)string
{
    NSString *myRegex = @"^[\\w\\s-]*$";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", myRegex];
    return [myTest evaluateWithObject:string];
}

- (BOOL) validateEmail:(NSString *)string
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
	
    return [emailTest evaluateWithObject:string];
}

+(UIView *)paymentView
{
    UIView *paymentAlertView = [[UIView alloc] initWithFrame:CGRectMake(45, 130, 230, 230)];
    [paymentAlertView setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *alertImage = [[UIImageView alloc] initWithFrame:CGRectMake(20, 40, 30, 30)];
    [alertImage setImage:[UIImage imageNamed:@"warningIcon.png"]];
    
    [paymentAlertView addSubview:alertImage];
    UILabel *alertLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 40, 160, 50)];
    [alertLabel setText:@"We could not process payment!"];
    alertLabel.font = CenturyGothiCRegular15;
    alertLabel.textAlignment = NSTextAlignmentLeft;
    alertLabel.lineBreakMode = NSLineBreakByWordWrapping;
    alertLabel.numberOfLines = 2.0f;
    alertLabel.textColor = [UIColor darkGrayColor];
    [paymentAlertView addSubview:alertLabel];
    return paymentAlertView;
}


+(UIButton *) checkedInModeButton
{
//    UIButton *modeButton;
//    if (modeButton == nil)
//    {
        UIButton *modeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        modeButton.tag = 111;
        [modeButton setFrame:CGRectMake(205, 15, 20, 20)];
//    }
   
    [modeButton setUserInteractionEnabled:NO];
    
    if (![UserDefaultClass sharedInstance].theCurrentProjectId)
    {
        [modeButton setImage:[UIImage imageNamed:@"offlineImg.png"] forState:UIControlStateNormal];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideNotificaitonView" object:[NSNumber numberWithBool:YES]];
    }else
    {
        [modeButton setImage:[UIImage imageNamed:@"onlineImg.png"] forState:UIControlStateNormal];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideNotificaitonView" object:[NSNumber numberWithBool:NO]];
    }
    return modeButton;
}

+(NSString *)theID {
    
    return sfs(@selector(theID));
}


@end
