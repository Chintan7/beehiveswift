//
//  Global.h
//  Beehive
//
//  Created by Deepak Dixit on 02/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//Local Server
//#define BaseWebRequestUrl @"http://172.10.1.4:8632/mobile/v1/"
//#define ImageBaseUrl @"http://172.10.1.4:8632"

//Staging Server
//#define BaseWebRequestUrl @"http://192.155.246.146:9072/mobile/v1/"
//#define ImageBaseUrl @"http://192.155.246.146:9072"

//Amazon or Live Server
#define BaseWebRequestUrl @"http://54.68.29.83/mobile/v1/"
#define ImageBaseUrl @"http://54.68.29.83"
#define LoginRequestUrl @"signIn.json"
#define GetMyProjects @"getMyProjects.json"
#define SignupRequestUrl @"signUp.json"
#define ForgotPasswordUrl @"getPassword.json"
#define CheckEmailVerificationUrl @"checkEmailVerification.json"
#define ResendEmailVerificationLink @"resendEmailVerification.json"
#define GetMyProfileDetails @"getMyProfile.json"
#define SaveMyProfileURL @"saveProfile.json"
#define DeleteCrewMemberURL @"deleteCrewMember.json"
#define CreateProjectUrl @"createProject.json"
#define GetProjectDetails @"getProjectDetails/"
#define GetAllArchivedProject @"getAllMyArchivedProjects.json?"
#define GetProjectNotesURL @"getNotes/"
#define UploadImageUrl @"imageURL"
#define GetNoteDetailsURL @"getNoteDetail/"
#define SaveNotesURL @"saveNote.json"
#define DeleteProjectURL @"deleteProject/"
#define MakeProjectArchiveURL @"makeArchive/"
#define DisableCrewNotificationsUrl @"settingPushNotification.json"
#define GetUnreadNotificationCount @"countUnreadNotifications.json"
#define GetAllNotifications @"getNotifications.json"
#define MarkNotificationAsReadURL @"markReadAction.json"
#define MarkAllNotificationAsReadUrl @"markReadAllNotifications.json"
#define GetSearchedCrewForProject @"searchCrews.json"
#define AddCrewBySelection @"sendInvite.json"
#define InviteCrewViaEmail @"sendInviteEmail.json"

//#define GetMyProject @"getMyProjects.json?"

//  Notification:
#define DeclineProjectURL @"declineProject.json"
#define AcceptProjectURL @"acceptProject.json"

#define GetNotificationData @"getInvitationData"
#define TentetativeCheckInNotificationUrl @"tentativeCheckedIn"

#define CheckInUrl @"checkIn.json"
#define CheckOutUrl @"checkOut.json"


//Reports Url : For Report Section
#define GetProjectReportListURL @"getPojectsReportsList.json"
#define GetCrewReportListURL @"getProjectCrewsReportsList.json"


#define GetProjectWeeklyReportURL @"getProjectWeekyView"
#define GetProjectCrewReportURL @"getProjectCrewView"
#define GetProjectMonthReportURL @"getProjectMonthView"
#define GetProjectMonthWeeklyReport @"getProjectMonthViewByWeek.json"




#define GetMyCrewReportsURL @"getMyCrewsReport.json"
#define GetCrewProjectWeeklyDeatilReportURL @"getCrewWeeklyView.json"

#define AdjustTimeURL @"adjustTime.json"


//Account
#define GetAccountBillingInfo @"getBillingInfo.json"
#define SendInvoiceEmailURL @"emailInvoice/"
#define StoreCreditCard @"storeCreditCard.json"
#define GetPayPalAttributes @"getPaypalAttributes.json"




// iOS API Key
//AIzaSyBH93oB1aAuiPosJ45asrwe_VPdUQh-rEs
//AIzaSyC9JFQPkBJIzYynX53MYa-jKmGX2ZsejAU
//Beehive //AIzaSyCiUIv2fuca8vl-vDlEDu5D_ncNJ3Miylw
#define kGOOGLE_MAP_API_KEY @"AIzaSyCiUIv2fuca8vl-vDlEDu5D_ncNJ3Miylw"
//#define kGOOGLE_MAP_API_KEY @"AIzaSyBUfBgxOogTCgFYfv9wT4iN5Yn-G9AD3rg"
    //@"AIzaSyDvVAhIn0CL7oDQQum6gBolRISZXZ9rQuE" //Old Key
    //@"AIzaSyA0GU2EinRX-42CXftOrjOhcV_QynYhAYM"
// Working Key With any IP Address
#define kGOOGLE_MAP_SERVER_API_KEY @"AIzaSyCXD3ot9EPY_OLWpDiU7CoTh-7AgbRwR-A"


// Latest
#define kGOOGLE_MAP_SERVER_API_KEY_19JUNE @"AIzaSyDpHx_quaGc5QRo9dDjgUHml_wsFGolVRQ"
//@"AIzaSyCXD3ot9EPY_OLWpDiU7CoTh-7AgbRwR-A" //old
//@"AIzaSyCTuvQQt9El6ru8EszYadRLLvTN66MQ0uU"
//@"AIzaSyBoZc1d1GnWNX4R74OprIEL5ryXIdgYW78"
//@"AIzaSyBg9dbWmk9GsSJ4DTkN8gv8eTmZiVs7oVY"

//Direction
#define GOOGLE_AUTOCOMPLETE_SEARCH_URL @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
#define kGOOGLE_GEOCODE_URL @"http://maps.googleapis.com/maps/api/geocode/json?"
#define GOOGLE_MAP_Direction_URL @"http://maps.googleapis.com/maps/api/directions/json?"


// Font Size
#define CenturyGothicBold9     [UIFont fontWithName:@"Gothic725 Bd BT" size:9.0]
#define CenturyGothicBold10    [UIFont fontWithName:@"Gothic725 Bd BT" size:10.0]
#define CenturyGothicBold11    [UIFont fontWithName:@"Gothic725 Bd BT" size:11.0]
#define CenturyGothicBold12    [UIFont fontWithName:@"Gothic725 Bd BT" size:12.0]
#define CenturyGothicBold15    [UIFont fontWithName:@"Gothic725 Bd BT" size:15.0]
#define CenturyGothicBold18    [UIFont fontWithName:@"Gothic725 Bd BT" size:18.0]

#define CenturyGothiCRegular11 [UIFont fontWithName:@"Century Gothic" size:11.0]
#define CenturyGothiCRegular12 [UIFont fontWithName:@"Century Gothic" size:12.0]
#define CenturyGothiCRegular13 [UIFont fontWithName:@"Century Gothic" size:13.0]
#define CenturyGothiCRegular14 [UIFont fontWithName:@"Century Gothic" size:14.0]
#define CenturyGothiCRegular15 [UIFont fontWithName:@"Century Gothic" size:15.0]
#define CenturyGothiCRegular16 [UIFont fontWithName:@"Century Gothic" size:16.0]
#define CenturyGothiCRegular18 [UIFont fontWithName:@"Century Gothic" size:18.0]


//  TitleColors
#define WHITECOLOR [UIColor whiteColor]
#define BLACKCOLOR [UIColor blackColor]
#define GRAYCOLOR [UIColor lightGrayColor]
#define DARKGRAYCOLOR [UIColor darkGrayColor]
#define CALENDARBACKGROUNDCOLOR [UIColor colorWithRed:245/255.0f green:245/255.0f blue:245/255.0f alpha:1.0]

#define NSLog if (1) NSLog

//  PayPal URLs
#define PayPalSanboxURL @"https://api.sandbox.paypal.com"
#define PayPalLiveURL @"https://api.paypal.com"


#define PRODUCTION_ENV_CLIENT_ID_PAYPAL @"AfP7WBCu8WXVoBgfc1VVU1PB8RW5_biKF9LYrsy6smKkW3e4LFXd7ZLR2zQl"
#define SANDBOX_ENV_CLIENT_ID_PAYPAL @"AfP7WBCu8WXVoBgfc1VVU1PB8RW5_biKF9LYrsy6smKkW3e4LFXd7ZLR2zQl"


#define hasLogin @"USERLOGGEDIN"
#define hasCheckedIn @"CHECKEDIN"
#define isMidNightNotificationSet @"MidNightNotificationSet"
#define DISTANCE_FILTER 50;
#define TIMEBASED_LOCATION_CHECK 50; //in seconds


@interface Global : NSObject

- (BOOL) validateEmail: (NSString *) string;
- (BOOL) validateName: (NSString *) string;

+(UIView *)paymentView;
+(UIButton *) checkedInModeButton;
+(NSString *)theID;
@end
