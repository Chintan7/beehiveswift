//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  DirectionViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 16/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit


class DirectionViewController: BaseViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    var projectMapView: GMSMapView!
    var projectLocation: CLLocation!
    var projectAddressString: String!
    var gmsCameraPosition: GMSCameraPosition!
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation!
    var waypoints = NSMutableArray()
    var waypointStrings = NSMutableArray()


    override func viewDidLoad() {
        super.viewDidLoad()
        //     Do any additional setup after loading the view.
        self.waypointStrings = NSMutableArray()
        // Create a GMSCameraPosition that tells the map to display the coordinate at zoom level 6
        self.customizeView()
        print("Project Location : \(self.projectLocation)")
        print("Project Location : \(self.projectLocation)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func customizeView() {
//        if screenSize.height != 568 {
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg")!)
//        }
//        else {
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg-568h")!)
//        }
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let backBtn = UIButton(type: .custom)
        backBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        backBtn.setImage(image, for: .normal)
        backBtn.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let bckBarButton = UIBarButtonItem(customView: backBtn)
        let leftFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titlelabel = UILabel()
        titlelabel.text = "Project Location"
        titlelabel.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(17.0))!
        titlelabel.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(120), height: CGFloat(20))
        titlelabel.textAlignment = .right
        let titleLabel = UIBarButtonItem(customView: titlelabel)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        var barItems = [UIBarButtonItem]()
        barItems.append(bckBarButton)
        barItems.append(leftFlexSpace)
        barItems.append(titleLabel)
        barItems.append(rightFlexSpace)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        //    [GMSServices provideAPIKey:kGOOGLE_MAP_SERVER_API_KEY_19JUNE];
        //    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:43.680250 longitude:-79.630832 zoom:12.0f];
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
//        if IS_OS_8_OR_LATER {
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.requestAlwaysAuthorization()
//        }
        gmsCameraPosition = GMSCameraPosition.camera(withLatitude: self.projectLocation.coordinate.latitude, longitude: self.projectLocation.coordinate.longitude, zoom: 9.0)
        projectMapView = GMSMapView.map(withFrame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(240)), camera: gmsCameraPosition)
        
        projectMapView.isMyLocationEnabled = true
        projectMapView.mapType = kGMSTypeNormal
        //    self.mapView.mapType = kGMSTypeHybrid;
        //    self.mapView.mapType = kGMSTypeSatellite;
        //    self.mapView.mapType = kGMSTypeTerrain;
        //    self.mapView.mapType = kGMSTypeNone;
        projectMapView.isIndoorEnabled = true
        projectMapView.accessibilityElementsHidden = true
        projectMapView.settings.scrollGestures = true
        projectMapView.settings.zoomGestures = true
        projectMapView.settings.compassButton = true
        projectMapView.settings.myLocationButton = true
        projectMapView.delegate = self
        self.view.addSubview(projectMapView)
        //    [self placeMarkers];
        self.waypoints = NSMutableArray()
        let projectlocationLabel = UILabel(frame: CGRect(x: CGFloat(60), y: CGFloat(304), width: CGFloat(220), height: CGFloat(50)))
        projectlocationLabel.lineBreakMode = .byWordWrapping
        //    nameLabel.text = @"112 George St., Toronto, Ontario M5A 2M5";
        projectlocationLabel.text = self.projectAddressString
        projectlocationLabel.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        projectlocationLabel.textAlignment = .center
        projectlocationLabel.textColor = UIColor.black
        projectlocationLabel.numberOfLines = 2
        self.view.addSubview(projectlocationLabel)
        let directionButton = UIButton(type: .custom)
        directionButton.frame = CGRect(x: CGFloat(60), y: CGFloat(365), width: CGFloat(200), height: CGFloat(35))
        directionButton.setBackgroundImage(UIImage(named: "plainOrangeBtn.png")!, for: .normal)
        directionButton.setTitle("Get Directions", for: .normal)
        directionButton.addTarget(self, action: #selector(self.getDirections), for: .touchUpInside)
        directionButton.setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(directionButton)
    }

    override func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }

    func backBtnClick() {
        popTo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        projectMapView.addObserver(self, forKeyPath: "myLocation", options: .new, context: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        projectMapView.removeObserver(self, forKeyPath: "myLocation")
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "myLocation") && (object is GMSMapView) {
            print("KVO triggered. Location Latitude: \(projectMapView.myLocation?.coordinate.latitude) Longitude: \(projectMapView.myLocation?.coordinate.longitude)")
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = CLLocation()
        currentLocation = (locations.last! )
        gmsCameraPosition = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 9.0)
        //    [locationManager stopUpdatingLocation];
        locationManager = nil

    }


    func updateLocation() {
        gmsCameraPosition = GMSCameraPosition.camera(withLatitude: (projectMapView.myLocation?.coordinate.latitude)!, longitude: (projectMapView.myLocation?.coordinate.longitude)!, zoom: 9.0)
        //myLocation
        print("Lat : \(projectMapView.myLocation?.coordinate.latitude) \n Long : \(projectMapView.myLocation?.coordinate.longitude) ")
        //    projectMapView.delegate = self;
        //    currentLocationMapView.settings.myLocationButton = YES;
        projectMapView.camera = gmsCameraPosition
    }
// MARK: - GMSMapViewDelegate

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {

        /*
            NSLog(@"you tapped at %f, %f", coordinate.longitude, coordinate.latitude);
            
            CLLocationCoordinate2D tapPosition = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
            
            GMSMarker *tapMarker = [GMSMarker markerWithPosition:tapPosition];
            tapMarker.map = projectMapView;
            [self.waypoints addObject:tapMarker];
            
            NSString *positionString = [NSString stringWithFormat:@"%f,%f", coordinate.latitude,coordinate.longitude];
            [self.waypointStrings addObject:positionString];
            
            if (self.waypoints.count > 1) {
                NSDictionary *query = @{ @"sensor" : @"false", @"waypoints" : self.waypointStrings };
                MDDirectionService *mds = [[MDDirectionService alloc] init];
                SEL selector = @selector(addDirections:);
                [mds setDirectionsQuery:query withSelector:selector withDelegate:self];
            }
             */
    }

    func addDirections(_ json: NSMutableDictionary) {
//        var routes = json["routes"][0]
//        var route = routes["overview_polyline"]
//        var overview_route = route["points"]
//        var path = GMSPath.fromEncodedPath(overview_route)
//        var polyline = GMSPolyline(path)
//        polyline.strokeColor = UIColor.red
//        polyline.strokeWidth = 3.0
//        polyline.map = projectMapView
    }

    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //    [mapView clear];
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("map became idle")
        //    [self placeMarkers];
    }

    func getDirections() {
            //  Source Point
            //    CLLocationCoordinate2D firstTapPosition = CLLocationCoordinate2DMake(43.669000,-79.698053);
            //    CLLocationCoordinate2D firstTapPosition = CLLocationCoordinate2DMake(projectMapView.myLocation.coordinate.latitude,projectMapView.myLocation.coordinate.longitude);
        let firstTapPosition = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
        let firstTapMarker = GMSMarker(position: firstTapPosition)
        firstTapMarker.map = projectMapView
        self.waypoints.add(firstTapMarker)
            //    NSString *firstPositionString = [NSString stringWithFormat:@"%f,%f", 43.669000,-79.698053];
        let firstPositionString = "\(projectMapView.myLocation!.coordinate.latitude),\(projectMapView.myLocation!.coordinate.longitude)"
        self.waypointStrings.add(firstPositionString)
            //  Destination Point
            //    CLLocationCoordinate2D secondTapPosition = CLLocationCoordinate2DMake(-43.669000,-79.698053);
        let secondTapPosition = CLLocationCoordinate2DMake(self.projectLocation.coordinate.latitude, self.projectLocation.coordinate.longitude)
        let secondTapMarker = GMSMarker(position: secondTapPosition)
        secondTapMarker.map = projectMapView
        self.waypoints.add(secondTapMarker)
            //    NSString *secondPositionString = [NSString stringWithFormat:@"%f,%f", -43.669000,-79.698053];
        let secondPositionString = "\(self.projectLocation.coordinate.latitude),\(self.projectLocation.coordinate.longitude)"
        self.waypointStrings.add(secondPositionString)
        print("self.waypoints..\(self.waypoints)")
        if self.waypoints.count > 1 {
            let query = ["sensor": "false", "waypoints": self.waypointStrings] as [String : Any]
//            let mds = MDDirectionService()
            let selector = #selector(self.addDirections)
//            mds.setDirectionsQuery(query as NSDictionary, with: selector, withDelegate: self)
        }
    }

}
//
//  DirectionViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 16/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
