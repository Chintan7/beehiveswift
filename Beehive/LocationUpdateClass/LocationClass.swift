//
//  LocationClass.swift
//  Beehive
//
//  Created by Ankit on 2/1/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

class LocationClass: NSObject, CLLocationManagerDelegate,UNUserNotificationCenterDelegate {

    var locationManager: CLLocationManager!
    var previousLocation: CLLocation?
    var currentLocation: CLLocation?
    var selectedLocation: CLLocation?
    var checkIn: Bool? = false
    var center = UNUserNotificationCenter.current()
    var arrBeehiveProject = [BeehiveProject]()
    var strProjectDetail: String?
    var strDistance: String? = ""
    var batteryLevel: Float {
        return UIDevice.current.batteryLevel * 100
    }
    
    
    var munites: Int = 1
    var extraMeter: Int = 50
    
    var checkOutCount = 0
    var checkInCount = 0
    var checkOutlocationDict = [Any]()
    var checkInlocationDict = [Any]()
    var lastCheckoutDate = Date.init()
    var lastCheckInDate = Date.init()
    
    var inTimer: Timer?
    var locationUpdateTimer: Timer?
    var chekoutPress: Bool?
    
    var sucessCall: (() -> Void)?
    
    var i = 0
    
    public class var sharedInstance: LocationClass {
        struct Singleton {
            static let instance: LocationClass = LocationClass()
        }
        return Singleton.instance
    }
    
    func getMunits(munits: Int) -> Int {
        return munits * 60
    }

    override init() {
        super.init()
        
        //Timer.scheduledTimer(timeInterval: TimeInterval.init(120), target: self, selector: #selector(self.startMonitoring), userInfo: nil, repeats: true)
        
        Timer.scheduledTimer(timeInterval: TimeInterval.init(60), target: self, selector: #selector(self.saveTextFile), userInfo: nil, repeats: true)
        
        if let munite = UserDefaults.standard.object(forKey: "munites") as? Int {
            munites = munite
        }
        
        if let date = UserDefaults.standard.object(forKey: "lastCheckoutDate") as? Date {
            lastCheckoutDate = date
        }
        
        
        DispatchQueue.main.async {
            self.initLocationManager()
        }
        
        let options: UNAuthorizationOptions = [.alert, .sound, .badge];
        center.delegate = self
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("Something went wrong")
            }
        }
        //Timer.scheduledTimer(timeInterval: TimeInterval.init(5.0), target: self, selector: #selector(self.timerUpdate), userInfo: nil, repeats: true)
    }
    
    func timerUpdate() {
        let strTimer = "\nUpdate Log Timer---\(APPDELEGATE.getDate())---\n"
        self.setLog(str: strTimer)
        print(strTimer)
    }
    
    func initLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
//        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = 40//30//50
//        self.locationManager.distanceFilter = kCLLocationAccuracyBest
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.requestAlwaysAuthorization()
    }
    
    func startMonitoring() {
//      self.setNotification(body: "update location manually", title: "Location")
        
        self.locationManager.startMonitoringSignificantLocationChanges()
        self.locationManager.startUpdatingLocation()
        
//        startReceivingSignificantLocationChanges()
    }
    
    func stopMonitoring() {
        self.locationManager.stopMonitoringSignificantLocationChanges()
        self.locationManager.stopUpdatingLocation()
        
//        stopReceivingSignificantLocationChanges()
    }
    
    //MARK: Jaimesh
    
    func startReceivingSignificantLocationChanges() {
        
//        locationManager = CLLocationManager()
        
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedAlways {
            // User has not authorized access to location information.
            self.setLog(str: "User is not authorized.")
            return
        }
        
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            // The service is not available.
            self.setLog(str: "Significant location change service is not available.")
            return
        }
        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = true
//        locationManager.activityType =
        
        locationManager.startMonitoringSignificantLocationChanges()
        
    }
    
    func stopReceivingSignificantLocationChanges() {
        self.locationManager.stopMonitoringSignificantLocationChanges()
    }
    
//    func startRegionMonitoring(geotification: Geotification) {
//        // 1
//        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
//            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
//            return
//        }
//        // 2
//        if CLLocationManager.authorizationStatus() != .authorizedAlways {
//            showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
//        }
//        // 3
//        let region = self.region(withGeotification: geotification)
//        // 4
//        locationManager.startMonitoring(for: region)
//    }
//    
//    func handleEvent(forRegion region: CLRegion!) {
//        print("Geofence triggered!")
//    }
//    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
//        if !UserDefaults.standard.bool(forKey: hasCheckedIn) {
//            UserDefaults.standard.set(true, forKey: hasCheckedIn)
//            UserDefaults.standard.synchronize()
//            self.check(in: "\(region.identifier)", checkInTime: dateTime)
//        }
        self.setLog(str: "Project \(region.identifier) entered region at \(dateTime)")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
//        if UserDefaults.standard.bool(forKey: hasCheckedIn) {
//            self.checkOut("\(region.identifier)", checkOutTime: dateTime)
//        }
        self.setLog(str: "Project \(region.identifier) exited region at \(dateTime)")

    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        let theRegion = (region as Any) as! CLCircularRegion
        theRegion.notifyOnEntry = true
        theRegion.notifyOnExit = true
        
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
        self.setLog(str: "Project \(region.identifier) monitoring started at \(dateTime)")

        if currentLocation?.coordinate != nil {
            if theRegion.contains(currentLocation!.coordinate) {
                locationManager(manager, didEnterRegion: theRegion)
            }
        }
    }
    
    //
    
    func checkDate() {
        
        
    }
    
    
    // MARK: ------------ method ------------
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
//        let hour = Calendar.current.component(.hour, from: Date())
//        
//        if hour < 7 && hour > 19 {
//            return
//        }
//        
        //self.locationManager.distanceFilter = self.locationManager.distanceFilter + 10.0
        
        //print("Location manager \(self.locationManager.distanceFilter)")
        i = i + 1
        print("Index: \(i)     Date: \(APPDELEGATE.getDate())")
        
        var locationUpdateString = ""
        let location = locations.last
        let currentLat = location!.coordinate.latitude
        let currentLong = location!.coordinate.longitude
        let currentLocationHorizontalAcc = location!.horizontalAccuracy
        _ = location!.timestamp
        locationUpdateString = "lat- \(currentLat) \nLong- \(currentLong)\n Speed- \(location!.speed)\n"
        var theNearestProject: BeehiveProject?
        var totalDistance = 0.0
        
        if currentLocationHorizontalAcc <= 100 {
            
            if self.arrBeehiveProject.count > 0 {
                
                for project in self.arrBeehiveProject {
                    let projectCorrinate = CLLocation.init(latitude: CLLocationDegrees.init(project.theLatitude)!, longitude: CLLocationDegrees.init(project.theLongitude)!)
                    totalDistance = (location?.distance(from: projectCorrinate))!
                    if totalDistance <=  Double.init(project.theRadius)! {
                        theNearestProject = project
                        strDistance = "\(totalDistance)"
                        locationUpdateString.append("Name - \(theNearestProject!.theName)\nRadius- \(theNearestProject!.theRadius)\nprojectID-\(theNearestProject!.theId)\nstrDistance \(strDistance!)\n")
                        break
                    }
                }

                if UserDefaultClass.sharedInstance().theCurrentProjectId == theNearestProject?.theId {

                    if theNearestProject?.theId == nil {
                        locationUpdateString.append("Already Checkd Out\n")
                        strDistance = ""
                        
                    } else {
                        
                        locationUpdateString.append("Already Checkd In ID- \(theNearestProject!.theId)\n")
                        
                    }
                    checkInlocationDict.removeAll()
                    checkOutlocationDict.removeAll()

                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "CheckInNotification"), object: nil)
                    
                    checkOutCount = 0
                    checkInCount = 0                   
                    
                } else {
                    
                    if let projectID = UserDefaultClass.sharedInstance().theCurrentProjectId {
                        
                        locationUpdateString.append("Go To check out for This id - \(projectID)\n")
                        
                        if (inTimer != nil)
                        {
                            inTimer?.invalidate()
                            inTimer = nil
                        }
                        
                        inTimer = Timer.scheduledTimer(timeInterval: TimeInterval.init(65), target: self, selector: #selector(self.restartMonitoring), userInfo: nil, repeats: false)
                        
                        if let tempProject = self.getProjectWithID(projectID: projectID) {
                            
                            locationUpdateString.append("Find project from database by this id - \(projectID)\n")
                            let projectCorrinateTemp = CLLocation.init(latitude: CLLocationDegrees.init(tempProject.theLatitude)!, longitude: CLLocationDegrees.init(tempProject.theLongitude)!)

                            let totalDistanceTemp = (location?.distance(from: projectCorrinateTemp))!
                            let totalRadiusPlusAccuracy = Double.init(tempProject.theRadius)!
                            
                            if (totalRadiusPlusAccuracy < totalDistanceTemp) {
                                
                                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "CheckInNotification"), object: nil)
                                locationUpdateString.append("check Out Project ID- \(tempProject.theId) \ntotalRadiusPlusAccuracy- \(totalRadiusPlusAccuracy) \ntotalDistanceTemp-\(totalDistanceTemp)\n")
                                
                                if checkOutCount == 3 {
                                    
                                    if checkOutlocationDict.count > 0 {
                                        
                                        if let locationObjFromDict = checkOutlocationDict[0] as? [String: Any] {
                                            
                                            if let date = locationObjFromDict["date"] as? Date {
                                                
                                                if self.seconds(from: date) > self.getMunits(munits: munites) {
                                                    munites = 1
                                                    checkOutCount = 0
                                                    UserDefaultClass.sharedInstance().theCurrentProjectId = nil
                                                    self.setNotification(body: "\(tempProject.theName)", title: "Check Out")
                                                    ProjectManager.sharedInstance().methodCheck(in: false, projectId: projectID, timeStamp: getTimeStepStringFromDate(date: date), noCopy: {})
                                                    if UIApplication.shared.applicationState == .active {
                                                        self.showAlert(str: "Check Out - \(tempProject.theName)")
                                                        chekoutPress = nil
                                                    }
                                                    locationUpdateString.append("go final check out\n")
                                                    lastCheckoutDate = Date.init()
                                                    UserDefaults.standard.set(lastCheckoutDate, forKey: "lastCheckoutDate")
                                                    checkInlocationDict.removeAll()
                                                } else {
                                                    if theNearestProject?.theId != nil {
                                                        if projectID != theNearestProject!.theId {
                                                            // project is other
                                                            checkOutCount = 0
                                                            UserDefaultClass.sharedInstance().theCurrentProjectId = nil
                                                            self.setNotification(body: "\(tempProject.theName)", title: "Check Out")
                                                            ProjectManager.sharedInstance().methodCheck(in: false, projectId: projectID, timeStamp: getTimeStepStringFromDate(date: date), noCopy: {})
                                                            if UIApplication.shared.applicationState == .active {
                                                                self.showAlert(str: "Check Out - \(tempProject.theName)")
                                                                chekoutPress = nil
                                                            }
                                                            locationUpdateString.append("go final check out user is in other radius\n")
                                                            lastCheckoutDate = Date.init()
                                                            UserDefaults.standard.set(lastCheckoutDate, forKey: "lastCheckoutDate")
                                                            checkInlocationDict.removeAll()
                                                        } else {
                                                            locationUpdateString.append("checking out Project \(self.seconds(from: date)) \n Project - \(tempProject.theName)\n")
                                                        }
                                                    } else {
                                                        locationUpdateString.append("checking out Project \(self.seconds(from: date)) \n Project - \(tempProject.theName)\n")
                                                    }
                                                }
                                            } else{
                                                locationUpdateString.append("unable to get date from first object")
                                            }
                                        } else {
                                            locationUpdateString.append("unable to get first object from array")
                                        }
                                    } else {
                                        locationUpdateString.append("0 location update count")
                                    }
                                } else {
                                    checkOutCount = checkOutCount + 1
                                    locationUpdateString.append("go to \(checkOutCount) time check out\n")
                                    let locationDetails = ["totalRadiusPlusAccuracy":"\(totalRadiusPlusAccuracy)","totalDistanceTemp":"\(totalDistanceTemp)","date":Date.init()] as [String : Any]
                                    checkOutlocationDict.append(locationDetails)
                                }
                            } else {
                                locationUpdateString.append("TotalRadiusPlusAcuracy - \(totalRadiusPlusAccuracy) < TotalDistance- \(totalDistanceTemp)\n")
                            }
                        } else {
                            locationUpdateString.append("Project not found for This id - \(projectID)\n")
                        }
                    } else {

                        if checkInCount == 0 {
                            
                            checkInCount = 1
                            lastCheckInDate = Date.init()
                            locationUpdateString.append("First Check in ProjectID- \(theNearestProject!.theId)\n")
                            if (inTimer != nil) && inTimer?.isValid == true
                            {
                                inTimer?.invalidate()
                                inTimer = nil
                            }
                            
                            inTimer = Timer.scheduledTimer(timeInterval: TimeInterval.init(65), target: self, selector: #selector(self.startMonitoring), userInfo: nil, repeats: false)
                            
                        } else {
                            
                            let locationDetails = ["totalDistance":"\(totalDistance)","date":Date.init()] as [String : Any]
                            checkInlocationDict.append(locationDetails)
                            locationUpdateString.append("Go To check in for This id - \(theNearestProject!.theId)\n")
                            
                            if self.seconds(from: lastCheckInDate) > self.getMunits(munits: munites) {
                                
                                if let locationObjFromDict = checkInlocationDict[0] as? [String: Any] {
                                    
                                    if let date = locationObjFromDict["date"] as? Date {
                                        munites = 1
                                        checkOutCount = 0
                                        checkInCount = 0
                                        UserDefaultClass.sharedInstance().theCurrentProjectId = theNearestProject!.theId
                                        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "CheckInNotification"), object: nil)
                                        self.setNotification(body: "\(theNearestProject!.theName)", title: "Check IN")
                                        if UIApplication.shared.applicationState == .active {
                                            self.showAlert(str: "Check In - \(theNearestProject!.theName)")
                                            chekoutPress = nil
                                        }
                                        locationUpdateString.append("Check in ProjectID- \(theNearestProject!.theId)\n")
                                        ProjectManager.sharedInstance().methodCheck(in: true, projectId: (theNearestProject!.theId), timeStamp: getTimeStepStringFromDate(date: date), noCopy: {})
                                        checkOutlocationDict.removeAll()
                                    }
                                }
                            } else {
                                locationUpdateString.append("Checking in ProjectID- \(theNearestProject!.theId)\n \(self.seconds(from: lastCheckInDate))\n")
                            }
                        }
                    }
                }
            } else {
                locationUpdateString.append("Project Not found\n")
            }
        } else {
            
        }
        
//        for project in self.arrBeehiveProject {
//        let projectCorrinate = CLLocation.init(latitude: CLLocationDegrees.init(project.theLatitude)!, longitude: CLLocationDegrees.init(project.theLongitude)!)
//        totalDistance = (location?.distance(from: projectCorrinate))!
        
      /*  if let nearedProject = self.arrBeehiveProject.first(where: { (project: BeehiveProject) -> Bool in
            let projectCorrinate = CLLocation.init(latitude: CLLocationDegrees.init(project.theLatitude)!, longitude: CLLocationDegrees.init(project.theLongitude)!)
            totalDistance = (location?.distance(from: projectCorrinate))!
            let radius = Double.init(project.theRadius)
            return (totalDistance + radius!) < 400.0
        })
        {
            locationUpdateString.append("\nFound nearest project: \(nearedProject.theName).  Setting filter 30")
            self.locationManager.distanceFilter = 10.0
        }
        else
        {
            locationUpdateString.append("\nNot Found nearest project. Setting filter 200")
            self.locationManager.distanceFilter = 200.0
        }
        */
        if currentLocationHorizontalAcc < 50 {
            locationUpdateString.append("Accuracy in 0 - 50\n")
        } else if currentLocationHorizontalAcc < 100 {
            locationUpdateString.append("Accuracy in 50 - 100\n")
        } else if currentLocationHorizontalAcc < 200 {
            locationUpdateString.append("Accuracy more then 100 - 200\n")
        } else {
            locationUpdateString.append("Accuracy more then 200 - x\n")
        }
        
        locationUpdateString.append("CurrentLocationHorizontalAcc- \(currentLocationHorizontalAcc)\n")
        self.setLog(str:locationUpdateString)
        self.strProjectDetail = "lat - \(currentLat) \nlong - \(currentLong) \nTotalDistance-\(strDistance!)\naccuracy-\(currentLocationHorizontalAcc)"
        NotificationCenter.default.post(name: NSNotification.Name.init("setprojectDetail"), object: nil)
        
        
        if (locationUpdateTimer != nil)
        {
            locationUpdateTimer?.invalidate()
            locationUpdateTimer = nil
        }
        
        locationUpdateTimer = Timer.scheduledTimer(timeInterval: TimeInterval.init(60 * 3), target: self, selector: #selector(self.timerLocationUpdate), userInfo: nil, repeats: false)
    }
        
    func timerLocationUpdate() {
        self.setLog(str: "Location update after 3 mins")
        self.restartMonitoring()
    }
    
    
    func showChekinoutAlert() {
        
        if chekoutPress != nil {
            
            if chekoutPress == true {
                self.showAlert(str: "You are already check in to project area")
            } else {
                self.showAlert(str: "You are already check out from project area")
            }
            chekoutPress = nil
        }
        
        sucessCall!()
    }
    

    func restartMonitoring() {
        self.stopMonitoring()
        self.startMonitoring()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {}
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {}
    
    func setNotification(body: String, title: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                        repeats: false)
        // Swift
        let identifier = "UYLLocalNotification"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if error != nil {}
        })
    }
    
    func getTimeStepString() -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.locale = Locale.init(identifier: "en_US")
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormat.string(from: Date.init())
    }
    
    func getTimeStepStringFromDate(date: Date) -> String {
        
        let dateFormat = DateFormatter()
        dateFormat.locale = Locale.init(identifier: "en_US")
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormat.string(from: date)
    }
    
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        print("\(String(describing: error))")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("\(error)")
    }
    
    func showAlert(str: String) {
        
        UIAlertView.init(title: kAPPNAME, message: "\(str)", delegate: nil, cancelButtonTitle: "OK").show()
        
//        let alert = UIAlertController(title: "Alert", message: "\(str)", preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
//        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func getAllCurrentProjects() {
        
        BeehiveProject.methodDownloadAllProjectsWithNoCopy { (projects:[BeehiveProject]?, error:Error?) in
            if (error == nil) {
                if projects != nil {
                    if (projects?.count)! > 0 {
                        self.arrBeehiveProject = projects!
                        for project in self.arrBeehiveProject {
                            print(project.theName)
                            
                            //Jaimesh
                            let regions = self.locationManager.monitoredRegions
                            
                            var centre = CLLocationCoordinate2D()
                            
                            let lati = project.theLatitude
                            let long = project.theLongitude
                            let radius = project.theRadius
                            let ide = project.theId + project.theName
                            
                            centre.latitude = (lati as NSString).doubleValue
                            centre.longitude = (long as NSString).doubleValue
                            
                            let region = CLCircularRegion.init(center: centre, radius: CLLocationDistance.init(radius)!, identifier: ide )
                            
                            if !regions.contains(region) {
                                self.locationManager?.startMonitoring(for: (region as CLRegion))
                            }
                        }
                        self.startMonitoring()
                    }
                }
            } else {
                self.arrBeehiveProject = BeehiveProject.getObjectsArray(with: nil, propertyToFetch: nil, sortDescriptorArray: nil) as! [BeehiveProject]
                self.startMonitoring()
            }
        }
    }
    
    func getProjectWithID(projectID: String) -> BeehiveProject? {
        
        let fetchRequest = NSFetchRequest<BeehiveProject>(entityName: "BeehiveProject")
        fetchRequest.predicate = NSPredicate(format: "theId == %@", projectID)
        
        do {
            let locations = try APPDELEGATE.managedObjectContext.fetch(fetchRequest)
            
            if locations.count > 0 {
                return locations.first!
            }
        } catch {}
        return nil
    }
    
    func seconds(from date: Date) -> Int {
        
        let unitFlags = Set<Calendar.Component>([.second])
        return Calendar.current.dateComponents(unitFlags, from: date, to: Date.init()).second ?? 0
    }
    
    func setLog(str: String) {
        
        LOGSTRING = LOGSTRING + "\nV-3.9.2\nBattery - \(self.batteryLevel) \nDate - \(APPDELEGATE.getDate())\n-------------\n\(str)\n-------------\n"
//        self.saveTextFile()
    }
    
    func saveTextFile()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let basePath = ((paths.count) > 0) ? "\(paths[0])" : ""
        let fileDestinationUrl = (basePath as NSString).appendingPathComponent("file.txt") as NSString?
        let text = LOGSTRING
        do {
            try text.write(toFile: fileDestinationUrl! as String, atomically: false, encoding: .utf32)
        } catch let error as NSError {
            print("error writing to url \(String(describing: fileDestinationUrl))")
            print(error.localizedDescription)
        }
    }
    
    func setupLogFile() {
//        if let str = UserDefaults.standard.object(forKey: "LOGSTRING") as? String {
//            LOGSTRING = str
//        } else {
            let file = "file.txt"
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                
                let path = dir.appendingPathComponent(file)
                
//                //writing
//                do {
//                    try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
//                }
//                catch {/* error handling here */}
                
                //reading
                do {
                    LOGSTRING = try String(contentsOf: path, encoding: String.Encoding.utf32)
                    print(LOGSTRING)
                }
                catch let error {
                    print(error)
                }
            }
//        }
    }
    
}
