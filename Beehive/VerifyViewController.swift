//
//  VerifyViewController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class VerifyViewController: BaseViewController,UIAlertViewDelegate {

    @IBOutlet var topView: UIView!
    @IBOutlet var resendEmailButton: UIButton!
//    @IBOutlet var welcomeLabel: UILabel!
//    @IBOutlet var welcomeMessageLabel: UILabel!
    var projectsArray: NSMutableArray = []
    var emailId: String!
    var responseTokenString: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.navigationItem.setHidesBackButton(true, animated: false)
        topView.backgroundColor = UIColor(patternImage: UIImage(named: "verify_header.png")!)
        resendEmailButton.titleLabel!.font = UIFont(name: FONTTYPE, size: CGFloat(15.0))!
//        welcomeLabel.font = UIFont(name: "Helvetica", size: CGFloat(17.0))!
//        welcomeMessageLabel.font = UIFont(name: "Helvetica", size: CGFloat(12.0))!
        let newAccountButton = UIButton(type: .custom)
        newAccountButton.frame = CGRect(x: CGFloat(150), y: CGFloat(252), width: CGFloat(150), height: CGFloat(18))
        newAccountButton.setTitle("Set up a new account ?", for: .normal)
        newAccountButton.setTitleColor(UIColor.darkGray, for: .normal)
//      newAccountButton.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(11.0))!
        newAccountButton.titleLabel?.font = UIFont.init(name: FONTTYPE, size: CGFloat(11))
        newAccountButton.addTarget(self, action: #selector(self.createNewAccount), for: .touchUpInside)
        self.view.addSubview(newAccountButton)
        self.checkEmailVerification()
        // Do any additional setup after loading the view.

        let myAlert = UIAlertView(title: "Welcome", message: "Thank You For Email verification !!", delegate: self, cancelButtonTitle: "OK")
        myAlert.tag = 1
        myAlert.show()

    }

    //  Converted with Swiftify v1.0.6176 - https://objectivec2swift.com/

    func createNewAccount() {
        pushTo(string: "SignUpViewController")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigation(true)
    }

    @IBAction func resendVerificationButtonClick(_ sender: Any) {
        self.resendVerificationEmail()
    }

    func resendVerificationEmail() {

    }

    func checkEmailVerification() {

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView.tag == 1 {
            if buttonIndex == 0 {
                self.loginWebservice()
            }
        }
    }

    func loginWebservice() {
        pushTo(string: "")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
