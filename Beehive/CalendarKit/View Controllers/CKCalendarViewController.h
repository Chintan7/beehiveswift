//
//  CKCalendarViewController.h
//  MBCalendarKit
//
//  Created by Moshe Berman on 4/17/13.
//  Copyright (c) 2013 Moshe Berman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CKCalendarDataSource.h"
#import "CKCalendarDelegate.h"
#import "CKCalendarEvent.h"

@interface CKCalendarViewController : UINavigationController <CKCalendarViewDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) id<CKCalendarViewDataSource> dataSource;
@property (nonatomic, weak) id<CKCalendarViewDelegate, UINavigationControllerDelegate> delegate;

- (CKCalendarView *)calendarView;

@end

