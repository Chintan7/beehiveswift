//
//  CKCalendarHeaderColors.h
//   MBCalendarKit
//
//  Created by Moshe Berman on 4/14/13.
//  Copyright (c) 2013 Moshe Berman. All rights reserved.
//

#ifndef MBCalendarKit_CKCalendarHeaderColors_h
#define MBCalendarKit_CKCalendarHeaderColors_h

#import "NSString+Color.h"

#define kCalendarColorHeaderWeekdayTitle    [@"#545454" toColor]
//#define kCalendarColorHeaderWeekdayShadow   [@"#f3f3f4" toColor]

#define kCalendarColorHeaderWeekdayShadow [UIColor colorWithRed:245/255.0f green:245/255.0f blue:245/255.0f alpha:1.0]

#define kCalendarColorHeaderMonth           [@"#545454" toColor]
#define kCalendarColorHeaderMonthShadow     [@"#f6f6f7" toColor]

//#define kCalendarColorHeaderGradientLight   [@"#f4f4f5" toColor]
//#define kCalendarColorHeaderGradientDark    [@"#ccccd1" toColor]

#define kCalendarColorHeaderGradientLight [UIColor colorWithRed:245/255.0f green:245/255.0f blue:245/255.0f alpha:1.0]
#define kCalendarColorHeaderGradientDark [UIColor colorWithRed:245/255.0f green:245/255.0f blue:245/255.0f alpha:1.0]

#define kCalendarColorHeaderTitleHighlightedBlue [@"#1980e5" toColor]

#endif
