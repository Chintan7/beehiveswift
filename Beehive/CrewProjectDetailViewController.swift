//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CrewProjectDetailViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 05/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class CrewProjectDetailViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, GMSMapViewDelegate {

    var projectMapView: GMSMapView!
    @IBOutlet var crewsCollectionView: UICollectionView!
    @IBOutlet var projectNameLabel: UILabel!
    @IBOutlet var projectLocationLabel: UILabel!
    var projectDetailDict = JSONDictionary()
    var projectCrewArray = [JSONDictionary]()
    var projectLocation: CLLocation!
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    var projectId: String!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let topToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        topToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(100), y: CGFloat(0), width: CGFloat(100), height: CGFloat(44)))
        titleLabel.text = "Project"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold18
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.toolbarBackBtnClick), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        topToolbar.setItems(barItems, animated: true)
        topToolbar.addSubview(titleLabel)
        self.view.addSubview(topToolbar)
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.getProjectDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }

    func toolbarBackBtnClick() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        popTo()
    }

    func viewCustomization() {
        if let name = self.projectDetailDict["project_name"] as? String {
            projectNameLabel.text = name
        }

        if let name = self.projectDetailDict["project_location"] as? String {
            self.projectLocationLabel.text = name
        }

        projectNameLabel.font = CenturyGothicBold15
        projectLocationLabel.font = CenturyGothiCRegular15

        var lanti = "00"
        var longi = "00"

        if let lat = self.projectDetailDict["project_lat"] as? String {
            lanti = lat
        }

        if let lng = self.projectDetailDict["project_lng"] as? String {
            longi = lng
        }

        projectLocation = CLLocation.init(latitude: Double(lanti)!, longitude: Double(longi)!)
        let camera = GMSCameraPosition.camera(withLatitude: projectLocation.coordinate.latitude, longitude: projectLocation.coordinate.longitude, zoom: 12.0)
        projectMapView = GMSMapView.map(withFrame: CGRect(x: CGFloat(0), y: CGFloat(100), width: CGFloat(320), height: CGFloat(100)), camera: camera)
        projectMapView.delegate = self
        projectMapView.isMyLocationEnabled = true
        let marker = GMSMarker()
        marker.icon = UIImage(named: "smallMarker.png")!
        marker.position = CLLocationCoordinate2DMake(projectLocation.coordinate.latitude, projectLocation.coordinate.longitude)
        marker.map = projectMapView
        self.view.addSubview(projectMapView)
        let projectLocationText = UILabel(frame: CGRect(x: CGFloat(45), y: CGFloat(200), width: CGFloat(220), height: CGFloat(60)))
        projectLocationText.lineBreakMode = .byWordWrapping
        projectLocationText.numberOfLines = 2
        self.view.addSubview(projectLocationText)

        var firstLabel = ""
        var secondLabel = ""
        var thirdLabel = ""
        if let firstLBL = self.projectDetailDict["project_street"] as? String{
            firstLabel = firstLBL
        }

        if let scndLBL = self.projectDetailDict["project_state"] as? String{
            secondLabel = scndLBL
        }

        if let thrdLBL = self.projectDetailDict["project_country"] as? String{
            thirdLabel = thrdLBL
        }

            //    Project Name and Location
//        var firstLabel = ((projectDetailDict["project"] as! String)["project_street"] as! String)
//            //    NSString *secondLabel = @"Toronto, Ontario";
//        var secondLabel = ((projectDetailDict["project"] as! String)["project_state"] as! String)
//        var thirdLabel = ((projectDetailDict["project"] as! String)["project_country"] as! String)
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel),\(thirdLabel)")
//        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel),\(thirdLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: 0, length: (firstLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: (firstLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: (firstLabel.characters.count) + 1, length: (secondLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (firstLabel.characters.count) + 1, length: (secondLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: ((firstLabel.characters.count) + 1) + ((secondLabel.characters.count) + 1), length: (thirdLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: ((firstLabel.characters.count) + 1) + ((secondLabel.characters.count) + 1), length: (thirdLabel.characters.count)))
        projectLocationText.attributedText = customTextLabelAttribute


        let projectLocationPin = UIImageView(frame: CGRect(x: CGFloat(20), y: CGFloat(215), width: CGFloat(20), height: CGFloat(32)))
        projectLocationPin.image = UIImage(named: "markerPin.png")!
        self.view.addSubview(projectLocationPin)
        let tempLbl = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(265), width: CGFloat(280), height: CGFloat(1)))
        tempLbl.backgroundColor = UIColor.lightGray
        self.view.addSubview(tempLbl)
        let crewTitleLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(275), width: CGFloat(220), height: CGFloat(15)))
        crewTitleLabel.lineBreakMode = .byWordWrapping
        crewTitleLabel.text = "Crew"
        crewTitleLabel.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        crewTitleLabel.textColor = UIColor.darkGray
        crewTitleLabel.numberOfLines = 1
        self.view.addSubview(crewTitleLabel)
        crewsCollectionView.reloadData()
    }
// MARK: CollectionView Delegate & DataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Crew Count >>>>>>:\(UInt(projectCrewArray.count))")
        if projectCrewArray.count == 0 {
            return 1
        }
        else {
            return projectCrewArray.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "MyCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        if projectCrewArray.count == 0 {
            let imageView = (cell.viewWithTag(100)! as! UIImageView)
            imageView.image = UIImage(named: "noCrew.png")!
            imageView.layer.borderColor = GRAYCOLOR.cgColor
            imageView.layer.borderWidth = 0.5
            let nameLabel = (cell.viewWithTag(200)! as! UILabel)
            nameLabel.text = ""
            let status = (cell.viewWithTag(1000)! as! UIImageView)
            status.backgroundColor = UIColor.gray
            status.layer.cornerRadius = status.bounds.size.height / 2
            status.layer.masksToBounds = true
        }
        else {
            let nameLabel = (cell.viewWithTag(200)! as! UILabel)
            let imageView = (cell.viewWithTag(100)! as! UIImageView)
            let status = (cell.viewWithTag(1000)! as! UIImageView)

            if let user = projectCrewArray[indexPath.row]["User"] as? JSONDictionary {

                nameLabel.text = user["first_name"]! as? String

                if let checkin = projectCrewArray[indexPath.row]["CheckinCheckoutTracker"] as? JSONDictionary {


                    if let statusTemp = checkin["status"] as? String  {
                        if statusTemp == "checked in" {
                            status.backgroundColor = UIColor.green
                        } else if statusTemp == "checked out" {
                            status.backgroundColor = UIColor.red
                        } else {
                            status.backgroundColor = UIColor.gray
                        }
                    }
                }
                let url = URL(string: "\(ImageBaseUrl)\(user["user_avatar"]!)")!
                imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))

            }

            //            nameLabel.text = ((projectCrewArray[indexPath.row].value(forKey: "User") as! String).value(forKey: "first_name") as! String)
            nameLabel.font = UIFont(name: "Century Gothic", size: CGFloat(10.0))!
            nameLabel.tintColor = UIColor.darkGray


            status.layer.cornerRadius = status.bounds.size.height / 2
            status.layer.masksToBounds = true

            imageView.layer.borderColor = GRAYCOLOR.cgColor
            imageView.layer.borderWidth = 0.5

            //            imageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
        }
        cell.tag = indexPath.row
        return cell
    }

    // MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
//        notificationView = NotificationView()
//        notificationView.customView()
//        if screenSize.height == 568 {
//            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
//        }
//        else {
//            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
//        }
//        notificationView.delegate = self
//        self.view.addSubview(notificationView)
//        isNotificationButtonSelected = true

    }


    func updateNotificationCountValue(_ count: Any) {
        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

//    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
//        if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            APPDELEGATE.notificationReadWebService((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            APPDELEGATE.navigate(toProfileView: self)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        //    else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
    /*
    #pragma mark - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
// MARK: - Web Service & Delegate

    func getProjectDetails() {
        APPDELEGATE.addChargementLoader()
//        var strURL = "\(BaseWebRequestUrl)\(GetProjectDetails)\(projectId).json"
//        strURL = strURL.replacingOccurrences(of: " ", with: "")
//        let url = URL(string: strURL)!
//        print("Login URL- \(url)")


        APIManager.sharedInstance.getProjectDetails(projectID: self.projectId) { (dict:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                print(dict)

                if let projectDictTemp = dict!["project"] as? JSONDictionary {
                    self.projectDetailDict = projectDictTemp
                }

                if let creqArr = dict!["pojectCrews"] as? [JSONDictionary] {
                    self.projectCrewArray = creqArr
                    self.viewCustomization()
                }
            }
        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Project Detail Dict = \(returnDict)")
//        if (((returnDict["result"] as! String)["status"] as! String) == "INVALID") {
//            var myAlert = UIAlertView(title: "Warning", message: ((returnDict["result"] as! String)["message"] as! String), delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
//            myAlert.tag = 100
//            myAlert.show()
//            return
//        }
//        else if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//            projectDetailDict = returnDict
//            print("Project Data : \(projectDetailDict)")
//            projectCrewArray = [Any]()
//            projectCrewArray = (projectDetailDict["pojectCrews"] as! String)
//            //        if (![[projectDetailDict objectForKey:@"pojectCrews"] isEqualToString:@""]) {
//            //            projectCrewArray = [projectDetailDict objectForKey:@"pojectCrews"];
//            //        }
//            self.viewCustomization()
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var .error = request.error
//        var alert: UIAlertView?
//        if .error.code == 1 {
//            alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//
//    }
}
//
//  CrewProjectDetailViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 05/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
