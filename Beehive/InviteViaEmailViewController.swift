
//
//  InviteViaEmailViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 02/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class InviteViaEmailViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var emailTextFieldBG: UIImageView!
    @IBOutlet var invitationButton: UIButton!
    var notificationBarButton: UIBarButtonItem!
    var isNotificationButtonSelected = false
    var notificationButton: UIButton!
    var notificationView: NotificationView!
    var alertLabel: UILabel!
    var projectId: String!

    @IBAction func sendInvitation(_ sender: Any) {
        _=self.textFieldAlert()

        if alertLabel != nil {
            alertLabel.removeFromSuperview()
        }

        emailTextFieldBG.image = UIImage(named: "GreyTf.png")!
        emailTextField.resignFirstResponder()

        if self.textFieldAlert() {
            self.sendInvitationViaEmail()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.viewCustomization()
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        emailTextField.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewCustomization() {


        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png"), style: .plain, target: self, action: #selector(self.sideMenuBar))

        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //    Top Toolbar Create
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(125), height: CGFloat(44)))
        titleLabel.text = "Send an Invite"
        titleLabel.textAlignment = .right
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold15
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        barItems.append(toolbarTitleLbl)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        invitationButton.setTitle("Send Invite", for: .normal)
        invitationButton.titleLabel!.font = UIFont(name: "Helvetica-Bold", size: CGFloat(20.0))!
        invitationButton.setTitleColor(UIColor(red: CGFloat(255.0), green: CGFloat(255.0), blue: CGFloat(255.0), alpha: CGFloat(0.7)), for: .normal)
        var firstLabel = "Need to invite someone to beehive and add them to a project?"
        var secondLabel = "Just enter their email address and we'll take care of the rest."
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(17.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(13.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        messageLabel.attributedText = customTextLabelAttribute
        emailTextField.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(15.0))!
        emailTextField.textColor = UIColor.darkGray
        alertLabel = UILabel(frame: CGRect(x: CGFloat(emailTextField.frame.origin.x), y: CGFloat(emailTextField.frame.origin.y + 35), width: CGFloat(emailTextField.frame.size.width), height: CGFloat(15)))
    }


    func backBtnClick() {
        popTo()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if screenSize.height != 568 {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.5)
            UIView.setAnimationTransition(.none, for: self.view, cache: true)
            self.view.frame = CGRect(x: CGFloat(0), y: CGFloat(64), width: CGFloat(self.view.frame.size.width), height: CGFloat(self.view.frame.size.height))
            UIView.commitAnimations()
        }
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            alertLabel.removeFromSuperview()
            emailTextFieldBG.image = UIImage(named: "GreyTf.png")!
            if screenSize.height != 568 {
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.5)
                UIView.setAnimationTransition(.none, for: textField, cache: true)
                self.view.frame = CGRect.init(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
                UIView.commitAnimations()
            }
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            _=self.textFieldAlert()
        }
    }

    func textFieldAlert() -> Bool {
        alertLabel.textColor = UIColor.red
        alertLabel.textAlignment = .left
        alertLabel.font = UIFont(name: "Century Gothic", size: CGFloat(12.0))!
        if (emailTextField.text == "") {
            alertLabel.text = "Email Field Cannot be blank"
            emailTextFieldBG.image = UIImage(named: "AlertTf.png")!
            self.view.addSubview(alertLabel)
            return false
        } else if emailTextField.text!.validateEmail() == false {
            alertLabel.text = "Please enter a valid email format"
            emailTextField.placeholder! = "Re-enter Email"
            emailTextFieldBG.image = UIImage(named: "AlertTf.png")!
            self.view.addSubview(alertLabel)
            return false
        }

        return true
    }


    func sendInvitationViaEmail() {
        APPDELEGATE.addChargementLoader()
        let urlString = "\(BaseWebRequestUrl)\(InviteCrewViaEmail)"
        _ = URL(string: urlString)!

        APIManager.sharedInstance.sendInvitationViaEmail(email: self.emailTextField.text!, projectID: self.projectId) { (error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "InviteSentViewController")
                self.navigationController!.pushViewController(newView, animated: true)
            }
        }

//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.delegate = self
//        request.setPostValue(self.projectId, forKey: "project_id")
//        request.setPostValue(emailTextField.text, forKey: "email")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Invitation Dict = \(returnDict)")
//        if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var newView = sb.instantiateViewController(withIdentifier: "InviteSentViewController")
//            self.navigationController!.pushViewController(newView, animated: true)
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        print("Error : \(.error)")
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
//        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
//        if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            appDelegate.notificationReadWebService((appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            appDelegate.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            appDelegate.navigate(toProfileView: self)
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }

        //    else if ([[[appDelegate.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
        //    {
        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
        //        [self.navigationController pushViewController:newView animated:YES];
        //    }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
