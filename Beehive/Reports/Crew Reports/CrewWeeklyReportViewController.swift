//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CrewWeeklyReportViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 04/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
//#import "NSString+SBJSON.h"
class CrewWeeklyReportViewController: BaseViewController, CKCalendarViewDelegate, CKCalendarViewDataSource, UITableViewDelegate, UITableViewDataSource {


    public func calendarView(_ calendarView: CKCalendarView!, eventsFor date: Date!) -> [Any]! {
        return []
    }
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    @IBOutlet var crewImageView: UIImageView!
    @IBOutlet var crewNameLabel: UILabel!
    var tableViewReportList: UITableView!
    var projectsListArray = NSMutableArray()
    var weeklyReportDictionary = NSMutableDictionary()
    var crewId = ""
    var userInfoDict = JSONDictionary()

    var totalHourLabelForToolbar: UILabel!
    var totalWeekHoursString = ""
    var calendar: CKCalendarView!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        print("Crew Detail Data : \(userInfoDict)")
        self.viewCustomization()
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func updateNotificationCountValue(_ count: Any) {
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewCustomization() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //  Header  ToolBar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let btnImg = UIImage(named: "backBtn.png")!
        let tempBtn = UIButton(type: .custom)
        tempBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(btnImg.size.width), height: CGFloat(btnImg.size.height))
        tempBtn.setImage(btnImg, for: .normal)
        tempBtn.addTarget(self, action: #selector(self.backButtonClicked), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: tempBtn)
        let leftFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titlelabel = UILabel()
        titlelabel.text = "Report"
        titlelabel.font = CenturyGothicBold18
        titlelabel.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(20))
        titlelabel.textAlignment = .left
        let titleLbl = UIBarButtonItem(customView: titlelabel)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        totalHourLabelForToolbar = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(75), height: CGFloat(44)))
        totalHourLabelForToolbar.textAlignment = .right
        totalHourLabelForToolbar.numberOfLines = 2
            //    totalHourLabelForToolbar.text = @"00 hrs\nThis Week";
        let hourLbl = UIBarButtonItem(customView: totalHourLabelForToolbar)
        var barItems = [UIBarButtonItem]()
        barItems.append(backButton)
        barItems.append(leftFlexSpace)
        barItems.append(titleLbl)
        barItems.append(rightFlexSpace)
        barItems.append(hourLbl)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        crewNameLabel.text = "\(userInfoDict["first_name"] as! String) \(userInfoDict["last_name"] as! String)"
        _ = URL(string: "\(ImageBaseUrl)\(userInfoDict["user_avatar"] as! String)")!
//        crewImageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
        crewImageView.layer.borderWidth = 0.5
        crewImageView.layer.borderColor = UIColor.lightGray.cgColor
        tableViewReportList = UITableView()
        if screenSize.height == 568 {
            tableViewReportList.frame = CGRect(x: CGFloat(0), y: CGFloat(200), width: CGFloat(320), height: CGFloat(screenSize.height - 264))
        }
        else {
            tableViewReportList.frame = CGRect(x: CGFloat(0), y: CGFloat(200), width: CGFloat(320), height: CGFloat(screenSize.height - 264))
        }
        tableViewReportList.dataSource = self
        tableViewReportList.delegate = self
        tableViewReportList.separatorColor = UIColor.darkGray
        tableViewReportList.backgroundColor = UIColor(red: CGFloat(245 / 255.0), green: CGFloat(245 / 255.0), blue: CGFloat(245 / 255.0), alpha: CGFloat(1.0))
        self.view.addSubview(tableViewReportList)
        calendar = CKCalendarView()
        // 2. Optionally, set up the datasource and delegates
        calendar.delegate = self
        calendar.dataSource = self
        // 3. Present the calendar
        self.view.addSubview(calendar)
        let tempView = UIView(frame: CGRect(x: CGFloat(80), y: CGFloat(100), width: CGFloat(160), height: CGFloat(30)))
        tempView.backgroundColor = UIColor.clear
        self.view.addSubview(tempView)
            //    Calling Web Services
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
            // you can use your format.
            //Week Start Date
        let gregorian = Calendar(identifier: .gregorian)
        var components = gregorian.dateComponents([.weekday,.month,.year,.day], from: today)
        let dayofweek = components.day!
        // this will give you current day of week
        components.day = (components.day! - (dayofweek - 1))
            // for beginning of the week.
        let beginningOfWeek = gregorian.date(from: components)!
        let dateFormat_first = DateFormatter()
        dateFormat_first.dateFormat = "yyyy-MM-dd"
        let dateString2Prev = dateFormat.string(from: beginningOfWeek)
        print("Current Week Start Date in my format: \(dateString2Prev)")
        //    NSDate *weekstartDate = [dateFormat_first dateFromString:dateString2Prev];
        //    NSLog(@"Current Week Start Date : %@",weekstartDate);
        self.fetchCrewProjectsWeekly(dateString2Prev, crewId: (userInfoDict["_user_id"] as! String))
    }

    func updateView(_ crewProjectsArray: [Any]) {
    }


    func backButtonClicked() {
        calendar.delegate = nil
        calendar.dataSource = nil
        calendar.isPopClicked = true
        popTo()
    }

    func totalHoursThisWeekString(_ totalHrs: String) -> NSMutableAttributedString {
        var firstLabel = "\(totalHrs) Hrs"
        var secondLabel = "This Week"
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        //    cell.userName.attributedText = customTextLabelAttribute;
        return customTextLabelAttribute
    }
// MARK: - TableViewDelegate & DataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectsListArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "WeeklyReport"
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? CrewWeeklyProjectCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("CrewWeeklyProjectCell", owner: self, options: nil)
            cell = nib![0] as? CrewWeeklyProjectCell
        }
//            cell.projectNameLabel.text = @"George Project";
        cell?.projectNameLabel.text = ((projectsListArray[indexPath.row] as! NSDictionary).value(forKey: "project_name") as! String)
        //    cell.projectLocationLabel.text = @"Toronto, Canada";
        cell?.projectLocationLabel.text = "\((projectsListArray[indexPath.row] as! NSDictionary).value(forKey: "project_city") as! String), \((projectsListArray[indexPath.row] as! NSDictionary).value(forKey: "project_state") as! String)"
            //    cell.projectHrsLabel.text = @"24 Hrs";
        let hrs = ((projectsListArray[indexPath.row] as! NSDictionary).value(forKey: "total_hrs") as! String)
        if (hrs == "") {
            cell?.projectHrsLabel.text = "00 hrs"
        }
        else {
            cell?.projectHrsLabel.text = "\((projectsListArray[indexPath.row] as! NSDictionary).value(forKey: "total_hrs") as! String) hrs"
        }
        cell?.projectNameLabel.textColor = UIColor.darkGray
        cell?.projectNameLabel.font = CenturyGothicBold15
        cell?.projectLocationLabel.font = CenturyGothiCRegular11
        cell?.projectHrsLabel.font = CenturyGothiCRegular13
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selected Project Report :\(projectsListArray[indexPath.row])")
        if let dict = projectsListArray.object(at: indexPath.row) as? NSDictionary {
            let detailView = STORYBOARD.instantiateViewController(withIdentifier: "CrewWeeklyReportDetailController") as! CrewWeeklyReportDetailController
            detailView.projectDetailDict = dict
            detailView.crewDetailDict = userInfoDict
            self.navigationController!.pushViewController(detailView, animated: true)
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    /*
    #pragma mark - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
//        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
            var invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
            var invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }

        //    else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
        //    {
        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
        //        [self.navigationController pushViewController:newView animated:YES];
        //    }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func calendarView(_ calendarView: CKCalendarView, didSelect date: Date) {
            //    NSLog(@"Current Date : %@",[NSDate date]);
            //    NSLog(@"Selected Date :::::>>>>>>>> %@",date);
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        let str = dateFormat.string(from: today)
        print("Selected Week Start Date : \(str)")
        let STR1 = dateFormat.string(from: date)
        if (str == STR1) {
            let gregorian = Calendar(identifier: .gregorian)
            var components = gregorian.dateComponents([.weekday, .day, .month, .year], from: today)
            let dayofweek = components.weekday
            // this will give you current day of week
            components.day = (components.day! - (dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            //        NSDate *weekStartDate = [dateFormat_first dateFromString:dateString2Prev];
            self.fetchCrewProjectsWeekly(dateString2Prev, crewId: (userInfoDict["_user_id"] as! String))
        }
        else {
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString = dateFormat_first.string(from: date)
            self.fetchCrewProjectsWeekly(dateString, crewId: (userInfoDict["_user_id"] as! String))
        }
    }
// MARK: - ASIHTTP Requests & Delegates

    func fetchCrewProjectsWeekly(_ weekFirstDate: String, crewId crewID: String) {
        APPDELEGATE.addChargementLoader()
        var urlString = "\(BaseWebRequestUrl)\(GetMyCrewReportsURL)"
        urlString = urlString.replacingOccurrences(of: " ", with: "")
        _ = URL(string: urlString)!

        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.callWebServiceWithParameter(url: urlString, dict: ["crew_id":crewID,"start_date":weekFirstDate], method: .post) { (json:JSONDictionary?, error:NSError?) in
            
            APPDELEGATE.removeChargementLoader()

            if error == nil {

                if let result = json?[kResult] as? JSONDictionary {
                    
                    if "\(result[kstatus]!)" == kOK {

                        if let loppCount = json?["reports"] as? [JSONDictionary] {
                            var totalHrs = 0
                            self.projectsListArray = NSMutableArray.init()
                            for loop in loppCount {
                                self.projectsListArray.add(loop)
                                totalHrs = totalHrs + Int((loop["total_hrs"] as! String))!
                            }
                            self.totalHourLabelForToolbar.attributedText = self.totalHoursThisWeekString("\(totalHrs)")
                            self.tableViewReportList.dataSource = self
                            self.tableViewReportList.delegate = self
                            self.tableViewReportList.reloadData()
                        }

                    } else {

                        }

                    } else {

                }

//                print("Return Crew Dict : \(returnDict)")
//
//                if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                    projectsListArray = NSMutableArray()
//                    var loopCountValue = (returnDict["reports"] as! String).count
//                    for i in 0..<loopCountValue {
//                        projectsListArray.append((returnDict["reports"] as! String)[i])
//                        totalHrs = totalHrs + ((returnDict["reports"] as! String)[i]["total_hrs"] as! String).integerValue
//                    }
//                    print("Crew Projects Report : \(projectsListArray)")
//                }
//                totalHourLabelForToolbar.attributedText = self.totalHoursThisWeekString("\(totalHrs)")


            }

        }



//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.delegate = self
//        request.setPostValue((userInfoDict["_user_id"] as! String), forKey: "crew_id")
//        print(" Week Date >>>> = \(weekFirstDate)")
//        //    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        //    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//        //    NSString *weekDate = [dateFormatter stringFromDate:weekFirstDate];
//        //    NSLog(@"Selected Week Date >>>> = %@",weekDate);
//        request.setPostValue(weekFirstDate, forKey: "start_date")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = [AnyHashable: Any]()
//            returnDict = request.responseString().jsonValue()
//            print("Return Crew Dict : \(returnDict)")
//            var totalHrs = 0
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                projectsListArray = [Any]()
//                var loopCountValue = (returnDict["reports"] as! String).count
//                for i in 0..<loopCountValue {
//                    projectsListArray.append((returnDict["reports"] as! String)[i])
//                    totalHrs = totalHrs + ((returnDict["reports"] as! String)[i]["total_hrs"] as! String).integerValue
//                }
//                print("Crew Projects Report : \(projectsListArray)")
//            }
//            totalHourLabelForToolbar.attributedText = self.totalHoursThisWeekString("\(totalHrs)")
//            tableViewReportList.dataSource! = self
//            tableViewReportList.delegate! = self
//            tableViewReportList.reloadData()
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Fail Project Data Dict = \(returnDict)")
//        var alert: UIAlertView?
//        var error = request.error
//        if error.code == 1 {
//            alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.show()
//            alert = nil
//        }
//
//    }

}
//
//  CrewWeeklyReportViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 04/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
