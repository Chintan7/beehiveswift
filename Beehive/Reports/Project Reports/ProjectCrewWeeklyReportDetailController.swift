//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  ProjectCrewWeeklyReportDetailController.h
//  Beehive
//
//  Created by Deepak Dixit on 09/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class ProjectCrewWeeklyReportDetailController: BaseViewController, UITableViewDelegate, UITableViewDataSource, CKCalendarViewDelegate, CKCalendarViewDataSource {

    public func calendarView(_ calendarView: CKCalendarView!, eventsFor date: Date!) -> [Any]! {
        return []
    }


    @IBOutlet var crewProfileImage: UIImageView!
    @IBOutlet var crewNameLabel: UILabel!
    var tableViewReportList: UITableView!
    var projectsListArray = NSMutableArray()
    var weeklyReportDictionary = NSMutableDictionary()
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
        //   Overlay Calendar
    var currentDayConstant = 0
    var currentMonthConstant = 0
    var currentYearConstant = 0
    var currentDay = 0
    var currentMonth = 0
    var currentYear = 0
    var currentDayWeekDayNumber = 0
    var FirstDayWeekDayNumber = 0
    var LastDayWeekDayNumber = 0
    var strCurrentConstantDate = ""
    var strCurrentDate = ""
    var tempButton: UIButton!
    var btnsetGoals: UIButton!
    var arr_calViewButtons = NSMutableArray()
    var arr_calViewButtons2 = NSMutableArray()
    var myCurrentMonth = 0
    var arrMonth = [String]()
    var arrDates = NSMutableArray()
    var myIndex = 0
    var ismontButtonsClicked = false
    var strMonthDate: String?
    var calView: UIView!
    var myCalDetailsArray = NSMutableArray()
    var tempArray = NSMutableArray()
    var strLastDate = ""
    var currentDateValue = 0
    var lastDateValue = 0
    var controllerObj: AnyObject!
    var check = ""
    var currentDate: Date!
    var EventArray = NSMutableArray()
    var strSelectedDate = ""
    var senderDateSelect: UIButton!
    var grid = 0
    var selectedProjectId = ""
    var selectedProjectName = ""
    var userImageView = ""
    var userDict = JSONDictionary()
    var totalHourLabelForToolbar: UILabel!
    var headerHoursCount = 0
    @IBOutlet var overlayBackGroundView: UIView!
    var overlayCalendarView: UIView!
    var timePickerView: UIView!
    var timePicker: UIDatePicker!
    var weekDayID = 0
    var overlayTapRecognizer: UITapGestureRecognizer!
    var calendar: CKCalendarView!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        // you can use your format.
        //Week Start Date
        let gregorian = Calendar(identifier: .gregorian)

        var components = gregorian.dateComponents([.day, .month, .year, .hour], from: today)

        let dayofweek = components.day!

        // this will give you current day of week
        // for beginning of the week.
        components.day = (components.day! - (dayofweek - 1))
        // for beginning of the week.
        let beginningOfWeek = gregorian.date(from: components)!
        let dateFormat_first = DateFormatter()
        dateFormat_first.dateFormat = "yyyy-MM-dd"
        let dateString2Prev = dateFormat.string(from: beginningOfWeek)

        //    NSDate *weekstartPrev = [dateFormat_first dateFromString:dateString2Prev];
        self.fetchWeeklyDetailedData(dateString2Prev)
        self.viewCustomization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        print("oooooooo..\(strSelectedDate)")
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        //    [overlayTapRecognizer ]
    }

    func viewCustomization() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        _ = URL(string: "\(ImageBaseUrl)\(userDict["user_avatar"] as! String)")!
//        crewProfileImage.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
        crewProfileImage.layer.borderWidth = 0.5
        crewProfileImage.layer.borderColor = UIColor.lightGray.cgColor
        crewNameLabel.text! = "\(userDict["first_name"] as! String) \(userDict["last_name"] as! String)"
        crewNameLabel.font = CenturyGothicBold15
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //  Header  ToolBar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let btnImg = UIImage(named: "backBtn.png")!
        let tempBtn = UIButton(type: .custom)
        tempBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(btnImg.size.width), height: CGFloat(btnImg.size.height))
        tempBtn.setImage(btnImg, for: .normal)
        tempBtn.addTarget(self, action: #selector(self.backButtonClicked), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: tempBtn)
        let leftFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titlelabel = UILabel()
        titlelabel.text! = "Report"
        titlelabel.font = CenturyGothicBold18
        titlelabel.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(20))
        titlelabel.textAlignment = .left
        let titleLbl = UIBarButtonItem(customView: titlelabel)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        totalHourLabelForToolbar = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(75), height: CGFloat(44)))
        totalHourLabelForToolbar.textAlignment = .right
        totalHourLabelForToolbar.numberOfLines = 2
        let hourLbl = UIBarButtonItem(customView: totalHourLabelForToolbar)
        var barItems = [UIBarButtonItem]()
        barItems.append(backButton)
        barItems.append(leftFlexSpace)
        barItems.append(titleLbl)
        barItems.append(rightFlexSpace)
        barItems.append(hourLbl)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        let projectNameLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(190), width: CGFloat(screenSize.width), height: CGFloat(25)))
        projectNameLabel.text! = selectedProjectName
        projectNameLabel.textAlignment = .center
        projectNameLabel.textColor = UIColor.darkGray
        projectNameLabel.numberOfLines = 2
        projectNameLabel.font = CenturyGothiCRegular15
        self.view.addSubview(projectNameLabel)
        tableViewReportList = UITableView()
        if screenSize.height == 568 {
            tableViewReportList.frame = CGRect(x: CGFloat(0), y: CGFloat(225), width: CGFloat(320), height: CGFloat(screenSize.height - 289))
        }
        else {
            tableViewReportList.frame = CGRect(x: CGFloat(0), y: CGFloat(225), width: CGFloat(320), height: CGFloat(screenSize.height - 289))
        }
        tableViewReportList.dataSource! = self
        tableViewReportList.delegate! = self
        tableViewReportList.separatorColor = UIColor.darkGray
        tableViewReportList.backgroundColor = UIColor(red: CGFloat(245 / 255.0), green: CGFloat(245 / 255.0), blue: CGFloat(245 / 255.0), alpha: CGFloat(1.0))
        self.view.addSubview(tableViewReportList)
        // 1. Present Weekly calendar
        calendar = CKCalendarView()
        // 2. Optionally, set up the datasource and delegates
        calendar.delegate = self
        calendar.dataSource = self
        self.view.addSubview(calendar)
        self.view.addSubview(overlayBackGroundView)
        overlayCalendarView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(90), width: CGFloat(APPDELEGATE.window!.bounds.size.width), height: CGFloat(224)))
//        overlayCalendarView.backgroundColor = CALENDARBACKGROUNDCOLOR
        overlayCalendarView.layer.borderWidth = 1.0
        overlayCalendarView.layer.borderColor = DARKGRAYCOLOR.cgColor
        self.view.addSubview(overlayCalendarView)
        overlayCalendarView.isHidden = true
        overlayBackGroundView.isHidden = true
        timePickerView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(125), width: CGFloat(320), height: CGFloat(25)))
//        timePickerView.backgroundColor = CALENDARBACKGROUNDCOLOR
        self.view.addSubview(timePickerView)
        overlayTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideOverLayCalendar))
        self.view.addGestureRecognizer(overlayTapRecognizer)
        let tempView = UIView(frame: CGRect(x: CGFloat(80), y: CGFloat(100), width: CGFloat(160), height: CGFloat(30)))
        tempView.backgroundColor = UIColor.clear
        self.view.addSubview(tempView)
        let calendarTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showOverLayCalendar))
        tempView.addGestureRecognizer(calendarTapRecognizer)
        arrDates = NSMutableArray()
        let Month = DateFormatter()
        arrMonth = Month.monthSymbols

        self.designCalender()
        let cancelButton = UIButton(frame: CGRect(x: CGFloat(5), y: CGFloat(3), width: CGFloat(60), height: CGFloat(20)))
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(DARKGRAYCOLOR, for: .normal)
        cancelButton.backgroundColor = UIColor.clear
        cancelButton.titleLabel!.font = CenturyGothiCRegular14
        cancelButton.addTarget(self, action: #selector(self.cancelTimeSelection), for: .touchUpInside)
        timePickerView.addSubview(cancelButton)
        let selectTimeLabel = UILabel(frame: CGRect(x: CGFloat(70), y: CGFloat(3), width: CGFloat(175), height: CGFloat(20)))
        selectTimeLabel.text = "Select Time"
        selectTimeLabel.backgroundColor = UIColor.clear
        selectTimeLabel.textColor = DARKGRAYCOLOR
        selectTimeLabel.font = CenturyGothicBold15
        selectTimeLabel.textAlignment = .center
        timePickerView.addSubview(selectTimeLabel)
        let timeDoneButton = UIButton(frame: CGRect(x: CGFloat(250), y: CGFloat(3), width: CGFloat(60), height: CGFloat(20)))
        timeDoneButton.setTitle("Done", for: .normal)
        timeDoneButton.setTitleColor(DARKGRAYCOLOR, for: .normal)
        timeDoneButton.backgroundColor = UIColor.clear
        timeDoneButton.titleLabel!.font = CenturyGothiCRegular14
        timeDoneButton.addTarget(self, action: #selector(self.doneTimeSelection), for: .touchUpInside)
        timePickerView.addSubview(timeDoneButton)
        timePickerView.isHidden = true
        timePicker = UIDatePicker(frame: CGRect(x: CGFloat(0), y: CGFloat(150), width: CGFloat(0), height: CGFloat(0)))
        timePicker.datePickerMode = .countDownTimer
        timePicker.backgroundColor = UIColor.white
        timePicker.layer.borderColor = DARKGRAYCOLOR.cgColor
        timePicker.layer.borderWidth = 1.0
        self.view.addSubview(timePicker)
        timePicker.isHidden = true
    }

    func showTimePicker(_ sender: UIButton) {
        weekDayID = sender.tag
        timePickerView.isHidden = false
        timePicker.isHidden = false
        //    [overlayCalendarView setHidden:NO];
        overlayBackGroundView.isHidden = false
    }

    func overLayCoalendarDisplay() {
        overlayCalendarView.isHidden = false
        overlayBackGroundView.isHidden = false
    }

    func hideOverLayCalendar(_ sender: UITapGestureRecognizer) {
        let view = sender.view
        timePickerView.isHidden = true
        //    [timePicker setHidden:YES];
        overlayBackGroundView.isHidden = true
        overlayCalendarView.isHidden = true
        print("\(view?.tag)")
        //By tag, you can find out where you had typed.
    }

    func showOverLayCalendar(_ sender: UITapGestureRecognizer) {
        print("Tap Weekly Calendar Header")
        self.view.bringSubview(toFront: overlayCalendarView)
        overlayCalendarView.isHidden = false
        overlayBackGroundView.isHidden = false
    }

    func cancelTimeSelection() {
        timePickerView.isHidden = true
        timePicker.isHidden = true
        overlayBackGroundView.isHidden = true
    }

    func doneTimeSelection() {
        let hours = floor(timePicker.countDownDuration / (60 * 60))
        let minutes = floor((timePicker.countDownDuration / 60) - hours * 60)
        let seconds = floor(timePicker.countDownDuration - (minutes * 60) - (hours * 60 * 60))
        let str = String(format: "%.02zd:%.02zd:%.02zd", hours, minutes, seconds)
        print(String(format: "time left    = %.02zd:%.02zd:%.02zd", hours, minutes, seconds))
        self.callAdjustTimeWebService(str, weekDayId: weekDayID)
        timePickerView.isHidden = true
        timePicker.isHidden = true
        overlayBackGroundView.isHidden = true
    }

    func backButtonClicked() {
        //    [calendar nil];
        calendar.delegate = nil
        calendar.dataSource = nil
        calendar.isPopClicked = true
        popTo()
    }

    func updateNotificationCountValue(_ count: Any) {
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    func totalHoursThisWeekString(_ totalHrs: String) -> NSMutableAttributedString {
        var firstLabel = "\(totalHrs) Hrs"
        var secondLabel = "This Week"
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        //    cell.userName.attributedText = customTextLabelAttribute;
        return customTextLabelAttribute
    }
// MARK: - TableView Delegate & DataSource

    func numberOfSections(in tableView: UITableView) -> Int {

        if weeklyReportDictionary.count > 0 {
            return weeklyReportDictionary.allKeys.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys[section] as! String)) as! NSArray).count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellIdentifier = "MyCell"
        var cell = tableViewReportList.dequeueReusableCell(withIdentifier: cellIdentifier) as? ReportCheckInCheckOutCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("ReportCheckInCheckOutCell", owner: self, options: nil)
            cell = nib?[0] as? ReportCheckInCheckOutCell
        }
//        cell?.checkInTimeLabel.text = ((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys[indexPath.section] as! String) as AnyObject)[indexPath.row].value(forKey: "check_in_time") as! String)
//        cell?.checkInTimeLabel.font = CenturyGothicBold12
//        cell?.checkOutTimeLabel.text = ((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys()[indexPath.section]) as! AnyObject)[indexPath.row].value(forKey: "check_out_time") as! String)
//        cell?.checkOutTimeLabel.font = CenturyGothicBold12
//        cell?.checkInLabel.font = CenturyGothiCRegular14
//        cell?.checkOutLabel.font = CenturyGothiCRegular14
//        cell?.workTimeLabel.text = ((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys()[indexPath.section]) as! AnyObject
//            )[indexPath.row].value(forKey: "total_hours") as! String)
//        cell?.workTimeLabel.font = CenturyGothiCRegular14
        //    headerHoursCount = headerHoursCount+ [[[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] valueForKey:@"total_hours"] integerValue];
        cell?.backgroundColor = UIColor(red: CGFloat(245 / 255.0), green: CGFloat(245 / 255.0), blue: CGFloat(245 / 255.0), alpha: CGFloat(1.0))
        cell?.selectionStyle = .none
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        headerHoursCount = 0
        let customView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 325, height: 20))
        let headerLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(100), height: CGFloat(25)))
        headerLabel.text! = weeklyReportDictionary.allKeys[section] as! String
        headerLabel.textColor = UIColor(red: CGFloat(90 / 255.0), green: CGFloat(90 / 255.0), blue: CGFloat(90 / 255.0), alpha: CGFloat(1.0))
        headerLabel.font = CenturyGothicBold15
        customView.addSubview(headerLabel)
        customView.backgroundColor = UIColor(red: CGFloat(210 / 255.0), green: CGFloat(210 / 255.0), blue: CGFloat(210 / 255.0), alpha: CGFloat(1.0))
            /*
            //    NSLog(@"Data : %@",[[weeklyReportDictionary allKeys] objectAtIndex:section]);
                NSInteger countValue = [[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] count];
                
                NSDate *date;
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                [df setDateFormat:@"hh:mm"];
                for (int i = 0; i < countValue; i++)
                {
                    
                    
                     headerHoursCount = headerHoursCount + [[[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"] integerValue];
                    
                     if (date == nil) {
                     date = [df dateFromString:@"00:00"];
                     }
                     
                     NSLog(@"<<<<< : %@",[[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"]);
                     
                     NSDate *date1 = [df dateFromString:[[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"]];
                     
                     i++;
                    NSDate *date2;
                    if (i < countValue)
                    {
                        date2 = [df dateFromString:[[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"]];
                    }
                    
                     
                     NSTimeInterval interval1 = [date1 timeIntervalSinceDate:date];
                     NSTimeInterval interval2 = [date2 timeIntervalSinceDate:date];
                     
                     NSDate *addedDate = [date dateByAddingTimeInterval:interval1+interval2];
                     date = addedDate;
                    
                     
            //        
            //        
            //
            //        NSString *time1 = [[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"];
            //        i++;
            //        NSString *time2 = [[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"];
            //        
            //        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //        [formatter setDateFormat:@"hh:mm"];
            //        
            //        NSDate *date1= [formatter dateFromString:time1];
            //        NSDate *date2 = [formatter dateFromString:time2];
            //    
            //        NSComparisonResult result = [date1 compare:date2];
            //        if(result == NSOrderedDescending)
            //        {
            //            NSLog(@"date1 is later than date2");
            //        }
            //        else if(result == NSOrderedAscending)
            //        {
            //            NSLog(@"date2 is later than date1"); 
            //        }
            //        else
            //        {
            //            NSLog(@"date1 is equal to date2");
            //        }
                    
                    
            
                    
            //        NSString *str = [[[weeklyReportDictionary valueForKey:[[weeklyReportDictionary allKeys] objectAtIndex:section]] objectAtIndex:i] valueForKey:@"total_hours"];
            //        int addTime = headerHoursCount;
            //        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            //        [formatter setDateFormat:@"HH:mm"];
            //        NSDate *dateInput=[formatter dateFromString:str];
            //        dateInput=[dateInput dateByAddingTimeInterval:addTime*60];
            //        NSString *dateOutPut=[formatter stringFromDate:dateInput];
            //        headerHoursCount = [dateOutPut integerValue];
            //        NSLog(@"OutputDate= %i",headerHoursCount);
                    
                }
                NSString *resultDate = [df stringFromDate:date];
                
                */
        let headerHrsLabel = UILabel(frame: CGRect(x: CGFloat(180), y: CGFloat(0), width: CGFloat(100), height: CGFloat(25)))
//        var str = "\((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys[section] as! String) as AnyObject)[0].value(forKey: "sum_hours_for_the_day") as! String) hrs"
//        headerHrsLabel.text! = "\(str.substring(to: str.index(str.startIndex, offsetBy: 5))) Hrs"
        headerHrsLabel.textColor = UIColor.darkGray
        headerHrsLabel.textAlignment = .right
        headerHrsLabel.font = CenturyGothicBold15
        customView.addSubview(headerHrsLabel)
        let headerGearBtn = UIButton(type: .custom)
        headerGearBtn.frame = CGRect(x: CGFloat(290), y: CGFloat(0), width: CGFloat(25), height: CGFloat(25))
        headerGearBtn.setImage(UIImage(named: "settingIcon.png")!, for: .normal)
        headerGearBtn.addTarget(self, action: #selector(self.showTimePicker), for: .touchUpInside)
//        headerGearBtn.tag = ((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys[section] as! String) as AnyObject)[0].value(forKey: "id") as! String).integerValue
        customView.addSubview(headerGearBtn)
//        if (((weeklyReportDictionary.value(forKey: weeklyReportDictionary.allKeys[section] as! String) as AnyObject)[0].value(forKey: "overridden_hrs") as! String) == "yes") {
//            var overhiddenHrsBtn = UIButton(type: .custom)
//            overhiddenHrsBtn.frame = CGRect(x: CGFloat(190), y: CGFloat(0), width: CGFloat(25), height: CGFloat(25))
//            overhiddenHrsBtn.setImage(UIImage(named: "greyWarning.png")!, for: .normal)
//            customView.addSubview(overhiddenHrsBtn)
//        }
        customView.clipsToBounds = false
        return customView
    }
// MARK: - Calendar

    func designCalender() {
        self.setCalendar()
            //Header Design...
        let lblMonth = UILabel(frame: CGRect(x: CGFloat(72), y: CGFloat(5), width: CGFloat(175), height: CGFloat(28)))
        lblMonth.backgroundColor = UIColor.clear
        lblMonth.tag = 111
        lblMonth.font = CenturyGothicBold15
        lblMonth.textAlignment = .center
        lblMonth.text! = "  \(arrMonth[currentMonth - 1]) \(Int(currentYear))"
        lblMonth.textColor = UIColor.darkGray
        overlayCalendarView.addSubview(lblMonth)
        calView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(74), width: CGFloat(APPDELEGATE.window!.frame.size.width), height: CGFloat(150)))
        calView.backgroundColor = CALENDARBACKGROUNDCOLOR
        let date = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormat.string(from: date)
        print("Date: \(dateString):")
        
        let btnPrevMonth = UIButton(type: .custom)
        btnPrevMonth.frame = CGRect(x: CGFloat(20), y: CGFloat(5), width: CGFloat(28), height: CGFloat(35))
        btnPrevMonth.setImage(UIImage(named: "previousMonthBtn@2x.png")!, for: .normal)
        btnPrevMonth.addTarget(self, action: #selector(self.btnPrevMonth_OnTouchUpInside), for: .touchUpInside)
        //    [self.view addSubview:btnPrevMonth];
        overlayCalendarView.addSubview(btnPrevMonth)
        
        let btnNextMonth = UIButton(type: .custom)
        btnNextMonth.frame = CGRect(x: CGFloat(270), y: CGFloat(5), width: CGFloat(28), height: CGFloat(35))
        btnNextMonth.setImage(UIImage(named: "nextMonthBtn@2x.png")!, for: .normal)
        btnNextMonth.addTarget(self, action: #selector(self.btnNextMonth_OnTouchUpInside), for: .touchUpInside)
        //    [self.view addSubview:btnNextMonth];
        overlayCalendarView.addSubview(btnNextMonth)
        
        let lblDayNames = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(APPDELEGATE.window!.frame.size.width), height: CGFloat(30)))
        lblDayNames.text! = "      S          M           T          W          T           F          S  "
        lblDayNames.font = CenturyGothiCRegular13
        lblDayNames.backgroundColor = UIColor(red: CGFloat(235 / 255.0), green: CGFloat(235 / 255.0), blue: CGFloat(235 / 255.0), alpha: CGFloat(1.0))
        lblDayNames.textColor = UIColor.darkGray
        if UI_USER_INTERFACE_IDIOM() == .pad {
            lblDayNames.frame = CGRect(x: CGFloat(0), y: CGFloat(lblDayNames.frame.origin.y + lblDayNames.frame.size.height), width: CGFloat(768), height: CGFloat(70))
            lblDayNames.text! = "      S            M           T           W            T           F             S"
            lblDayNames.textAlignment = .left
            lblDayNames.font = CenturyGothiCRegular15
            btnPrevMonth.frame = CGRect(x: CGFloat(70), y: CGFloat(50), width: CGFloat(100), height: CGFloat(60))
            btnPrevMonth.setBackgroundImage(UIImage(named: "previousMonthBtn@2x.png")!, for: .normal)
                //        lblMonth.frame = CGRectMake(320, 20, 200, 40);
                //        lblMonth.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0];
                //        lblMonth.backgroundColor = [UIColor clearColor];
            let monthBg = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(326), width: CGFloat(768), height: CGFloat(100)))
            //        monthBg.image = [UIImage imageNamed:@"cal_mnth_bg_iphone@2x.png"];
            self.view.addSubview(monthBg)
            btnNextMonth.frame = CGRect(x: CGFloat(600), y: CGFloat(50), width: CGFloat(100), height: CGFloat(60))
            btnNextMonth.setBackgroundImage(UIImage(named: "nextMonthBtn@2x.png")!, for: .normal)
            calView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(lblDayNames.frame.size.height + lblDayNames.frame.origin.y), width: CGFloat(768), height: CGFloat(250)))
            calView.backgroundColor! = UIColor.gray
        }
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        // Setting the swipe direction.
        swipeLeft.direction = .left
        swipeRight.direction = .right
        // Adding the swipe gesture on image view
        //    [calView addGestureRecognizer:swipeLeft];
        //    [calView addGestureRecognizer:swipeRight];
        overlayCalendarView.addSubview(lblDayNames)
        overlayCalendarView.addSubview(calView)
        // [self.view addSubview:btnNextMonth];
        //    [self.view addSubview:segmentedControl];
        // for adding buttons on the calender view(calvw)
        self.addButtons()
        //InOrder to fill the calender with current month and year
        self.fillCalender(withMonth: currentMonth, year: currentYear)
    }

    func setCalendar() {
        currentDate = Date()

        let gregorian = Calendar(identifier: .gregorian)

        var comps = gregorian.dateComponents([.day, .month, .year, .hour], from: currentDate)

        currentYear = comps.year!
        currentMonth = comps.month!
        currentDay = comps.day!
        //    NSTimeZone *timeZone = [comps timeZone];
        currentDayWeekDayNumber = comps.weekday!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        currentYearConstant = currentYear
        currentMonthConstant = currentMonth
        currentDayConstant = currentDay
        FirstDayWeekDayNumber = ((8 + currentDayWeekDayNumber - (currentDay % 7)) % 7)
        if FirstDayWeekDayNumber == 0 {
            FirstDayWeekDayNumber = 7
        }
        let NoOfDaysInCurrMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        LastDayWeekDayNumber = (FirstDayWeekDayNumber + NoOfDaysInCurrMonth - 1) % 7
        if LastDayWeekDayNumber == 0 {
            LastDayWeekDayNumber = 7
        }
    }

    func getNumberofDays(inMonth monthInt: Int, yearVlue yearInt: Int) -> Int {
        var isLeap = Bool()
        if (yearInt % 400 == 0) || (yearInt % 4 == 0) && (yearInt % 100 != 0) {
            isLeap = true
        }
        switch monthInt {
            case 1:
                return 31
            case 2:
                if isLeap == true {
                    return 29
                }
                return 28
            case 3:
                return 31
            case 4:
                return 30
            case 5:
                return 31
            case 6:
                return 30
            case 7:
                return 31
            case 8:
                return 31
            case 9:
                return 30
            case 10:
                return 31
            case 11:
                return 30
            case 12:
                return 31
        default:
            return 0
        }

    }

    func handleSwipe(_ swipe: UISwipeGestureRecognizer) {
        if swipe.direction == .left {
            print("Left Swipe")
            self.btnNextMonth_OnTouchUpInside()
        }
        if swipe.direction == .right {
            print("Right Swipe")
            self.btnPrevMonth_OnTouchUpInside()
        }
    }

    func checkDateExists(inCurrentArray strDate: String) -> Bool {
//        var index = -1
//        var array = [Any](arrayLiteral: EventArray)
//        index = array.contains(strDate)
//        if index > 0 {
//            return true
//        }
        return false
    }

    func getShiftDetails() {
        EventArray = NSMutableArray()
        if strMonthDate == nil {
            let currentDateTime = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            // Get the date time in NSString
            strMonthDate = dateFormatter.string(from: currentDateTime)
            print("String Month Date :\(strMonthDate)")
        }
        //    NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
        //    [[NSUserDefaults standardUserDefaults] synchronize];
    }

    func btnPrevMonth_OnTouchUpInside() {
        for j in 0..<arr_calViewButtons.count {
            let tempBtn = (arr_calViewButtons[j] as! UIButton)
            tempBtn.backgroundColor = UIColor.clear
            //        [tempBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        check = "1"
//        myCalDetailsArray.removeAll()
        //    [tblView reloadData];
        ismontButtonsClicked = true
        currentMonth -= 1
        if currentMonth == 0 {
            currentMonth = 12
            currentYear -= 1
        }
        LastDayWeekDayNumber = FirstDayWeekDayNumber - 1
        if LastDayWeekDayNumber == 0 {
            LastDayWeekDayNumber = 7
        }
        let NoOfDaysInMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        FirstDayWeekDayNumber = (7 + FirstDayWeekDayNumber - (NoOfDaysInMonth % 7)) % 7
        if FirstDayWeekDayNumber == 0 {
            FirstDayWeekDayNumber = 7
        }
        self.updateMonthName()
        if currentMonth < 10 {
            strMonthDate = "\(currentYear)-0\(currentMonth)"
        }
        else {
            strMonthDate = "\(currentYear)-\(currentMonth)"
        }
        self.fillCalender(withMonth: currentMonth, year: currentYear)
        // [self requestServer_ForCaledarEvents:strMonthDate];
        self.showAnimation("LEFT")
    }

    func btnNextMonth_OnTouchUpInside() {
        for j in 0..<arr_calViewButtons.count {
            let tempBtn = (arr_calViewButtons[j] as! UIButton)
            tempBtn.backgroundColor = UIColor.clear
            //        [tempBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        check = "2"
//        myCalDetailsArray.removeAll()
        //    [tblView reloadData];
        ismontButtonsClicked = true
        currentMonth += 1
        if currentMonth == 13 {
            currentMonth = 1
            currentYear += 1
        }
        FirstDayWeekDayNumber = LastDayWeekDayNumber + 1
        if FirstDayWeekDayNumber == 8 {
            FirstDayWeekDayNumber = 1
        }
        let NoOfDaysInMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        LastDayWeekDayNumber = (FirstDayWeekDayNumber + NoOfDaysInMonth - 1) % 7
        if LastDayWeekDayNumber == 0 {
            LastDayWeekDayNumber = 7
        }
        self.updateMonthName()
        if currentMonth < 10 {
            strMonthDate = "\(currentYear)-0\(currentMonth)"
        }
        else {
            strMonthDate = "\(currentYear)-\(currentMonth)"
        }
        self.fillCalender(withMonth: currentMonth, year: currentYear)
        //[self requestServer_ForCaledarEvents:strMonthDate];
        self.showAnimation("RIGHT")
    }

    func showAnimation(_ needAnimation: String) {
        if (needAnimation == "LEFT") {
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeRemoved
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
//            t//ransition.delegate = self
            calView.layer.add(transition, forKey: nil)
            let temp1 = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(150.0)
            temp1.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(1000))
            //		[UIView setAnimationDidStopSelector:@selector(stopAnimation:finished:context:)];
            UIView.setAnimationDelegate(calView)
            UIView.commitAnimations()
        }
        else if (needAnimation == "RIGHT") {
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeRemoved
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            //transition.delegate = self
            calView.layer.add(transition, forKey: nil)
            let temp2 = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(150.0)
            temp2.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(1000))
            //[UIView setAnimationDidStopSelector:@selector(stopAnimation:finished:context:)];
            UIView.setAnimationDelegate(calView)
            UIView.commitAnimations()
        }

    }

    func dateSelected(_ sender: UIButton) {
        print("sender.tag...\(sender.tag)")
        _ = 0
        senderDateSelect = sender
        _ = sender.tag
        let strDate = "\(currentYear)-%.2zd-%.2zd"
        strSelectedDate = "\(strDate)"
        print("strSelectedDate is \(strSelectedDate)")
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let calObject = Calendar.current
        var comp = calObject.dateComponents([.weekday], from: df.date(from: strDate)!)
            //provide date here
        let day = comp.weekday!

        print("Dayyyy....: \(day)")
        for j in 0..<arr_calViewButtons.count {
            let tempBtn = (arr_calViewButtons[j] as! UIButton)
            tempBtn.backgroundColor = UIColor.clear
            //        [tempBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        let tempBtn = sender
        tempBtn.backgroundColor = UIColor.orange
            //    NSInteger diff = 7-day;
            //
            //    for (int i = 0; i < 31; i++)
            //    {
            //        ((UIButton *)[calView viewWithTag:intTag + i]).backgroundColor = CALENDARBACKGROUNDCOLOR;
            //    }
            //
            //
            //    for (int i = 1; i < day; i++)
            //    {
            //        ((UIButton *)[calView viewWithTag:intTag-day+i]).backgroundColor = [UIColor orangeColor];
            //    }
            //
            //    for (int i = 0; i <= diff; i++)
            //    {
            //        ((UIButton *)[calView viewWithTag:intTag + i]).backgroundColor = [UIColor orangeColor];
            //    }
        var isDateExists = false
        isDateExists = self.checkDateExists(inCurrentArray: strDate)
        if isDateExists == true {
            //        for (NSDictionary *dict in EventArray)
            //        {
            //            if ([[dict objectForKey:@"eventDate"] isEqualToString:strSelectedDate])
            //            {
            //                NSLog(@"Date is existed");
            //                break;
            //            }
            //
            //            indexVAlue ++;
            //        }
            print(" strDate clicked:\("\(strDate)")")
//            EventArray[indexVAlue]
            //myCalDetailsArray = [[NSMutableArray alloc]initWithArray:[[EventArray objectAtIndex:indexVAlue] valueForKey:@"eventData"]];
            //  NSLog(@"shiftsArray %@",[EventArray objectAtIndex:indexVAlue]);
            //        [tblView reloadData];
        }
        else {
//            myCalDetailsArray = nil
            //        [tblView reloadData];
        }
        //    Reallocate the Calendar
        overlayBackGroundView.isHidden = true
        overlayCalendarView.isHidden = true
        //    [calendar removeFromSuperview];
        print("Calender SuperView..\(self.view.subviews)")
        for obj: Any in self.view.subviews {
            if (obj is CKCalendarView) {
                (obj as AnyObject).removeFromSuperview()
            }
        }
        calendar = CKCalendarView(mode: CKCalendarViewModeWeek)
        calendar.updateSelectedDate(strSelectedDate)
        calendar.delegate = self
        calendar.dataSource = self
        self.view.addSubview(calendar)
        self.view.addSubview(overlayBackGroundView)
        let tempView = UIView(frame: CGRect(x: CGFloat(80), y: CGFloat(100), width: CGFloat(160), height: CGFloat(30)))
        tempView.backgroundColor = UIColor.clear
        self.view.addSubview(tempView)
        let calendarTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.showOverLayCalendar))
        tempView.addGestureRecognizer(calendarTapRecognizer)
        self.selectedDate(fromOverlayCalendar: strSelectedDate)
    }

    func updateMonthName() {
        for aView: UIView in overlayCalendarView.subviews {
            if (aView is UILabel) {
                if aView.tag == 111 {
                    let lblMonth = (aView as! UILabel)
                    lblMonth.text! = "  \(arrMonth[currentMonth - 1]) \(currentYear)"
                }
            }
        }
    }

    func addButtons() {
        arr_calViewButtons = NSMutableArray.init()
        var orgX: CGFloat = 0
        var orgY: CGFloat = 0
        for i in 0..<35 {
            let btnOnCal = UIButton(type: .custom)
            btnOnCal.isUserInteractionEnabled = false
            btnOnCal.setTitleColor(UIColor.black, for: .normal)
            btnOnCal.addTarget(self, action: #selector(self.dateSelected), for: .touchUpInside)
            btnOnCal.titleLabel!.font = CenturyGothicBold12
            btnOnCal.layer.borderColor = UIColor.red.cgColor
            //      button size
            if UI_USER_INTERFACE_IDIOM() == .pad {
                btnOnCal.contentEdgeInsets = UIEdgeInsetsMake(-50, -10, -35, 30)
                btnOnCal.frame = CGRect(x: orgX, y: orgY, width: CGFloat(115), height: CGFloat(55))
                orgX += 110
                if (i + 1) % 7 == 0 {
                    orgY += 56
                    orgX = 0
                }
            }
            else {
                btnOnCal.contentEdgeInsets = UIEdgeInsetsMake(-20, -10, -15, 10)
                btnOnCal.frame = CGRect(x: orgX, y: orgY, width: CGFloat(46), height: CGFloat(30))
                orgX += 45.7
                if (i + 1) % 7 == 0 {
                    orgY += 30
                    orgX = 0
                }
            }
            calView.addSubview(btnOnCal)
            arr_calViewButtons.add(btnOnCal)
        }
    }

    func fillCalender(withMonth month: Int, year: Int) {
        
        if tempButton != nil {
            tempButton.removeFromSuperview()
        }
        
        
        self.clearLabels()
        currentMonth = month
        currentYear = year
        var tempbtn: UIButton?
        grid = FirstDayWeekDayNumber - 1
        print("grid.....\(grid)")
        var intMonth: Int
        var intYear: Int
        intYear = year
        intMonth = month - 1
        if intMonth == 0 {
            intMonth = 12
            intYear -= 1
        }
        var NoOfDaysInMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        myIndex = 0
        for i in 1...NoOfDaysInMonth {
            tempbtn = arr_calViewButtons.object(at: grid) as? UIButton
            tempbtn!.titleLabel!.font = CenturyGothicBold12
            if UI_USER_INTERFACE_IDIOM() == .pad {
                tempbtn!.titleLabel!.font = CenturyGothicBold12
            }
            tempbtn!.setTitle("\(i)", for: .normal)
            tempbtn!.setTitleColor(UIColor.darkGray, for: .normal)
            //        [tempbtn setTitleColor:[UIColor colorWithRed:71/255.f green:173/255.f blue:242/255.f alpha:0.8] forState:UIControlStateNormal];
            tempbtn!.contentEdgeInsets = UIEdgeInsetsMake(-25, 0, -25, 0)
            if currentDayConstant == i && currentMonthConstant == currentMonth && currentYearConstant == currentYear {
                tempbtn!.frame = CGRect(x: CGFloat(tempbtn!.frame.origin.x), y: CGFloat(tempbtn!.frame.origin.y), width: CGFloat(46), height: CGFloat(30))
                tempbtn!.setTitleColor(UIColor.black, for: .normal)
                tempbtn!.addSubview(tempButton)
                tempButton.setTitle("\(i)", for: .normal)
                tempbtn!.contentEdgeInsets = UIEdgeInsetsMake(15, 5, 10, 5)
            }
            else {
                //            tempbtn.contentEdgeInsets=UIEdgeInsetsMake(-20, -10, -15, 10);
                tempbtn!.backgroundColor = UIColor.clear
            }
            tempbtn!.tag = i
            tempbtn!.isUserInteractionEnabled = true
            grid += 1
            if grid == 35 {
                grid = 0
            }
                //  NSString *strDate = [NSString stringWithFormat:@"%.2i-%.2i-%i",currentMonth,tempbtn.tag,currentYear];
            var strDate = "\(currentYear)-%.2zd-%.2zd"
            //        NSLog(@"strDate is %@ %d",strDate,myIndex);
            strLastDate = strDate
            var isDateExists = false
            isDateExists = self.checkDateExists(inCurrentArray: strDate)
            if isDateExists == true {
                tempbtn!.titleLabel!.textColor = UIColor.red
                var lblCount: UILabel?
                if UI_USER_INTERFACE_IDIOM() == .pad {
                    lblCount = UILabel(frame: CGRect(x: CGFloat(35), y: CGFloat(0), width: CGFloat(100), height: CGFloat(30)))
                    lblCount!.font = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(13.0))!
                }
                else {
                    lblCount = UILabel(frame: CGRect(x: CGFloat(3), y: CGFloat(2), width: CGFloat(20), height: CGFloat(20)))
                    lblCount!.font = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(10.0))!
                    lblCount!.numberOfLines = 2
                }
                //lblCount.textColor=[UIColor colorWithRed:249/255.0 green:205/255.0 blue:9/255.0 alpha:1.0];
                //lblCount.textColor=[UIColor lightGrayColor];
                lblCount!.tag = 100 + i
                lblCount!.adjustsFontSizeToFitWidth = true
                lblCount!.backgroundColor = UIColor(patternImage: UIImage(named: "cross-icon-red.png")!)
                // lblCount.textAlignment = NSTextAlignmentCenter;
                //   lblCount.layer.cornerRadius=10;
                lblCount!.alpha = 0.8
                // lblCount.text = @"X";
                if currentDayConstant == i && currentMonthConstant == currentMonth && currentYearConstant == currentYear {
                    //lblCount.textColor=[UIColor whiteColor];
                }
                ///----------------------------------
                //            myCalDetailsArray=[[EventArray objectAtIndex:myIndex] valueForKey:@"eventData"];
                ///----------------------------------------------------------------------------------------
                if (lblCount!.text! == "0") {
                    lblCount!.text! = ""
                }
                myIndex += 1
                tempbtn!.addSubview(lblCount!)
                var rect = CGRect(x: CGFloat(1), y: CGFloat(23), width: CGFloat(5), height: CGFloat(5))
                let viewColor = UIView(frame: rect)
                viewColor.backgroundColor! = UIColor.red
                viewColor.tag = 200 + i
                tempbtn!.addSubview(viewColor)
                // rect.size.width= tempbtn.frame.size.width/lblCount.text.integerValue-1;
                rect.origin.y = 0
                let value = Int.init(lblCount!.text!)
                for _ in 0...value! {
                    let lbl = UILabel(frame: rect)
                    lbl.layer.cornerRadius = 2.5
                    //  lbl.backgroundColor=[[Global sharedInstance] colorWithHexString:[NSString stringWithFormat:@"#%@",[[myCalDetailsArray objectAtIndex:i] valueForKey:@"catColor"]]];
                    lbl.backgroundColor = UIColor.orange
                    rect.origin.x = lbl.frame.origin.x + lbl.frame.size.width + 1
                    viewColor.addSubview(lbl)
//                    lbl = nil
                }
//                viewColor = nil
//                lblCount = nil
            }
        }
    }

    func clearLabels() {
//        _: UIButton? = nil
//        for i in 0..<arr_calViewButtons.count {
//            tempBtn1 = arr_calViewButtons[i]
//            var lblDate = (tempBtn1!.viewWithTag(tempBtn1!.tag + 100)! as! UILabel)
//            if lblDate {
//                lblDate.removeFromSuperview()
//            }
//            var viewcolor = (tempBtn1!.viewWithTag(tempBtn1!.tag + 200)! as! UIView)
//            if viewcolor {
//                viewcolor.removeFromSuperview()
//            }
//            tempBtn1!.setTitle("", for: .normal)
//            tempBtn1!.titleLabel!.font = UIFont.boldSystemFont(ofSize: CGFloat(20))
//            tempBtn1!.setTitleColor(UIColor.red, for: .normal)
//            //		[tempBtn1 setBackgroundImage:[UIImage imageNamed:@"calender-bg.png"] forState:UIControlStateNormal];
//            tempBtn1!.isUserInteractionEnabled = false
//        }
    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
//        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

//    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
//        if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//            
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            APPDELEGATE.notificationReadWebService((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            APPDELEGATE.navigate(toProfileView: self)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        //    else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
//    }

    func calendarView(_ calendarView: CKCalendarView, didSelect date: Date) {
            //    NSLog(@"Current Date : %@",[NSDate date]);
            //    NSLog(@"Selected Date :::::>>>>>>>> %@",date);
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        let str = dateFormat.string(from: today)
        let STR1 = dateFormat.string(from: date)
        if (str == STR1) {
            let gregorian = Calendar(identifier: .gregorian)
            var components = gregorian.dateComponents([.weekday,.year,.calendar,.day], from: today)
            let dayofweek = components.weekday
            // this will give you current day of week
            components.day = (components.day! - (dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            //        NSDate *weekstartPrev = [dateFormat_first dateFromString:dateString2Prev];
            self.fetchWeeklyDetailedData(dateString2Prev)
        }
        else {
                //        NSDateFormatter *dateFormat_first = [[NSDateFormatter alloc] init];
                //        [dateFormat_first setDateFormat:@"yyyy-MM-dd"];
                //        NSString *dateString = [dateFormat_first stringFromDate:date];
                //        ********
            let gregorian = Calendar(identifier: .gregorian)
            var components = gregorian.dateComponents([.day, .month, .year, .day], from: today)
            let dayofweek = components.weekday
            // this will give you current day of week
            components.day = (components.day! - (dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            self.fetchWeeklyDetailedData(dateString2Prev)
        }
    }

    func selectedDate(fromOverlayCalendar dateStr: String) {
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        let str = dateFormat.string(from: today)
        let selectDate = dateFormat.date(from: dateStr)!
        let STR1 = dateFormat.string(from: selectDate)
        if (str == STR1) {
            let gregorian = Calendar(identifier: .gregorian)
            var components = gregorian.dateComponents([.weekday,.year,.calendar,.day], from: selectDate)
            let dayofweek = components.weekday
            // this will give you current day of week
            components.day = (components.day! - (dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            //        NSDate *weekStartDate = [dateFormat_first dateFromString:dateString2Prev];
            self.fetchWeeklyDetailedData(dateString2Prev)
        }
        else {
            let dateFormat_ = DateFormatter()
            dateFormat_.dateFormat = "yyyy-MM-dd"
                //        NSString *dateString = [dateFormat_first stringFromDate:date];
            let selectDate = dateFormat_.date(from: dateStr)!
                //        NSString *STR2 = [dateFormat_ stringFromDate:selectDate];
            let gregorian = Calendar(identifier: .gregorian)
            //var components = gregorian.dateComponents(NSWeekdayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit, from: selectDate)
            var components = gregorian.dateComponents([.weekday,.year,.calendar,.day], from: selectDate)
            let dayofweek = components.weekday
            // this will give you current day of week
            components.day = (components.day! - (dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            self.fetchWeeklyDetailedData(dateString2Prev)
        }
    }
// MARK: - ASIHTTP Request & delegates

    func fetchWeeklyDetailedData(_ WeeklyDate: String) {
        APPDELEGATE.addChargementLoader()
        var urlString = "\(BaseWebRequestUrl)\(GetCrewProjectWeeklyDeatilReportURL)"
        urlString = urlString.replacingOccurrences(of: " ", with: "")
        _ = URL(string: urlString)!
//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.delegate = self
//        //    NSLog(@"Crew Id  = %@",[userDict  objectForKey:@"id"]);
//        //    NSLog(@"Project Id  = %@",selectedProjectId);
//        request.setPostValue(selectedProjectId, forKey: "project_id")
//        request.setPostValue((userDict["id"] as! String), forKey: "crew_id")
//        print("Web Service calling Week Date >>>> = \(WeeklyDate)")
//        request.setPostValue(WeeklyDate, forKey: "date")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData?.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

    func callAdjustTimeWebService(_ time: String, weekDayId dayID: Int) {
        APPDELEGATE.addChargementLoader()
        var urlString = "\(BaseWebRequestUrl)\(AdjustTimeURL)"
        urlString = urlString.replacingOccurrences(of: " ", with: "")
        _ = URL(string: urlString)!
//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.delegate = self
//        request.setPostValue(time, forKey: "time")
//        request.setPostValue("\(dayID)", forKey: "id")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 2
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            //        NSLog(@"Return Project Weelky Report Data = %@",returnDict);
//            weeklyReportDictionary = nil
//            weeklyReportDictionary = [AnyHashable: Any]()
//            if !((returnDict["data"] as! String) is (NSNull)) {
//                weeklyReportDictionary = (returnDict["data"] as! String)
//            }
//            //        NSLog(@"Weekly Report Dictionary Data: %@",weeklyReportDictionary);
//            totalHourLabelForToolbar.attributedText = self.totalHoursThisWeekString((returnDict["total_hrs_this_week"] as! String))
//            headerHoursCount = 0
//            tableViewReportList.reloadData()
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Adjust Data Response = \(returnDict)")
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Fail Project Data Dict = \(returnDict)")
//        var .error = request.error
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }



}
//
//  ProjectCrewWeeklyReportDetailController.m
//  Beehive
//
//  Created by Deepak Dixit on 09/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
