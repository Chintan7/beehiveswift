//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  ProjectReportsViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 21/08/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
//#import "NSString+SBJSON.h"
//#import "ProjectCrewWeeklyReportViewController.h"
class ProjectReportsViewController: BaseViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource {


    
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    var topToolbar: UIToolbar!
    var segmentedViewTitleLabel: UILabel!
    var segmentedControllerProjectReports: UISegmentedControl!
    var lineMarkerLabel: UILabel!
    var calendarSegmentView: UIView!
    var weekSegmentView: UIView!
    var crewSegmentView: UIView!
    var WeeklyReportArray = NSMutableArray()
    var crewReportArray = NSMutableArray()
    //    Calendar
    var currentDayConstant = 0
    var currentMonthConstant = 0
    var currentYearConstant = 0
    var currentDay = 0
    var currentMonth = 0
    var currentYear = 0
    var currentDayWeekDayNumber = 0
    var FirstDayWeekDayNumber = 0
    var LastDayWeekDayNumber = 0
    var strCurrentConstantDate = ""
    var strCurrentDate = ""
    var tempButton: UIButton!
    var btnsetGoals: UIButton!
    var arr_calViewButtons = NSMutableArray()
    var arr_calViewButtons2 = NSMutableArray()
    var myCurrentMonth = 0
    var arrMonth = [String]()
    var arrDates = NSMutableArray()
    var myIndex = 0
    var ismontButtonsClicked = false
    var strMonthDate: String?
    var calView: UIView!
    var myCalDetailsArray = NSMutableArray()
    var tempArray = NSMutableArray()
    var strLastDate = ""
    var currentDateValue: Int!
    var lastDateValue = 0
    var controllerObj: AnyObject!
    var check = ""
    var currentDate: Date!
    var EventArray = NSMutableArray()
    var strSelectedDate = ""
    var senderDateSelect: UIButton!

    var projectId = ""
    var projectName = ""
    var weeklyReportTableView: UITableView!
    var crewReportTableView: UITableView!

    var grid = 0
    var totalProjectHoursStr = ""
    var totalHourLabelForToolbar: UILabel!
    var totalProjectHoursThisMonthStr = ""
    var totalProjectHoursThisWeek = ""
    var totalWeekHourLabel: UILabel!
    var totalMonthHourLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("\(projectId)")
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //  Header  ToolBar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let btnImg = UIImage(named: "backBtn.png")!
        let tempBtn = UIButton(type: .custom)
        tempBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(btnImg.size.width), height: CGFloat(btnImg.size.height))
        tempBtn.setImage(btnImg, for: .normal)
        tempBtn.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let backButton = UIBarButtonItem(customView: tempBtn)
        let leftFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titlelabel = UILabel()
        titlelabel.text = "Report"
        titlelabel.font = CenturyGothicBold18
        titlelabel.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(20))
        titlelabel.textAlignment = .left
        let titleLbl = UIBarButtonItem(customView: titlelabel)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        totalHourLabelForToolbar = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(75), height: CGFloat(44)))
        totalHourLabelForToolbar.textAlignment = .right
        totalHourLabelForToolbar.numberOfLines = 2
        let hourLbl = UIBarButtonItem(customView: totalHourLabelForToolbar)
        var barItems = [UIBarButtonItem]()
        barItems.append(backButton)
        barItems.append(leftFlexSpace)
        barItems.append(titleLbl)
        barItems.append(rightFlexSpace)
        barItems.append(hourLbl)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        self.selectedProjectWeeklyReport()
        //    [self viewModification];
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myIndex = 0
        myCurrentMonth = currentMonth
        print("Current month is \(currentMonth)")
        print("WeeklyReportArray month is \(WeeklyReportArray.count)")
        print("crewReportArray month is \(crewReportArray.count)")
        //    [segmentedControllerProjectReports setSelectedSegmentIndex:0];
        //    [weeklyReportTableView reloadData];
        //    [crewReportTableView reloadData];
        //    [self selectedProjectWeeklyReport];
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewModification() {
            //    Custom Report Segment Controller
        let items = ["Week", "Month", "Crew"]
        segmentedControllerProjectReports = UISegmentedControl(items: items)
        lineMarkerLabel = UILabel()
        lineMarkerLabel.backgroundColor = UIColor.gray
        let upperMarkerLine = UILabel(frame: CGRect(x: CGFloat(5), y: CGFloat(75), width: CGFloat(310), height: CGFloat(1)))
        upperMarkerLine.backgroundColor = UIColor.gray
        upperMarkerLine.shadowColor = UIColor.lightGray
        upperMarkerLine.layer.masksToBounds = true
        if screenSize.height == 480 {
            segmentedControllerProjectReports.frame = CGRect(x: CGFloat(30), y: CGFloat(365), width: CGFloat(260), height: CGFloat(35))
            lineMarkerLabel.frame = CGRect(x: CGFloat(5), y: CGFloat(355), width: CGFloat(310), height: CGFloat(1))
        }
        else {
            lineMarkerLabel.frame = CGRect(x: CGFloat(5), y: CGFloat(449), width: CGFloat(310), height: CGFloat(1))
            segmentedControllerProjectReports.frame = CGRect(x: CGFloat(30), y: CGFloat(460), width: CGFloat(260), height: CGFloat(35))
        }
        segmentedControllerProjectReports.selectedSegmentIndex = 0
        segmentedControllerProjectReports.addTarget(self, action: #selector(self.segmentedControlSelection), for: .valueChanged)
        segmentedControllerProjectReports.layer.borderWidth = 1.0
        segmentedControllerProjectReports.layer.borderColor = UIColor.darkGray.cgColor
        segmentedControllerProjectReports.layer.cornerRadius = 5.0
        segmentedControllerProjectReports.tintColor = UIColor(red: CGFloat(239 / 255.0), green: CGFloat(155 / 255.0), blue: CGFloat(0 / 255.0), alpha: CGFloat(1.0))
        segmentedControllerProjectReports.layer.masksToBounds = true
        self.view.addSubview(segmentedControllerProjectReports)
        self.view.addSubview(upperMarkerLine)
        self.view.addSubview(lineMarkerLabel)
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: CenturyGothicBold12], for: .normal)
        segmentedViewTitleLabel = UILabel(frame: CGRect(x: CGFloat(15), y: CGFloat(47), width: CGFloat(200), height: CGFloat(28)))
        segmentedViewTitleLabel.text = "Weekly Report"
        segmentedViewTitleLabel.font = CenturyGothicBold15
        segmentedViewTitleLabel.textColor = UIColor.black
        self.view.addSubview(segmentedViewTitleLabel)
        weekSegmentView = UIView()
        crewSegmentView = UIView()
        weeklyReportTableView = UITableView()
        crewReportTableView = UITableView()
        if screenSize.height == 480 {
            weekSegmentView.frame = CGRect(x: CGFloat(0), y: CGFloat(78), width: CGFloat(320), height: CGFloat(280))
            crewSegmentView.frame = CGRect(x: CGFloat(0), y: CGFloat(78), width: CGFloat(320), height: CGFloat(280))
            weeklyReportTableView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(280))
            crewReportTableView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(280))
        }
        else {
            weekSegmentView.frame = CGRect(x: CGFloat(0), y: CGFloat(78), width: CGFloat(320), height: CGFloat(365))
            crewSegmentView.frame = CGRect(x: CGFloat(0), y: CGFloat(78), width: CGFloat(320), height: CGFloat(365))
            weeklyReportTableView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(365))
            crewReportTableView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(365))
        }
        weeklyReportTableView.delegate = self
        weeklyReportTableView.dataSource = self
        weekSegmentView.addSubview(weeklyReportTableView)
        self.view.addSubview(weekSegmentView)
        //    [weekSegmentView setHidden:YES];
        crewReportTableView.delegate = self
        crewReportTableView.dataSource = self
        crewSegmentView.addSubview(crewReportTableView)
        self.view.addSubview(crewSegmentView)
        crewSegmentView.isHidden = true
        //    Calendar View
        calendarSegmentView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(75), width: CGFloat(320), height: CGFloat(280)))
        self.view.addSubview(calendarSegmentView)
        calendarSegmentView.isHidden = true
        totalWeekHourLabel = UILabel(frame: CGRect(x: CGFloat(85), y: CGFloat(220), width: CGFloat(150), height: CGFloat(44)))
        totalWeekHourLabel.textAlignment = .center
        totalWeekHourLabel.numberOfLines = 1
        var weekHours = "0 hrs"
        var week = "This week"
        let weekLabelAttribute = NSMutableAttributedString(string: "\(weekHours) \(week)")
        weekLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold15, range: NSRange(location: 0, length: weekHours.characters.count))
        weekLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: weekHours.characters.count))
        weekLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: weekHours.characters.count + 1, length: week.characters.count))
        weekLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: weekHours.characters.count + 1, length: week.characters.count))
        totalWeekHourLabel.attributedText = weekLabelAttribute
        calendarSegmentView.addSubview(totalWeekHourLabel)
        totalMonthHourLabel = UILabel(frame: CGRect(x: CGFloat(85), y: CGFloat(240), width: CGFloat(150), height: CGFloat(44)))
        totalMonthHourLabel.textAlignment = .center
        totalMonthHourLabel.numberOfLines = 1
        var monthHours = "1745 hrs"
        var month = "This month"
        let monthLabelAttribute = NSMutableAttributedString(string: "\(monthHours) \(month)")
        monthLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold15, range: NSRange(location: 0, length: monthHours.characters.count))
        monthLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: monthHours.characters.count))
        monthLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: monthHours.characters.count + 1, length: month.characters.count))
        monthLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: monthHours.characters.count + 1, length: month.characters.count))
        totalMonthHourLabel.attributedText = monthLabelAttribute
            //    [calendarSegmentView addSubview:totalMonthHourLabel];
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        //  Setting the swipe direction.
        swipeLeft.direction = .left
        swipeRight.direction = .right
        calendarSegmentView.addGestureRecognizer(swipeRight)
        calendarSegmentView.addGestureRecognizer(swipeLeft)
        //    self.title=@"Calendar";
        arrDates = NSMutableArray()
        let Month = DateFormatter()
        arrMonth = Month.monthSymbols
        self.designCalender()
        //Call webservice
        myCalDetailsArray = NSMutableArray()
        if strMonthDate == nil {
            let currentDateTime = Date()
            let currentDateFormatter = DateFormatter()
            currentDateFormatter.dateFormat = "MM-dd-yyyy"
            strMonthDate = currentDateFormatter.string(from: currentDateTime)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd"
            let date = Date()
            let lstDate = dateFormatter.string(from: date)
            let curDate = Date()
            let currentCalendar = Calendar.current


            let daysRange = currentCalendar.range(of: .day, in: .month, for: curDate)

            lastDateValue = (daysRange?.count)!
            currentDateValue = Int(lstDate)

            print("Final Detals are startdate \(currentDateValue) and last date \(lastDateValue)")
            print("strMonthDate is \(strMonthDate) strLastDate \(strLastDate)")
            if currentMonth < 10 {
                strMonthDate = "\(currentYear)-0\(currentMonth)"
            }
            else {
                strMonthDate = "\(currentYear)-\(currentMonth)"
            }
            self.fillCalender(withMonth: currentMonth, year: currentYear)
            //[self requestServer_ForCaledarEvents:strMonthDate];
        }
        EventArray = NSMutableArray()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCalendar), name: NSNotification.Name(rawValue: "updateCalendar"), object: nil)
    }



    func backBtnClick() {
        popTo()
    }

    func updateCalendar() {
    }

    func totalHoursThisWeekString(_ totalHrs: String) -> NSMutableAttributedString {
        var firstLabel = "\(totalHrs) Hrs"
        var secondLabel = "This Week"
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        //    cell.userName.attributedText = customTextLabelAttribute;
        return customTextLabelAttribute
    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
//        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    func updateNotificationCountValue(_ count: Any) {
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

//    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray?[indexPath.row])")
//        if (((APPDELEGATE.allNotificationArray?[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            APPDELEGATE.notificationReadWebService((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            APPDELEGATE.navigate(toProfileView: self)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        //    else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
// MARK: - Button Events

    func segmentedControlSelection(_ sender: Any) {
        if segmentedControllerProjectReports.selectedSegmentIndex == 0 {
            print("Week Selected")
            calendarSegmentView.isHidden = true
            weekSegmentView.isHidden = false
            crewSegmentView.isHidden = true
            segmentedViewTitleLabel.text = "Weekly Report"
            weeklyReportTableView.reloadData()
        }
        else if segmentedControllerProjectReports.selectedSegmentIndex == 1 {
            print("Month Selected")
            calendarSegmentView.isHidden = false
            segmentedViewTitleLabel.text = "Select A Week"
            weekSegmentView.isHidden = true
            crewSegmentView.isHidden = true
            let today = Date()
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd"

            let gregorian = Calendar.init(identifier: .gregorian)
            let unitFlags = Set<Calendar.Component>([.day, .month, .day, .year])
            let calendar = Calendar.current
            var components = calendar.dateComponents(unitFlags, from: today)
            let dayofweek = components.day
            // this will give you current day of week
            components.day = (components.day! - (dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            self.getMonthChangeWebService(dateString2Prev)
            //        [self getMonthChangeWebService];
            //        [self calendarView];
        }
        else {
            print("Crew Selected")
            calendarSegmentView.isHidden = true
            weekSegmentView.isHidden = true
            crewSegmentView.isHidden = false
            self.getProjectCrewReports()
            //        [crewReportTableView reloadData];
            segmentedViewTitleLabel.text = "Select A Crew"
        }

    }

    func calendarView() {
    }
// MARK: - TableView DataSource & Delegates

    func numberOfSections(in tableView: UITableView) -> Int {
        if segmentedControllerProjectReports.selectedSegmentIndex == 0 {
            return 1
        }
        else if segmentedControllerProjectReports.selectedSegmentIndex == 2 {
            return 1
        }
        else {
            return 0
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControllerProjectReports.selectedSegmentIndex == 0 {
            return WeeklyReportArray.count
        }
        else if segmentedControllerProjectReports.selectedSegmentIndex == 2 {
            return crewReportArray.count
        }
        else {
            return 0
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ProjectsReportCell?
        if segmentedControllerProjectReports.selectedSegmentIndex == 0 {
            let CellIdentifier = "WeeklyReport"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? ProjectsReportCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("ProjectsReportCell", owner: self, options: nil)
                cell = nib?[0] as? ProjectsReportCell
            }


            let dict = WeeklyReportArray.object(at: indexPath.row) as! JSONDictionary

            cell!.weekTitleLabel.text = "\(dict["from_date"]!) -\n\(dict["to_date"]!)"
            cell!.weekTitleLabel.font = CenturyGothicBold15
            cell!.weekTitleLabel.textColor = UIColor.darkGray
            cell!.weekHoursLabel.text = "\(dict["total_hrs"]!)"
            cell!.weekHoursLabel.textColor = UIColor.darkGray
            cell!.weekHoursLabel.font = CenturyGothiCRegular14
            cell!.selectionStyle = .none
        }
        else if segmentedControllerProjectReports.selectedSegmentIndex == 2 {
            let CellIdentifier = "CrewReport"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? ProjectsReportCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("ProjectsReportCell", owner: self, options: nil)
                cell = nib?[1] as? ProjectsReportCell
            }

            let userData = crewReportArray.object(at: indexPath.row) as! JSONDictionary


            if let user = userData["User"] as? JSONDictionary {
                cell!.crewNameLabel.text = "\(user["first_name"] as! String) \(user["last_name"] as! String)"
                cell!.crewNameLabel.font = CenturyGothicBold15
                var url = URL(string: "\(ImageBaseUrl)\(user["user_avatar"] as! String)")!
                cell!.crewImageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
                cell!.crewImageView.layer.borderWidth = 0.5
                cell!.crewImageView.layer.borderColor = UIColor.lightGray.cgColor
//                var firstLbl = "\(user["total_hrs"] as! String)"
//                var secondLbl = "This week"
//                var customWeekTextLabelAttribute = NSMutableAttributedString(string: "\(firstLbl) \(secondLbl)")
//                customWeekTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: 0, length: firstLbl.characters.count))
//                customWeekTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLbl.characters.count))
//                customWeekTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular12, range: NSRange(location: firstLbl.characters.count + 1, length: secondLbl.characters.count))
//                customWeekTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: firstLbl.characters.count + 1, length: secondLbl.characters.count))
//                cell!.crewWeekHoursLabel.attributedText = customWeekTextLabelAttribute
            }


            if let user = userData["0"] as? JSONDictionary {

                var totalHrsStr = "\(user["total_hrs"] as! String)"
                var firstLabel: String
                if (totalHrsStr == "") {
                    firstLabel = "0"
                } else {
                    firstLabel = totalHrsStr
                }
                var secondLabel = "Total Project"
                var customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel) \(secondLabel)")
                customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: 0, length: firstLabel.characters.count))
                customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
                customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular12, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
                customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
                cell!.crewProjectsHoursLabel.attributedText = customTextLabelAttribute


            }
        }

        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segmentedControllerProjectReports.selectedSegmentIndex == 0 {
            print("Week Cell Selected")
        }
        else if segmentedControllerProjectReports.selectedSegmentIndex == 2 {
            print("Crew Cell Selected")
//
//            let crewReportDetail = STORYBOARD.instantiateViewController(withIdentifier: "ProjectCrewWeeklyReportDetailController") as! ProjectCrewWeeklyReportDetailController
//            crewReportDetail.selectedProjectId = projectId
//            crewReportDetail.selectedProjectName = projectName
////            crewReportDetail.userDict = (crewReportArray[indexPath.row]["User"] as! String)
//            self.navigationController!.pushViewController(crewReportDetail, animated: true)
            //        [crewReportTableView deselectRowAtIndexPath:indexPath animated:YES];
            //        CKDemoViewController *calendarView = [[CKDemoViewController alloc] init];
            //        [self.navigationController presentViewController:calendarView animated:NO completion:^{}];
        }

    }
// MARK: - Calendar

    func designCalender() {
        self.setCalendar()
            //Header Design...
        let lblMonth = UILabel(frame: CGRect(x: CGFloat(72), y: CGFloat(5), width: CGFloat(175), height: CGFloat(28)))
        lblMonth.backgroundColor = UIColor.clear
        lblMonth.tag = 111
        lblMonth.font = CenturyGothicBold15
        lblMonth.textAlignment = .center
        lblMonth.text = "  \(arrMonth[currentMonth - 1]) \(Int(currentYear))"
        lblMonth.textColor = UIColor.darkGray
        calendarSegmentView.addSubview(lblMonth)
        calView = UIView(frame: CGRect(x: CGFloat(3), y: CGFloat(74), width: CGFloat(APPDELEGATE.window!.frame.size.width), height: CGFloat(150)))
        calView.backgroundColor = CALENDARBACKGROUNDCOLOR
        let date = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd/MM/yyyy"
        let dateString = dateFormat.string(from: date)
        print("Date: \(dateString):")
        let btnPrevMonth = UIButton(type: .custom)
        btnPrevMonth.frame = CGRect(x: CGFloat(20), y: CGFloat(5), width: CGFloat(28), height: CGFloat(35))
        btnPrevMonth.setImage(UIImage(named: "previousMonthBtn@2x.png")!, for: .normal)
        btnPrevMonth.addTarget(self, action: #selector(self.btnPrevMonth_OnTouchUpInside), for: .touchUpInside)
        //    [self.view addSubview:btnPrevMonth];
        calendarSegmentView.addSubview(btnPrevMonth)
        let btnNextMonth = UIButton(type: .custom)
        btnNextMonth.frame = CGRect(x: CGFloat(270), y: CGFloat(5), width: CGFloat(28), height: CGFloat(35))
        btnNextMonth.setImage(UIImage(named: "nextMonthBtn@2x.png")!, for: .normal)
        btnNextMonth.addTarget(self, action: #selector(self.btnNextMonth_OnTouchUpInside), for: .touchUpInside)
        //    [self.view addSubview:btnNextMonth];
        calendarSegmentView.addSubview(btnNextMonth)
        let lblDayNames = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(APPDELEGATE.window!.frame.size.width), height: CGFloat(30)))
        lblDayNames.text = "      S          M           T          W          T           F          S  "
        lblDayNames.font = CenturyGothiCRegular13
        lblDayNames.backgroundColor = CALENDARBACKGROUNDCOLOR
        lblDayNames.textColor = UIColor.darkGray
        if UI_USER_INTERFACE_IDIOM() == .pad {
            lblDayNames.frame = CGRect(x: CGFloat(0), y: CGFloat(lblDayNames.frame.origin.y + lblDayNames.frame.size.height), width: CGFloat(768), height: CGFloat(70))
            lblDayNames.text = "      S            M           T           W            T           F             S"
            lblDayNames.textAlignment = .left
            lblDayNames.font = CenturyGothiCRegular15
            btnPrevMonth.frame = CGRect(x: CGFloat(70), y: CGFloat(50), width: CGFloat(100), height: CGFloat(60))
            btnPrevMonth.setBackgroundImage(UIImage(named: "previousMonthBtn@2x.png")!, for: .normal)
                //        lblMonth.frame = CGRectMake(320, 20, 200, 40);
                //        lblMonth.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22.0];
                //        lblMonth.backgroundColor = [UIColor clearColor];
            let monthBg = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(326), width: CGFloat(768), height: CGFloat(100)))
            //        monthBg.image = [UIImage imageNamed:@"cal_mnth_bg_iphone@2x.png"];
            self.view.addSubview(monthBg)
            btnNextMonth.frame = CGRect(x: CGFloat(600), y: CGFloat(50), width: CGFloat(100), height: CGFloat(60))
            btnNextMonth.setBackgroundImage(UIImage(named: "nextMonthBtn@2x.png")!, for: .normal)
            calView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(lblDayNames.frame.size.height + lblDayNames.frame.origin.y), width: CGFloat(768), height: CGFloat(250)))
            calView.backgroundColor = UIColor.gray
        }
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe))
        // Setting the swipe direction.
        swipeLeft.direction = .left
        swipeRight.direction = .right
        // Adding the swipe gesture on image view
        calView.addGestureRecognizer(swipeLeft)
        calView.addGestureRecognizer(swipeRight)
        calendarSegmentView.addSubview(lblDayNames)
        calendarSegmentView.addSubview(calView)
        // [self.view addSubview:btnNextMonth];
        //    [self.view addSubview:segmentedControl];
        // for adding buttons on the calender view(calvw)
        self.addButtons()
        //InOrder to fill the calender with current month and year
        self.fillCalender(withMonth: currentMonth, year: currentYear)
    }

    func setCalendar() {
        currentDate = Date()
        let gregorian = Calendar(identifier: .gregorian)
        var comps = gregorian.dateComponents([.weekday, .day, .month, .year], from: currentDate)
        currentYear = comps.year!
        currentMonth = comps.month!
        currentDay = comps.day!
        //    NSTimeZone *timeZone = [comps timeZone];
        currentDayWeekDayNumber = comps.weekday!
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        currentYearConstant = currentYear
        currentMonthConstant = currentMonth
        currentDayConstant = currentDay
        FirstDayWeekDayNumber = ((8 + currentDayWeekDayNumber - (currentDay % 7)) % 7)
        if FirstDayWeekDayNumber == 0 {
            FirstDayWeekDayNumber = 7
        }
        let NoOfDaysInCurrMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        LastDayWeekDayNumber = (FirstDayWeekDayNumber + NoOfDaysInCurrMonth - 1) % 7
        if LastDayWeekDayNumber == 0 {
            LastDayWeekDayNumber = 7
        }
    }

    func getNumberofDays(inMonth monthInt: Int, yearVlue yearInt: Int) -> Int {
        var isLeap = Bool()
        if (yearInt % 400 == 0) || (yearInt % 4 == 0) && (yearInt % 100 != 0) {
            isLeap = true
        }
        switch monthInt {
            case 1:
                return 31
            case 2:
                if isLeap == true {
                    return 29
                }
                return 28
            case 3:
                return 31
            case 4:
                return 30
            case 5:
                return 31
            case 6:
                return 30
            case 7:
                return 31
            case 8:
                return 31
            case 9:
                return 30
            case 10:
                return 31
            case 11:
                return 30
            case 12:
                return 31
            default:
            return 0
        }

    }

    func handleSwipe(_ swipe: UISwipeGestureRecognizer) {
        if swipe.direction == .left {
            print("Left Swipe")
            self.btnNextMonth_OnTouchUpInside()
        }
        if swipe.direction == .right {
            print("Right Swipe")
            self.btnPrevMonth_OnTouchUpInside()
        }
    }

    func checkDateExists(inCurrentArray strDate: String) -> Bool {
//        var index = -1
//        var array = [Any](arrayLiteral: EventArray)
//        index = array.contains(where: strDate)
//        if index > 0 {
//            return true
//        }
        return false
    }

    func getShiftDetails() {
        EventArray = NSMutableArray()
        if strMonthDate == nil {
            let currentDateTime = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            // Get the date time in NSString
            strMonthDate = dateFormatter.string(from: currentDateTime)
            print("String Month Date :\(strMonthDate)")
        }
        //    NSString *strUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"User_Id"];
        //    [[NSUserDefaults standardUserDefaults] synchronize];
    }

    func btnPrevMonth_OnTouchUpInside() {
        for j in 0..<arr_calViewButtons.count {
            let tempBtn = (arr_calViewButtons[j] as! UIButton)
            tempBtn.backgroundColor = UIColor.clear
            //        [tempBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        check = "1"
//        myCalDetailsArray.removeAll()
        //    [tblView reloadData];
        ismontButtonsClicked = true
        currentMonth -= 1
        if currentMonth == 0 {
            currentMonth = 12
            currentYear -= 1
        }
        LastDayWeekDayNumber = FirstDayWeekDayNumber - 1
        if LastDayWeekDayNumber == 0 {
            LastDayWeekDayNumber = 7
        }
        let NoOfDaysInMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        FirstDayWeekDayNumber = (7 + FirstDayWeekDayNumber - (NoOfDaysInMonth % 7)) % 7
        if FirstDayWeekDayNumber == 0 {
            FirstDayWeekDayNumber = 7
        }
        self.updateMonthName()
        if currentMonth < 10 {
            strMonthDate = "\(currentYear)-0\(currentMonth)"
        }
        else {
            strMonthDate = "\(currentYear)-\(currentMonth)"
        }
        self.fillCalender(withMonth: currentMonth, year: currentYear)
        // [self requestServer_ForCaledarEvents:strMonthDate];
        self.showAnimation("LEFT")
            //    [];
        let dateStr = "\(strMonthDate)-01"
        self.getMonthChangeWebService(dateStr)
    }

    func btnNextMonth_OnTouchUpInside() {
        for j in 0..<arr_calViewButtons.count {
            let tempBtn = (arr_calViewButtons[j] as! UIButton)
            tempBtn.backgroundColor = UIColor.clear
            //        [tempBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        check = "2"
//        myCalDetailsArray.removeAll()
        //    [tblView reloadData];
        ismontButtonsClicked = true
        currentMonth += 1
        if currentMonth == 13 {
            currentMonth = 1
            currentYear += 1
        }
        FirstDayWeekDayNumber = LastDayWeekDayNumber + 1
        if FirstDayWeekDayNumber == 8 {
            FirstDayWeekDayNumber = 1
        }
        let NoOfDaysInMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        LastDayWeekDayNumber = (FirstDayWeekDayNumber + NoOfDaysInMonth - 1) % 7
        if LastDayWeekDayNumber == 0 {
            LastDayWeekDayNumber = 7
        }
        self.updateMonthName()
        if currentMonth < 10 {
            strMonthDate = "\(currentYear)-0\(currentMonth)"
        }
        else {
            strMonthDate = "\(currentYear)-\(currentMonth)"
        }
        self.fillCalender(withMonth: currentMonth, year: currentYear)
        //[self requestServer_ForCaledarEvents:strMonthDate];
        self.showAnimation("RIGHT")
        let dateStr = "\(strMonthDate)-01"
        self.getMonthChangeWebService(dateStr)
    }

    func showAnimation(_ needAnimation: String) {
        if (needAnimation == "LEFT") {
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeRemoved
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromLeft
//            transition.delegate = self
            calView.layer.add(transition, forKey: nil)
            let temp1 = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(150.0)
            temp1.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(1000))
            //		[UIView setAnimationDidStopSelector:@selector(stopAnimation:finished:context:)];
            UIView.setAnimationDelegate(calView)
            UIView.commitAnimations()
        }
        else if (needAnimation == "RIGHT") {
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.fillMode = kCAFillModeRemoved
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
//            transition.delegate = self
            calView.layer.add(transition, forKey: nil)
            let temp2 = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(0)))
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(150.0)
            temp2.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(0), height: CGFloat(1000))
            //[UIView setAnimationDidStopSelector:@selector(stopAnimation:finished:context:)];
            UIView.setAnimationDelegate(calView)
            UIView.commitAnimations()
        }

    }

    func dateSelected(_ sender: UIButton) {
//        var indexVAlue = 0
        senderDateSelect = sender
        let intTag = sender.tag
        let strDate = "\(currentYear)-\(currentMonth)-\(intTag)"
        strSelectedDate = "\(strDate)"
        print("strSelectedDate is \(strSelectedDate)")
        
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        let calObject = Calendar.current
        let comp = calObject.dateComponents([.weekday], from: df.date(from: strDate)!)
            //provide date here
        let day = comp.weekday!
        print("Dayyyy....: \(day)")
        for j in 0..<arr_calViewButtons.count {
            let tempBtn = (arr_calViewButtons[j] as! UIButton)
            tempBtn.backgroundColor = UIColor.clear
            //        [tempBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        }
        let tempBtn = sender
        tempBtn.backgroundColor = UIColor.orange
            //    NSInteger diff = 7-day;
            //
            //    for (int i = 0; i < 31; i++)
            //    {
            //        ((UIButton *)[calView viewWithTag:intTag + i]).backgroundColor = CALENDARBACKGROUNDCOLOR;
            //    }
            //
            //    
            //    for (int i = 1; i < day; i++)
            //    {
            //        ((UIButton *)[calView viewWithTag:intTag-day+i]).backgroundColor = [UIColor orangeColor];
            //    }
            //    
            //    for (int i = 0; i <= diff; i++)
            //    {
            //        ((UIButton *)[calView viewWithTag:intTag + i]).backgroundColor = [UIColor orangeColor];
            //    }
        var isDateExists = false
        isDateExists = self.checkDateExists(inCurrentArray: strDate)
        if isDateExists == true {
            //        for (NSDictionary *dict in EventArray)
            //        {
            //            if ([[dict objectForKey:@"eventDate"] isEqualToString:strSelectedDate])
            //            {
            //                NSLog(@"Date is existed");
            //                break;
            //            }
            //
            //            indexVAlue ++;
            //        }
            print(" strDate clicked:\("\(strDate)")")
//            EventArray[indexVAlue]
            //myCalDetailsArray = [[NSMutableArray alloc]initWithArray:[[EventArray objectAtIndex:indexVAlue] valueForKey:@"eventData"]];
            //  NSLog(@"shiftsArray %@",[EventArray objectAtIndex:indexVAlue]);
            //        [tblView reloadData];
        }
        else {
//            myCalDetailsArray = nil
            //        [tblView reloadData];
        }
        self.selectedWeek(strSelectedDate)
    }

    func updateMonthName() {
        for aView: UIView in calendarSegmentView.subviews {
            if (aView is UILabel) {
                if aView.tag == 111 {
                    let lblMonth = (aView as! UILabel)
                    lblMonth.text = "  \(arrMonth[currentMonth - 1]) \(currentYear)"
                }
            }
        }
    }

    func addButtons() {
        arr_calViewButtons = NSMutableArray()
        var orgX: CGFloat = 0
        var orgY: CGFloat = 0
        for i in 0..<35 {
            let btnOnCal = UIButton(type: .custom)
            btnOnCal.isUserInteractionEnabled = false
            btnOnCal.setTitleColor(UIColor.black, for: .normal)
            btnOnCal.addTarget(self, action: #selector(self.dateSelected), for: .touchUpInside)
            btnOnCal.titleLabel!.font = CenturyGothicBold12
            btnOnCal.layer.borderColor = UIColor.red.cgColor
            //      button size
            if UI_USER_INTERFACE_IDIOM() == .pad {
                btnOnCal.contentEdgeInsets = UIEdgeInsetsMake(-50, -10, -35, 30)
                btnOnCal.frame = CGRect(x: orgX, y: orgY, width: CGFloat(115), height: CGFloat(55))
                orgX += 110
                if (i + 1) % 7 == 0 {
                    orgY += 56
                    orgX = 0
                }
            }
            else {
                btnOnCal.contentEdgeInsets = UIEdgeInsetsMake(-20, -10, -15, 10)
                btnOnCal.frame = CGRect(x: orgX, y: orgY, width: CGFloat(46), height: CGFloat(30))
                orgX += 45.7
                if (i + 1) % 7 == 0 {
                    orgY += 30
                    orgX = 0
                }
            }
            calView.addSubview(btnOnCal)
            arr_calViewButtons.add(btnOnCal)
        }
    }

    func fillCalender(withMonth month: Int, year: Int) {

        if tempButton != nil {
            tempButton.removeFromSuperview()
        }


        self.clearLabels()
        currentMonth = month
        currentYear = year
        var tempbtn =
            UIButton()
        grid = FirstDayWeekDayNumber - 1
        print("grid.....\(grid)")
        var intMonth: Int
        var intYear: Int
        intYear = year
        intMonth = month - 1
        if intMonth == 0 {
            intMonth = 12
            intYear -= 1
        }
        let NoOfDaysInMonth = self.getNumberofDays(inMonth: currentMonth, yearVlue: currentYear)
        myIndex = 0
        for i in 1...NoOfDaysInMonth {
            tempbtn = arr_calViewButtons.object(at: grid) as! UIButton
            tempbtn.titleLabel?.font = CenturyGothicBold12
            if UI_USER_INTERFACE_IDIOM() == .pad {
                tempbtn.titleLabel?.font = CenturyGothicBold12
            }
            tempbtn.setTitle("\(i)", for: .normal)
            tempbtn.setTitleColor(UIColor.darkGray, for: .normal)
            //        [tempbtn setTitleColor:[UIColor colorWithRed:71/255.f green:173/255.f blue:242/255.f alpha:0.8] forState:UIControlStateNormal];
            tempbtn.contentEdgeInsets = UIEdgeInsetsMake(-25, 0, -25, 0)
            if currentDayConstant == i && currentMonthConstant == currentMonth && currentYearConstant == currentYear {
                tempbtn.frame = CGRect(x: CGFloat(tempbtn.frame.origin.x), y: CGFloat(tempbtn.frame.origin.y), width: CGFloat(46), height: CGFloat(30))
                tempbtn.setTitleColor(UIColor.black, for: .normal)
//                tempbtn.addSubview(tempButton)
                tempButton.setTitle("\(i)", for: .normal)
                tempbtn.contentEdgeInsets = UIEdgeInsetsMake(15, 5, 10, 5)
            }
            else {
                //            tempbtn.contentEdgeInsets=UIEdgeInsetsMake(-20, -10, -15, 10);
                tempbtn.backgroundColor = UIColor.clear
            }

            
            tempbtn.tag = i
            tempbtn.isUserInteractionEnabled = true
            grid += 1
            if grid == 35 {
                grid = 0
            }
                //  NSString *strDate = [NSString stringWithFormat:@"%.2i-%.2i-%i",currentMonth,tempbtn.tag,currentYear];
            let strDate = "\(currentYear)-%.2zd-%.2zd"
            //        NSLog(@"strDate is %@ %d",strDate,myIndex);
            strLastDate = strDate
            var isDateExists = false
            isDateExists = self.checkDateExists(inCurrentArray: strDate)
            if isDateExists == true {
                tempbtn.titleLabel!.textColor = UIColor.red
                var lblCount = UILabel()
                
                if UI_USER_INTERFACE_IDIOM() == .pad {
                    lblCount = UILabel(frame: CGRect(x: CGFloat(35), y: CGFloat(0), width: CGFloat(100), height: CGFloat(30)))
                    lblCount.font = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(13.0))!
                }
                else {
                    lblCount = UILabel(frame: CGRect(x: CGFloat(3), y: CGFloat(2), width: CGFloat(20), height: CGFloat(20)))
                    lblCount.font = UIFont(name: "HelveticaNeue-Bold", size: CGFloat(10.0))!
                    lblCount.numberOfLines = 2
                }
                //lblCount.textColor=[UIColor colorWithRed:249/255.0 green:205/255.0 blue:9/255.0 alpha:1.0];
                //lblCount.textColor=[UIColor lightGrayColor];
                lblCount.tag = 100 + i
                lblCount.adjustsFontSizeToFitWidth = true
                lblCount.backgroundColor = UIColor(patternImage: UIImage(named: "cross-icon-red.png")!)
                 lblCount.textAlignment = .center;
                lblCount.layer.cornerRadius=10;
                lblCount.alpha = 0.8
                // lblCount.text = @"X";
                if currentDayConstant == i && currentMonthConstant == currentMonth && currentYearConstant == currentYear {
                    //lblCount.textColor=[UIColor whiteColor];
                }
                ///----------------------------------
                //            myCalDetailsArray=[[EventArray objectAtIndex:myIndex] valueForKey:@"eventData"];
                ///----------------------------------------------------------------------------------------
                if (lblCount.text! == "0") {
                    lblCount.text = ""
                }
                myIndex += 1
                tempbtn.addSubview(lblCount)
                var rect = CGRect(x: CGFloat(1), y: CGFloat(23), width: CGFloat(5), height: CGFloat(5))
                let viewColor = UIView(frame: rect)
                viewColor.backgroundColor! = UIColor.red
                viewColor.tag = 200 + i
                tempbtn.addSubview(viewColor)
                // rect.size.width= tempbtn.frame.size.width/lblCount.text.integerValue-1;
                rect.origin.y = 0

                let value = Int.init(lblCount.text!)
                for _ in 0...value!
                {
                    let lbl = UILabel(frame: rect)
                    lbl.layer.cornerRadius = 2.5
                    //  lbl.backgroundColor=[[Global sharedInstance] colorWithHexString:[NSString stringWithFormat:@"#%@",[[myCalDetailsArray objectAtIndex:i] valueForKey:@"catColor"]]];
                    lbl.backgroundColor = UIColor.orange
                    rect.origin.x = lbl.frame.origin.x + lbl.frame.size.width + 1
                    viewColor.addSubview(lbl)
                    //lbl = nil
                }
                //viewColor = nil
                //lblCount = nil
            }
        }
    }

    func clearLabels() {
        let tempBtn1 = UIButton()
        for i in 0..<arr_calViewButtons.count {
            tempButton = arr_calViewButtons[i] as! UIButton
            if let lblDate = tempBtn1.viewWithTag(tempBtn1.tag + 100) as? UILabel
            {
                lblDate.removeFromSuperview()
            }
        
            if let viewcolor = tempBtn1.viewWithTag(tempBtn1.tag + 200) as? UIButton {
                    viewcolor.removeFromSuperview()
            }

            tempBtn1.setTitle("", for: .normal)
            tempBtn1.titleLabel!.font = UIFont.boldSystemFont(ofSize: CGFloat(20))
            tempBtn1.setTitleColor(UIColor.red, for: .normal)
            //		[tempBtn1 setBackgroundImage:[UIImage imageNamed:@"calender-bg.png"] forState:UIControlStateNormal];
            tempBtn1.isUserInteractionEnabled = false
        }
    }

    func selectedWeek(_ dateStr: String) {
        let today = Date()
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        let str = dateFormat.string(from: today)
        let selectDate = dateFormat.date(from: dateStr)!
        let STR1 = dateFormat.string(from: selectDate)
        if (str == STR1) {
            let gregorian = Calendar(identifier: .gregorian)
            var components = gregorian.dateComponents([.day, .weekday, .year, .month], from: today)
            let dayofweek = components.weekday
            // this will give you current day of week
            components.day = (components.day! - (
                dayofweek! - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            //        NSDate *weekStartDate = [dateFormat_first dateFromString:dateString2Prev];
            self.getMonthChangeWebService(dateString2Prev)
        }
        else {
            let dateFormat_ = DateFormatter()
            dateFormat_.dateFormat = "yyyy-MM-dd"
                //        NSString *dateString = [dateFormat_first stringFromDate:date];
            let selectDate = dateFormat_.date(from: dateStr)!
                //        NSString *STR2 = [dateFormat_ stringFromDate:selectDate];
            let gregorian = Calendar(identifier: .gregorian)
            var components = gregorian.dateComponents([.day, .year, .month, .weekday], from: selectDate)
            let dayofweek = components.weekday!
            // this will give you current day of week
            components.day = (components.day! - (dayofweek - 1))
                // for beginning of the week.
            let beginningOfWeek = gregorian.date(from: components)!
            let dateFormat_first = DateFormatter()
            dateFormat_first.dateFormat = "yyyy-MM-dd"
            let dateString2Prev = dateFormat.string(from: beginningOfWeek)
            self.getMonthChangeWebService(dateString2Prev)
        }
    }
// MARK: - ASIHTTP Requests

    func selectedProjectWeeklyReport() {

        let requestString = "/\(projectId).json"
        var strURL = "\(BaseWebRequestUrl)\(GetProjectWeeklyReportURL)\(requestString)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!

        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.callWebService(url: strURL, method: .get) { (response:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()

            if error == nil {

                if let result = response?["result"] as? JSONDictionary {

                    if "\(result["status"]!)" == "OK" {

                        print(response)
                        self.WeeklyReportArray = NSMutableArray()

                        if let weekArr = response?["data"] as? [JSONDictionary] {

                            for day in weekArr {
                                self.WeeklyReportArray.add(day)
                            }

                        }
                        self.totalProjectHoursStr = response?["total_project_hrs"] as! String
                        self.viewModification()

                    } else {

                    }
                }
            }

        }

            //    NSLog(@"URL- %@",url);
//        var request = ASIFormDataRequest.init(url: url)
//        var pref = UserDefaults.standard
//        var emailString = pref.object(forKey: "UserId")!
//        var userTokenString = pref.object(forKey: "User_Token")!
//        var authStr = "\(emailString):\(userTokenString)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

    func getProjectCrewReports() {
        APPDELEGATE.addChargementLoader()
        let requestString = "/\(projectId).json"
        var strURL = "\(BaseWebRequestUrl)\(GetProjectCrewReportURL)\(requestString)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!


        APIManager.sharedInstance.callWebService(url: strURL, method: .get) { (response:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()

            if error == nil {

                if let result = response?["result"] as? JSONDictionary {

                    if "\(result["status"]!)" == "OK" {

                        self.crewReportArray = NSMutableArray()

                        if let weekArr = response?["data"] as? JSONDictionary {

                            print(response)
                            if let loopArr = weekArr["crewReport"] as? [JSONDictionary]{

                                for crew in loopArr {
                                    self.crewReportArray.add(crew)
                                }
                                self.crewReportTableView.reloadData()

                            }

                        }
                    } else {
                        
                    }
                }
            }

        }



            //    NSLog(@"URL- %@",url);
//        var request = ASIFormDataRequest.init(url: url)
//        var pref = UserDefaults.standard
//        var emailString = pref.object(forKey: "UserId")!
//        var userTokenString = pref.object(forKey: "User_Token")!
//        var authStr = "\(emailString):\(userTokenString)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 2
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

    func getMonthChangeWebService(_ selectedWeekDate: String) {
        APPDELEGATE.addChargementLoader()
            //    NSString *requestString = [NSString stringWithFormat:@"/%@.json",projectId];
        var strURL = "\(BaseWebRequestUrl)\(GetProjectMonthWeeklyReport)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!

        let dict = ["date":"\(selectedWeekDate)","project_id":"\(projectId)"]

        APIManager.sharedInstance.callWebServiceWithParameter(url: strURL, dict: dict, method: .post) { (response:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            if error == nil {

                if let data  = response?["data"] as? JSONDictionary {

                    if let hrsThisWek = data["total_hrs_this_week"] as? String {
                        self.totalProjectHoursThisWeek = hrsThisWek
                    }

                    if let projHrs = data["total_project_hrs"] as? String {
                        self.totalProjectHoursThisMonthStr = projHrs
                    }
                }

                var weekHours = self.totalProjectHoursThisWeek
                var week = "This week"
                let weekLabelAttribute = NSMutableAttributedString(string: "\(weekHours) \(week)")
                weekLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold15, range: NSRange(location: 0, length: weekHours.characters.count))
                weekLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: weekHours.characters.count))
                weekLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: weekHours.characters.count + 1, length: week.characters.count))
                weekLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: weekHours.characters.count + 1, length: week.characters.count))
                self.totalWeekHourLabel.attributedText = weekLabelAttribute


            }

        }
        

            //    NSLog(@"URL- %@",url);
//        var request = ASIFormDataRequest.init(url: url)
//        print("Selected Week Date <><><<><><><<><><><>: \(selectedWeekDate)")
//        request.setPostValue(selectedWeekDate, forKey: "date")
//        request.setPostValue(projectId, forKey: "project_id")
//        var pref = UserDefaults.standard
//        var emailString = pref.object(forKey: "UserId")!
//        var userTokenString = pref.object(forKey: "User_Token")!
//        var authStr = "\(emailString):\(userTokenString)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 3
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Project Report Data = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                WeeklyReportArray = [Any]()
//                for i in 0..<(returnDict["data"] as! String).count {
//                    WeeklyReportArray.append((returnDict["data"] as! String)[i])
//                }
//                print("Weekly Report : \(WeeklyReportArray)")
//                //            [self getProjectCrewReports];
//                self.viewModification()
//            }
//            totalProjectHoursStr = (returnDict["total_project_hrs"] as! String)
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Project Data = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                crewReportArray = [Any]()
//                var loopCountValue = ((returnDict["data"] as! String)["crewReport"] as! String).count
//                for i in 0..<loopCountValue {
//                    crewReportArray.append(((returnDict["data"] as! String)["crewReport"] as! String)[i])
//                }
//                print("Crew Report : \(crewReportArray)")
//                crewReportTableView.reloadData()
//                //            [crewReportTableView reloadData];
//            }
//        }
//        else if request.tag == 3 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Month Hours : \(returnDict)")
//            totalProjectHoursThisWeek = ((returnDict["data"] as! String)["total_hrs_this_week"] as! String)
//            totalProjectHoursThisMonthStr = ((returnDict["data"] as! String)["total_project_hrs"] as! String)
//            var weekHours = totalProjectHoursThisWeek
//            var week = "This week"
//            var weekLabelAttribute = NSMutableAttributedString(string: "\(weekHours) \(week)")
//            weekLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold15, range: NSRange(location: 0, length: weekHours.characters.count))
//            weekLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSRange(location: 0, length: weekHours.characters.count))
//            weekLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothicBold12, range: NSRange(location: weekHours.characters.count + 1, length: week.characters.count))
//            weekLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: weekHours.characters.count + 1, length: week.characters.count))
//            totalWeekHourLabel.attributedText = weekLabelAttribute
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Fail Project Data Dict = \(returnDict)")
//        var .error = request.error
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }


}
