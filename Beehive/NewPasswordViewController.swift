//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  NewPasswordViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 23/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
//#import "NSString+SBJSON.h"
class NewPasswordViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var emailTextfieldBG: UIImageView!
    @IBOutlet var newpasswordField: UITextField!
    var responseTokenString = ""
    var responseEmailString = ""

    var alertLabel: UILabel!

    @IBAction func sign(inButtonPressed sender: Any) {
//        self.validateAllData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.navigationController!.navigationItem.hidesBackButton = true
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = backBtn
        var firstLabel = "Congrats !"
        var secondLabel = "We've sent a reset password link to your email address."
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(13.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        headerLabel.attributedText = customTextLabelAttribute
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func backBtn() {
        popTo()
        self.navigationController!.isNavigationBarHidden = true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailTextField {
            alertLabel.removeFromSuperview()
            emailTextfieldBG.image = UIImage(named: "GreyTf.png")!
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextField {
            if textField.text?.validateEmail() == false {
                emailTextfieldBG.image = UIImage(named: "AlertTf.png")!
                alertLabel = UILabel(frame: CGRect(x: CGFloat(emailTextField.frame.origin.x), y: CGFloat(emailTextField.frame.origin.y + 40), width: CGFloat(emailTextField.frame.size.width), height: CGFloat(15)))
                alertLabel.text = "Please enter a valid email format"
                alertLabel.textColor = UIColor.red
                alertLabel.textAlignment = .left
                alertLabel.font = UIFont(name: "Century Gothic", size: CGFloat(12.0))!
                self.view.addSubview(alertLabel)
                //      UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"Please enter valid email id"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                //            [alert show];
            }
        }
    }

    func validateAllData() -> Bool {
        if emailTextField.text?.characters.count == 0 {
            let alert = UIAlertView(title: " Alert", message: "All field are mandatory", delegate: nil, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        else if newpasswordField.text?.characters.count == 0 {
            let alert = UIAlertView(title: " Alert", message: "All field are mandatory", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        else {
            self.getLoginWebService()
        }

        return true
    }
// MARK: - Parser & ASIHTTPRequest delegates

    func getLoginWebService() {
        APPDELEGATE.addChargementLoader()
        let urlString = "\(BaseWebRequestUrl)\(LoginRequestUrl)"
        _ = URL(string: urlString)!
//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        request.setPostValue(emailTextField.text, forKey: "email")
//        request.setPostValue(newpasswordField.text, forKey: "password")
//        if UserDefaults.standard.object(forKey: "deviceTocken")! != nil {
//            request.setPostValue(UserDefaults.standard.object(forKey: "deviceTocken")!, forKey: "device_token")
//        }
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

    func requestTokenWebServices() {
        //http://172.10.1.4:8600/webservices/login?userEmail=consumer@mailinator.com&userPassword=123456&currLat=32.9014&currLong=-117.2079&deviceType=Android&deviceId=
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(GetMyProjects)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!
            //    NSLog(@"Login URL- %@",url);
//        var request = ASIFormDataRequest(URL: url)
//        var authStr = "\(responseEmailString):\(responseTokenString)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 2
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return  Dict = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "INVALID") {
//                var myAlert = UIAlertView(title: "Warning", message: ((returnDict["result"] as! String)["message"] as! String), delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
//                myAlert.show()
//                appDelegate.removeChargementLoader()
//                return
//            }
//            else if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                var pref = UserDefaults.standard
//                pref.set(((returnDict["result"] as! String)["_username"] as! String), forKey: "UserId")
//                pref.set(((returnDict["result"] as! String)["_token"] as! String), forKey: "User_Token")
//                UserDefaults.standard.synchronize()
//                self.navigateToHomeScreen()
//            }
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                var pref = UserDefaults.standard
//                pref.set(((returnDict["result"] as! String)["_username"] as! String), forKey: "UserId")
//                pref.set(((returnDict["result"] as! String)["_token"] as! String), forKey: "User_Token")
//                //            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:hasLogin];
//                UserDefaults.standard.synchronize()
//                //            [appDelegate getNotificationCountValue];
//                self.navigateToHomeScreen()
//            }
//        }
//        else if request.tag == 3 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Google Places Data = \(returnDict)")
//        }
//
//        appDelegate.removeChargementLoader()
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return  Dict = \(returnDict)")
//        var .error = request.error
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }
// MARK: Side Menu

    func navigateToHomeScreen() {
        self.navigationController!.navigationBar.isHidden = true
        APPDELEGATE.getNotificationCountValue()
        UserDefaults.standard.set(true, forKey: hasLogin)
        UserDefaults.standard.synchronize()
        
        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController") as! HomeScreenController
        homeScreen.emailIdString = (UserDefaults.standard.value(forKey: "UserId") as! String)
        homeScreen.tokenString = (UserDefaults.standard.value(forKey: "User_Token") as! String)
        let menuView = MenuEventViewController(nibName: "MenuEventViewController", bundle: nil)
        let navLeft = UINavigationController(rootViewController: homeScreen)
        let container = MFSideMenuContainerViewController.container(withCenter: navLeft, leftMenuViewController: menuView, rightMenuViewController: nil)
        self.navigationController!.pushViewController(container!, animated: true)
    }
}
//
//  NewPasswordViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 23/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
