//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  NotesViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 08/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit

class NotesViewController: UIViewController, ELCImagePickerControllerDelegate, ELCAssetSelectionDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIActionSheetDelegate, UINavigationBarDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate, UITextViewDelegate, UITextFieldDelegate, KRImageViewerDelegate {

    @IBOutlet var notesTextView: UITextView!
    @IBOutlet var imageCollectionView: UICollectionView!
    var viewToolbar: UIToolbar!
    var backButton: UIBarButtonItem!
    var addImageButton: UIBarButtonItem!
    var barEditButton: UIBarButtonItem!
    var cameraBarButton: UIBarButtonItem!
    var imagePickerActionSheet: UIActionSheet!
    @IBOutlet var notesTitleTxtFld: UITextField!
    var existingNoteDictionary = JSONDictionary()
    var isNewNoteFlag = false
    var noteId = ""
    var projectId = ""
//    var = [Any]()
    var chosedImages = [UIImage]()
    var postDict = JSONDictionary()
    var elcPicker = ELCImagePickerController()
    var imagePickerController: UIImagePickerController!
    var krImageViewer: KRImageViewer!

    var editBtn: UIButton!
    var keyboardHideTap: UITapGestureRecognizer!
    var fullImageView: UIView!
    var selectedImageView: UIImageView!
    var elcImagePicker: ELCImagePickerController!
    var collectionViewImages = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        notesTextView.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.

        keyboardHideTap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyBoard))
        //    [self.view addGestureRecognizer:keyboardHideTap];
        self.customizeView()
        if noteId != "" {
            self.getNoteData(noteId)
        }
        else {
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
    }

    func selectedAssets(_ assets: [Any]!) {

    }

    override func viewDidAppear(_ animated: Bool) {
        self.setupKRImageViewer()
        self.krImageViewer.useKeyWindow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func hideKeyBoard() {
        notesTitleTxtFld.resignFirstResponder()
        notesTextView.resignFirstResponder()
    }

    func customizeView() {

        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        backButton = UIBarButtonItem(customView: button)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let cameraBtnImage = UIImage(named: "cameraBtn.png")!
        let cameraBtn = UIButton(type: .custom)
        cameraBtn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        cameraBtn.setBackgroundImage(cameraBtnImage, for: .normal)
        cameraBtn.addTarget(self, action: #selector(self.cameraBtnClick), for: .touchUpInside)
        cameraBarButton = UIBarButtonItem(customView: cameraBtn)
        let addNoteBtnImage = UIImage(named: "smallGreybtn.png")!
        editBtn = UIButton(type: .custom)
        editBtn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        editBtn.setBackgroundImage(addNoteBtnImage, for: .normal)
        var title: String
        if (noteId == "") {
            title = "Save"
        }
        else {
            title = "Edit"
            notesTextView.isUserInteractionEnabled = true
            cameraBarButton.isEnabled = false
            notesTitleTxtFld.isUserInteractionEnabled = false
        }
        editBtn.setTitle(title, for: .normal)
        editBtn.titleLabel!.textColor = UIColor.white
        editBtn.titleLabel!.font = CenturyGothicBold15
        editBtn.addTarget(self, action: #selector(self.editButtonClick), for: .touchUpInside)
        barEditButton = UIBarButtonItem(customView: editBtn)
        var barItems = [UIBarButtonItem]()
        barItems.append(backButton)
        barItems.append(flexSpace)
        barItems.append(barEditButton)
        barItems.append(flexSpace)
        barItems.append(cameraBarButton)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        if (noteId == "") {
            notesTextView.text = "Please Write Your Note Description Here......"
        }
        let frame = UIScreen.main.bounds
        fullImageView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(screenSize.width), height: CGFloat(screenSize.height - 40)))
        fullImageView.backgroundColor = UIColor.black
        selectedImageView = UIImageView(frame: CGRect(x: CGFloat(0), y: CGFloat(fullImageView.frame.origin.y + 40), width: CGFloat(fullImageView.frame.size.width), height: CGFloat(fullImageView.frame.size.height - 40)))
        selectedImageView.backgroundColor = UIColor.black
        fullImageView.addSubview(selectedImageView)
        let doneButton = UIButton(type: .custom)
        doneButton.frame = CGRect(x: CGFloat(frame.size.width - 60), y: CGFloat(5), width: CGFloat(50), height: CGFloat(30))
        doneButton.layer.cornerRadius = 1.0
        doneButton.layer.borderWidth = 1.0
        doneButton.layer.borderColor = UIColor.white.cgColor
        doneButton.setImage(UIImage(named: "doneRectImage.jpeg")!, for: .normal)
        doneButton.addTarget(self, action: #selector(self.hidefullImageView), for: .touchUpInside)
        fullImageView.addSubview(doneButton)
        self.view.addSubview(fullImageView)
        fullImageView.isHidden = true
    }

    func hidefullImageView() {
        fullImageView.isHidden = true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == "Please Write Your Note Description Here......") {
            notesTextView.text = ""
            notesTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Please Write Your Note Description Here......"
            notesTextView.textColor = UIColor.lightGray
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        notesTitleTxtFld.resignFirstResponder()
        return true
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            print("New Line")
        }
        return true
    }

    func backBtnClick() {
        popTo()
    }

    func editButtonClick() {
        if (editBtn.titleLabel!.text == "Save") {
            notesTextView.resignFirstResponder()
            if (notesTitleTxtFld.text == "") {
                let saveAlert = UIAlertView(title: "Beehive", message: "Please give note name..!!", delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
                saveAlert.show()
                return
            }
            self.uploadAlbumImages(chosedImages)
        }
        else {
            editBtn.setTitle("Save", for: .normal)
            barEditButton.customView = editBtn
            cameraBarButton.isEnabled = true
            notesTextView.isUserInteractionEnabled = true
            notesTitleTxtFld.isUserInteractionEnabled = true
        }
    }

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {

        //    [self.navigationController popViewControllerAnimated:YES];
    }
// MARK: Camera Button Actions

    func cameraBtnClick() {
        notesTextView.resignFirstResponder()
        if collectionViewImages.count != 12 {

            self.imagePickerActionSheet = UIActionSheet.init(title: "Limit 12 Photos Per Note", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
            self.imagePickerActionSheet.show(in: self.view)
//            if !self.actionSheet().isVisible() {
//                self.actionSheet().show(in: self.view)
//            }
        }
        else {
            let alert = UIAlertView(title: "Alert", message: "Limit 12 Photos per note.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
            alert.show()
        }
    }

    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        
        let buttonTitle = imagePickerActionSheet.buttonTitle(at: buttonIndex)
        if (buttonTitle == "Gallery") {
            UINavigationBar.appearance().setBackgroundImage(nil, for: .default)

            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self

                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: {() -> Void in
                })
//            elcPicker = ELCImagePickerController.init()
//            elcPicker.maximumImagesCount = 12 - collectionViewImages.count
//            elcPicker.returnsOriginalImage = false
//            //Only return the fullScreenImage, not the fullResolutionImage
//            elcPicker.imagePickerDelegate = self
//            self.present(elcPicker, animated: true, completion: { _ in })
        }
        else if (buttonTitle == "Camera") {
            imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            if !UIImagePickerController.isSourceTypeAvailable(.camera) {
                let alert = UIAlertView(title: "Hello", message: "No Camera Found", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
                alert.show()
            }
            else {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: {() -> Void in
                })
            }
        }
        else if (buttonTitle == "Cancel") {
            imagePickerActionSheet = nil
        }
        else {

        }

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        print("Image Picker Finish..")
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        chosedImages.insert(image, at: 0)
        postDict["image"] = chosedImages as AnyObject?
        let imageShow = info[UIImagePickerControllerOriginalImage] as! UIImage
//        let imageData = UIImageJPEGRepresentation(imageShow, 1)
        collectionViewImages.insert(imageShow, at: 0)
        imageCollectionView.reloadData()
        picker.dismiss(animated: true) { 

        }
    }

    func elcImagePickerController(_ picker: ELCImagePickerController, didFinishPickingMediaWithInfo info: [Any]) {
        //    For sending to Server....
//        for dict: [AnyHashable: Any] in info {
//            var image = (dict[UIImagePickerControllerOriginalImage] as! String)
//            chosedImages.insert(image, at: 0)
//        }
//        PostDict["image"] = chosedImages
        //    For Showing Purpose Images
//        for tempDict: [AnyHashable: Any] in info {
//            var image = (tempDict[UIImagePickerControllerOriginalImage] as! String)
//            var imageData = .uiImageJPEGRepresentation()!
//            collectionViewImages.insert(imageData, at: 0)
//        }
        imageCollectionView.reloadData()
        picker.dismiss(animated: true, completion: { _ in })
        elcImagePicker = nil
    }

    func elcImagePickerControllerDidCancel(_ picker: ELCImagePickerController) {
        self.dismiss(animated: true, completion: { _ in })
        elcImagePicker = nil
    }
// MARK: CollectionView Delegate & Datasource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "MyCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        let imageView = (cell.viewWithTag(200)! as! UIImageView)
        if let image = collectionViewImages[indexPath.row] as? UIImage {
            imageView.image = image
        } else {
            let url = URL(string: "\(ImageBaseUrl)\(collectionViewImages[indexPath.row])")!
            imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
        }

        cell.tag = indexPath.row
        cell.isSelected = true
//        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .none)
        cell.layer.cornerRadius = 5.0
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = DARKGRAYCOLOR.cgColor
        return cell
    }

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("select cell Image :\(collectionViewImages[indexPath.row])")
//        if (collectionViewImages[indexPath.row] is Data) {
//            var imageData = collectionViewImages[indexPath.row]
//            var img = UIImage(data: imageData)!
//            selectedImageView.image = img
//        }
//        else {
//            var url = URL(string: "\(ImageBaseUrl)\(collectionViewImages[indexPath.row])")!
//            selectedImageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
//        }
//        fullImageView.isHidden = false
//    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        print("Deselect Image :\(collectionViewImages[indexPath.row])")
    }

    func setupKRImageViewer() {
        krImageViewer = KRImageViewer.init(dragMode: krImageViewerModes(rawValue: UInt32(0)))
        self.krImageViewer.delegate = self

        self.krImageViewer.maxConcurrentOperationCount = 1
        self.krImageViewer.dragDisapperMode = .init(0)
        self.krImageViewer.allowOperationCaching = false
        self.krImageViewer.timeout = 30.0
        self.krImageViewer.doneButtonTitle = "DONE"
        //Auto supports the rotations.
        self.krImageViewer.supportsRotations = true
        //It'll release caches when caches of image over than X photos, but it'll be holding current image to display on the viewer.
        self.krImageViewer.overCacheCountRelease = 200
        //Sorting Rule, Default ASC is YES, DESC is NO.
        self.krImageViewer.sortAsc = true

//        self.krImageViewer.setBrowsingHandler({(_ browsingPage: Int) -> Void in
//            //Current Browsing Page.
//            //...Do Something.
//        })
//        self.krImageViewer.setScrollingHandler({(_ scrollingPage: Int) -> Void in
//            //Current Scrolling Page.
//            //...Do Something.
//        })
    }
    /*
    -(void)handleImageZoom: (UITapGestureRecognizer *)sender
    {
        UIImage *imageToZoom;
        if(memoryData.memoryItemUploadComplete)
        {
            //NSURL *imageURL = [NSURL URLWithString:memoryData.memoeryImageURLPath];
            //NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            imageToZoom = [UIImage imageWithData:imageDataForMail];
        }
        else
        {
            memoryItems *dataItem1 = (memoryItems*)[memoryData.memoryItemData objectAtIndex:0];
            imageToZoom = dataItem1.memoeryItemLocalImage;
        }
        
        NSArray *_directWatchs = [NSArray arrayWithObjects:imageToZoom,nil];
        [self.krImageViewer browseImages:_directWatchs];
    }
    */
// MARK: - ASIHTTP Requests

    func getNoteData(_ notesId: String) {
        APPDELEGATE.addChargementLoader()
        let urlString = "\(BaseWebRequestUrl)\(GetNoteDetailsURL)\(notesId).json"
        let serviceURL = URL(string: urlString)!
        print("Note Url : \(serviceURL)")
        
        APIManager.sharedInstance.callWebService(url: urlString, method: .get) { (json:JSONDictionary?, error:NSError?) in
            
            APPDELEGATE.removeChargementLoader()
            
            if error == nil {
                /*
                 existingNoteDictionary = [[NSMutableDictionary alloc] init];
                 existingNoteDictionary = [[request responseString] JSONValue];
                 NSLog(@"Return Corespondent Note  = %@",existingNoteDictionary);
                 [notesTitleTxtFld setText:[[[[existingNoteDictionary objectForKey:@"pojectNote"] objectAtIndex:0] objectForKey:@"ProjectNote"] objectForKey:@"note_title"]];
                 
                 [notesTextView setText:[[[[existingNoteDictionary objectForKey:@"pojectNote"] objectAtIndex:0] objectForKey:@"ProjectNote"] objectForKey:@"note_description"]];
                 
                 NSMutableArray *arr = [[NSMutableArray alloc] init];
                 [arr addObjectsFromArray:[[[existingNoteDictionary objectForKey:@"pojectNote"] objectAtIndex:0] objectForKey:@"ProjectNotePhoto"]];
                 
                 for (int i = 0; i < arr.count; i++)
                 {
                 [collectionViewImages insertObject:[[[[[existingNoteDictionary objectForKey:@"pojectNote"] objectAtIndex:0] objectForKey:@"ProjectNotePhoto"] objectAtIndex:i] objectForKey:@"note_photo_path"] atIndex:i];
                 }
                 NSLog(@"Images Array : %@",collectionViewImages);
                 [imageCollectionView reloadData];
                 */
                
                if let projectNotes = json?["pojectNote"] as? [JSONDictionary] {
                    if projectNotes.count > 0 {
                        if let notes = projectNotes[0]["ProjectNote"] {
                            if let desc = notes["note_description"] as? String{
                                self.notesTextView.text = desc
                                self.notesTextView.textColor = UIColor.black
                                
                                self.notesTextView.isScrollEnabled = true
                                
                            }
                            if let title = notes["note_title"] as? String {
                                self.notesTitleTxtFld.text = title
                            }
                            var arrImg = [String]()
                            
                            print(notes)
                            if let arr = projectNotes[0]["ProjectNotePhoto"] as? [JSONDictionary] {
                                self.collectionViewImages = []
                                for dict in arr {
                                    self.collectionViewImages.append("\(dict["note_photo_path"]!)" as AnyObject)
                                }
                                self.imageCollectionView.reloadData()
                            }
                        }
                    }
                }
            }
        }
        
        
//        var requestAPI = ASIFormDataRequest.init(url: serviceURL)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        requestAPI.addHeader("Authorization", value: authValue)
//        requestAPI.delegate = self
//        requestAPI.requestMethod = "GET"
//        requestAPI.delegate = self
//        requestAPI.tag = 1
//        requestAPI.startAsynchronous()
    }

    func encode(toBase64String image: UIImage) -> String {

        let data = UIImageJPEGRepresentation(image, 0.4)
        return data!.base64EncodedString(options: .lineLength64Characters)
    }

    func uploadAlbumImages(_ imageArray: [UIImage]) {


        var dict: JSONDictionary = [:]

        dict["id"] = noteId as AnyObject?
        dict["project_id"] = projectId as AnyObject?
        dict["note_title"] = notesTitleTxtFld.text as AnyObject?
        dict["note_description"] = notesTextView.text as AnyObject?

        for i in 0..<imageArray.count {
            let image = imageArray[i]
            dict["photo[note_photo][\(i)]"] = self.encode(toBase64String: image) as AnyObject?
            dict["photo[extension][\(i)]"] = "JPEG" as AnyObject?
        }


        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.uploadAlbumImages(dict: dict) { (result:JSONDictionary?, error:NSError?) in

            if error == nil {
                APPDELEGATE.removeChargementLoader()
                self.popTo()
            }
            
        }

//        appDelegate.addChargementLoader()
//        var urlString = "\(BaseWebRequestUrl)\(SaveNotesURL)"
//        var serviceURL = URL(string: urlString)!
//        var requestAPI = ASIFormDataRequest.init(url: serviceURL)
//        requestAPI.delegate = self
//        requestAPI.setPostValue(noteId, forKey: "id")
//        requestAPI.setPostValue(projectId, forKey: "project_id")
//        requestAPI.setPostValue(notesTitleTxtFld.text, forKey: "note_title")
//        requestAPI.setPostValue(notesTextView.text, forKey: "note_description")
//        var keysArray = [Any]()
//        var extensionArray = [Any]()
//        for i in 0..<imageArray.count {
//            var keyStr = "photo[note_photo][\(i)]"
//            var extensionStr = "photo[extension][\(i)]"
//            keysArray.insert(keyStr, at: i)
//            extensionArray.insert(extensionStr, at: i)
//        }
//        for i in 0..<imageArray.count {
//            var image = imageArray[i]
//            var pictureData = .uiImageJPEGRepresentation()!
//            var pictureDataString = pictureData.base64EncodedString(withOptions: .lineLength64Characters)
//            requestAPI.setPostValue(pictureDataString, forKey: keysArray[i])
//            requestAPI.setPostValue("JPEG", forKey: extensionArray[i])
//        }
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        requestAPI.addHeader("Authorization", value: authValue)
//        requestAPI.requestMethod = "POST"
//        requestAPI.delegate = self
//        requestAPI.tag = 2
//        requestAPI.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            existingNoteDictionary = [AnyHashable: Any]()
//            existingNoteDictionary = request.responseString().jsonValue()
//            print("Return Corespondent Note  = \(existingNoteDictionary)")
//            notesTitleTxtFld.text = (((existingNoteDictionary["pojectNote"] as! String)[0]["ProjectNote"] as! String)["note_title"] as! String)
//            notesTextView.text = (((existingNoteDictionary["pojectNote"] as! String)[0]["ProjectNote"] as! String)["note_description"] as! String)
//            var arr = [Any]()
//            arr += ((existingNoteDictionary["pojectNote"] as! String)[0]["ProjectNotePhoto"] as! String)
//            for i in 0..<arr.count {
//                collectionViewImages.insert((((existingNoteDictionary["pojectNote"] as! String)[0]["ProjectNotePhoto"] as! String)[i]["note_photo_path"] as! String), at: i)
//            }
//            print("Images Array : \(collectionViewImages)")
//            imageCollectionView.reloadData()
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return new Note Response  = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                self.navigationController!.popViewController(animated: true)!
//            }
//        }
//
//        if networkQueue.requestsCount == 1 {
//            print("All Request Complete.........!!!!!")
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        print("Error>>>>>>>>>>: \(.error)")
//        var alert: UIAlertView?
//        if .error.code == 1 {
//            alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//
//    }


}
