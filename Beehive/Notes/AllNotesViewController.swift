//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  AllNotesViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 08/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class AllNotesViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    var viewToolbar: UIToolbar!
    var backButton: UIBarButtonItem!
    var addNoteButton: UIBarButtonItem!
    @IBOutlet var notesTableView: UITableView!
    var notesArray = [JSONDictionary]()
    var projectId = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        notesTableView.estimatedRowHeight =  40
        notesTableView.rowHeight = UITableViewAutomaticDimension
        
        print("Project Id :\(self.projectId)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.getProjectNotes()
        self.viewCustomization()
    }

    func viewCustomization() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        backButton = UIBarButtonItem(customView: button)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let addNoteBtnImage = UIImage(named: "addBtn.png")!
        let addNoteBtn = UIButton(type: .custom)
        addNoteBtn.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(60), height: CGFloat(25))
        addNoteBtn.setBackgroundImage(addNoteBtnImage, for: .normal)
        addNoteBtn.setTitleColor(UIColor.white, for: .normal)
        addNoteBtn.addTarget(self, action: #selector(self.addNoteButtonClick), for: .touchUpInside)
        addNoteButton = UIBarButtonItem(customView: addNoteBtn)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(50), height: CGFloat(44)))
        titleLabel.text = "Notes"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold18
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        let flexSpace1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        var barItems = [UIBarButtonItem]()
        barItems.append(backButton)
        barItems.append(flexSpace)
        barItems.append(toolbarTitleLbl)
        barItems.append(flexSpace1)
        barItems.append(addNoteButton)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
    }


    func backBtnClick() {
        popTo()
    }

    func addNoteButtonClick() {

        let notesView = STORYBOARD.instantiateViewController(withIdentifier: "NotesViewController") as! NotesViewController
        notesView.noteId = ""
        notesView.projectId = self.projectId
        self.navigationController!.pushViewController(notesView, animated: true)
    }
// MARK: - Tableview Delegate & DataSource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notesArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)


        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }

        var titleTxt = ""

        if let projectNotes = notesArray[indexPath.row]["ProjectNote"] as? JSONDictionary {
            if let title = projectNotes["note_title"] as? String {
                titleTxt = title
            }
        }

        cell?.textLabel!.text = titleTxt
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel!.font = CenturyGothiCRegular15
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let notesView = STORYBOARD.instantiateViewController(withIdentifier: "NotesViewController") as? NotesViewController
        if let projectNotes = notesArray[indexPath.row]["ProjectNote"] as? JSONDictionary {
            if let noteIDTemp = projectNotes["id"] as? String {
                notesView?.noteId = noteIDTemp
                notesView?.projectId = self.projectId
                notesView?.isNewNoteFlag = true
                self.navigationController!.pushViewController(notesView!, animated: true)
            }
        }

        tableView.deselectRow(at: indexPath, animated: true)

    }
// MARK: - ASIHTTP Requests

    func getProjectNotes() {
        APPDELEGATE.addChargementLoader()



        APIManager.sharedInstance.getProjectNotes(projectId: self.projectId) { (dict:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()

            if error == nil {

                if let tempDict = dict?["pojectNotes"] as? [JSONDictionary] {
                    self.notesArray = tempDict
                    self.notesTableView.reloadData()
                }
            }

        }


            //    NSLog(@"URL- %@",url);
//        var request = ASIFormDataRequest.init(url: url)
//        var pref = UserDefaults.standard
//        var emailString = pref.object(forKey: "UserId")!
//        var userTokenString = pref.object(forKey: "User_Token")!
//        var authStr = "\(emailString):\(userTokenString)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Project Notes = \(returnDict)")
//            notesArray = [Any]()
//            //        [notesArray addObject:[returnDict objectForKey:@"pojectNotes"]];
//            notesArray += (returnDict["pojectNotes"] as! String)
//            //        [self viewCustomization];
//            notesTableView.reloadData()
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Fail Project Data Dict = \(returnDict)")
//        var .error = request.error
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }



}
//
//  AllNotesViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 08/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
