//
//  SLError.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

enum SLErrorCode: Int {
    case ServerError = 0
    case EmptyDataError = 1
    case DataParseError = 2
}

struct SLError {
    static let EmptyResultError = NSError(domain: "", code: SLErrorCode.EmptyDataError.rawValue, userInfo: [NSLocalizedDescriptionKey : "No Result Found."])

    static let ServerError = NSError(domain: "", code: SLErrorCode.ServerError.rawValue, userInfo: [NSLocalizedDescriptionKey : "An error occured while getting data from server."])

    static let DataParseError = NSError(domain: "", code: SLErrorCode.DataParseError.rawValue, userInfo: [NSLocalizedDescriptionKey : "An error occured while getting data from server."])
}
