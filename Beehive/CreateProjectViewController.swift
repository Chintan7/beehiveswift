//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CreateProjectViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 03/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
//#define kGOOGLE_API_KEY @"AIzaSyDtdiFCndL0A_PCX7bnA2uGOEW0qNXDgic"
class CreateProjectViewCovaroller: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate {

    @IBOutlet var fieldsContainer: UIScrollView!
    @IBOutlet var serachField: UITextField!
    @IBOutlet var projectNameField: UITextField!
    @IBOutlet var streetField: UITextField!
    @IBOutlet var streetNoField: UITextField!
    @IBOutlet var cityField: UITextField!
    @IBOutlet var stateField: UITextField!
    @IBOutlet var codeField: UITextField!
    @IBOutlet var countryField: UITextField!
    @IBOutlet var projectNameTextField: UITextField!
    @IBOutlet var projectNameLabel: UILabel!
    var addressString = ""
    var actualLocationCoordinates: CLLocation!
    var completeAddressDict = NSMutableDictionary()
    var searchDataList = NSMutableArray()
    var isCheckInSelected: Bool! = false
    @IBOutlet var autocompleteDataTable: UITableView!
    var isComingIndicationFlag = false

    var completeAddressReturnDict = NSMutableDictionary()

    @IBAction func addButtonClicked(_ sender: Any) {
        if projectNameField.text!.characters.count == 0 || streetNoField.text!.characters.count == 0 || streetField.text!.characters.count == 0 || cityField.text!.characters.count == 0 || countryField.text!.characters.count == 0 || stateField.text!.characters.count == 0 || codeField.text!.characters.count == 0 {
            let myAlert = UIAlertView(title: "Warning", message: "Please fill all the fields", delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
            myAlert.show()
            return
        }

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "AdjustRadiusViewController")
//        newView.actualLocation = actualLocationCoordinates

        if completeAddressDict.count > 0 {
            completeAddressDict.removeAllObjects()
        }

        completeAddressDict["project_name"] = projectNameField.text
        completeAddressDict["project_location"] = serachField.text
        completeAddressDict["project_street_number"] = streetNoField.text
        completeAddressDict["project_street"] = streetField.text
        completeAddressDict["project_city"] = cityField.text
        completeAddressDict["project_state"] = stateField.text
        completeAddressDict["project_country"] = countryField.text
        completeAddressDict["project_postal_code"] = codeField.text
//        newView.completeProjectAddressData = completeAddressDict
//        newView.locationLatitude = ((((completeAddressReturnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lat"] as! String).doubleValue
//        newView.locationLongitude = ((((completeAddressReturnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lng"] as! String).doubleValue
        self.navigationController!.pushViewController(newView, animated: true)
    }

    @IBAction func currentLocationClicked(_ sender: Any) {
        autocompleteDataTable.isHidden = true
//        let sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
        let newView = STORYBOARD.instantiateViewController(withIdentifier: "CurrentLocationViewController")
        self.navigationController!.pushViewController(newView, animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.viewCustomization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        //    if (searchDataList == nil) {
        //        [autocompleteDataTable setHidden:YES];
        //    }
    }

    func viewCustomization() {
        autocompleteDataTable.isHidden = true
        searchDataList = NSMutableArray()
        fieldsContainer.contentSize = CGSize(width: CGFloat(320), height: CGFloat(620))
        if screenSize.height != 568 {
            fieldsContainer.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg")!)
            autocompleteDataTable.frame = CGRect(x: CGFloat(0), y: CGFloat(74), width: CGFloat(320), height: CGFloat(131))
        }
        else {
            fieldsContainer.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg-568h")!)
        }
        var menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem! = menuButton
        var cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelProject))
        cancelButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem! = cancelButton
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        projectNameField.font = CenturyGothiCRegular15
        streetField.font = CenturyGothiCRegular15
        streetNoField.font = CenturyGothiCRegular15
        cityField.font = CenturyGothiCRegular15
        stateField.font = CenturyGothiCRegular15
        countryField.font = CenturyGothiCRegular15
        codeField.font = CenturyGothiCRegular15
        projectNameLabel.font = CenturyGothicBold15
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }
// MARK: TextField Delegates

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if screenSize.height != 568 {
            if textField == stateField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(55)), animated: true)
            }
            else if textField == countryField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(95)), animated: true)
            }
            else if textField == codeField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(135)), animated: true)
            }
        }
        else {
            if textField == codeField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(55)), animated: true)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        textField.resignFirstResponder()
        return true
    }


    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == serachField {
            if textField.text!.characters.count > 0 {
                self.requestGoogleWebServices()
                return true
            }
        }
        //    autocompleteDataTable.hidden = NO;
        //    NSString *substring = [NSString stringWithString:textField.text];
        //    substring = [substring
        //                 stringByReplacingCharactersInRange:range withString:string];
        //    [self searchAutocompleteEntriesWithSubstring:substring];
        return true
    }

    func searchAutocompleteEntries(withSubstring substring: String) {
        // Put anything that starts with this substring into the autocompleteUrls array
        // The items in this array is what will show up in the table view
        //    [autocompleteUrls removeAllObjects];
        //    for(NSString *curString in pastUrls) {
        //        NSRange substringRange = [curString rangeOfString:substring];
        //        if (substringRange.location == 0) {
        //            [autocompleteUrls addObject:curString];
        //        }
        //    }
        //    [autocompleteTableView reloadData];
    }

    func cancelProject() {
        if self.isComingIndicationFlag == true {
            self.isComingIndicationFlag = false
            if self.menuContainerViewController.menuState == MFSideMenuStateLeftMenuOpen {
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }
            else {
                self.menuContainerViewController.menuState = MFSideMenuStateLeftMenuOpen
            }
        }
        else {
            popTo()
        }
    }

    // MARK: Autocomplete Table Delegate/Datasource


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }



    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDataList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        cell?.textLabel!.text = ((searchDataList[indexPath.row] as! NSDictionary).value(forKey: "description") as! String)
        //    NSLog(@"Cell COntent = %@",[[searchDataList objectAtIndex:indexPath.row] objectForKey:@"description"]);
        cell?.tag = indexPath.row
        return cell!
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        serachField.resignFirstResponder()
        autocompleteDataTable.isHidden = true
        var dataDict = searchDataList[indexPath.row] as? [String: AnyObject]
        serachField.text = (dataDict?["description"] as! String)
        addressString = (dataDict?["description"] as! String)
        self.getCompleteData()
    }

    func splitReturnData() {
//        var typeArr = (((completeAddressReturnDict.value(forKey: "results") as! String)[0].value(forKey: "address_components") as! String).value(forKey: "types") as! String)
//        completeAddressDict = [AnyHashable: Any]()
//        if typeArr.count > 0 {
//            for i in 0..<typeArr.count {
//                completeAddressDict[typeArr[i][0]] = (((completeAddressReturnDict.value(forKey: "results") as! String)[0].value(forKey: "address_components") as! String)[i].value(forKey: "long_name") as! String)
//            }
//        }
//        completeAddressDict["formatted_address"] = ((completeAddressDict.value(forKey: "results") as! String)[0].value(forKey: "formatted_address") as! String)
//        print("dict.. = \(completeAddressDict)")
//        self.fillAllFields()
//        actualLocationCoordinates = CLLocation(latitude: ((((completeAddressReturnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lat"] as! String).doubleValue, longitude: ((((completeAddressReturnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lng"] as! String).doubleValue)
        //    NSLog(@"Lat & Long: %@", actualLocationCoordinates);
    }

    func fillAllFields() {
        autocompleteDataTable.isHidden = true
        streetNoField.text = (completeAddressDict["street_number"] as! String)
        cityField.text = (completeAddressDict["locality"] as! String)
        streetField.text = (completeAddressDict["administrative_area_level_2"] as! String)
        stateField.text = (completeAddressDict["administrative_area_level_1"] as! String)
        countryField.text = (completeAddressDict["country"] as! String)
        codeField.text = (completeAddressDict["postal_code"] as! String)
    }
// MARK: Webservice Requests and Delegate

    func requestGoogleWebServices() {
        APPDELEGATE.addChargementLoader()
        let textFieldText = serachField.text
            //    https://maps.googleapis.com/maps/api/place/autocomplete/output?parameters;
        var strURL = "\(GOOGLE_AUTOCOMPLETE_SEARCH_URL)=\(textFieldText)&types=geocode&sensor=false&key=\(kGOOGLE_MAP_SERVER_API_KEY_19JUNE)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!
//        var request = ASIFormDataRequest(URL: url)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

    func getCompleteData() {
        APPDELEGATE.addChargementLoader()
            //    NSString *str = @"411.ca,Eglinton Avenue East,Toronto,ON,Canada";
            //    NSString *strURL = [NSString stringWithFormat:@"%@address=%@&sensor=false",kGOOGLE_GEOCODE_URL,str];
        var strURL = "\(kGOOGLE_GEOCODE_URL)=\(addressString)&sensor=false"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        let url = URL(string: strURL)!
        print("Address Url = \(url)")
//        var request = ASIFormDataRequest(URL: url)
//        request.delegate = self
//        request.tag = 2
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            //        NSLog(@"Return Dict Data : %@",returnDict);
//            if !((returnDict["status"] as! String) == "INVALID_REQUEST") {
//                searchDataList = [Any](arrayLiteral: (returnDict["predictions"] as! String))
//            }
//            //        NSLog(@"Array Data : %@",searchDataList);
//            //        if ([searchDataList count]!=0)
//            //        {
//            autocompleteDataTable.isHidden = false
//            autocompleteDataTable.reloadData()
//            //        }
//            //        else;
//        }
//        else if request.tag == 2 {
//            completeAddressReturnDict = request.responseString().jsonValue()
//            if ((completeAddressReturnDict["status"] as! String) == "OK") {
//                self.splitReturnData()
//            }
//            else {
//                var newAlert = UIAlertView(title: "Warning", message: "Please select correct location", delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
//                newAlert.show()
//                return
//            }
//        }
//
//        appDelegate.removeChargementLoader()
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Error Dict = \(returnDict)")
//        var .error = request.error
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }


}
//
//  CreateProjectViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 03/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
