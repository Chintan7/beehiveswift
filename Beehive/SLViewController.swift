//
//  SLViewController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

extension UIViewController {

    func pushTo(string: String) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: string)
        self.navigationController?.pushViewController(viewController!, animated: true)
    }

    func pushToVC(controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
    }

    func popTo() {
        _=self.navigationController?.popViewController(animated: true)
    }

    func hideNavigation(_ hide: Bool) {
        self.navigationController!.isNavigationBarHidden = hide
    }
}
