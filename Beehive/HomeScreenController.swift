//
//  HomeScreenController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
import CoreData
import MessageUI



class HomeScreenController: BaseViewController,NotificationViewPrtotcol,CLLocationManagerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UINavigationControllerDelegate,MFMailComposeViewControllerDelegate {

    @IBOutlet var checkInButton: UIButton!
    @IBOutlet var addProjectButton: UIButton!
    @IBOutlet var viewProjectButton: UIButton!
    @IBOutlet var manualCheckInCheckOutButton: UIButton!
    @IBOutlet var customCheckoutBackground: UIView!
    @IBOutlet var checkoutCustomView: UIView!
    @IBOutlet var customViewTextLabel: UILabel!
    @IBOutlet var checkInDetailButton: UIButton!
    @IBOutlet var customCheckInView: UIView!
    @IBOutlet var checkinProjectLabel: UILabel!
    @IBOutlet var checkinProjectMarkerImage: UIImageView!

    var notificationButton: UIButton!
    var notificationBtn: UIBarButtonItem!
    var notificationView: NotificationView!
    var pickerBackgroundView: UIView!
    var isCheckBtnNotSelected = false
    var onlineBtn: UIButton!
    var checkInDataDict = NSMutableDictionary()
    var trackerDetailArray = NSMutableArray()
    var internetStatus: NetworkStatus?
    var dataObjectIndex = 0
    var plistPath = ""
    var geofences = NSMutableArray()
    var projectDictionary = NSMutableDictionary()
    var geofenceMonitor: GeofenceMonitor!
//    var hostReachability: Reachability!
//    var internetReachability: Reachability!
//    var wifiReachability: Reachability!
    var isCheckIn = false
    var notificationBarButton: UIBarButtonItem!
    var isNotificationButtonSelected = false
    var projectsArray = NSMutableArray()
    var manualCheckInProjectsArray = NSMutableArray()
    var projectPickerView: UIPickerView!
    var pickerToolbar: UIToolbar!
    var emailIdString = ""
    var tokenString = ""
    var projectsDataArray = NSMutableArray()
    var checkedInProjectName = ""
    var checkedInProjectLocation = ""
    var checkedInProjectID = ""

    @IBOutlet var lblProjectDetail: UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocationClass.sharedInstance.getAllCurrentProjects()
        LocationClass.sharedInstance.restartMonitoring()
        self.setupDoScreen()
        self.customCheckOutView()
        self.viewCustomization()
        
        ODKit.methodAddObserver(forName: keyProjectManagerDidCheckInOutNotification, withWeakSelf: self) { (noti:Notification) in
//            DispatchQueue.main.async {
                self.viewModification()
//            }
        }
        //Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(self.viewModification), userInfo: nil, repeats: true)
        // Do any additional setup after loading the view.
    }

    func setupDoScreen() {
        geofences = NSMutableArray()
        geofenceMonitor = GeofenceMonitor()
        self.getGeoFences()
//        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        self.customCheckOutView()
        self.viewCustomization()
        //    [self requestGetMyProjectsWebServices];
        self.manualCheckInCheckOutButton.isHidden = false
        self.manualCheckInCheckOutButton.alpha = 30
    }


    func viewCustomization() {
        // [checkInButton setSelected:YES];

        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        

        if let count = APPDELEGATE.unreadNotificationCount {
            notificationButton.setTitle(count, for: .normal)
        }

        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGesturehandler))
        self.customCheckInView.addGestureRecognizer(tapGesture)
//        print(UserDefaults.sharedInstance().theCurrentProjectId!)
        if UserDefaultClass.sharedInstance().theCurrentProjectId == nil {
            APPDELEGATE.checkInButtonSelected = false
            manualCheckInCheckOutButton.isSelected = false
            self.customCheckInView.isHidden = true
        }
        else {
            APPDELEGATE.checkInButtonSelected = true
            self.viewModification()
        }
        for geo in geofences {
            if geofenceMonitor.isChecked(in: geo as! Geofence) {
                APPDELEGATE.checkInButtonSelected = true
                self.viewModification()
            }
        }
    }

    func getGeoFences() {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as? [String]
        let basePath = ((paths?.count)! > 0) ? paths?[0] : nil
        plistPath = URL(fileURLWithPath: basePath!).appendingPathComponent("BeehiveLocalDB.plist").absoluteString

        if NSMutableDictionary(contentsOfFile: plistPath) != nil {
            projectDictionary = NSMutableDictionary(contentsOfFile: plistPath)!
        }

        let projectDict = NSMutableDictionary(contentsOfFile: plistPath)
        let storeProjectDetailArray = NSMutableArray()

        if geofences.count > 0 {
            geofences.removeAllObjects()
        }

        //        geofences.removeAll()

        //print(projectDict)

        if (projectDict != nil) {
            if (projectDict?["ProjectDetails"] as! NSArray).count > 0 {
                for i in 0..<(projectDict?["ProjectDetails"] as! NSArray).count {
                    storeProjectDetailArray.add((projectDict?.object(forKey: "ProjectDetails") as! NSArray)[i])
                    //                storeProjectDetailArray.append((projectDict["ProjectDetails"] as! String)[i])
                    let project = (projectDict?["ProjectDetails"] as! NSArray)[i] as! NSDictionary as! [AnyHashable: Any]
                    let geofenc = geofenceMonitor.getGeofence(project)
                    //                var geofence = geofenceMonitor.getGeofence(project)
                    geofences.add(geofenc)
                    //                geofences.append(geofence)
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())

        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init(rawValue: "CheckInNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.viewModification), name: NSNotification.Name(rawValue: "CheckInNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideView), name: NSNotification.Name(rawValue: "hideNotificaitonView"), object: nil)
        self.navigationItem.setHidesBackButton(true, animated: false)
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        let basePath = (paths.count > 0) ? paths[0] : ""
        plistPath = URL(fileURLWithPath: basePath).appendingPathComponent("BeehiveLocalDB.plist").absoluteString
        if NSMutableDictionary(contentsOfFile: plistPath) != nil {
            projectDictionary = NSMutableDictionary(contentsOfFile: plistPath)!
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.setprojectDetail), name: NSNotification.Name(rawValue: "setprojectDetail"), object: nil)
    }
    
    func setprojectDetail() {
        self.lblProjectDetail?.text = LocationClass.sharedInstance.strProjectDetail
    }
    
    func hideView(noti: Notification) {
        
        if let hide = noti.object as? Bool {
            if hide {
                DispatchQueue.main.async {
                    self.customCheckInView.isHidden = true
                }
            } else {
                DispatchQueue.main.async {
                    self.customCheckInView.isHidden = false
                }
            }
        }
    }

    func updateNotificationCountValue(_ count: Any) {
        //        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    override func viewWillDisappear(_ animated: Bool) {
        //        if self.isMovingFromParent() || self.isBeingDismissed() {
        //            print("abc")
        //            // This view controller is being popped or dismissed
        //        }
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }


    @IBAction func viewMyProject(_ sender: Any) {

        let viewMyProject = STORYBOARD.instantiateViewController(withIdentifier: "AllProjectsViewController") as! AllProjectsViewController
        viewMyProject.wayIndicationFlag = true
        pushToVC(controller: viewMyProject)
    }

    @IBAction func addNewProjectBtnPressed(_ sender: Any) {
        let viewMyProject = STORYBOARD.instantiateViewController(withIdentifier: "CreateProjectViewController") as! CreateProjectViewController
        pushToVC(controller: viewMyProject)
    }

    func customCheckOutView() {
        checkoutCustomView.isHidden = true
        customCheckoutBackground.isHidden = true
        checkoutCustomView.layer.cornerRadius = 5.0
        checkoutCustomView.layer.borderWidth = 2.0
        checkoutCustomView.layer.borderColor = UIColor.gray.cgColor
    }

    @IBAction func manualCheck(inCheckOutButtonClick sender: Any) {

        var str = "Check Out"
        if self.customCheckInView.isHidden == true {
            str = "Check in"
        }
        
        let alertController = UIAlertController.init(title: kAPPNAME, message: "Are you sure you want to \(str)? \n this will work if acuracy is less then 100.", preferredStyle: .alert)
        
        let alertNO = UIAlertAction.init(title: "NO", style: .default) { (action) in
            
        }
        
        let alertOk = UIAlertAction.init(title: "YES", style: .default) { (action) in
            
            if str == "Check Out" {
                LocationClass.sharedInstance.checkOutCount = 2
                LocationClass.sharedInstance.chekoutPress = true
            } else {
                LocationClass.sharedInstance.checkInCount = 1
                LocationClass.sharedInstance.chekoutPress = false
            }
            LocationClass.sharedInstance.munites = 0
            LocationClass.sharedInstance.getAllCurrentProjects()
            LocationClass.sharedInstance.perform(#selector(LocationClass.sharedInstance.showChekinoutAlert), with: nil, afterDelay: 4.0)
            LocationClass.sharedInstance.sucessCall = {
                print("Call closure....")
            }
        }
        
        alertController.addAction(alertNO)
        alertController.addAction(alertOk)
        self.present(alertController, animated: true, completion: nil)
        
        /*
        if manualCheckInCheckOutButton.isSelected {
            checkoutCustomView.isHidden = false
            customCheckoutBackground.isHidden = false
        }
        else {
            let checkReachability = Reachability.init(hostname: "http://www.google.com")


            if checkReachability?.isReachable == false {
                if (projectDictionary["ProjectDetails"] as! NSArray).count > 0 {
                    manualCheckInProjectsArray = NSMutableArray()
                    for i in 0..<(projectDictionary["ProjectDetails"] as! NSArray).count {
                        manualCheckInProjectsArray.add((projectDictionary["ProjectDetails"] as! NSArray)[i])
                    }
                }
                if !(manualCheckInProjectsArray.count == 0) {
                    self.pickerViewAllocation()
                }
                else {
                    manualCheckInCheckOutButton.isSelected = false
                    let msgAlert = UIAlertView(title: "Beehive", message: "You are not assign in any project", delegate: nil, cancelButtonTitle: "OK")
                    msgAlert.show()
                }
            }
            else {
                self.getManualCheckInProjectsArray()
            }
            //        [self getManualCheckInProjectsArray];
        }
        */
    }

    @IBAction func confirmCheckoutPressed(_ sender: Any) {
        let currentDateTime = Date()
        print("\(currentDateTime)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
        print("Date & Time = \(dateTime)")
        let checkReachability = Reachability.init(hostname: "https://www.google.com")

        if checkReachability?.isReachable == false {
            self.storeDataLocally("\(checkedInProjectID)", checkOutTime: dateTime, checkOutIntervalType: "30")
        }
        else {
            self.checkOuter("\(checkedInProjectID)", checkOutTime: dateTime, checkOutIntervalType: "30")
        }
    }

    @IBAction func confirmCancelPressed(_ sender: Any) {
        checkoutCustomView.isHidden = true
        customCheckoutBackground.isHidden = true
    }

    @IBAction func temporaryCheckOut(_ sender: Any) {
        print("Temporary Check Out")
        let currentDateTime = Date()
        print("\(currentDateTime)")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
        print("Date & Time = \(dateTime)")
        let checkReachability = Reachability.init(hostname: "http://www.google.com")

        if checkReachability?.isReachable == false {
            self.storeDataLocally("\(checkedInProjectID)", checkOutTime: dateTime, checkOutIntervalType: "30")
        }
        else {
            self.checkOuter("\(checkedInProjectID)", checkOutTime: dateTime, checkOutIntervalType: "30")
        }
    }

    func pickerViewAllocation() {
        pickerBackgroundView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(568)))
        pickerBackgroundView.backgroundColor = UIColor.gray
        pickerBackgroundView.alpha = 0.6
        self.view.addSubview(pickerBackgroundView)
        if screenSize.height != 568 {
            pickerToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(220), width: CGFloat(320), height: CGFloat(40)))
            projectPickerView = UIPickerView(frame: CGRect(x: CGFloat(0), y: CGFloat(260), width: CGFloat(320), height: CGFloat(220)))
        }
        else {
            pickerToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(308), width: CGFloat(320), height: CGFloat(40)))
            projectPickerView = UIPickerView(frame: CGRect(x: CGFloat(0), y: CGFloat(348), width: CGFloat(320), height: CGFloat(220)))
        }
        pickerToolbar.backgroundColor = UIColor.white
        projectPickerView.backgroundColor = UIColor.white
        projectPickerView.delegate = self
        projectPickerView.dataSource = self
        self.view.addSubview(projectPickerView)
        let button1 = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneSelection))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let button2 = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelSelection))
        pickerToolbar.items = [button2, flex, button1]
        self.view.addSubview(pickerToolbar)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return manualCheckInProjectsArray.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (manualCheckInProjectsArray[row] as! NSDictionary).object(forKey:"project_name") as? String
    }

    func doneSelection() {
        let row = projectPickerView.selectedRow(inComponent: 0)
//        manualCheckInProjectsArray[row]
        checkInDataDict = NSMutableDictionary()
        checkInDataDict = manualCheckInProjectsArray[row] as! NSMutableDictionary
        checkedInProjectID = ((manualCheckInProjectsArray[row] as! NSMutableDictionary).value(forKey: "id") as! String)
        let currentDateTime = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTime = dateFormatter.string(from: currentDateTime)
        print("Date & Time = \(dateTime)")
        let reachec = Reachability.init(hostname: "http://www.google.com")
        if reachec?.isReachable == false {
            self.storeDataLocally("\(checkedInProjectID)", checkInTime: dateTime)
        }
        else {
            self.checkInner("\(checkedInProjectID)", checkInTime: dateTime)
        }
        pickerBackgroundView.removeFromSuperview()
        projectPickerView.removeFromSuperview()
        pickerToolbar.removeFromSuperview()
    }

    func cancelSelection() {
        manualCheckInCheckOutButton.isSelected = false
        pickerBackgroundView.removeFromSuperview()
        projectPickerView.removeFromSuperview()
        pickerToolbar.removeFromSuperview()
    }

    func viewModification() {
        
        if UserDefaultClass.sharedInstance().theCurrentProjectId == nil {
            APPDELEGATE.checkInButtonSelected = false
        }
        else {
            APPDELEGATE.checkInButtonSelected = true
        }

        if APPDELEGATE.checkInButtonSelected == true {
            manualCheckInCheckOutButton.isSelected = true
            if UserDefaultClass.sharedInstance().theCurrentProjectId != nil {


                var locations  = [BeehiveProject]() // Where Locations = your NSManaged Class

                let fetchRequest = NSFetchRequest<BeehiveProject>(entityName: "BeehiveProject")
//                fetchRequest.predicate = NSPredicate(format: "theID == %@", UserDefaultClass.sharedInstance().theCurrentProjectId!)

                do {
                     locations = try APPDELEGATE.managedObjectContext.fetch(fetchRequest)
                } catch {

                }

                // Then you can use your properties.
                
                for location in locations {
                    
                    if let projectID = UserDefaultClass.sharedInstance().theCurrentProjectId {
                        
                        if location.theId == projectID {
                            checkedInProjectName = location.theName
                            checkedInProjectLocation = location.theLocation
                            checkedInProjectID = location.theId
                            let customTextLabelAttribute = NSMutableAttributedString(string: "\(checkedInProjectName)\n\(checkedInProjectLocation)\n")
                            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular15, range: NSRange(location: 0, length: checkedInProjectName.characters.count))
                            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: checkedInProjectName.characters.count))
                            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular11, range: NSRange(location: checkedInProjectName.characters.count + 1, length: checkedInProjectLocation.characters.count))
                            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: checkedInProjectName.characters.count + 1, length: checkedInProjectLocation.characters.count))
                            
                           
                            DispatchQueue.main.async {
                                self.checkinProjectLabel.attributedText = customTextLabelAttribute
                                self.customCheckInView.isHidden = false
                                self.navigationController?.navigationBar.addSubview(Global.checkedInModeButton())
                            }
                            break
                        } else {
                            self.customCheckInView.isHidden = true
                            APPDELEGATE.checkInButtonSelected = false
                        }
                    }
                }
            } else {
                self.customCheckInView.isHidden = true
                APPDELEGATE.checkInButtonSelected = false
            }
            
        } else  {
            
            DispatchQueue.main.async {
                self.manualCheckInCheckOutButton.isSelected = false
                self.customCheckInView.isHidden = true
                //        APPDELEGATE.checkInButtonSelected = NO;
                self.checkoutCustomView.isHidden = true
                self.customCheckoutBackground.isHidden = true
                self.navigationController?.navigationBar.addSubview(Global.checkedInModeButton())
            }
        }

    }


    func tapGesturehandler(_ gesture: UITapGestureRecognizer) {

        if (UserDefaultClass.sharedInstance().theCurrentProjectId != nil) {

            let projectDetailPage = STORYBOARD.instantiateViewController(withIdentifier: "CrewProjectDetailViewController") as! CrewProjectDetailViewController
            projectDetailPage.projectId = UserDefaultClass.sharedInstance().theCurrentProjectId
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
            self.navigationController!.pushViewController(projectDetailPage, animated: true)
        }
    }


    @IBAction func projectDetailButtonPressed(_ sender: Any) {
        if UserDefaultClass.sharedInstance().theCurrentProjectId != nil {

            let projectDetailPage = STORYBOARD.instantiateViewController(withIdentifier: "CrewProjectDetailViewController") as! CrewProjectDetailViewController
            projectDetailPage.projectId = UserDefaultClass.sharedInstance().theCurrentProjectId
            self.navigationController!.pushViewController(projectDetailPage, animated: true)
        }
    }


    func getManualCheckInProjectsArray() {
        APIManager.sharedInstance.requestGetMyCurrentProject { (returnDict:JSONDictionary?, error:NSError?) in

            if error == nil {

                print(returnDict)
                if ((returnDict?["status"] as! String) == "OK") {
                    for i in 0..<(returnDict?["projects"] as! NSArray).count {
                        self.manualCheckInProjectsArray.insert((((returnDict?["projects"] as! NSArray)[i] as! NSDictionary).object(forKey: "Project") as! String), at: i)
                    }
                    if self.manualCheckInProjectsArray.count > 0 {
                        self.saveData(inDocumentDirectory: self.manualCheckInProjectsArray)
                    }
                    if self.manualCheckInProjectsArray.count > 0 {
                        self.pickerViewAllocation()
                    }
                    else {
                        self.manualCheckInCheckOutButton.isSelected = false
                        let msgAlert = UIAlertView(title: "Sorry", message: "You are not assign in any project", delegate: nil, cancelButtonTitle: "OK")
                        msgAlert.show()
                    }

                }   
            }
        }
    }

    func checkInner(_ projectId: String, checkInTime checkinTime: String) {

        var dictTemp: JSONDictionary = [:]

        dictTemp["project_id"] = projectId as AnyObject?
        dictTemp["check_in_time"] = checkinTime as AnyObject?
        dictTemp["auto"] = "\(0)" as AnyObject?

        APIManager.sharedInstance.checkInProject(dict: dictTemp) { (json:JSONDictionary?, error:NSError?) in

            if error == nil {

                if ((json?["status"] as! String) == "OK") {
                    self.checkedInProjectName = (json?["project_name"] as! String)
                    self.checkedInProjectLocation = "\(json?["project_city"] as! String) ,\(json?["project_state"] as! String)"
                    //            Remove Due to midnight check in check out is handle in Server side...!!!!
                    //            [appDelegate initializeLocalNotification];
                    UserDefaults.standard.set(true, forKey: hasCheckedIn)
                    UserDefaults.standard.setValue(self.checkedInProjectName, forKey: "Project_Name")
                    UserDefaults.standard.setValue(self.checkedInProjectLocation, forKey: "Project_Location")
                    UserDefaults.standard.setValue(self.checkedInProjectID, forKey: "Project_Id")
                    UserDefaults.standard.synchronize()
                    self.onlineBtn.isSelected = true
                    self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
                    APPDELEGATE.checkInButtonSelected = true
                    self.viewModification()
                }
                else if ((json?["status"] as! String) == "INVALID") {
                    var myAlertView = UIAlertView(title: "Beehive", message: (json?["message"] as! String), delegate: nil, cancelButtonTitle: "OK")
                    myAlertView.show()
                }
                else {
                    self.manualCheckInCheckOutButton.isSelected = false
                }
            }
        }
    }

    func checkOuter(_ projectId: String, checkOutTime: String, checkOutIntervalType type: String) {


        var dictTemp: JSONDictionary = [:]

        dictTemp["project_id"] = projectId as AnyObject?
        dictTemp["check_out_time"] = checkOutTime as AnyObject?
        dictTemp["checkout_interval"] = type as AnyObject?
        dictTemp["auto"] = "\(0)" as AnyObject?

        APIManager.sharedInstance.checkOutProject(dict: dictTemp) { (result:JSONDictionary?, error:NSError?) in

            if error == nil {
                if ((result?["status"] as! String) == "OK") {
                    APPDELEGATE.checkInButtonSelected = false
                    UIApplication.shared.cancelAllLocalNotifications()
                    UserDefaults.standard.set(false, forKey: hasCheckedIn)
                    UserDefaults.standard.setValue("", forKey: "Project_Name")
                    UserDefaults.standard.setValue("", forKey: "Project_Location")
                    UserDefaults.standard.setValue("", forKey: "Project_Id")
                    UserDefaults.standard.synchronize()
                    self.onlineBtn.isSelected = false
                    self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
                    self.viewModification()
                }
                else {
                    self.manualCheckInCheckOutButton.isSelected = true
                    self.checkoutCustomView.isHidden = true
                    self.customCheckoutBackground.isHidden = true
                }
            }
        }
    }
    
    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
            isNotificationButtonSelected = true
        }
        else {
            APPDELEGATE.markAsReadAllNotification()
            if ((notificationView) != nil) {
                if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            }
            
            isNotificationButtonSelected = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showNotificationView(_ note: Notification) {
        //    NSLog(@"Received Notification - Someone seems to have logged in");
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }
    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {
            
            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }  else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.removeObject(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }

    func storeDataLocally(_ projectid: String, checkInTime time: String) {
        if projectDictionary.allKeys.count == 0 {
            projectDictionary = NSMutableDictionary() /* capacity: 0 */
        }
        var trackerArray = (projectDictionary["CheckInCheckOutTracker"] as? NSMutableArray
        )
        if trackerArray == nil {
            trackerArray = NSMutableArray() /* capacity: 0 */
        }
        let timeTrackerDict = NSMutableDictionary()
        timeTrackerDict.setValue((checkInDataDict["id"] as! String), forKey: "id")

        timeTrackerDict.setValue((checkInDataDict["project_name"] as! String), forKey: "project_name")

        timeTrackerDict.setValue((checkInDataDict["project_city"] as! String), forKey: "project_city")

        timeTrackerDict.setValue((checkInDataDict["project_state"] as! String), forKey: "project_state")

        timeTrackerDict.setValue(time, forKey: "checkin_time")

        trackerArray?.insert(timeTrackerDict, at: (trackerArray?.count)!)

        projectDictionary["CheckInCheckOutTracker"] = trackerArray
        projectDictionary.write(toFile: plistPath, atomically: true)
        let saveStatus = projectDictionary.write(toFile: plistPath, atomically: true)
        print(" Status : \(saveStatus)\n \(projectDictionary["CheckInCheckOutTracker"] as! String)")
        checkedInProjectName = (checkInDataDict["project_name"] as! String)
        checkedInProjectLocation = "\(checkInDataDict["project_city"] as! String) ,\(checkInDataDict["project_state"] as! String)"
        UserDefaults.standard.set(true, forKey: hasCheckedIn)
        UserDefaults.standard.setValue(checkedInProjectName, forKey: "Project_Name")
        UserDefaults.standard.setValue(checkedInProjectLocation, forKey: "Project_Location")
        UserDefaults.standard.setValue(checkedInProjectID, forKey: "Project_Id")
        UserDefaults.standard.synchronize()
        onlineBtn.isSelected = true
        APPDELEGATE.checkInButtonSelected = true
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        self.viewModification()
    }
//chris.jackson@momentum.com,chris.jacqueson@gmail.com,j_a_bro@hotmail.com
    func storeDataLocally(_ projectid: String, checkOutTime time: String, checkOutIntervalType type: String) {
        if projectDictionary.allKeys.count == 0 {
            projectDictionary = NSMutableDictionary() /* capacity: 0 */
        }
        var trackerArray = (projectDictionary["CheckInCheckOutTracker"] as? NSMutableArray)
        if trackerArray == nil {
            trackerArray = NSMutableArray()
        }
        let timeTrackerDict = NSMutableDictionary()
//        timeTrackerDict["id"] = (UserDefaults.standard.value(forKey: "Project_Id") as! String)
        timeTrackerDict.setValue((UserDefaults.standard.value(forKey: "Project_Id") as! String), forKey: "Project_Id")
//        timeTrackerDict["checkout_time"] = time
        timeTrackerDict.setValue(time, forKey: "checkout_time")
//        timeTrackerDict["checkout_interval"] = type
        timeTrackerDict.setValue(type, forKey: "checkout_interval")
        trackerArray?.insert(timeTrackerDict, at: (trackerArray?.count)!)
        projectDictionary["CheckInCheckOutTracker"] = trackerArray
        projectDictionary.write(toFile: plistPath, atomically: true)
        let saveStatus = projectDictionary.write(toFile: plistPath, atomically: true)
        print(" Status : \(saveStatus)\n \(projectDictionary["CheckInCheckOutTracker"] as! String)")
        APPDELEGATE.checkInButtonSelected = false
        UserDefaults.standard.set(false, forKey: hasCheckedIn)
        UserDefaults.standard.removeObject(forKey: "Project_Name")
        UserDefaults.standard.removeObject(forKey: "Project_Location")
        UserDefaults.standard.removeObject(forKey: "Project_Id")
        UserDefaults.standard.synchronize()
        onlineBtn.isSelected = false
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        self.viewModification()
    }

    func saveData(inDocumentDirectory returnDataArray: NSMutableArray) {
        if projectDictionary.allKeys.count == 0 {
            projectDictionary = NSMutableDictionary() /* capacity: 0 */
        }
        projectDictionary["ProjectDetails"] = manualCheckInProjectsArray
        projectDictionary.write(toFile: plistPath, atomically: true)
        let saveStatus = projectDictionary.write(toFile: plistPath, atomically: true)
        print(" Status : \(saveStatus)")
    }

    @IBAction func btnSavePress() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let basePath = ((paths.count) > 0) ? "\(paths[0])" : ""
        
        let fileDestinationUrl = (basePath as NSString).appendingPathComponent("file.txt") as NSString?
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["ankit@solulab.com","raj.rr7@gmail.com","dipen@solulab.com","rohan@solulab.com"])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            let url = URL.init(fileURLWithPath:"\(fileDestinationUrl!)")
            
            do {
                let data = try Data.init(contentsOf: url)
                mail.addAttachmentData(data, mimeType: "application/txt", fileName: "file.txt")
                APPDELEGATE.window?.rootViewController?.present(mail, animated: true, completion: nil)
            } catch {}
        } else {
            // show failure alert
        }
    }
    
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["paul@hackingwithswift.com"])
            mail.setMessageBody("<p>You're so awesome!</p>", isHTML: true)
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

class HomeSettingViewController: BaseViewController {
    
    
    @IBOutlet var lblMeter: UILabel?
    @IBOutlet var lblMunits: UILabel?
    @IBOutlet var sliderMeter: UISlider?
    @IBOutlet var sliderMunites: UISlider?
    @IBOutlet var textView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView?.text = LOGSTRING
//        NotificationCenter.default.addObserver(self, selector: #selector(self.setText), name: NSNotification.Name(rawValue: "setText"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.perform(#selector(self.setText), with: nil, afterDelay: 2.0)
        textView?.isEditable = false
        
    }
    
    func setText() {
        
        let range = NSMakeRange((textView?.text.characters.count)! - 1, 0)
        textView?.scrollRangeToVisible(range)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.sliderMunites?.setValue(Float(LocationClass.sharedInstance.munites), animated: true)
        lblMunits?.text = "\(LocationClass.sharedInstance.munites)"
    }
    
    @IBAction func backBtnPress() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sliderMeterChanged(slider: UISlider) {
        
//        lblMeter?.text = "\(sliderValue)"
//        UserDefaults.standard.set(sliderValue, forKey: "extraMeter")
//        LocationClass.sharedInstance.extraMeter = sliderValue
    }
    
    @IBAction func sliderMunitesChanged(slider: UISlider) {
        let sliderValue = Int.init(slider.value)
        lblMunits?.text = "\(sliderValue)"
        UserDefaults.standard.set(slider.value, forKey: "munites")
        LocationClass.sharedInstance.munites = sliderValue
    }
}
