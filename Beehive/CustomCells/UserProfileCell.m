//
//  UserProfileCell.m
//  Beehive
//
//  Created by Deepak Dixit on 13/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import "UserProfileCell.h"

@implementation UserProfileCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
