//
//  CrewTableCell.m
//  Beehive
//
//  Created by Deepak Dixit on 30/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import "CrewTableCell.h"

@implementation CrewTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)detailButtonPressed:(id)sender
{
    [self.delegate crewDetailButtonTappedOnCell:sender];
}

-(IBAction)contactButtonPressed:(id)sender
{
    [self.delegate contactButtonTappedOnCell:sender];
}

//- (IBAction)selectCrew:(id)sender
//{
//    [self.delegate crewForInviteTappedOnCell:sender];
//}

@end
