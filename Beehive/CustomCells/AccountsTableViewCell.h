//
//  AccountsTableViewCell.h
//  Beehive
//
//  Created by Deepak Dixit on 26/08/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountsTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *monthNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *amountLabel;
@end
