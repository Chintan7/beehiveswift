//
//  EditUserProfileCell.h
//  Beehive
//
//  Created by sdnmacmini8 on 26/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol EditUserProfileCellDelegate
//
//@optional
//- (void)imagePickerButtonTappedOnCell:(id)sender;
//
//@end

@interface EditUserProfileCell : UITableViewCell<UITextFieldDelegate>


//@property (nonatomic, strong) id  delegate;
@property (nonatomic,strong) IBOutlet UIButton *imagePickerButton;
@property (nonatomic,strong) IBOutlet UITextField *firstNameTextField;
@property (nonatomic,strong) IBOutlet UITextField *lastNameTextField;
@property (nonatomic,strong) IBOutlet UITextField *companyTextField;
@property (nonatomic,strong) IBOutlet UITextField *phoneTextField;
@property (nonatomic,strong) IBOutlet UILabel *phoneTypeLabel;

@property (nonatomic,strong) IBOutlet UITextField *emailTextField;
@property (nonatomic,strong) IBOutlet UITextField *streetTextField;
@property (nonatomic,strong) IBOutlet UITextField *streetNoTextField;
@property (nonatomic,strong) IBOutlet UITextField *cityTextField;
@property (nonatomic,strong) IBOutlet UITextField *stateTextField;
@property (nonatomic,strong) IBOutlet UITextField *countryTextField;
@property (nonatomic,strong) IBOutlet UITextField *postalCodeTextField;
@property (nonatomic,strong) IBOutlet UITextField *serviceTextField;


-(IBAction)imagePickerbuttonClick:(id)sender;


@end
