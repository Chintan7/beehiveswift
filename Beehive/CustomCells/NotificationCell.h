//
//  NotificationCell.h
//  Beehive
//
//  Created by Deepak Dixit on 10/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (nonatomic, strong)IBOutlet UILabel *textLabel;
@property (nonatomic,strong) IBOutlet UIImageView *imageView;
@end
