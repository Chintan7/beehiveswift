//
//  ReportsCell.h
//  Beehive
//
//  Created by sdnmacmini5 on 28/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *crewNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *crewWorkTimeLabel;
@property (nonatomic, strong) IBOutlet UIImageView *crewImageView;


@property (nonatomic, strong) IBOutlet UILabel *projectNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *projectLocationLabel;
@property (nonatomic, strong) IBOutlet UILabel *projectworkTimeLabel;

@end
