//
//  UserProfileCell.h
//  Beehive
//
//  Created by Deepak Dixit on 13/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface UserProfileCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *profileImage;
@property (nonatomic, strong) IBOutlet UILabel *userName;
@property (nonatomic, strong) IBOutlet UILabel *companyName;


@property (nonatomic, strong) IBOutlet UILabel *numberTextLbl;

@property (nonatomic, strong) IBOutlet UILabel *descriptionTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;

@property (nonatomic, strong) IBOutlet AsyncImageView *userProfileImage;


@end
