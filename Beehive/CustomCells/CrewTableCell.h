//
//  CrewTableCell.h
//  Beehive
//
//  Created by Deepak Dixit on 30/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CrewCustomCellDelegate

@optional
- (void)contactButtonTappedOnCell:(id)sender;
- (void)crewDetailButtonTappedOnCell:(id)sender;
- (void)crewForInviteTappedOnCell:(id)sender;

@end

@interface CrewTableCell : UITableViewCell
{
    
}
@property (nonatomic, strong) id  delegate;

// First Cell
@property (nonatomic, strong) IBOutlet UIButton *detailButton;
@property (nonatomic, strong) IBOutlet UIButton *contactButton;
@property (nonatomic, strong) IBOutlet UIImageView *crewImageView;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *statusImage;

-(IBAction)detailButtonPressed:(id)sender;
-(IBAction)contactButtonPressed:(id)sender;

// Second Cell
@property (nonatomic, strong) IBOutlet UIButton *selectionButton;
@property (nonatomic, strong) IBOutlet UIImageView *crewProfileImageview;
@property (nonatomic, strong) IBOutlet UILabel *crewNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *statusImageView;

@end
