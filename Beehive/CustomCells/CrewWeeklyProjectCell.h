//
//  CrewWeeklyProjectCell.h
//  Beehive
//
//  Created by Deepak Dixit on 10/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CrewWeeklyProjectCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel *projectNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *projectLocationLabel;
@property (nonatomic, strong) IBOutlet UILabel *projectHrsLabel;

@end
