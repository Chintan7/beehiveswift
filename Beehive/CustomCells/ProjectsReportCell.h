//
//  ProjectsReportCell.h
//  Beehive
//
//  Created by Deepak Dixit on 25/08/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectsReportCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *weekTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *weekHoursLabel;

@property (nonatomic, strong) IBOutlet UILabel *crewNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *crewWeekHoursLabel;
@property (nonatomic, strong) IBOutlet UILabel *crewProjectsHoursLabel;
@property (nonatomic, strong) IBOutlet UIImageView *crewImageView;

@end
