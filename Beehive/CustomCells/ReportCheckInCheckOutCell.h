//
//  ReportCheckInCheckOutCell.h
//  Beehive
//
//  Created by Deepak Dixit on 10/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportCheckInCheckOutCell : UITableViewCell
{
    
}
@property (nonatomic, strong) IBOutlet UILabel *checkInTimeLabel;
@property (nonatomic, strong) IBOutlet UILabel *checkOutTimeLabel;
@property (nonatomic, strong) IBOutlet UILabel *checkInLabel;
@property (nonatomic, strong) IBOutlet UILabel *checkOutLabel;
@property (nonatomic, strong) IBOutlet UILabel *workTimeLabel;

@end
