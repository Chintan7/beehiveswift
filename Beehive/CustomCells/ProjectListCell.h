//
//  ProjectListCell.h
//  Beehive
//
//  Created by Deepak Dixit on 18/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectListCell : UITableViewCell
{
    
}
@property (nonatomic,strong) IBOutlet UILabel *namePlaceLabel;
@property (nonatomic,strong) IBOutlet UILabel *onlineCount;
@property (nonatomic,strong) IBOutlet UILabel *offlineCount;
@property (nonatomic,strong) IBOutlet UILabel *notificationLabel;
@property (nonatomic, strong) IBOutlet UIImageView *cellImage;


@end
