//
//  ReportsCell.m
//  Beehive
//
//  Created by sdnmacmini5 on 28/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import "ReportsCell.h"

@implementation ReportsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
