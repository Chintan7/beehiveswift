//
//  EditUserProfileCell.m
//  Beehive
//
//  Created by sdnmacmini8 on 26/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//

#import "EditUserProfileCell.h"

@implementation EditUserProfileCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(IBAction)imagePickerbuttonClick:(id)sender
{
//    [self.delegate imagePickerButtonTappedOnCell:self];
}


@end
