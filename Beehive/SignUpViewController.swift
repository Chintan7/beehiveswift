//
//  SignUpViewController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController,UIAlertViewDelegate {


    @IBOutlet var fieldsContainer: UIScrollView!
    @IBOutlet var firstNameTextfield: UITextField!
    @IBOutlet var lastNameTextfield: UITextField!
    @IBOutlet var emailIdTextfield: UITextField!
    @IBOutlet var emailTextfieldBG: UIImageView!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var passwordTextfieldBG: UIImageView!
    @IBOutlet var reenterPasswordTextfield: UITextField!
    @IBOutlet var reenterPasswordBG: UIImageView!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var termConditionButtion: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var checkbox: UIButton!
    @IBOutlet var conditionView: UIView!
    @IBOutlet var termCondBackGround: UIView!
    @IBOutlet var termConditionTextView: UITextView!
    
    var isOpened: Bool!
    var termCondition: String!
    @IBOutlet var topView: UIView!
    var appDelegate = AppDelegate()
    var charSet: CharacterSet!
    var alertLabel: UILabel!
    var passwordAlertLabel: UILabel!
    var passwordValidateLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        appDelegate = (UIApplication.shared.delegate! as! AppDelegate)
        if screenSize.height == 568 {
            fieldsContainer.contentSize = CGSize(width: CGFloat(320), height: CGFloat(520))
        } else {
            fieldsContainer.frame = CGRect(x: CGFloat(0), y: CGFloat(117), width: CGFloat(320), height: CGFloat(363))
            fieldsContainer.contentSize = CGSize(width: CGFloat(320), height: CGFloat(490))
        }

//        firstNameTextfield.text = "Ankit"
//        lastNameTextfield.text = "Solulab"
//        emailIdTextfield.text = "ankit@solulab.com"
//        passwordTextfield.text = "qwertyuiop"
//        reenterPasswordTextfield.text = "qwertyuiop"

        signUpButton.layer.cornerRadius = 5.0
        self.navigationController!.isNavigationBarHidden = true

        let topViewImg = UIImageView.init(image: UIImage(named: "SignupHeader.png")!)
        topViewImg.bounds = topView.bounds
//        topView.backgroundColor = UIColor(patternImage: UIImage(named: "SignupHeader.png")!)
        topView.addSubview(topViewImg)
        conditionView.frame = CGRect(x: CGFloat(conditionView.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(conditionView.frame.size.width), height: CGFloat(conditionView.frame.size.height))
        conditionView.layer.cornerRadius = 5.0
        conditionView.layer.borderWidth = 1.0
        conditionView.layer.borderColor = UIColor.darkGray.cgColor
        termCondBackGround.isHidden = true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == emailIdTextfield {

            if alertLabel != nil {
                alertLabel.removeFromSuperview()
            }

            emailTextfieldBG.image = UIImage(named: "GreyTf.png")!
            if screenSize.height == 568 {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
            } else {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(25)), animated: true)
            }
        } else if textField == passwordTextfield {


            if passwordValidateLabel != nil {
                passwordValidateLabel.removeFromSuperview()
            }

            passwordTextfieldBG.image = UIImage(named: "GreyTf.png")!
            reenterPasswordBG.image = UIImage(named: "GreyTf.png")!
            if screenSize.height == 568 {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(55)), animated: true)
            }
            else {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(75)), animated: true)
            }
        }else if textField == reenterPasswordTextfield {

            if passwordAlertLabel != nil {
                passwordAlertLabel.removeFromSuperview()
            }

            reenterPasswordBG.image = UIImage(named: "GreyTf.png")!
            passwordTextfieldBG.image = UIImage(named: "GreyTf.png")!
            if screenSize.height == 568 {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(55)), animated: true)
            }
            else {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(135)), animated: true)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextfield {
            lastNameTextfield.becomeFirstResponder()
        }
        else if textField == lastNameTextfield {
            emailIdTextfield.becomeFirstResponder()
        }
        else if textField == emailIdTextfield {
            passwordTextfield.becomeFirstResponder()
        }
        else if textField == passwordTextfield {
            reenterPasswordTextfield.becomeFirstResponder()
        }
        else if textField == reenterPasswordTextfield {
            if screenSize.height == 568 {
            }
        }
        
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == firstNameTextfield {

        }
        else if textField == lastNameTextfield {

        }
        else if textField == emailIdTextfield {
            if textField.text!.validateEmail() == false {
                alertLabel = UILabel(frame: CGRect(x: CGFloat(emailIdTextfield.frame.origin.x), y: CGFloat(emailIdTextfield.frame.origin.y + 40), width: CGFloat(emailIdTextfield.frame.size.width), height: CGFloat(15)))
                alertLabel.text = "Please enter valid Email Id"
                alertLabel.textColor = UIColor.red
                alertLabel.textAlignment = .left
                alertLabel.font = UIFont.init(name: "Century Gothic", size: CGFloat(10.0))!
                emailTextfieldBG.image = UIImage(named: "AlertTf.png")!
                fieldsContainer.addSubview(alertLabel)
            }
        } else if textField == passwordTextfield {
            if (passwordTextfield.text?.characters.count)! < 8 {
                passwordValidateLabel = UILabel(frame: CGRect(x: CGFloat(passwordTextfield.frame.origin.x), y: CGFloat(passwordTextfield.frame.origin.y + 40), width: CGFloat(passwordTextfield.frame.size.width), height: CGFloat(15)))
                passwordValidateLabel.text = "Password must be alphanumeric 8 characters"
                passwordValidateLabel.textColor = UIColor.red
                passwordValidateLabel.textAlignment = .left
                passwordValidateLabel.font = UIFont.init(name: "Century Gothic", size: CGFloat(12.0))!
                passwordTextfieldBG.image = UIImage(named: "AlertTf.png")!
                fieldsContainer.addSubview(passwordValidateLabel)
            }
        } else if textField == reenterPasswordTextfield {
            if !(passwordTextfield.text == reenterPasswordTextfield.text) {
                passwordAlertLabel = UILabel(frame: CGRect(x: CGFloat(reenterPasswordTextfield.frame.origin.x), y: CGFloat(reenterPasswordTextfield.frame.origin.y + 40), width: CGFloat(reenterPasswordTextfield.frame.size.width), height: CGFloat(15)))
                passwordAlertLabel.text = "Password are not matched"
                passwordAlertLabel.textColor = UIColor.red
                passwordAlertLabel.textAlignment = .left
                passwordAlertLabel.font = UIFont.init(name: "Century Gothic", size: CGFloat(12.0))!
                reenterPasswordBG.image = UIImage(named: "AlertTf.png")!
                passwordTextfieldBG.image = UIImage(named: "AlertTf.png")!
                fieldsContainer.addSubview(passwordAlertLabel)
            }
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == firstNameTextfield || textField == lastNameTextfield {

            charSet = CharacterSet.init(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz' ").inverted

            let strArr = string.components(separatedBy: charSet)
            let filteredString = strArr.joined(separator: "")
            return (string == filteredString)
        }
        return true
    }

    @IBAction func signupButtonPressed(_ sender: Any) {
        self.validateAllData()
    }
    func validateAllData() {

        if firstNameTextfield.text?.characters.count == 0 || lastNameTextfield.text?.characters.count == 0 || emailIdTextfield.text?.characters.count == 0 || passwordTextfield.text?.characters.count == 0 || reenterPasswordTextfield.text?.characters.count == 0 {
            let myAlert = UIAlertView(title: "Beehive", message: "Please fill All Details", delegate: self, cancelButtonTitle: "OK")
            myAlert.show()
            return
        }
        if (passwordTextfield.text?.characters.count)! < 8 || (reenterPasswordTextfield.text?.characters.count)! < 8 {
            let myAlert = UIAlertView(title: "Beehive", message: "Please make password with minimum 8 alphanumeric characters", delegate: self, cancelButtonTitle: "OK")
            myAlert.show()
            return
        }
        else if !checkbox.isSelected {
            let myAlert = UIAlertView(title: "Beehive", message: "Please agree terms & conditions", delegate: self, cancelButtonTitle: "OK")
            myAlert.show()
            return
        }

        termCondition = "Y"
        self.getSignUpWebService()
    }

    func getSignUpWebService() {

        let user = Register()

        user.firstName = firstNameTextfield.text!
        user.lastName = lastNameTextfield.text!
        user.email = emailIdTextfield.text!
        user.password = passwordTextfield.text!
        user.cPassword = reenterPasswordTextfield.text!
        user.terms = "Y"

        APIManager.sharedInstance.RegisterNewUser(register: user) { (error:NSError?) in
            if error == nil {_=self.navigationController?.popViewController(animated: true)} else {}
        }
    }

    @IBAction func termButtonPressed(_ sender: Any) {
        termCondBackGround.isHidden = false
        if screenSize.height == 568 {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                var bgframe = self.conditionView.frame
                bgframe.origin.y = 218
                self.conditionView.frame = bgframe
                var frame = self.termConditionTextView.frame
                frame.size.height = 260
                self.termConditionTextView.frame = frame
                }, completion: {(_ finished: Bool) -> Void in
            })
        } else {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                var bgframe = self.conditionView.frame
                bgframe.origin.y = 218
                bgframe.size.height = 262
                self.conditionView.frame = bgframe
                var frame = self.termConditionTextView.frame
                frame.size.height = 260
                self.termConditionTextView.frame = frame
                }, completion: {(_ finished: Bool) -> Void in
            })
        }
        self.view.endEditing(true)
    }

    @IBAction func cancelTermView(_ sender: Any) {
        termCondBackGround.isHidden = true
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var frame = self.conditionView.frame
            frame.origin.y = screenSize.height
            self.conditionView.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
                //        NSLog(@"Closed");
        })
    }


    @IBAction func cancelRegistration(_ sender: Any) {
        popTo()
        self.navigationController!.isNavigationBarHidden = false
    }

    @IBAction func agreeTermCondition(_ sender: Any) {
        if checkbox.isSelected {
            checkbox.setBackgroundImage(UIImage(named: "checkbox_uncheched.png")!, for: .normal)
            termCondition = "N"
            checkbox.isSelected = false
        }
        else {
            checkbox.setBackgroundImage(UIImage(named: "checkbox_cheched.png")!, for: .normal)
            termCondition = "Y"
            checkbox.isSelected = true
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
