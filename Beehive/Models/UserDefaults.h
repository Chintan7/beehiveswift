//
//  UserDefaults.h
//  VKMusicPlayer
//
//  Created by OlDor on 3/23/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef enum : NSUInteger
//{
//    UserDefaultsRequestIndexLatitude,
//    UserDefaultsRequestIndexLongitude,
//    UserDefaultsRequestIndexDate
//} UserDefaultsRequest;

@interface UserDefaults : NSObject

+ (instancetype _Nonnull)sharedInstance;

@property (nonatomic, strong, nullable) NSArray<NSArray<NSString *> *> *theRequestArray;
@property (nonatomic, strong, nullable) NSString *theCurrentProjectId;

@end






























