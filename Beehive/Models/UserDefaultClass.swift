////
////  UserDefaultClass.swift
////  Beehive
////
////  Created by SoluLab on 15/12/16.
////  Copyright © 2016 SoluLab. All rights reserved.
////
//
//import Foundation
//
//enum UserDefaultsRequest : Int {
//    case indexLatitude
//    case indexLongitude
//    case indexDate
//}
//
//class UserDefaultClassSwift {
//
//    var theUserDefaults = UserDefaults()
//    var theRequestArray: AnyObject?
//    var theCurrentProjectId: String?
//
//
//    public class var sharedInstance: UserDefaultClass {
//        struct Singleton {
//            static let instance: UserDefaultClass = UserDefaultClass()
//        }
//        return Singleton.instance
//    }
//
//    override init() {
//        super.init()
//        self.methodInitUserDefaults()
//    }
//
//    func methodInitUserDefaults() {
//
//        self.theUserDefaults = UserDefault.standard
//
//    }
//
//    func <#name#>(<#parameters#>) -> <#return type#> {
//        <#function body#>
//    }
//
//}
