//
//  UserDefaultClass.h
//  Beehive
//
//  Created by SoluLab on 13/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger
{
    UserDefaultsRequestIndexLatitude,
    UserDefaultsRequestIndexLongitude,
    UserDefaultsRequestIndexDate
} UserDefaultsRequest;


@interface UserDefaultClass : NSObject

@property (nonatomic, strong, nonnull) NSUserDefaults *theUserDefaults;

+ (instancetype _Nonnull)sharedInstance;

@property (nonatomic, strong, nullable) NSArray<NSArray<NSString *> *> *theRequestArray;
@property (nonatomic, strong, nullable) NSString *theCurrentProjectId;


@end
