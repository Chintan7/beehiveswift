//
//  StatsCell.m
//  Beehive
//
//  Created by OlDor on 5/27/16.
//  Copyright © 2016 SDNMACMINI13. All rights reserved.
//

#import "StatsCell.h"

@interface StatsCell ()

@property (nonatomic, strong, nonnull) UILabel *theMainLabel;

@end

@implementation StatsCell

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

- (void)setTheOldRequest:(OldRequest * _Nonnull)theOldRequest
{
    methodAssert(theOldRequest);
    if (_theOldRequest == theOldRequest)
    {
        return;
    }
    _theOldRequest = theOldRequest;
    [self createAllViews];
    
    NSDateFormatter *theDateFormatter = [NSDateFormatter new];
    theDateFormatter.locale = [NSLocale getDefaultLocale];
    theDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *theDateString = [theDateFormatter stringFromDate:theOldRequest.theDate];
    
    self.theMainLabel.text = [NSString stringWithFormat:@"%@\ntime: %@\nproject: %@", theOldRequest.theCheckINOUT, theDateString, theOldRequest.theProjectId];
}

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

- (void)createAllViews
{
    if (!self.isFirstLoad)
    {
        return;
    }
    self.isFirstLoad = NO;
    
    if (self.isAbstractCell)
    {
        return;
    }
    
    UILabel *theMainLabel;
    
    theMainLabel = [UILabel new];
    self.theMainLabel = theMainLabel;
    [self addSubview:theMainLabel];
    theMainLabel.theWidth = self.theWidth;
    theMainLabel.theHeight = [self getCalculatedHeight];
    theMainLabel.numberOfLines = 0;
    theMainLabel.font = [UIFont fontWithName:theMainLabel.font.fontName size:theMainLabel.font.pointSize/1.5];
}

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

- (double)getCalculatedHeight
{
    return 56;
//    static double theHeight = 0;
//    if (!theHeight)
//    {
//        [self createAllViews];
//        theHeight = self.theArtistLabel.theMaxY + self.theBottomSeparatorView.theHeight + self.theTitleLabel.theMinY + 4;
//    }
//    return theHeight;
}

- (void)adjustToAbstractCell:(UITableViewCell<ODTableViewCellHeightProtocol> *)abstractCell
{
    typeof(self) theCell = (id)abstractCell;
    self.theOldRequest = theCell.theOldRequest;
}

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end
