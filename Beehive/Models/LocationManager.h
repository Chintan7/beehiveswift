//
//  LocationManager.h
//  Beehive
//
//  Created by Sunil on 7/22/15.
//  Copyright (c) 2015 SDNMACMINI13. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeofenceMonitor.h"
#import "Geofence.h"
@interface LocationManager : NSObject
+ (instancetype)sharedInstance;

@property (nonatomic,strong) CLLocation *oldLocation;
@property (nonatomic,strong) CLLocation *currentLocation;
- (void)startUpdatingLocationWithGeofenceMonitor:(GeofenceMonitor *)geofenceMon withGeolist:(NSArray *)geofencesList;
@end
