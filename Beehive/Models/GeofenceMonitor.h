//
//  GeofenceMonitor.h
//  Beehive
//
//  Created by Rohit on 11/05/15.
//  Copyright (c) 2015 SDNMACMINI13. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Geofence.h"

@interface GeofenceMonitor : NSObject

-(GeofenceMonitor*)sharedInctance;
-(NSMutableArray*)monitorWithLocation:(CLLocation *)location GeoList:(NSArray*)geofences;
-(Geofence *)getGeofence:(NSDictionary *)project;
-(BOOL)isCheckedIn:(Geofence *)geofence;

@end
