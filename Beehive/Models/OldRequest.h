//
//
//
//
//  Created by od1914@my.bristol.ac.uk.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ODKit.h"

@interface OldRequest : NSManagedObject

+ (instancetype _Nonnull)methodInitWithJSONDictionary:(NSDictionary * _Nonnull)theDictionary;
+ (NSArray * _Nonnull)getObjectsArrayWithPredicate:(NSPredicate * _Nullable)thePredicate
                              propertyToFetchArray:(NSArray<NSString *> * _Nullable)thePropertyToFetchArray
                               sortDescriptorArray:(NSArray<NSSortDescriptor *> * _Nullable)theSortDescriptorArray;

@property (nonatomic, retain, nullable) NSString *theProjectId;
@property (nonatomic, retain, nullable) NSString *theCheckINOUT;
@property (nonatomic, retain, nullable) NSDate *theDate;

- (void)methodFillWithDictionary:(NSDictionary * _Nonnull)theDictionary;

@end

//@interface <#ClassName#> (CoreDataGeneratedAccessors)
//
//- (void)add<#TheList#>Object:(<#SomeClassName#> * _Nonnull)value;
//- (void)remove<#TheList#>Object:(<#SomeClassName#> * _Nonnull)value;
//- (void)add<#TheList#>:(NSSet<<#SomeClassName#> *> * _Nonnull)values;
//- (void)remove<#TheList#>:(NSSet<<#SomeClassName#> *> * _Nonnull)values;
//
//@end






























