//
//  LocationManager.m
//  Beehive
//
//  Created by Sunil on 7/22/15.
//  Copyright (c) 2015 SDNMACMINI13. All rights reserved.
//

#import "LocationManager.h"
#import "GeofenceMonitor.h"
@interface LocationManager () <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSDate *lastTimestamp;

@end

@implementation LocationManager
{
    GeofenceMonitor *geofenceMonitor;
    NSArray *geofences;
}
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        LocationManager *instance = sharedInstance;
        instance.locationManager = [CLLocationManager new];
        instance.locationManager.delegate = instance;
       // instance.locationManager.distanceFilter = 50;
        instance.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; // you can use kCLLocationAccuracyHundredMeters to get better battery life
        instance.locationManager.pausesLocationUpdatesAutomatically = NO;
    });
    
    return sharedInstance;
}

- (void)startUpdatingLocationWithGeofenceMonitor:(GeofenceMonitor *)geofenceMon withGeolist:(NSArray *)geofencesList
{
    geofenceMonitor = geofenceMon;
    geofences       = geofencesList;
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"Location services are disabled in settings.");
    }
    else
    {
        NSLog(@"In else");
        // for iOS 8
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    BOOL flag = NO;
//    CLLocation *mostRecentLocation = locations.lastObject;
   // NSLog(@"Current location: %@ %@", @(mostRecentLocation.coordinate.latitude), @(mostRecentLocation.coordinate.longitude));
    
    NSDate *now = [NSDate date];
    NSTimeInterval interval = self.lastTimestamp ? [now timeIntervalSinceDate:self.lastTimestamp] : 0;
    
    if (!self.lastTimestamp || interval >= 60)
    {
        flag = YES;
        self.lastTimestamp = now;
        NSLog(@"Sending current location to web service.");
    }
    
    
    CLLocation* newLocation = [locations lastObject];
    NSTimeInterval age = -[newLocation.timestamp timeIntervalSinceNow];
    if (age > 120) return;    // ignore old (cached) updates
    if (newLocation.horizontalAccuracy < 0) return;   // ignore invalid udpates
    // EDIT: need a valid oldLocation to be able to compute distance
    if (self.oldLocation == nil || self.oldLocation.horizontalAccuracy < 0) {
        self.oldLocation = newLocation;
        return;
    }
    CLLocationDistance distance = [newLocation distanceFromLocation: self.oldLocation];
//    NSString *newLocCoord = [NSString stringWithFormat:@"New Lat : %f Long : %f ",newLocation.coordinate.latitude, newLocation.coordinate.longitude];
//    NSString *oldLocCoord = [NSString stringWithFormat:@"Old Lat  : %f Long : %f ",self.oldLocation.coordinate.latitude, self.oldLocation.coordinate.longitude];
    
    self.oldLocation = newLocation;
    
    self.currentLocation = (CLLocation *)[locations lastObject];
    
    int distanceFilterFromServer    =   25;
    int timeBasedPingFromServer     =   40;
    if(distanceFilterFromServer<=0 && timeBasedPingFromServer<=0){
        distanceFilterFromServer    =   50;
        timeBasedPingFromServer     =   50;
    }
    
    if(flag){
        if(distance>distanceFilterFromServer)
        {
//            UIAlertView *alv = [[UIAlertView alloc]initWithTitle:@"Distance & Time Breached" message:[NSString stringWithFormat:@"%@\n%@\nDistance : %f",newLocCoord,oldLocCoord,distance] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alv show];
            [geofenceMonitor monitorWithLocation:(CLLocation *)[locations lastObject] GeoList:geofences];
            NSLog(@"Location Manager Distance breached");
            //UIAlertView *alv = [[UIAlertView alloc]initWithTitle:@"Distance & Time Breached" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alv show];
            //[geofenceMonitor monitorWithLocation:currentLocation GeoList:geofences];
        }
        else
        {
//            UIAlertView *alv = [[UIAlertView alloc]initWithTitle:@"Time Breached" message:[NSString stringWithFormat:@"%@\n%@\nDistance : %f",newLocCoord,oldLocCoord,distance] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alv show];
            NSLog(@"Location Manager Time breached.");
        }
    }
    
    /*
    else
    {
        UIAlertView *alv = [[UIAlertView alloc]initWithTitle:@"Time Breached" message:[NSString stringWithFormat:@"%@\n%@\nDistance : %f",newLocCoord,oldLocCoord,distance] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alv show];
        NSLog(@"Location Manager Time breached.");
        //UIAlertView *alv = [[UIAlertView alloc]initWithTitle:@"Time Breached" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //[alv show];
    }
     */
}

@end
