//
//
//
//
//  Created by od1914@my.bristol.ac.uk.
//
//

#import <Foundation/Foundation.h>
#import "ODKit.h"
#import <CoreData/CoreData.h>

@interface BeehiveProject : NSManagedObject

+ (instancetype __nullable)methodInitWithJSONDictionary:(NSDictionary * _Nonnull)theDictionary;
+ (void)methodDownloadAllProjectsWithNoCopyBlock:(void (^ _Nullable)(NSArray<BeehiveProject *> * _Nullable theProjectArray, NSError * _Nullable theError))theBlock;

+ (NSArray * _Nonnull)getObjectsArrayWithPredicate:(NSPredicate * _Nullable)thePredicate
                              propertyToFetchArray:(NSArray<NSString *> * _Nullable)thePropertyToFetchArray
                               sortDescriptorArray:(NSArray<NSSortDescriptor *> * _Nullable)theSortDescriptorArray;

@property (nonatomic, retain, nonnull) NSString *theCreated;
@property (nonatomic, retain, nonnull) NSString *theId;
@property (nonatomic, retain, nonnull) NSString *theModified;
@property (nonatomic, retain, nonnull) NSString *theCity;
@property (nonatomic, retain, nonnull) NSString *theCountry;
@property (nonatomic, retain, nonnull) NSString *theLatitude;
@property (nonatomic, retain, nonnull) NSString *theLongitude;
@property (nonatomic, retain, nonnull) NSString *theLocation;
@property (nonatomic, retain, nonnull) NSString *theName;
@property (nonatomic, retain, nonnull) NSString *theNote;
@property (nonatomic, retain, nonnull) NSString *thePostCode;
@property (nonatomic, retain, nonnull) NSString *theRadius;
@property (nonatomic, retain, nonnull) NSString *theState;
@property (nonatomic, retain, nonnull) NSString *theStatus;
@property (nonatomic, retain, nonnull) NSString *theStreet;
@property (nonatomic, retain, nonnull) NSString *theStreetNumber;
@property (nonatomic, retain, nonnull) NSString *theTimeZone;
@property (nonatomic, retain, nonnull) NSString *theUserId;

//@property (nonatomic, retain, nullable) NSSet<<#SomeClassName#> *> *<#TheList#>;

- (void)methodFillWithDictionary:(NSDictionary * _Nonnull)theDictionary;
@end

//@interface <#ClassName#> (CoreDataGeneratedAccessors)
//
//- (void)add<#TheList#>Object:(<#SomeClassName#> * _Nonnull)value;
//- (void)remove<#TheList#>Object:(<#SomeClassName#> * _Nonnull)value;
//- (void)add<#TheList#>:(NSSet<<#SomeClassName#> *> * _Nonnull)values;
//- (void)remove<#TheList#>:(NSSet<<#SomeClassName#> *> * _Nonnull)values;
//
//@end






























