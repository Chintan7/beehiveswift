//
//
//
//
//  Created by od1914@my.bristol.ac.uk.
//
//

#import "OldRequest.h"
#import "Beehive-Swift.h"

@interface OldRequest ()

@end

@implementation OldRequest

@dynamic theProjectId;
@dynamic theCheckINOUT;
@dynamic theDate;

#pragma mark - Class Methods (Public)

+ (instancetype _Nonnull)methodInitWithJSONDictionary:(NSDictionary * _Nonnull)theDictionary
{
    methodAssert(theDictionary);
    return [[[self class] alloc] initWithJSONDictionary:theDictionary];
}

+ (NSArray * _Nonnull)getObjectsArrayWithPredicate:(NSPredicate * _Nullable)thePredicate
                              propertyToFetchArray:(NSArray<NSString *> * _Nullable)thePropertyToFetchArray
                               sortDescriptorArray:(NSArray<NSSortDescriptor *> * _Nullable)theSortDescriptorArray
{
    NSFetchRequest *theFetchRequest = [[NSFetchRequest alloc] initWithEntityName:sfc([self class])];
    if (thePredicate)
    {
        theFetchRequest.predicate = thePredicate;
    }
    theFetchRequest.includesSubentities = NO;
    if (thePropertyToFetchArray.count)
    {
        theFetchRequest.propertiesToFetch = thePropertyToFetchArray;
    }
    if (theSortDescriptorArray.count)
    {
        theFetchRequest.sortDescriptors = theSortDescriptorArray;
    }
    return [theMOC executeFetchRequest:theFetchRequest error:nil];
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype _Nonnull)initWithJSONDictionary:(NSDictionary * _Nonnull)theDictionary
{
    methodAssert(theDictionary);
    self = [self initWithEntity:[NSEntityDescription entityForName:sfc(self.class) inManagedObjectContext:theMOC] insertIntoManagedObjectContext:theMOC];
    if (self)
    {
//        [self methodInitOldRequest];
//        [self methodFillWithDictionary:theDictionary];
    }
    return self;
}

//- (void)methodInitOldRequest
//{
//    
//}

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

//- (void)methodFillWithDictionary:(NSDictionary * _Nonnull)theDictionary
//{
//    methodAssert(theDictionary);
//    //    {
//    //        id theValue = theDictionary[sfs(@selector(<#theProperty#>))];
//    //        self.<#theProperty#> = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
//    //    }
//}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























