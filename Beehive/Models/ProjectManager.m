
//
//  ProjectManager.m
//  Beehive
//
//  Created by OlDor on 4/21/16.
//  Copyright © 2016 SDNMACMINI13. All rights reserved.
//

#import "ProjectManager.h"
#import "UserDefaultClass.h"
#import "ODKit.h"
#import "BeehiveProject.h"
#import "OldRequest.h"
#import "Global.h"
#import "Beehive-Swift.h"

@interface ProjectManager ()

@end

@implementation ProjectManager

#pragma mark - Class Methods (Public)

+ (instancetype _Nonnull)sharedInstance
{
    static id theSharedInstance;
    static dispatch_once_t theOnceToken;
    dispatch_once(&theOnceToken, ^
                  {
                      theSharedInstance = [[[self class] alloc] initSharedInstance];
                  });
    return theSharedInstance;
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype _Nonnull)initSharedInstance
{
    self = [super init];
    if (self)
    {
        [self methodInitProjectManager];
    }
    return self;
}

- (void)methodInitProjectManager
{
    _isInProgress = NO;
}

#pragma mark - Setters (Public)

- (void)setIsInProgress:(BOOL)isInProgress
{
    if (_isInProgress == isInProgress)
    {
        return;
    }
    _isInProgress = isInProgress;
}

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodProcessLocation:(CLLocation * _Nonnull)theLocation
{
    methodAssert(theLocation);
    
    NSDateFormatter *theDateFormatter = [NSDateFormatter new];
    theDateFormatter.locale = [NSLocale getDefaultLocale];
    theDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *theDateString = [theDateFormatter stringFromDate:[NSDate date]];
    
    //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:[NSString stringWithFormat:@"--------- checkinCheckOutTimeIntervel --------- \n methodProcessLocation for Location %@ on time %@ --------- \n",theLocation,theDateString]];
    
    NSMutableArray *theArray = [NSMutableArray new];
    [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
    
    [theArray addObject:@[@(theLocation.coordinate.latitude), @(theLocation.coordinate.longitude), theDateString]];
    [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
    
    if (!self.isInProgress)
    {
        [self methodSendAllInfo];
    }
}

- (void)methodSendAllInfo
{
    self.isInProgress = YES;
    [BeehiveProject methodDownloadAllProjectsWithNoCopyBlock:^(NSArray<BeehiveProject *> * _Nullable theProjectArray, NSError * _Nullable theError)
     {
         if (theProjectArray.count && !theError)
         {
             NSArray<NSArray *> *theRequestArray = [UserDefaultClass sharedInstance].theRequestArray;
             if (!theRequestArray.count)
             {
                 NSLog(@"no coordinates to send");
                 self.isInProgress = NO;
                 return;
             }
             
             NSArray *theObject = theRequestArray.firstObject;
             
             double theLatitude = ((NSNumber *)theObject[0]).doubleValue;
             double theLongitude = ((NSNumber *)theObject[1]).doubleValue;
             CLLocation *theCoordinateLocation = [[CLLocation alloc] initWithLatitude:theLatitude longitude:theLongitude];
             NSString *theTimeStampString = theObject[2];
             CLLocationCoordinate2D theLocationCoordinate2D = CLLocationCoordinate2DMake(theLatitude, theLongitude);
             
             double theClosestDistance = -1;
             BeehiveProject *theClosestProject;
             
             for (NSUInteger j = 0; j < theProjectArray.count; j++)
             {
                 BeehiveProject *theProject = theProjectArray[j];
                 CLCircularRegion *theCircularRegion = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(theProject.theLatitude.doubleValue, theProject.theLongitude.doubleValue)
                                                                                         radius:theProject.theRadius.doubleValue
                                                                                     identifier:theProject.theId];

                 if ([theCircularRegion containsCoordinate:theLocationCoordinate2D])
                 {
                     CLLocation *theRegionLocation = [[CLLocation alloc] initWithLatitude:theProject.theLatitude.doubleValue longitude:theProject.theLongitude.doubleValue];
                     double theDistance = [theCoordinateLocation distanceFromLocation:theRegionLocation];
                     NSLog(@"Distance From Project Location - %f",theDistance);
                     
                     //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:[NSString stringWithFormat:@"Distance From Project Location %f meters",theDistance]];
                     if (theClosestDistance < 0 || theDistance < theClosestDistance)
                     {
                         theClosestDistance = theDistance;
                         theClosestProject = theProject;
                     }
                 }
             }
             
             BOOL isTest = NO;
             if (isTest)
             {
                 if ([UserDefaultClass sharedInstance].theCurrentProjectId)
                 {
                     [self methodCheckIn:NO
                               projectId:[UserDefaultClass sharedInstance].theCurrentProjectId
                         timeStampString:theTimeStampString
                             noCopyBlock:^
                      {
                          NSMutableArray *theArray = [NSMutableArray new];
                          [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                          [theArray removeObjectAtIndex:0];
                          [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                          [UserDefaultClass sharedInstance].theCurrentProjectId = nil;
                          [self methodSendAllInfo];
                          [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                      }];
                     return;
                 }
                 else
                 {
                     theClosestProject = [BeehiveProject getObjectsArrayWithPredicate:nil propertyToFetchArray:nil sortDescriptorArray:nil].firstObject;
                     [self methodCheckIn:YES
                               projectId:theClosestProject.theId
                         timeStampString:theTimeStampString
                             noCopyBlock:^
                      {
                          NSMutableArray *theArray = [NSMutableArray new];
                          [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                          [theArray removeObjectAtIndex:0];
                          [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                          [UserDefaultClass sharedInstance].theCurrentProjectId = theClosestProject.theId;
                          [self methodSendAllInfo];
                          [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                      }];
                     return;
                 }
             }
             else
             {
                 if (isEqual([UserDefaultClass sharedInstance].theCurrentProjectId, theClosestProject.theId))
                 {
                     NSMutableArray *theArray = [NSMutableArray new];
                     [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                     [theArray removeObjectAtIndex:0];
                     [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                     [self methodSendAllInfo];
                     [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                     //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" check isEqualProjectID Already checked in Loop Call \n"];
                     //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:[NSString stringWithFormat:@"Latitude - %f Longitude %f \n",theLatitude,theLongitude]];
                     NSLog(@"coordinate deleted");
                     return;
                 }
                 else
                 {
                     if ([UserDefaultClass sharedInstance].theCurrentProjectId)
                     {
                         //                         [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool NO with before call web-service \n"];
                         //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool NO with before call web-service \n --------------- Checkout --------------- \n"];
                         [self methodCheckIn:NO
                                   projectId:[UserDefaultClass sharedInstance].theCurrentProjectId
                             timeStampString:theTimeStampString
                                 noCopyBlock:^
                          {
                             // [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" --------------- got the current project id with bool NO with after call web-service --------------- \n --------------- Checkout --------------- \n"];
                              NSMutableArray *theArray = [NSMutableArray new];
                              [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                              [theArray removeObjectAtIndex:0];
                              [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                              [UserDefaultClass sharedInstance].theCurrentProjectId = nil;
                              [self methodSendAllInfo];
                              [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                          }];
                         return;
                     }
                     else
                     {
                         
                         //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@"--------------- got the current project id with bool YES before webservice call --------------- \n --------------- Check In --------------- \n"];
                         
                         [self methodCheckIn:YES
                                   projectId:theClosestProject.theId
                             timeStampString:theTimeStampString
                                 noCopyBlock:^
                          {
                              //[((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool YES after webservice call --------------- \n --------------- Check In --------------- \n"];
                              NSMutableArray *theArray = [NSMutableArray new];
                              [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                              [theArray removeObjectAtIndex:0];
                              [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                              [UserDefaultClass sharedInstance].theCurrentProjectId = theClosestProject.theId;
                              [self methodSendAllInfo];
                              [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                          }];
                         return;
                     }
                 }
                 /*
                  if ([UserDefaultClass sharedInstance].theCurrentProjectId)
                  {
                  [self methodCheckIn:NO
                  projectId:[UserDefaultClass sharedInstance].theCurrentProjectId
                  timeStampString:theTimeStampString
                  noCopyBlock:^
                  {
                  NSMutableArray *theArray = [NSMutableArray new];
                  [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                  [theArray removeObjectAtIndex:0];
                  [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                  [UserDefaultClass sharedInstance].theCurrentProjectId = nil;
                  [self methodSendAllInfo];
                  [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                  }];
                  return;
                  }
                  else
                  {
                  NSMutableArray *theArray = [NSMutableArray new];
                  [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
                  [theArray removeObjectAtIndex:0];
                  [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
                  [self methodSendAllInfo];
                  [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
                  NSLog(@"coordinate deleted in the end");
                  return;
                  }*/
             }
             
             
             
             NSLog(@"coordinate ignored");
             self.isInProgress = NO;
         }
         else
         {
             self.isInProgress = NO;
             //             [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
         }
     }];
}

#pragma mark - Methods (Private)

- (void)methodCheckIn:(BOOL)isCheckIn
            projectId:(NSString * _Nonnull)theProjectId
      timeStampString:(NSString * _Nonnull)theTimeStampString
          noCopyBlock:(void (^ _Nonnull)())theBlock
{
    
    NSString *strURL=[NSString stringWithFormat:@"%@%@", BaseWebRequestUrl, (isCheckIn ? CheckInUrl : CheckOutUrl)];
    strURL = [strURL stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *emailString =[pref objectForKey:@"UserId"];
    //    [pref objectForKey:@"email"];
    NSString *userTokenString = [pref objectForKey:@"User_Token"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", emailString, userTokenString];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@",[authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:authValue forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    
    NSMutableDictionary *mapData = [[NSMutableDictionary alloc] init];
    [mapData setObject:theProjectId forKey:@"project_id"];
    [mapData setObject:theTimeStampString forKey:(isCheckIn ? @"check_in_time" : @"check_out_time")];
    [mapData setObject:@"0" forKey:@"auto"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:postData];
    
    //[((AppDelegate *)[UIApplication sharedApplication].delegate) checkINCheckoutWithDict:mapData];
    
    //    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    //    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                  
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      
                                      
                                      NSArray *arr =[OldRequest getObjectsArrayWithPredicate:nil propertyToFetchArray:nil sortDescriptorArray:nil];
                                      
                                      if (error) {
                                          // Handle error...
                                          
                                          
                                          
                                          OldRequest *theOldRequest = [OldRequest methodInitWithJSONDictionary:@{}];
                                          theOldRequest.theProjectId = @"request failed!";
                                          theOldRequest.theDate = [NSDate date];
                                          theOldRequest.theCheckINOUT = isCheckIn ? @"checkIN" : @"checkOUT";
                                          
                                          
                                          if (![arr containsObject:theOldRequest]){
                                              [theOldRequest.managedObjectContext save:nil];
                                          }
                                          
                                          //                                          [theOldRequest.managedObjectContext save:nil];
                                          [((AppDelegate *)[UIApplication sharedApplication].delegate) methodReloadStatsArray];
                                          
                                          if (theBlock)
                                          {
                                              theBlock();
                                          }
                                          
                                      } else {
                                          NSDictionary *theJSONDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                                          
                                          OldRequest *theOldRequest = [OldRequest methodInitWithJSONDictionary:@{}];
                                          theOldRequest.theProjectId = theProjectId;
                                          theOldRequest.theDate = [NSDate date];
                                          theOldRequest.theCheckINOUT = isCheckIn ? @"checkIN" : @"checkOUT";
                                          
                                          if (![arr containsObject:theOldRequest]){
                                              [theOldRequest.managedObjectContext save:nil];
                                          }
                                          //                                          for (OldRequest *req in arr){
                                          //                                              if ([req.theCheckINOUT isEqualToString:theOldRequest.theCheckINOUT] && [req.theDate isEqual:theOldRequest.theDate] && [req.theProjectId isEqualToString:theOldRequest.theProjectId]) {
                                          //                                                  break;
                                          //                                              } else {
                                          //                                                  [theOldRequest.managedObjectContext save:nil];
                                          //                                                  break;
                                          //                                              }
                                          //                                          }
                                          
                                          [((AppDelegate *)[UIApplication sharedApplication].delegate) methodReloadStatsArray];
                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckInNotification" object:nil];
                                          
                                          if (theBlock)
                                          {
                                              theBlock();
                                          }
                                          
                                      }
                                      
                                      //                                      [[NSNotificationCenter defaultCenter] postNotification:@"CheckInNotification"];
                                  }];
    [task resume];
    
    //
    //    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    //    [request setDelegate:self];
    //    [request addRequestHeader:@"Authorization" value:authValue];
    //    [request setPostValue:theProjectId forKey:@"project_id"];
    //    [request setPostValue:theTimeStampString forKey:(isCheckIn ? @"check_in_time" : @"check_out_time")];
    //    [request setPostValue:@"0" forKey:@"auto"];
    //    [request setRequestMethod:@"POST"];
    //    [request setCompletionBlock:^
    //     {
    //         NSLog(@"request success for isCheckIn:%zd, projectId:%@,\nDATE theTimeStampString:%@", isCheckIn, theProjectId, theTimeStampString);
    //
    //     }];
    //    [request setFailedBlock:^
    //     {
    //         NSLog(@"request failed for isCheckIn:%zd, projectId:%@,\nDATE theTimeStampString:%@", isCheckIn, theProjectId, theTimeStampString);
    //              }];
    //    [request startAsynchronous];
}

#pragma mark - Standard Methods

@end

////
////  ProjectManager.m
////  Beehive
////
////  Created by OlDor on 4/21/16.
////  Copyright © 2016 SDNMACMINI13. All rights reserved.
////
//
//#import "ProjectManager.h"
//#import "UserDefaultClass.h"
//#import "ODKit.h"
//#import "BeehiveProject.h"
//#import "OldRequest.h"
//#import "Global.h"
//#import "Beehive-Swift.h"
//
//@interface ProjectManager ()
//
//@end
//
//@implementation ProjectManager
//
//#pragma mark - Class Methods (Public)
//
//+ (instancetype _Nonnull)sharedInstance
//{
//    static id theSharedInstance;
//    static dispatch_once_t theOnceToken;
//    dispatch_once(&theOnceToken, ^
//                  {
//                      theSharedInstance = [[[self class] alloc] initSharedInstance];
//                  });
//    return theSharedInstance;
//}
//
//#pragma mark - Class Methods (Private)
//
//#pragma mark - Init & Dealloc
//
//- (instancetype _Nonnull)initSharedInstance
//{
//    self = [super init];
//    if (self)
//    {
//        [self methodInitProjectManager];
//    }
//    return self;
//}
//
//- (void)methodInitProjectManager
//{
//    _isInProgress = NO;
//}
//
//#pragma mark - Setters (Public)
//
//- (void)setIsInProgress:(BOOL)isInProgress
//{
//    if (_isInProgress == isInProgress)
//    {
//        return;
//    }
//    _isInProgress = isInProgress;
//}
//
//#pragma mark - Getters (Public)
//
//#pragma mark - Setters (Private)
//
//#pragma mark - Getters (Private)
//
//#pragma mark - Actions
//
//#pragma mark - Gestures
//
//#pragma mark - Notifications
//
//#pragma mark - Delegates ()
//
//#pragma mark - Methods (Public)
//
//- (void)methodProcessLocation:(CLLocation * _Nonnull)theLocation
//{
//    methodAssert(theLocation);
//    
//    NSDateFormatter *theDateFormatter = [NSDateFormatter new];
//    theDateFormatter.locale = [NSLocale getDefaultLocale];
//    theDateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
//    NSString *theDateString = [theDateFormatter stringFromDate:[NSDate date]];
//    
//    NSMutableArray *theArray = [NSMutableArray new];
//    [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//
//    [theArray addObject:@[@(theLocation.coordinate.latitude), @(theLocation.coordinate.longitude), theDateString]];
//    [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//    
//    if (!self.isInProgress)
//    {
//        [self methodSendAllInfo];
//    }
//}
//
//- (void)methodSendAllInfo
//{
//    self.isInProgress = YES;
//    [BeehiveProject methodDownloadAllProjectsWithNoCopyBlock:^(NSArray<BeehiveProject *> * _Nullable theProjectArray, NSError * _Nullable theError)
//     {
//         if (theProjectArray.count && !theError)
//         {
//             NSArray<NSArray *> *theRequestArray = [UserDefaultClass sharedInstance].theRequestArray;
//             if (!theRequestArray.count)
//             {
//                 NSLog(@"no coordinates to send");
//                 self.isInProgress = NO;
//                 return;
//             }
//             
//             NSArray *theObject = theRequestArray.firstObject;
//             
//             double theLatitude = ((NSNumber *)theObject[0]).doubleValue;
//             double theLongitude = ((NSNumber *)theObject[1]).doubleValue;
//             CLLocation *theCoordinateLocation = [[CLLocation alloc] initWithLatitude:theLatitude longitude:theLongitude];
//             NSString *theTimeStampString = theObject[2];
//             CLLocationCoordinate2D theLocationCoordinate2D = CLLocationCoordinate2DMake(theLatitude, theLongitude);
//             
//             double theClosestDistance = -1;
//             BeehiveProject *theClosestProject;
//             
//             for (NSUInteger j = 0; j < theProjectArray.count; j++)
//             {
//                 BeehiveProject *theProject = theProjectArray[j];
//                 CLCircularRegion *theCircularRegion = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake(theProject.theLatitude.doubleValue, theProject.theLongitude.doubleValue)
//                                                                                         radius:theProject.theRadius.doubleValue
//                                                                                     identifier:theProject.theId];
//                 
//                 if ([theCircularRegion containsCoordinate:theLocationCoordinate2D])
//                 {
//                     CLLocation *theRegionLocation = [[CLLocation alloc] initWithLatitude:theProject.theLatitude.doubleValue longitude:theProject.theLongitude.doubleValue];
//                     double theDistance = [theCoordinateLocation distanceFromLocation:theRegionLocation];
//                     if (theClosestDistance < 0 || theDistance < theClosestDistance)
//                     {
//                         theClosestDistance = theDistance;
//                         theClosestProject = theProject;
//                     }
//                }
//             }
//             
//             BOOL isTest = NO;
//             if (isTest)
//             {
//                 if ([UserDefaultClass sharedInstance].theCurrentProjectId)
//                 {
//                     [self methodCheckIn:NO
//                               projectId:[UserDefaultClass sharedInstance].theCurrentProjectId
//                         timeStampString:theTimeStampString
//                             noCopyBlock:^
//                      {
//                          NSMutableArray *theArray = [NSMutableArray new];
//                          [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                          [theArray removeObjectAtIndex:0];
//                          [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                          [UserDefaultClass sharedInstance].theCurrentProjectId = nil;
//                          [self methodSendAllInfo];
//                          [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                      }];
//                     return;
//                 }
//                 else
//                 {
//                     theClosestProject = [BeehiveProject getObjectsArrayWithPredicate:nil propertyToFetchArray:nil sortDescriptorArray:nil].firstObject;
//                     [self methodCheckIn:YES
//                               projectId:theClosestProject.theId
//                         timeStampString:theTimeStampString
//                             noCopyBlock:^
//                      {
//                          NSMutableArray *theArray = [NSMutableArray new];
//                          [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                          [theArray removeObjectAtIndex:0];
//                          [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                          [UserDefaultClass sharedInstance].theCurrentProjectId = theClosestProject.theId;
//                          [self methodSendAllInfo];
//                          [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                      }];
//                     return;
//                 }
//             }
//             else
//             {
//                 if (isEqual([UserDefaultClass sharedInstance].theCurrentProjectId, theClosestProject.theId))
//                 {
//                     NSMutableArray *theArray = [NSMutableArray new];
//                     [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                     [theArray removeObjectAtIndex:0];
//                     [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                     [self methodSendAllInfo];
//                     [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                     [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" check isEqualProjectID Already checked in Loop Call \n"];
//                     NSLog(@"coordinate deleted");
//                     return;
//                 }
//                 else
//                 {
//                     if ([UserDefaultClass sharedInstance].theCurrentProjectId)
//                     {
//                         [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool NO with before call web-service \n"];
//                         [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool NO with before call web-service -- Checkout \n"];
//                         [self methodCheckIn:NO
//                                   projectId:[UserDefaultClass sharedInstance].theCurrentProjectId
//                             timeStampString:theTimeStampString
//                                 noCopyBlock:^
//                          {
//                              [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool NO with after call web-service -- Checkout \n"];
//                              NSMutableArray *theArray = [NSMutableArray new];
//                              [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                              [theArray removeObjectAtIndex:0];
//                              [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                              [UserDefaultClass sharedInstance].theCurrentProjectId = nil;
//                              [self methodSendAllInfo];
//                              [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                          }];
//                         return;
//                     }
//                     else
//                     {
//                         
//                         [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool YES before webservice call - check in \n"];
//                         
//                         [self methodCheckIn:YES
//                                   projectId:theClosestProject.theId
//                             timeStampString:theTimeStampString
//                                 noCopyBlock:^
//                          {
//                              [((AppDelegate *)[UIApplication sharedApplication].delegate) setLogWithStr:@" got the current project id with bool YES after webservice call - check in \n"];
//                              NSMutableArray *theArray = [NSMutableArray new];
//                              [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                              [theArray removeObjectAtIndex:0];
//                              [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                              [UserDefaultClass sharedInstance].theCurrentProjectId = theClosestProject.theId;
//                              [self methodSendAllInfo];
//                              [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                          }];
//                         return;
//                     }
//                 }
//               /*
//                 if ([UserDefaultClass sharedInstance].theCurrentProjectId)
//                 {
//                     [self methodCheckIn:NO
//                               projectId:[UserDefaultClass sharedInstance].theCurrentProjectId
//                         timeStampString:theTimeStampString
//                             noCopyBlock:^
//                      {
//                          NSMutableArray *theArray = [NSMutableArray new];
//                          [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                          [theArray removeObjectAtIndex:0];
//                          [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                          [UserDefaultClass sharedInstance].theCurrentProjectId = nil;
//                          [self methodSendAllInfo];
//                          [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                      }];
//                     return;
//                 }
//                 else
//                 {
//                     NSMutableArray *theArray = [NSMutableArray new];
//                     [theArray addObjectsFromArray:[UserDefaultClass sharedInstance].theRequestArray];
//                     [theArray removeObjectAtIndex:0];
//                     [UserDefaultClass sharedInstance].theRequestArray = theArray.copy;
//                     [self methodSendAllInfo];
//                     [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//                     NSLog(@"coordinate deleted in the end");
//                     return;
//                 }*/
//             }
//             
//             
//             
//             NSLog(@"coordinate ignored");
//             self.isInProgress = NO;
//         }
//         else
//         {
//             self.isInProgress = NO;
////             [[NSNotificationCenter defaultCenter] postNotificationName:keyProjectManagerDidCheckInOutNotification object:nil];
//         }
//     }];
//}
//
//#pragma mark - Methods (Private)
//
//- (void)methodCheckIn:(BOOL)isCheckIn
//            projectId:(NSString * _Nonnull)theProjectId
//      timeStampString:(NSString * _Nonnull)theTimeStampString
//          noCopyBlock:(void (^ _Nonnull)())theBlock
//{
//
//    
//    
//    NSString *strURL=[NSString stringWithFormat:@"%@%@", BaseWebRequestUrl, (isCheckIn ? CheckInUrl : CheckOutUrl)];
//    strURL = [strURL stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//
//    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
//    NSString *emailString =[pref objectForKey:@"UserId"];
//    //    [pref objectForKey:@"email"];
//    NSString *userTokenString = [pref objectForKey:@"User_Token"];
//    NSString *authStr = [NSString stringWithFormat:@"%@:%@", emailString, userTokenString];
//    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *authValue = [NSString stringWithFormat:@"Basic %@",[authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
//    
//    NSError *error;
//    
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
//    NSURL *url = [NSURL URLWithString:strURL];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:60.0];
//    
//    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    [request addValue:authValue forHTTPHeaderField:@"Authorization"];
//    [request setHTTPMethod:@"POST"];
//    
//
//
//    NSMutableDictionary *mapData = [[NSMutableDictionary alloc] init];
//    [mapData setObject:theProjectId forKey:@"project_id"];
//    [mapData setObject:theTimeStampString forKey:(isCheckIn ? @"check_in_time" : @"check_out_time")];
//    [mapData setObject:@"0" forKey:@"auto"];
//
//    NSData *postData = [NSJSONSerialization dataWithJSONObject:mapData options:NSJSONWritingPrettyPrinted error:nil];
//    [request setHTTPBody:postData];
//
//    [((AppDelegate *)[UIApplication sharedApplication].delegate) checkINCheckoutWithDict:mapData];
//
//            //    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//
//    
//    //    [request setHTTPBody:postData];
//
//    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
//
//                                            completionHandler:
//                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
//
//                                      
//                                      NSArray *arr =[OldRequest getObjectsArrayWithPredicate:nil propertyToFetchArray:nil sortDescriptorArray:nil];
//                                      
//                                      if (error) {
//                                          // Handle error...
//
//
//                                          
//                                          OldRequest *theOldRequest = [OldRequest methodInitWithJSONDictionary:@{}];
//                                          theOldRequest.theProjectId = @"request failed!";
//                                          theOldRequest.theDate = [NSDate date];
//                                          theOldRequest.theCheckINOUT = isCheckIn ? @"checkIN" : @"checkOUT";
//                                          
//                                          
//                                          if (![arr containsObject:theOldRequest]){
//                                              [theOldRequest.managedObjectContext save:nil];
//                                          }
//                        
////                                          [theOldRequest.managedObjectContext save:nil];
//                                          [((AppDelegate *)[UIApplication sharedApplication].delegate) methodReloadStatsArray];
//
//                                          if (theBlock)
//                                          {
//                                              theBlock();
//                                          }
//    
//                                      } else {
//                                          NSDictionary *theJSONDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
//                                          
//                                          OldRequest *theOldRequest = [OldRequest methodInitWithJSONDictionary:@{}];
//                                          theOldRequest.theProjectId = theProjectId;
//                                          theOldRequest.theDate = [NSDate date];
//                                          theOldRequest.theCheckINOUT = isCheckIn ? @"checkIN" : @"checkOUT";
//                                          
//                                          if (![arr containsObject:theOldRequest]){
//                                              [theOldRequest.managedObjectContext save:nil];
//                                          }
////                                          for (OldRequest *req in arr){
////                                              if ([req.theCheckINOUT isEqualToString:theOldRequest.theCheckINOUT] && [req.theDate isEqual:theOldRequest.theDate] && [req.theProjectId isEqualToString:theOldRequest.theProjectId]) {
////                                                  break;
////                                              } else {
////                                                  [theOldRequest.managedObjectContext save:nil];
////                                                  break;
////                                              }
////                                          }
//                                          
//                                          [((AppDelegate *)[UIApplication sharedApplication].delegate) methodReloadStatsArray];
//                                          [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckInNotification" object:nil];
//                                          
//                                          if (theBlock)
//                                          {
//                                              theBlock();
//                                          }
//                                         
//                                      }
//                                      
////                                      [[NSNotificationCenter defaultCenter] postNotification:@"CheckInNotification"];
//                                  }];
//    [task resume];
//
////
////    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
////    [request setDelegate:self];
////    [request addRequestHeader:@"Authorization" value:authValue];
////    [request setPostValue:theProjectId forKey:@"project_id"];
////    [request setPostValue:theTimeStampString forKey:(isCheckIn ? @"check_in_time" : @"check_out_time")];
////    [request setPostValue:@"0" forKey:@"auto"];
////    [request setRequestMethod:@"POST"];
////    [request setCompletionBlock:^
////     {
////         NSLog(@"request success for isCheckIn:%zd, projectId:%@,\nDATE theTimeStampString:%@", isCheckIn, theProjectId, theTimeStampString);
////
////     }];
////    [request setFailedBlock:^
////     {
////         NSLog(@"request failed for isCheckIn:%zd, projectId:%@,\nDATE theTimeStampString:%@", isCheckIn, theProjectId, theTimeStampString);
////              }];
////    [request startAsynchronous];
//}
//
//#pragma mark - Standard Methods
//
//@end
