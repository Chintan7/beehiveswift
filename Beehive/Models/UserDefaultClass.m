//
//  UserDefaultClass.m
//  Beehive
//
//  Created by SoluLab on 13/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

#import "UserDefaultClass.h"
#import "ODKit.h"
#import "Beehive-Swift.h"

@implementation UserDefaultClass

+ (instancetype _Nonnull)sharedInstance
{
    static UserDefaultClass *theSharedInstance;
    static dispatch_once_t theOnceToken;
    dispatch_once(&theOnceToken, ^
                  {
                  theSharedInstance = [[[UserDefaultClass class] alloc] initSharedInstance];
                  });
    return theSharedInstance;
}

- (instancetype _Nonnull)initSharedInstance
{
    self = [super init];
    if (self)
        {
        [self methodInitUserDefaults];
        }
    return self;
}

- (void)methodInitUserDefaults
{
    _theUserDefaults = [NSUserDefaults standardUserDefaults];
}


- (void)setTheRequestArray:(NSArray<NSArray<NSString *> *> * _Nullable)theRequestArray
{
    if (isEqual(self.theRequestArray, theRequestArray))
        {
        return;
        }
    [self.theUserDefaults setValue:theRequestArray forKey:sfs(@selector(theRequestArray))];
    [self.theUserDefaults synchronize];
}

- (void)setTheCurrentProjectId:(NSString * _Nullable)theCurrentProjectId
{
    if (isEqual(self.theCurrentProjectId, theCurrentProjectId))
        {
        return;
        }
    [self.theUserDefaults setValue:theCurrentProjectId forKey:sfs(@selector(theCurrentProjectId))];
    [self.theUserDefaults synchronize];
    [((AppDelegate *)[UIApplication sharedApplication].delegate).navController.navigationBar addSubview:[Global checkedInModeButton]];
}

#pragma mark - Getters (Public)

- (NSArray<NSArray<NSString *> *> * _Nullable)theRequestArray
{
    return [self.theUserDefaults valueForKey:sfs(@selector(theRequestArray))];
}

- (NSString * _Nullable)theCurrentProjectId
{
    return [self.theUserDefaults valueForKey:sfs(@selector(theCurrentProjectId))];
}



@end
