//
//  UserDefaults.m
//  VKMusicPlayer
//
//  Created by OlDor on 3/23/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import "UserDefaults.h"
#import "ODKit.h"
#import "Beehive-Swift.h"

@interface UserDefaults ()

@property (nonatomic, strong, nonnull) NSUserDefaults *theUserDefaults;

@end

@implementation UserDefaults

#pragma mark - Class Methods (Public)

+ (instancetype _Nonnull)sharedInstance
{
    static UserDefaults *theSharedInstance;
    static dispatch_once_t theOnceToken;
    dispatch_once(&theOnceToken, ^
                  {
                      theSharedInstance = [[[UserDefaults class] alloc] initSharedInstance];
                  });
    return theSharedInstance;
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype _Nonnull)initSharedInstance
{
    self = [super init];
    if (self)
    {
        [self methodInitUserDefaults];
    }
    return self;
}

- (void)methodInitUserDefaults
{
    _theUserDefaults = [NSUserDefaults standardUserDefaults];
}

#pragma mark - Setters (Public)

- (void)setTheRequestArray:(NSArray<NSArray<NSString *> *> * _Nullable)theRequestArray
{
    if (isEqual(self.theRequestArray, theRequestArray))
    {
        return;
    }
    [self.theUserDefaults setValue:theRequestArray forKey:sfs(@selector(theRequestArray))];
    [self.theUserDefaults synchronize];
}

- (void)setTheCurrentProjectId:(NSString * _Nullable)theCurrentProjectId
{
    if (isEqual(self.theCurrentProjectId, theCurrentProjectId))
        {
        return;
        }
    [self.theUserDefaults setValue:theCurrentProjectId forKey:sfs(@selector(theCurrentProjectId))];
    [self.theUserDefaults synchronize];
    [((AppDelegate *)[UIApplication sharedApplication].delegate).navController.navigationBar addSubview:[Global checkedInModeButton]];
}

#pragma mark - Getters (Public)

- (NSArray<NSArray<NSString *> *> * _Nullable)theRequestArray
{
    return [self.theUserDefaults valueForKey:sfs(@selector(theRequestArray))];
}

- (NSString * _Nullable)theCurrentProjectId
{
    return [self.theUserDefaults valueForKey:sfs(@selector(theCurrentProjectId))];
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























