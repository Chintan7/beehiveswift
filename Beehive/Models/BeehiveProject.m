	//
//
//
//
//  Created by od1914@my.bristol.ac.uk.
//
//

#import "BeehiveProject.h"
#import "ODKit.h"
#import "Global.h"
#import "Beehive-Swift.h"

@interface BeehiveProject ()

@end

@implementation BeehiveProject

@dynamic theCreated;
@dynamic theId;
@dynamic theModified;
@dynamic theCity;
@dynamic theCountry;
@dynamic theLatitude;
@dynamic theLongitude;
@dynamic theLocation;
@dynamic theName;
@dynamic theNote;
@dynamic thePostCode;
@dynamic theRadius;
@dynamic theState;
@dynamic theStatus;
@dynamic theStreet;
@dynamic theStreetNumber;
@dynamic theTimeZone;
@dynamic theUserId;

#pragma mark - Class Methods (Public)

+ (instancetype)methodInitWithJSONDictionary:(NSDictionary * _Nonnull)theDictionary
{
//    methodAssert(theDictionary);
    return [[[self class] alloc] initWithJSONDictionary:theDictionary];
}

+ (void)methodDownloadAllProjectsWithNoCopyBlock:(void (^ _Nullable)(NSArray<BeehiveProject *> * _Nullable theProjectArray, NSError * _Nullable theError))theBlock
{
    NSUInteger thePageCounter = 1;
    NSString *requestString = [NSString stringWithFormat:@"getMyProjects.json?role_id=4&page=%zd", thePageCounter];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@", BaseWebRequestUrl, requestString];
    strURL = [strURL stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *url = [NSURL URLWithString:strURL];

    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *emailString = [pref objectForKey:@"UserId"];
//    [pref objectForKey:@"email"];
    NSString *userTokenString = [pref objectForKey:@"User_Token"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", emailString, userTokenString];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@",[authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];


//    NSString *post = [NSString stringWithFormat:@"username=%@&password=%@",username, password];
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    NSString *postLength = [NSString stringWithFormat:@"%lu" , (unsigned long)[postData length]];
//    NSURL *url = [NSURL URLWithString:@"http://demo.redmine.org/login"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];

    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
//    [request setHTTPBody:postData];

    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *task = [session dataTaskWithRequest:request

                               
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {

                                      if (error) {
                                          
                                          if (theBlock)
                                          {
                                              theBlock(nil, error);
                                          }
                                        // Handle error...
                                          return;
                                      }

                                      if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                          NSLog(@"Response HTTP Status code: %ld\n", (long)[(NSHTTPURLResponse *)response statusCode]);
                                          NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *)response allHeaderFields]);
                                      }

                                      NSDictionary *theJSONDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

                                      if (isEqual(theJSONDictionary[@"result"][@"status"], @"OK"))
                                          {
                                          NSArray *theArray = [BeehiveProject getObjectsArrayWithPredicate:nil
                                                                                      propertyToFetchArray:nil
                                                                                       sortDescriptorArray:nil];
                                          for (NSUInteger i = 0; i < theArray.count; i++)
                                              {
                                              BeehiveProject *theObject = theArray[i];
                                              NSLog(@"%@",theObject.theCreated);
                                              [theMOC deleteObject:theObject];
                                              }

                                          NSMutableArray *theFinalArray = [NSMutableArray new];
                                          NSArray *theProjectsArray = theJSONDictionary[@"projects"];
                                          NSLog(@"COUNT: %zd", theProjectsArray.count);
                                          for (NSUInteger i = 0; i < theProjectsArray.count; i++)
                                              {
                                              BeehiveProject *theProject = [BeehiveProject methodInitWithJSONDictionary:theProjectsArray[i][@"Project"]];

                                              [theFinalArray addObject:theProject];
                                              }

                                          [theMOC save:nil];
                                          if (theBlock)
                                              {
                                              theBlock(theFinalArray, nil);
                                              }
                                          }
                                      else
                                          {
                                          if (theBlock)
                                              {
                                              theBlock(nil, nil);
                                              }
                                          }

//                                      NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                                      NSLog(@"Response Body:\n%@\n", body);
                                  }];
    [task resume];


    /*
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSString *emailString = [pref objectForKey:@"UserId"];
    NSString *userTokenString = [pref objectForKey:@"User_Token"];
    NSString *authStr = [NSString stringWithFormat:@"%@:%@", emailString, userTokenString];
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat:@"Basic %@",[authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn]];
    [request addRequestHeader:@"Authorization" value:authValue];
    [request setTag:1];
    [request setRequestMethod:@"GET"];
    weakify(request);
    [request setCompletionBlock:^
     {
         strongify(request);
         
         NSDictionary *theJSONDictionary = request.responseString.JSONValue;
         if (isEqual(theJSONDictionary[@"result"][@"status"], @"OK"))
         {
             NSArray *theArray = [BeehiveProject getObjectsArrayWithPredicate:nil
                                                         propertyToFetchArray:nil
                                                          sortDescriptorArray:nil];
             for (NSUInteger i = 0; i < theArray.count; i++)
             {
                 BeehiveProject *theObject = theArray[i];
                 [theMOC deleteObject:theObject];
             }
             
             NSMutableArray *theFinalArray = [NSMutableArray new];
             NSArray *theProjectsArray = theJSONDictionary[@"projects"];
             NSLog(@"COUNT: %zd", theProjectsArray.count);
             for (NSUInteger i = 0; i < theProjectsArray.count; i++)
             {
                 BeehiveProject *theProject = [BeehiveProject methodInitWithJSONDictionary:theProjectsArray[i][@"Project"]];
                 [theFinalArray addObject:theProject];
             }
             [theMOC save:nil];
             if (theBlock)
             {
                 theBlock(theFinalArray, nil);
             }
         }
         else
         {
             if (theBlock)
             {
                 theBlock(nil, nil);
             }
         }
     }];
    [request setFailedBlock:^
     {
         if (theBlock)
         {
             theBlock(nil, nil);
         }
     }];
    [request startAsynchronous];
     
     */
}

+ (NSArray * _Nonnull)getObjectsArrayWithPredicate:(NSPredicate * _Nullable)thePredicate
                              propertyToFetchArray:(NSArray<NSString *> * _Nullable)thePropertyToFetchArray
                               sortDescriptorArray:(NSArray<NSSortDescriptor *> * _Nullable)theSortDescriptorArray
{
    //NSLog(@"%@",thePredicate);
   // NSLog(@"%@",thePropertyToFetchArray);
    //NSLog(@"%@",theSortDescriptorArray);

    NSFetchRequest *theFetchRequest = [[NSFetchRequest alloc] initWithEntityName:sfc([self class])];


//    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"MUSTHAFA"];

    NSError *error = nil;



    NSArray *results = [[(AppDelegate *)[[UIApplication sharedApplication ]delegate] managedObjectContext] executeFetchRequest:theFetchRequest error:&error];


    if (error != nil) {


        //Deal with failure
    }
    else {
        
        //Deal with success
    }
    NSLog(@"%@",error);


    if (thePredicate)
    {
        theFetchRequest.predicate = thePredicate;
    }
    theFetchRequest.includesSubentities = NO;
    if (thePropertyToFetchArray.count)
    {
        theFetchRequest.propertiesToFetch = thePropertyToFetchArray;
    }
    if (theSortDescriptorArray.count)
    {
        theFetchRequest.sortDescriptors = theSortDescriptorArray;
    }

    @try {

        //NSLog(@"%@",results);
        return results;
    } @catch (NSException *exception) {
        return nil;
    } @finally {}

}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype)initWithJSONDictionary:(NSDictionary * _Nonnull)theDictionary
{
//    methodAssert(theDictionary);

    self = [self initWithEntity:[NSEntityDescription entityForName:@"BeehiveProject" inManagedObjectContext:theMOC] insertIntoManagedObjectContext:theMOC];
    if (self)
    {
//        [self methodInitBeehiveProject];
    //NSLog(@"%@", theDictionary);

    @try {
//        [self methodFillWithDictionary:theDictionary];
        {
        id theValue = theDictionary[@"created"];
        self.theCreated = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"id"];
        self.theId = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"modified"];
        self.theModified = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_city"];
        self.theCity = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_country"];
        self.theCountry = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_lat"];
        self.theLatitude = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_lng"];
        self.theLongitude = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_location"];
        self.theLocation = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_name"];
        self.theName = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_note"];
        self.theNote = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_postal_code"];
        self.thePostCode = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_radius"];
        self.theRadius = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_state"];
        self.theState = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_status"];
        self.theStatus = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_street"];
        self.theStreet = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_street_number"];
        self.theStreetNumber = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"project_timezone"];
        self.theTimeZone = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
        {
        id theValue = theDictionary[@"user_id"];
        self.theUserId = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
        }
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
    } @finally {

    }


    }
    return self;
}

//- (void)methodInitBeehiveProject
//{
//    
//}

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodFillWithDictionary:(NSDictionary * _Nonnull)theDictionary
{
//    methodAssert(theDictionary);
    {
        id theValue = theDictionary[@"created"];
        self.theCreated = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"id"];
        self.theId = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"modified"];
        self.theModified = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_city"];
        self.theCity = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_country"];
        self.theCountry = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_lat"];
        self.theLatitude = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_lng"];
        self.theLongitude = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_location"];
        self.theLocation = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_name"];
        self.theName = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_note"];
        self.theNote = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_postal_code"];
        self.thePostCode = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_radius"];
        self.theRadius = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_state"];
        self.theState = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_status"];
        self.theStatus = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_street"];
        self.theStreet = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_street_number"];
        self.theStreetNumber = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"project_timezone"];
        self.theTimeZone = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
    {
        id theValue = theDictionary[@"user_id"];
        self.theUserId = theValue && theValue != [NSNull null] ? [NSString stringWithFormat:@"%@", theValue] : nil;
    }
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end
