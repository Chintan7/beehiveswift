//
//  Geofence.h
//  Beehive
//
//  Created by Rohit on 11/05/15.
//  Copyright (c) 2015 SDNMACMINI13. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Geofence : NSObject
@property (assign ,nonatomic) int project_radius;
@property (assign ,nonatomic) float project_lat;
@property (assign ,nonatomic) float project_lng;
@property (assign ,nonatomic) int user_id;
@property (assign ,nonatomic) int projectID;
@property (assign ,nonatomic) int project_status;
@property (assign ,nonatomic) int project_street_number;
@property (strong ,nonatomic) NSString * project_city;
@property (strong ,nonatomic) NSString * project_country;
@property (strong ,nonatomic) NSString * project_location;
@property (strong ,nonatomic) NSString * project_name;
@property (assign ,nonatomic) NSString * project_postal_code;
@property (strong ,nonatomic) NSString * project_note;
@property (strong ,nonatomic) NSString * project_state;
@property (strong ,nonatomic) NSString * project_street;
@property (strong ,nonatomic) NSString * project_timezone;
@property (assign ,nonatomic) NSString *created;
@property (assign ,nonatomic) NSString *modified;
@end
