//
//  ProjectManager.h
//  Beehive
//
//  Created by OlDor on 4/21/16.
//  Copyright © 2016 SDNMACMINI13. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define keyProjectManagerDidCheckInOutNotification @"keyProjectManagerDidCheckInOutNotification"

@interface ProjectManager : NSObject <NSURLSessionDelegate>

+ (instancetype _Nonnull)sharedInstance;

@property (nonatomic, assign, readonly) BOOL isInProgress;

- (void)methodProcessLocation:(CLLocation * _Nonnull)theLocation;
- (void)methodSendAllInfo;
- (void)methodCheckIn:(BOOL)isCheckIn
            projectId:(NSString * _Nonnull)theProjectId
      timeStampString:(NSString * _Nonnull)theTimeStampString
          noCopyBlock:(void (^ _Nonnull)())theBlock;
@end






























