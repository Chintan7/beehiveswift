//
//  GeofenceMonitor.m
//  Beehive
//
//  Created by Rohit on 11/05/15.
//  Copyright (c) 2015 SDNMACMINI13. All rights reserved.
//


#import "GeofenceMonitor.h"
#import "Geofence.h"
#import "Beehive-Swift.h"

@implementation GeofenceMonitor

-(GeofenceMonitor*)sharedInctance{
    static GeofenceMonitor *geofence;
    if(!geofence){
        geofence= [[GeofenceMonitor alloc]init];
    }
    return geofence;
}

-(NSMutableArray*)monitorWithLocation:(CLLocation *)currentLocation GeoList:(NSArray*)geofences
{
    NSMutableArray * inRegion= [[NSMutableArray alloc]init];
    
//    NSLog(@"location breached");
//    if ([[Reachability alloc] ] != NotReachable)
//    {
        for (Geofence* geo in geofences ) {
            CLLocation *locB = [[CLLocation alloc] initWithLatitude:geo.project_lat longitude:geo.project_lng];
            CLLocationDistance distance = [currentLocation distanceFromLocation:locB];
            NSLog(@"\n Lati 1 : %f & Longi 1: %f ",currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
            NSLog(@"\n Lati 2 : %f & Longi 2: %f ",locB.coordinate.latitude, locB.coordinate.longitude);
            NSLog(@"\n New Distance : %f",distance);
            NSLog(@"\n Radius : %d",geo.project_radius);
            if(distance<=geo.project_radius){
                if(![self isCheckedIn:geo]){
                [inRegion addObject:geo];
                    [self setCheckedIn:geo];
                }
            }
            else{
                if([self isCheckedIn:geo]){
                    [self setCheckedOut:geo];
                }
            }        
        }
//    }
//    else{
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Internet not available" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
//    }
    return inRegion;
}

-(BOOL)isCheckedIn:(Geofence *)geofence{
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    if([def objectForKey:[NSString stringWithFormat:@"project-%d",geofence.projectID]] && [def boolForKey:[NSString stringWithFormat:@"project-%d",geofence.projectID]]){
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Checkedd In" message:[NSString stringWithFormat:@"project-%d",geofence.projectID] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
        return YES;
    }else{
        return NO;
    }
}

-(BOOL)isCheckedOut:(Geofence *)geofence{
    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
    if([def objectForKey:[NSString stringWithFormat:@"project-%d",geofence.projectID]]){
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Checkedd Out" message:[NSString stringWithFormat:@"project-%d",geofence.projectID] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
        return YES;
    }else{
        return NO;
    }
    
}

-(void)setCheckedIn:(Geofence*)geofence {
//    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
//    [def setObject:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"project-%d",geofence.projectID]];
//    [def synchronize];

    [(AppDelegate *)[[UIApplication sharedApplication ]delegate]  checkIn:[NSString stringWithFormat:@"%d",geofence.projectID] checkInTime:[self GetDateTime]];
// [(AppDelegate *)[[UIApplication sharedApplication]delegate] checkIn:[NSString stringWithFormat:@"%d",geofence.projectID] CheckInTime:[self GetDateTime]];
}

-(NSString*)GetDateTime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
   return [dateFormatter stringFromDate:[NSDate date]];
}

-(void)setCheckedOut:(Geofence*)geofence{
//    NSUserDefaults *def=[NSUserDefaults standardUserDefaults];
//    [def setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"project-%d",geofence.projectID]];
//    [def synchronize];

[(AppDelegate *)[[UIApplication sharedApplication ]delegate]  checkOut:[ NSString stringWithFormat:@"%d",geofence.projectID] checkOutTime:[self GetDateTime]];
//[(AppDelegate *)[[UIApplication sharedApplication]delegate] checkOut:[ NSString stringWithFormat:@"%d",geofence.projectID] CheckOutTime:[self GetDateTime]];
}
-(Geofence *)getGeofence:(NSDictionary *)project
{
    Geofence *geofence = [[Geofence alloc] init];
    geofence.created = [project valueForKey:@"created"];
    geofence.projectID = (int)[[project valueForKey:@"id"] integerValue];
    geofence.created = [project valueForKey:@"created"];
    geofence.modified = [project valueForKey:@"modified"];
    geofence.project_city = [project valueForKey:@"project_city"];
    geofence.project_country = [project valueForKey:@"project_country"];
    geofence.project_lat = [[project valueForKey:@"project_lat"] floatValue];
    geofence.project_lng = [[project valueForKey:@"project_lng"] floatValue];
    geofence.project_location = [project valueForKey:@"project_location"];
    geofence.project_name = [project valueForKey:@"project_name"];
    geofence.project_note = [project valueForKey:@"project_note"];
    geofence.project_postal_code = [project valueForKey:@"project_postal_code"];
    geofence.project_radius = (int)[[project valueForKey:@"project_radius"] integerValue];
    geofence.project_state = [project valueForKey:@"project_state"];
    geofence.project_status = (int)[[project valueForKey:@"project_status"] integerValue];
    geofence.project_street = [project valueForKey:@"project_street"];
    geofence.project_street_number= (int)[[project valueForKey:@"project_street_number"] integerValue];
    geofence.project_timezone = [project valueForKey:@"project_timezone"];
    geofence.user_id = (int)[[project valueForKey:@"user_id"] integerValue];
    return geofence;
}
@end
