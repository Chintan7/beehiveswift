//
//  ForgotPasswordViewController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController {

    @IBOutlet var emailIdTextField: UITextField!
    @IBOutlet var emailTextfieldBG: UIImageView!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var forgotLbl: UILabel!
    @IBOutlet var frogotTxtLbl: UILabel!
    var alertLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewDidload()
        // Do any additional setup after loading the view.
    }

    func setupViewDidload() {

            // Do any additional setup after loading the view.
        self.navigationController!.isNavigationBarHidden = false
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = backBtn

        alertLabel = UILabel(frame: CGRect(x: CGFloat(emailIdTextField.frame.origin.x), y: CGFloat(emailIdTextField.frame.origin.y + 40), width: CGFloat(emailIdTextField.frame.size.width), height: CGFloat(15)))

    }

    func backBtn() {
        popTo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func textFieldDidBeginEditing(_ textField: UITextField) {
        alertLabel.removeFromSuperview()
        emailTextfieldBG.image = UIImage(named: "GreyTf.png")!
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        alertLabel.removeFromSuperview()
        emailTextfieldBG.image = UIImage(named: "GreyTf.png")!
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        return true
    }
    
    func textFieldAlert() -> Bool {
        alertLabel.textColor = UIColor.red
        alertLabel.textAlignment = .left
        alertLabel.font = UIFont(name: FONTTYPE, size: CGFloat(12.0))!
        if (emailIdTextField.text == "") {
            alertLabel.text = "Email Field Cannot be blank"
            emailTextfieldBG.image = UIImage(named: "AlertTf.png")!
            self.view.addSubview(alertLabel)
            return false
        }
        else if emailIdTextField.text!.validateEmail() == false {
            alertLabel.text = "Please enter a valid email format"
            emailTextfieldBG.image = UIImage(named: "AlertTf.png")!
            self.view.addSubview(alertLabel)
            return false
        }
        return true
    }

    @IBAction func sendPasswordBtnPressed(_ sender: Any) {

        alertLabel.removeFromSuperview()
        emailTextfieldBG.image = UIImage(named: "GreyTf")!
        emailIdTextField.resignFirstResponder()

        if self.textFieldAlert() {
            APPDELEGATE.addChargementLoader()
            self.getNewPassword()
        }
    }

    @IBAction func newAccountBtnPressed(_ sender: Any) {
        pushTo(string: "SignUpViewController")
    }

    func getNewPassword() {
        APIManager.sharedInstance.forgotPasswordUser(email: emailIdTextField.text!) { (error:NSError?) in
            
            APPDELEGATE.removeChargementLoader()
            if error == nil {self.popTo()}
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
