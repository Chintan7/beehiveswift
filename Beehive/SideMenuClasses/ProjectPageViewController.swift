
//
//  ProjectPageViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 12/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
import MessageUI

//#import "NSString+SBJSON.h"
class ProjectPageViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate, GMSMapViewDelegate,NotificationViewPrtotcol {

    var projectMapView: GMSMapView!
    @IBOutlet var fieldsContainer: UIScrollView!
    @IBOutlet var crewCollection: UICollectionView!
    @IBOutlet var crewViewButton: UIButton!
    @IBOutlet var greyBackground: UIView!
    @IBOutlet var projectDeleteView: UIView!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var closeArchiveButton: UIButton!
    @IBOutlet var deleteProjectButton: UIButton!
    @IBOutlet var disableNotificationButton: UIButton!
    @IBOutlet var projectNameLabel: UILabel!
    @IBOutlet var projectLocationLabel: UILabel!
    @IBOutlet var deleteConfirmationView: UIView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var yesDeleteButton: UIButton!
    @IBOutlet var noDeleteButton: UIButton!
    var myView: UIView!
    var projectDetailDict = JSONDictionary()
    var projectCrewArray = [JSONDictionary]()
    var emailViewController: MFMailComposeViewController!
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    var isIndicationFlag = false
    var isNewlyAddedProject = false
    var projectLocation: CLLocation!
    var projectDataDict = ProjectList()
    var projectId = ""
    var projectDict = JSONDictionary()
    var screenSize = CGSize.zero
    var menuButton: UIBarButtonItem!
    var backBtn: UIBarButtonItem!
    var prefBarButton: UIBarButtonItem!

//    var navView: UIView!

    @IBAction func crewIconButtonClick(_ sender: Any) {
//        var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
        let crewListView = STORYBOARD.instantiateViewController(withIdentifier: "ProjectCrewListViewController") as! ProjectCrewListViewController
        crewListView.projectCrewArray = projectCrewArray
        crewListView.projectID = self.projectId
        self.navigationController!.pushViewController(crewListView, animated: true)
    }

    @IBAction func cancelButtonPressed(_ sender: Any) {
        //    [greyBackground removeFromSuperview];
        greyBackground.isHidden = true
        projectDeleteView.isHidden = true
        self.disableButtonInteraction()
    }

    @IBAction func closeArchiveButtonPressed(_ sender: Any) {
        self.makeProjectArchiveWebSirvice()
        self.disableButtonInteraction()
        greyBackground.isHidden = true
        projectDeleteView.isHidden = true
    }

    @IBAction func deleteProjectButtonPressed(_ sender: Any) {
        greyBackground.isHidden = false
        deleteConfirmationView.isHidden = false
        projectDeleteView.isHidden = true
    }

    @IBAction func desableNotificationButtonPressed(_ sender: Any) {
        self.disableCrewNotificationWebService()
        self.disableButtonInteraction()
        greyBackground.isHidden = true
        projectDeleteView.isHidden = true
    }

    @IBAction func yesDeleteButtonPressed(_ sender: Any) {
        self.deleteProjectWebSerrviceCalling()
        self.disableButtonInteraction()
        greyBackground.isHidden = true
        deleteConfirmationView.isHidden = true
    }

    @IBAction func noDeleteButtonPressed(_ sender: Any) {
        self.disableButtonInteraction()
        greyBackground.isHidden = true
        deleteConfirmationView.isHidden = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        fieldsContainer.contentSize = CGSize(width: CGFloat(320), height: CGFloat(620))
        print("Project ID : \(self.projectId)")
        //    NSLog(@"Project lat : %f \n Project Lng : %f",projectLocation.coordinate.latitude,projectLocation.coordinate.longitude);
        //     NSLog(@"Project Id : %@",self.projectId);
        menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        //    Toolbar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.toolbarBackBtnClick), for: .touchUpInside)
        backBtn = UIBarButtonItem(customView: button)
        let leftFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titlelbl = UILabel()
        titlelbl.text = "Project"
        titlelbl.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(17.0))!
        titlelbl.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(20))
        titlelbl.textAlignment = .left
        let titleLabel = UIBarButtonItem(customView: titlelbl)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let prefImage = UIImage(named: "prefrenceIcon.png")!
        let prefbutton = UIButton(type: .custom)
        prefbutton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        prefbutton.setImage(prefImage, for: .normal)
        prefbutton.addTarget(self, action: #selector(self.prefBttonClick), for: .touchUpInside)
        prefBarButton = UIBarButtonItem(customView: prefbutton)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        barItems.append(leftFlexSpace)
        barItems.append(titleLabel)
        barItems.append(rightFlexSpace)
        barItems.append(prefBarButton)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        self.alertViews()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.getProjectDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
//        navView.isHidden = false
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func updateNotificationCountValue(_ count: Any) {
        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    func viewCustomization() {



        if let name = self.projectDict["project_name"] as? String {
            projectNameLabel.text = name
        }

        if let name = self.projectDict["project_location"] as? String {
            self.projectLocationLabel.text = name
        }

        projectNameLabel.font = CenturyGothicBold15
        projectLocationLabel.font = CenturyGothiCRegular15

        var lanti = "00"
        var longi = "00"

        if let lat = self.projectDict["project_lat"] as? String {
            lanti = lat
        }

        if let lng = self.projectDict["project_lng"] as? String {
            longi = lng
        }

        projectLocation = CLLocation.init(latitude: Double(lanti)!, longitude: Double(longi)!)
        //projectLocation = CLLocation.init(latitude: Double(self.projectDict["project_lat"]! as! NSNumber), longitude: Double(self.projectDict["project_lng"]! as! NSNumber))
//            Google Map
//        [GMSServices provideAPIKey:kGOOGLE_MAP_SERVER_API_KEY_19JUNE];
        let camera = GMSCameraPosition.camera(withLatitude: projectLocation.coordinate.latitude, longitude: projectLocation.coordinate.longitude, zoom: 12.0)
    
        projectMapView = GMSMapView.map(withFrame: CGRect.init(x: 0, y: 100, width: 320, height: 100), camera: camera)
        projectMapView.delegate = self
        projectMapView.isMyLocationEnabled = true
        let fullmapButton = UIButton(type: .custom)
        fullmapButton.frame = CGRect(x: CGFloat(215), y: CGFloat(83), width: CGFloat(100), height: CGFloat(15))
        fullmapButton.backgroundColor = UIColor.darkGray
        fullmapButton.addTarget(self, action: #selector(self.fullmapButtonPressed), for: .touchUpInside)
        fullmapButton.setTitle("View", for: .normal)
        fullmapButton.setTitleColor(UIColor.white, for: .normal)
        fullmapButton.layer.cornerRadius = 4.0
        projectMapView.addSubview(fullmapButton)
        //    Marker
        let marker = GMSMarker()
        marker.icon = UIImage(named: "smallMarker.png")!
        marker.position = CLLocationCoordinate2DMake(projectLocation.coordinate.latitude, projectLocation.coordinate.longitude)
        marker.map = projectMapView
        fieldsContainer.addSubview(projectMapView)
        let projectLocationText = UILabel(frame: CGRect(x: CGFloat(45), y: CGFloat(200), width: CGFloat(220), height: CGFloat(60)))
        projectLocationText.lineBreakMode = .byWordWrapping
        projectLocationText.numberOfLines = 2
        fieldsContainer.addSubview(projectLocationText)
        //    Project Name and Location
        var firstLabel = ""
        var secondLabel = ""
        var thirdLabel = ""
        if let firstLBL = self.projectDict["project_street"] as? String{
            firstLabel = firstLBL
        }

        if let scndLBL = self.projectDict["project_state"] as? String{
            secondLabel = scndLBL
        }

        if let thrdLBL = self.projectDict["project_country"] as? String{
            thirdLabel = thrdLBL
        }



        //    NSString *secondLabel = @"Toronto, Ontario";
//        var secondLabel = projectDataDict.project!.state!
//
//        var thirdLabel = projectDataDict.project!.country!

        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel),\(thirdLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: 0, length: (firstLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: (firstLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: (firstLabel.characters.count) + 1, length: (secondLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (firstLabel.characters.count) + 1, length: (secondLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: ((firstLabel.characters.count) + 1) + ((secondLabel.characters.count) + 1), length: (thirdLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: ((firstLabel.characters.count) + 1) + ((secondLabel.characters.count) + 1), length: (thirdLabel.characters.count)))
        projectLocationText.attributedText = customTextLabelAttribute
        let projectLocationPin = UIImageView(frame: CGRect(x: CGFloat(20), y: CGFloat(215), width: CGFloat(20), height: CGFloat(32)))
        projectLocationPin.image = UIImage(named: "markerPin.png")!
        fieldsContainer.addSubview(projectLocationPin)
        let tempLbl = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(265), width: CGFloat(280), height: CGFloat(1)))
        tempLbl.backgroundColor = UIColor.lightGray
        fieldsContainer.addSubview(tempLbl)
        let crewTitleLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(275), width: CGFloat(220), height: CGFloat(15)))
        crewTitleLabel.lineBreakMode = .byWordWrapping
        crewTitleLabel.text = "Crew"
        crewTitleLabel.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        crewTitleLabel.textColor = UIColor.darkGray
        crewTitleLabel.numberOfLines = 1
        fieldsContainer.addSubview(crewTitleLabel)
        crewCollection.delegate = self
        crewCollection.dataSource = self
        crewCollection.reloadData()
        //    ******* For Crew Manager Page Only ******
        let partLbl = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(425), width: CGFloat(280), height: CGFloat(1)))
        partLbl.backgroundColor = UIColor.lightGray
        fieldsContainer.addSubview(partLbl)
        let reportLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(430), width: CGFloat(220), height: CGFloat(20)))
        reportLabel.lineBreakMode = .byWordWrapping
        reportLabel.text = "Report"
        reportLabel.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        reportLabel.textColor = UIColor.darkGray
        reportLabel.numberOfLines = 1
        fieldsContainer.addSubview(reportLabel)
        let reportButton = UIButton(type: .custom)
        reportButton.frame = CGRect(x: CGFloat(20), y: CGFloat(465), width: CGFloat(130), height: CGFloat(35))
        reportButton.setBackgroundImage(UIImage(named: "emailReportBtn.png")!, for: .normal)
        reportButton.addTarget(self, action: #selector(self.emailReports), for: .touchUpInside)
        reportButton.setTitleColor(UIColor.white, for: .normal)
        fieldsContainer.addSubview(reportButton)
        let viewReport = UIButton(type: .custom)
        viewReport.frame = CGRect(x: CGFloat(170), y: CGFloat(465), width: CGFloat(130), height: CGFloat(35))
        viewReport.setBackgroundImage(UIImage(named: "shortArrowBtn.png")!, for: .normal)
        viewReport.setTitle("View Reports", for: .normal)
        viewReport.setTitleColor(UIColor.lightGray, for: .normal)
        viewReport.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(12.0))!
        viewReport.addTarget(self, action: #selector(self.viewReports), for: .touchUpInside)
        fieldsContainer.addSubview(viewReport)
        let partLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(510), width: CGFloat(280), height: CGFloat(1)))
        partLabel.backgroundColor = UIColor.lightGray
        fieldsContainer.addSubview(partLabel)
        let notesLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(515), width: CGFloat(220), height: CGFloat(20)))
        notesLabel.lineBreakMode = .byWordWrapping
        notesLabel.text = "Notes"
        notesLabel.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        notesLabel.textColor = UIColor.darkGray
        notesLabel.numberOfLines = 1
        fieldsContainer.addSubview(notesLabel)
        let addNoteButton = UIButton(type: .custom)
        addNoteButton.frame = CGRect(x: CGFloat(20), y: CGFloat(540), width: CGFloat(130), height: CGFloat(35))
        addNoteButton.setBackgroundImage(UIImage(named: "addNoteBtn.png")!, for: .normal)
        addNoteButton.addTarget(self, action: #selector(self.addNotes), for: .touchUpInside)
        //    [addNoteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        fieldsContainer.addSubview(addNoteButton)
        let viewAllNote = UIButton(type: .custom)
        viewAllNote.frame = CGRect(x: CGFloat(170), y: CGFloat(540), width: CGFloat(130), height: CGFloat(35))
        viewAllNote.setBackgroundImage(UIImage(named: "shortArrowBtn.png")!, for: .normal)
        viewAllNote.setTitle("View all notes", for: .normal)
        viewAllNote.setTitleColor(UIColor.lightGray, for: .normal)
        viewAllNote.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(12.0))!
        viewAllNote.addTarget(self, action: #selector(self.viewAllNotes), for: .touchUpInside)
        fieldsContainer.addSubview(viewAllNote)
    }



    func toolbarBackBtnClick() {
        //    if (self.indicationFlag == TRUE)
        //    {
        //        self.indicationFlag = FALSE;
        //        if (self.menuContainerViewController.menuState == MFSideMenuStateLeftMenuOpen)
        //        {
        //            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        //        }  else {
        //            [self.menuContainerViewController setMenuState:MFSideMenuStateLeftMenuOpen];
        //        }
        //    }
        //    else
        //    {
        //        [self.navigationController popViewControllerAnimated:YES];
        //    }
        if isNewlyAddedProject {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
            let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
            self.navigationController!.pushViewController(homeScreen, animated: true)
        }
        else {
            popTo()
        }
    }

    func prefBttonClick() {
        greyBackground.isHidden = false
        projectDeleteView.isHidden = false
        self.enableButtonsInteraction()
    }

    func emailReports() {
        UINavigationBar.appearance().setBackgroundImage(nil, for: .default)
        emailViewController = MFMailComposeViewController()
        emailViewController.mailComposeDelegate = self
//        navView.isHidden = true
        self.present(emailViewController, animated: true, completion: { _ in })
    }

    func viewReports() {

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "ReportsViewController") as! ReportsViewController
        newView.comeFromProjectPage = true
        self.navigationController!.pushViewController(newView, animated: true)
    }

    func addNotes() {

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "NotesViewController") as! NotesViewController
        newView.noteId = ""
        newView.projectId = self.projectId
        self.navigationController!.pushViewController(newView, animated: true)
    }

    func viewAllNotes() {

        var newView = STORYBOARD.instantiateViewController(withIdentifier: "AllNotesViewController") as! AllNotesViewController
        newView.projectId = self.projectId
        self.navigationController!.pushViewController(newView, animated: true)
    }

    func needButtonClick() {
    }


    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {


        self.dismiss(animated: true, completion: { _ in })
    }
    // MARK: CollectionView Delegate & DataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if projectCrewArray.count == 0 {
            return 1
        }
        else {
            return projectCrewArray.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = "MyCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        if projectCrewArray.count == 0 {
            let imageView = (cell.viewWithTag(100)! as! UIImageView)
            imageView.image = UIImage(named: "noCrew.png")!
            imageView.layer.borderColor = GRAYCOLOR.cgColor
            imageView.layer.borderWidth = 0.5
            let nameLabel = (cell.viewWithTag(200)! as! UILabel)
            nameLabel.text = ""
            let status = (cell.viewWithTag(1000)! as! UIImageView)
            status.backgroundColor = UIColor.gray
            status.layer.cornerRadius = status.bounds.size.height / 2
            status.layer.masksToBounds = true
        }
        else {
            let nameLabel = (cell.viewWithTag(200)! as! UILabel)
            let imageView = (cell.viewWithTag(100)! as! UIImageView)
            let status = (cell.viewWithTag(1000)! as! UIImageView)

            if let user = projectCrewArray[indexPath.row]["User"] as? JSONDictionary {

                nameLabel.text = user["first_name"]! as? String

                if let checkin = projectCrewArray[indexPath.row]["CheckinCheckoutTracker"] as? JSONDictionary {

                    if let statusTemp = checkin["status"] as? String  {
                        if statusTemp == "checked in" {
                            status.backgroundColor = UIColor.green
                        } else if statusTemp == "checked out" {
                            status.backgroundColor = UIColor.red
                        } else {
                            status.backgroundColor = UIColor.gray
                        }
                    }
                 }
                let url = URL(string: "\(ImageBaseUrl)\(user["user_avatar"]!)")!
                imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))

            }

//            nameLabel.text = ((projectCrewArray[indexPath.row].value(forKey: "User") as! String).value(forKey: "first_name") as! String)
            nameLabel.font = UIFont(name: "Century Gothic", size: CGFloat(10.0))!
            nameLabel.tintColor = UIColor.darkGray


            status.layer.cornerRadius = status.bounds.size.height / 2
            status.layer.masksToBounds = true

            imageView.layer.borderColor = GRAYCOLOR.cgColor
            imageView.layer.borderWidth = 0.5

//            imageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
        }
        cell.tag = indexPath.row
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if projectCrewArray.count != 0 {

            let crewDetailView = STORYBOARD.instantiateViewController(withIdentifier: "CrewProfileViewController") as? CrewProfileViewController
            crewDetailView?.crewDetailDict = projectCrewArray[indexPath.row]
            crewDetailView?.projectId = self.projectId
            self.navigationController!.pushViewController(crewDetailView!, animated: true)
        }
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//        var directionView = sb.instantiateViewController(withIdentifier: "DirectionViewController")
//        directionView.projectAddressString = "\((projectDetailDict["project"] as! String)["project_name"] as! String)\n\((projectDetailDict["project"] as! String)["project_state"] as! String), \((projectDetailDict["project"] as! String)["project_country"] as! String)"
//        var locationCoordinates = CLLocation(latitude: ((projectDetailDict["project"] as! String)["project_lat"] as! String).floatValue, longitude: ((projectDetailDict["project"] as! String)["project_lng"] as! String).floatValue)
//        directionView.projectLocation = locationCoordinates
//        self.navigationController!.pushViewController(directionView, animated: true)
    }

    func fullmapButtonPressed() {

        let directionView = STORYBOARD.instantiateViewController(withIdentifier: "DirectionViewController") as! DirectionViewController
        //    directionView.projectLocation =



        directionView.projectAddressString = "\(projectNameLabel.text!), \(self.projectLocationLabel.text!), \(self.projectDict["project_country"]!)"

        var lanti = "00"
        var longi = "00"

        if let lat = self.projectDict["project_lat"] as? String {
            lanti = lat
        }

        if let lng = self.projectDict["project_lng"] as? String {
            longi = lng
        }

        let locationCoordinates = CLLocation.init(latitude: Double(lanti)!, longitude: Double(longi)!)

//        var locationCoordinates = CLLocation(latitude: ((projectDetailDict["project"] as! String)["project_lat"] as! String).floatValue, longitude: ((projectDetailDict["project"] as! String)["project_lng"] as! String).floatValue)
        directionView.projectLocation = locationCoordinates
        self.navigationController!.pushViewController(directionView, animated: true)
    }
    // MARK: Project Delete Alert

    func alertViews() {
        //    myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 580)];
        greyBackground.alpha = 0.7
        greyBackground.isHidden = true
        deleteConfirmationView.isHidden = true
        projectDeleteView.isHidden = true
        //    messageLabel
        var firstLabel = "Are you sure you wish to delete this project?"
        var secondLabel = "All associated reports will also be deleted."
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(20.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        messageLabel.attributedText = customTextLabelAttribute
        deleteConfirmationView.layer.borderColor = UIColor.lightGray.cgColor
        deleteConfirmationView.layer.borderWidth = 1.5
        deleteConfirmationView.layer.cornerRadius = 3.0
        projectDeleteView.layer.borderColor = UIColor.lightGray.cgColor
        projectDeleteView.layer.borderWidth = 1.5
        projectDeleteView.layer.cornerRadius = 3.0
    }
    //  Confirmation Deleting buttons

    func enableButtonsInteraction() {
        prefBarButton.isEnabled = false
        menuButton.isEnabled = false
        backBtn.isEnabled = false
    }

    func disableButtonInteraction() {
        prefBarButton.isEnabled = true
        menuButton.isEnabled = true
        backBtn.isEnabled = true
    }
    // MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    // MARK: - Web Service & Delegate

    func getProjectDetails() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(GetProjectDetails)\(self.projectId).json"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        let url = URL(string: strURL)!
        print("Project Detail URL- \(url)")


        APIManager.sharedInstance.getProjectDetails(projectID: self.projectId) { (dict:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                print(dict)

                if let projectDictTemp = dict!["project"] as? JSONDictionary {
                    self.projectDict = projectDictTemp
                }

                if let creqArr = dict!["pojectCrews"] as? [JSONDictionary] {
                    self.projectCrewArray = creqArr
                    self.viewCustomization()
                }
            }
        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData?.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

    func deleteProjectWebSerrviceCalling() {
//        APPDELEGATE.addChargementLoader()
//        var strURL = "\(BaseWebRequestUrl)\(DeleteProjectURL)\(self.projectId).json"
//        strURL = strURL.replacingOccurrences(of: " ", with: "")
//        let url = URL(string: strURL)!
//        print("Login URL- \(url)")


        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.deleteProjectWebSerrviceCalling(projectID: self.projectId) { (error:NSError?) in

            APPDELEGATE.removeChargementLoader()

            if error == nil {
                self.popTo()
            }

        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 2
//        request.requestMethod = "DELETE"
//        request.startAsynchronous()
    }

    func makeProjectArchiveWebSirvice() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(MakeProjectArchiveURL)\(self.projectId).json"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        let url = URL(string: strURL)!
        print("Login URL- \(url)")


        APIManager.sharedInstance.makeProjectArchiveWebSirvice(projectID: self.projectId) { (error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                self.popTo()
            }

        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 3
//        request.requestMethod = "PUT"
//        request.startAsynchronous()
    }

    func disableCrewNotificationWebService() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(DisableCrewNotificationsUrl).json"
        //self.projectId
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        let url = URL(string: strURL)!
        print("Login URL- \(url)")

        var dict: JSONDictionary = [:]

        dict["project_id"] = self.projectId as AnyObject?
        dict["is_enabled"] = "Y" as AnyObject?


        APIManager.sharedInstance.disableCrewNotificationWebService(dict: dict) { (result: JSONDictionary?,error:NSError?) in
            if error == nil {
                APPDELEGATE.removeChargementLoader()
                UIAlertView(title: "Beehive", message: result!["message"]! as! String, delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "").show()

            }
        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.setPostValue(self.projectId, forKey: "project_id")
//        request.setPostValue("Y", forKey: "is_enabled")
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 4
//        request.requestMethod = "POST"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            //        NSLog(@"Return Project Detail Dict = %@",returnDict);
//            if (((returnDict["result"] as! String)["status"] as! String) == "INVALID") {
//                var myAlert = UIAlertView(title: "Warning", message: ((returnDict["result"] as! String)["message"] as! String), delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
//                myAlert.tag = 100
//                myAlert.show()
//                return
//            }
//            else if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                projectDetailDict = returnDict
//                print("Project Data : \(projectDetailDict)")
//                projectCrewArray = [Any]()
//                projectCrewArray = (projectDetailDict["pojectCrews"] as! String)
//                self.viewCustomization()
//            }
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Project Delete Response = \(returnDict)")
//            self.navigationController!.popViewController(animated: true)!
//        }
//        else if request.tag == 3 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Project Archive Response = \(returnDict)")
//            self.navigationController!.popViewController(animated: true)!
//        }
//        else if request.tag == 4 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Disable Notification Response = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                var myalert = UIAlertView(title: "Beehive", message: ((returnDict["result"] as! String)["message"] as! String), delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
//                myalert.show()
//            }
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        var alert: UIAlertView?
//        if .error.code == 1 {
//            alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//
//    }

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if alertView.tag == 100 {
            popTo()
        }
        else if alertView.tag == 404 {
            popTo()
        }
    }

    
}
