//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  ProfileEditViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 13/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
import CoreLocation

class ProfileEditViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UINavigationBarDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, CLLocationManagerDelegate,NotificationViewPrtotcol {

    @IBOutlet var customTableView: UITableView!
    @IBOutlet var customToolBar: UIToolbar!
    var menuButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var notificationView: NotificationView!
    var notificationBarButton: UIBarButtonItem!
    var isNotificationButtonSelected = false
    var imagePickerActionSheet: UIActionSheet!
    var contactsArray = NSMutableArray()
    var servicesArray = [JSONDictionary]()
    var currentIndex: IndexPath!
    var editedProfileDataDict = NSMutableDictionary()
    var currentLocation: CLLocation!
    var locationManager: CLLocationManager!
    var profileImage: UIImage!
    var profileDataDict: JSONDictionary = [:]
    var userProfileImage: UIImage?
    var servicesString: String!

    var cellRowHeight: CGFloat = 0.0
    var imagePickerBtn: UIButton!
    var tempDict = NSMutableDictionary()
    var serviceTempStr: String!

    var lastServiceCellTag = 0
    var textFldTemp: UITextField!
    var screenSize = CGSize.zero
    var textfieldTemp: UITextField!

    var selectImage: Bool = false
    
    var imgPickerController = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        serviceTempStr = ""
        // Do any additional setup after loading the view.

        print("Profile Data : \(profileDataDict)")
        editedProfileDataDict = NSMutableDictionary()
        //    NSString *str = [[NSBundle mainBundle] pathForResource:@"ContactService" ofType:@"plist"];
        //    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:str];
        //    contactsArray = [[NSMutableArray alloc] init];
        //    servicesArray = [[NSMutableArray alloc] init];
        //    
        //    contactsArray = [dict objectForKey:@"Contact"];
        //    servicesArray = [dict objectForKey:@"Service"];
        locationManager = CLLocationManager()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
//        if IS_OS_8_OR_LATER {
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.requestAlwaysAuthorization()
//        }

        if profileDataDict.count > 0 {


            if let servicesArr = self.profileDataDict["data"]!["UserService"]! as? [JSONDictionary] {
                servicesArray = servicesArr
            }
        }


        customTableView.setEditing(true, animated: true)
        self.viewCustomization()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func viewCustomization() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //    ToolBar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        let leftFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titlelabel = UILabel()
        titlelabel.text = "Edit Profile"
        titlelabel.font = CenturyGothicBold15
        titlelabel.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(20))
        titlelabel.textAlignment = .left
        let titleLabel = UIBarButtonItem(customView: titlelabel)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            //    UIImage *prefImage = [UIImage imageNamed:@"smallOrangeBtn.png"];
        let donebutton = UIButton(type: .custom)
        donebutton.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(55), height: CGFloat(28))
        donebutton.backgroundColor = UIColor(red: CGFloat(211 / 255.0), green: CGFloat(123 / 255.0), blue: CGFloat(38 / 255.0), alpha: CGFloat(1.0))
        donebutton.setTitle("Done", for: .normal)
        donebutton.setTitleColor(UIColor.white, for: .normal)
        donebutton.titleLabel!.font = CenturyGothicBold15
        donebutton.layer.cornerRadius = 15.0
        donebutton.addTarget(self, action: #selector(self.doneButtonClick), for: .touchUpInside)
        let doneBarButton = UIBarButtonItem(customView: donebutton)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        barItems.append(leftFlexSpace)
        barItems.append(titleLabel)
        barItems.append(rightFlexSpace)
        barItems.append(doneBarButton)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }

    func backBtn() {
        popTo()
    }



    func updateNotificationCountValue(_ count: Any) {
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    func imagePickerButtonTapped(onCell sender: Any) {
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        print("my location :\(locations.last!)")
        currentLocation = (locations.last! )
        print("my Latitude :\(currentLocation.coordinate.latitude),my Longitude :\(currentLocation.coordinate.longitude)")
        //    [locationManager stopUpdatingLocation];
    }

    func doneButtonClick() {
//        textfieldTemp.resignFirstResponder()
//        var lat = "\(currentLocation.coordinate.latitude)"
//        var lng = "\(currentLocation.coordinate.longitude)"
//        (profileDataDict["User"] as! String)["user_lat"] = lat
//        (profileDataDict["User"] as! String)["user_lng"] = lng
//        print("Complete Data : \(editedProfileDataDict)")
        self.updateUserProfile()
    }
// MARK:- UITextField Delegate Methods Pressed

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    //-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
    //{
    //    if (textField.tag == lastServiceCellTag)
    //    {
    //        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //        serviceTempStr = newString;
    //    }
    //    return YES;
    //    
    //}

//    func textFieldDidBeginEditing(_ textField: UITextField) {
//
//        let pointInTable = textField.superview?.convert(textField.frame.origin, to: customTableView)
//
//        var contentOffset = customTableView.contentOffset
////        if screenSize.height == 568 {
//            contentOffset.y = (pointInTable!.y - textField.inputAccessoryView!.frame.size.height - 200)
////        }
////        else {
////            contentOffset.y = (pointInTable!.y - textField.inputAccessoryView!.frame.size.height - 100)
////        }
//        if contentOffset.y >= 0 {
//            customTableView.setContentOffset(contentOffset, animated: true)
//        }
//    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textfieldTemp = textField
//        if textField.tag == 100 {
//            (profileDataDict["User"] as! String)["first_name"] = textField.text
//            (profileDataDict["User"] as! String)["first_name"] = textField.text
//        }
//        else if textField.tag == 101 {
//            (profileDataDict["User"] as! String)["last_name"] = textField.text
//        }
//        else if textField.tag == 102 {
//            (profileDataDict["User"] as! String)["user_company"] = textField.text
//        }
//        else if textField.tag == 103 {
//            (profileDataDict["User"] as! String)["user_contact_no_work"] = textField.text
//        }
//        else if textField.tag == 104 {
//            (profileDataDict["User"] as! String)["user_mobile"] = textField.text
//        }
//        else if textField.tag == 105 {
//            (profileDataDict["User"] as! String)["first_name"] = textField.text
//        }
//        else if textField.tag == 106 {
//            (profileDataDict["User"] as! String)["user_street_number"] = textField.text
//        }
//        else if textField.tag == 107 {
//            (profileDataDict["User"] as! String)["user_street"] = textField.text
//        }
//        else if textField.tag == 108 {
//            (profileDataDict["User"] as! String)["user_city"] = textField.text
//        }
//        else if textField.tag == 109 {
//            (profileDataDict["User"] as! String)["user_state"] = textField.text
//        }
//        else if textField.tag == 110 {
//            (profileDataDict["User"] as! String)["user_country"] = textField.text
//        }
//        else if textField.tag == 111 {
//            (profileDataDict["User"] as! String)["user_postal_code"] = textField.text
//        }
//        else if textField.tag == 112 {
//            serviceTempStr = textField.text
//            //        NSString *str = serviceTempStr;
//            //
//            //        if([serviceTempStr hasSuffix:@","])
//            //        {
//            //            serviceTempStr = [str substringToIndex:[str length]-1];
//            //        }
//            print("Services : \(serviceTempStr)")
//        }

//        customTableView.setContentOffset(CGPoint.zero, animated: true)
//        textField.resignFirstResponder()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        customTableView.setContentOffset(CGPoint.zero, animated: true)
        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

//        textfieldTemp = textField
//        if textField.tag == 100 {
//            (profileDataDict["User"] as! String)["first_name"] = textField.text
//            (profileDataDict["User"] as! String)["first_name"] = textField.text
//        }
//        else if textField.tag == 101 {
//            (profileDataDict["User"] as! String)["last_name"] = textField.text
//        }
//        else if textField.tag == 102 {
//            (profileDataDict["User"] as! String)["user_company"] = textField.text
//        }
//        else if textField.tag == 103 {
//            (profileDataDict["User"] as! String)["user_contact_no_work"] = textField.text
//        }
//        else if textField.tag == 104 {
//            (profileDataDict["User"] as! String)["user_mobile"] = textField.text
//        }
//        else if textField.tag == 105 {
//            (profileDataDict["User"] as! String)["first_name"] = textField.text
//        }
//        else if textField.tag == 106 {
//            (profileDataDict["User"] as! String)["user_street_number"] = textField.text
//        }
//        else if textField.tag == 107 {
//            (profileDataDict["User"] as! String)["user_street"] = textField.text
//        }
//        else if textField.tag == 108 {
//            (profileDataDict["User"] as! String)["user_city"] = textField.text
//        }
//        else if textField.tag == 109 {
//            (profileDataDict["User"] as! String)["user_state"] = textField.text
//        }
//        else if textField.tag == 110 {
//            (profileDataDict["User"] as! String)["user_country"] = textField.text
//        }
//        else if textField.tag == 111 {
//            (profileDataDict["User"] as! String)["user_postal_code"] = textField.text
//        }
//        else if textField.tag == 112 {
//            serviceTempStr = textField.text
//            //        NSString *str = serviceTempStr;
//            //
//            //        if([serviceTempStr hasSuffix:@","])
//            //        {
//            //            serviceTempStr = [str substringToIndex:[str length]-1];
//            //        }
//        }

        return true
    }
// MARK: - Image Picker

    func imagePickerButtonClickEvent() {
        imagePickerActionSheet = UIActionSheet.init(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera", "Gallery")
        self.imagePickerActionSheet.show(in: self.view)
    }

    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {

        let buttonTitle = imagePickerActionSheet.buttonTitle(at: buttonIndex)
        if (buttonTitle == "Gallery") {
            UINavigationBar.appearance().setBackgroundImage(nil, for: .default)
            imgPickerController = UIImagePickerController()
            imgPickerController.delegate = self
            imgPickerController.sourceType = .photoLibrary
            self.present(imgPickerController, animated: false, completion: {() -> Void in
            })
        }
        else if (buttonTitle == "Camera") {
            imgPickerController = UIImagePickerController()
            imgPickerController.delegate = self
            imgPickerController.modalPresentationStyle = .currentContext
            imgPickerController.sourceType = .camera
            imgPickerController.allowsEditing = true

            if !UIImagePickerController.isSourceTypeAvailable(.camera) {
                let alert = UIAlertView(title: "Hello", message: "No Camera Found", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                return
            }
            else {

            }
                                self.present(imgPickerController, animated: false, completion: {() -> Void in
                })
        }
        else if (buttonTitle == "Cancel") {
            imagePickerActionSheet = nil
        }
        else {

        }

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        profileImage = (info["UIImagePickerControllerOriginalImage"] as! UIImage)
        selectImage = true
        self.userProfileImage = profileImage
        customTableView.reloadData()
        picker.dismiss(animated: true) { 
        }

    }
// MARK: - Tableview Delegate & Datasource


    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }



    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 2
        }
        else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: EditUserProfileCell?

        if let data = self.profileDataDict["data"] as? JSONDictionary {
            
            if let user = data["User"] as? [String:String] {
                if indexPath.section == 0 {
                    let CellIdentifier = "Name"
                    cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? EditUserProfileCell
                    if cell == nil {
                        var nib = Bundle.main.loadNibNamed("EditUserProfileCell", owner: self, options: nil)
                        cell = nib?[0] as? EditUserProfileCell
                    }
                    cell!.firstNameTextField.tag = 100
                    cell!.lastNameTextField.tag = 101
                    cell!.companyTextField.tag = 102
                    cell!.firstNameTextField.delegate = self
                    cell!.lastNameTextField.delegate = self
                    cell!.companyTextField.delegate = self
                    //            cell!.firstNameTextField.text = ((self.profileDataDict["User"] as AnyObject)["first_name"] as! String)
                    cell!.firstNameTextField.text = user["first_name"]
                    //            cell!.lastNameTextField.text = ((self.profileDataDict["User"] as AnyObject)["last_name"] as! String)
                    cell!.lastNameTextField.text = user["last_name"]
                    //            cell!.companyTextField.text = ((self.profileDataDict["User"] as AnyObject)["user_company"] as! String)
                    cell!.companyTextField.text = user["user_company"]
                    imagePickerBtn = (cell!.viewWithTag(100)! as! UIButton)
                    //        cell.delegate = self;
                    imagePickerBtn.addTarget(self, action: #selector(self.imagePickerButtonClickEvent), for: .touchUpInside)
                    let url = URL(string: ImageBaseUrl + (user["user_avatar"]!))
                    
                    
                    if selectImage == true {
                        imagePickerBtn.setImage(self.userProfileImage, for: .normal)
                    } else {
                        imagePickerBtn.sd_setImage(with: url, for: .normal, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
                    }

                }
                else if indexPath.section == 1 {
                    let CellIdentifier = "Contact"
                    cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? EditUserProfileCell
                    if cell == nil {
                        var nib = Bundle.main.loadNibNamed("EditUserProfileCell", owner: self, options: nil)
                        cell = nib?[1] as? EditUserProfileCell
                    }
                    cell!.phoneTextField.delegate = self
                    if indexPath.row == 0 {
                        cell!.phoneTypeLabel.text = "Work"
                        cell!.phoneTextField.text = user["user_contact_no_work"]
                        // cell!.phoneTextField.text = ((profileDataDict["User"] as AnyObject)["user_contact_no_work"] as! String)
                        cell!.phoneTextField.tag = 103
                    }
                    else if indexPath.row == 1 {
                        cell!.phoneTypeLabel.text = "Mobile"
                        cell!.phoneTextField.text = user["user_mobile"]
                        //cell!.phoneTextField.text = ((profileDataDict["User"] as AnyObject)["user_mobile"] as! String)
                        cell!.phoneTextField.tag = 104
                    }
                    
                    //        if (indexPath.row < [contactsArray count])
                    //        {
                    //            cell.phoneTextField.text = [[contactsArray objectAtIndex:indexPath.row] valueForKey:@"Number"];
                    //            cell.phoneTextField.tag = 102 + indexPath.row+1;
                    //        }
                }
                else if indexPath.section == 2 {
                    let CellIdentifier = "Email"
                    cell = customTableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? EditUserProfileCell
                    if cell == nil {
                        var nib = Bundle.main.loadNibNamed("EditUserProfileCell", owner: self, options: nil)
                        cell = nib?[2] as? EditUserProfileCell
                    }
                    currentIndex = indexPath
                    cell!.emailTextField.delegate = self
                    cell!.emailTextField.tag = 105
                    
                    //cell!.emailTextField.text = ((self.profileDataDict["User"] as AnyObject)["email"] as! String)
                    cell!.emailTextField.text = user["email"]
                } else if indexPath.section == 3 {
                    let CellIdentifier = "Address"
                    cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? EditUserProfileCell
                    if cell == nil {
                        var nib = Bundle.main.loadNibNamed("EditUserProfileCell", owner: self, options: nil)
                        cell = nib?[3] as? EditUserProfileCell
                    }
                    cell!.streetNoTextField.tag = 106
                    cell!.streetTextField.tag = 107
                    cell!.cityTextField.tag = 108
                    cell!.stateTextField.tag = 109
                    cell!.countryTextField.tag = 110
                    cell!.postalCodeTextField.tag = 111
                    cell!.streetNoTextField.delegate = self
                    cell!.streetTextField.delegate = self
                    cell!.cityTextField.delegate = self
                    cell!.countryTextField.delegate = self
                    cell!.stateTextField.delegate = self
                    cell!.postalCodeTextField.delegate = self
                    //          cell!.streetNoTextField.text = ((self.profileDataDict["User"] as AnyObject)["user_street_number"] as! String)
                    
                    cell!.streetNoTextField.text = user["user_street_number"]
                    //          print("STreet >>>> \((self.profileDataDict["User"] as AnyObject)["user_street"] as! String)")
                    //          cell!.streetTextField.text = ((self.profileDataDict["User"] as AnyObject)["user_street"] as! String)
                    cell!.streetTextField.text = user["user_street"]
                    //          cell!.cityTextField.text = ((self.profileDataDict["User"] as AnyObject)["user_city"] as! String)
                    cell!.cityTextField.text = user["user_city"]
                    cell!.stateTextField.text = user["user_state"]
                    //((self.profileDataDict["User"] as AnyObject)["user_state"] as! String)
                    cell!.countryTextField.text = user["user_country"]
                    //          cell!.countryTextField.text = ((self.profileDataDict["User"] as AnyObject)["user_country"] as! String)
                    cell!.postalCodeTextField.text = user["user_postal_code"]
                    //          cell!.postalCodeTextField.text = ((self.profileDataDict["User"] as AnyObject)["user_postal_code"] as! String)
                } else if indexPath.section == 4 {
                    let CellIdentifier = "Service"
                    cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? EditUserProfileCell
                    if cell == nil {
                        var nib = Bundle.main.loadNibNamed("EditUserProfileCell", owner: self, options: nil)
                        cell = nib?[4] as? EditUserProfileCell
                    }
                    cell!.serviceTextField.delegate = self
                    cell!.serviceTextField.tag = 112
                    //        cell.serviceTextField.text = serviceTempStr;
                    //        cell.serviceTextField.tag = indexPath.row+1000;
                    //        NSLog(@"Text Field Tag : %i",indexPath.row+1000);
                    //        if (indexPath.row < [servicesArray count])
                    //        {
                    //            NSLog(@"Service : %@",[servicesArray objectAtIndex:indexPath.row]);
                    //            cell.serviceTextField.text = [servicesArray objectAtIndex:indexPath.row];
                    //        }
                    //        else if (indexPath.row < [servicesArray count]+1)
                    //        {
                    //            lastServiceCellTag = 1000+[servicesArray count];
                    //        }
                    //                var servicesString = ""
                    //
                    //                if let servicesArr = self.profileDataDict["data"]!["UserService"]! as? [JSONDictionary] {
                    //                    servicesString = servicesArr[0]["service"] as! String!
                    //                }
                    //                serviceTempStr = servicesString
                    //                cell!.serviceTextField.text = serviceTempStr
                    
                    var servicesString = ""
                    let servicesArr = self.profileDataDict["data"]!["UserService"]! as? [JSONDictionary]
                    if servicesArr?.count != 0 {
                        servicesString = (servicesArr?[0]["service"] as! String)
                    }
                    
                    if servicesArray.count > 0 {
                        for i in 1..<servicesArray.count {
                            servicesString = "\(servicesString), \(servicesArr?[i]["service"] as! String)"
                        }
                        serviceTempStr = servicesString
                        cell?.serviceTextField.text = serviceTempStr
                    }
                }
                
                return cell!
            } else {
                return UITableViewCell.init()
            }
        } else {
            return UITableViewCell.init()
        }

        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 130
        }
        else if indexPath.section == 1 || indexPath.section == 2 {
            return 44.0
        }
        else if indexPath.section == 3 {
            return 150
        }
        else {
            return 44
        }

    }

    // *************Not in Use*********

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            print("IndexPath.row...>>>\(indexPath.row)")
            servicesArray.remove(at: indexPath.row)
            customTableView.reloadData()
        }
        else if editingStyle == .insert {
            if lastServiceCellTag == 1000 + servicesArray.count {
                print("TextField Text >>>>>>>>>> \(serviceTempStr)")
                if serviceTempStr != nil {
//                    servicesArray.insert(serviceTempStr, at: indexPath.row)
                }
                customTableView.reloadData()
                serviceTempStr = nil
                return
            }
            if serviceTempStr != nil {
//                servicesArray.insert(serviceTempStr, at: indexPath.row)
                customTableView.reloadData()
            }
        }

        lastServiceCellTag = 0
        serviceTempStr = nil
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
        //    if (indexPath.section == 4)
        //    {
        //        return YES;
        //    }else
        //    {
        //        return NO;
        //    }
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if indexPath.section == 1 {
            if indexPath.row == contactsArray.count {
                return .insert
            }
            else {
                return .delete
            }
        }
        else if indexPath.section == 4 {
            if indexPath.row == servicesArray.count {
                return .insert
            }
            else {
                return .delete
            }
        }
        else {
            return .none
        }
    }

    func encode(toBase64String image: UIImage) -> String {

        let data = UIImageJPEGRepresentation(image, 0.4)
        return data!.base64EncodedString(options: .lineLength64Characters)
    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
        if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
        isNotificationButtonSelected = false

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

// MARK: - Web Service Calling

    func updateUserProfile() {
        APPDELEGATE.addChargementLoader()

        let profile = Profile()
        if let cell = self.customTableView.cellForRow(at: NSIndexPath(row: 0,section: 0) as IndexPath) as? EditUserProfileCell {

            profile.firstName = cell.firstNameTextField.text!
            profile.lastName = cell.lastNameTextField.text!
            profile.copmpany = cell.companyTextField.text!

            
            if selectImage == true {
                if let image = self.userProfileImage {
                    print(UIImageJPEGRepresentation(image, 0.5)!)
                    profile.userimg = self.encode(toBase64String: image)
                } else {
                    profile.userimg = ""
                }
            }
        }
        if let cell1 = self.customTableView.cellForRow(at: NSIndexPath(row: 0,section: 1) as IndexPath) as? EditUserProfileCell {
            profile.contactNoWork = cell1.phoneTextField.text!
        }
        if let cell1P = self.customTableView.cellForRow(at: NSIndexPath(row: 1,section: 1) as IndexPath) as? EditUserProfileCell {
            profile.mobile = cell1P.phoneTextField.text!
        }
//        if let cell2 = self.customTableView.cellForRow(at: NSIndexPath(row: 0,section: 2) as IndexPath) as? EditUserProfileCell {
//            profie.em = cell2.emailTextField.text!
//        }

        if let cell3 = self.customTableView.cellForRow(at: NSIndexPath(row: 0,section: 3) as IndexPath) as? EditUserProfileCell {

            profile.streetNumber = cell3.streetNoTextField.text!
            profile.street = cell3.streetTextField.text!
            profile.city = cell3.cityTextField.text!
            profile.state = cell3.stateTextField.text!
            profile.country = cell3.countryTextField.text!
            profile.postalCode = cell3.postalCodeTextField.text!
        }
        if let cell4 = self.customTableView.cellForRow(at: NSIndexPath(row: 0,section: 4) as IndexPath) as? EditUserProfileCell {
            profile.serviceID = cell4.serviceTextField.text!

        }

        self.view.endEditing(true)

        profile.lng = "\(LocationClass.sharedInstance.currentLocation?.coordinate.longitude)"
        profile.lat = "\(LocationClass.sharedInstance.currentLocation?.coordinate.latitude)"
        profile.extensi = "png"

        APIManager.sharedInstance.updateUserProfile(profile: profile) { (error:NSError?) in
            
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                self.popTo()
            }

        }

        //    NSLog(@"Login URL- %@",url);
//        var request = ASIFormDataRequest(URL: url)
//        request.delegate = self
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)

//        request.setPostValue(((self.profileDataDict["User"] as! String)["first_name"] as! String), forKey: "first_name")


//        request.setPostValue(((self.profileDataDict["User"] as! String)["last_name"] as! String), forKey: "last_name")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_company"] as! String), forKey: "user_company")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_contact_no_work"] as! String), forKey: "user_contact_no_work")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_mobile"] as! String), forKey: "user_mobile")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_street_number"] as! String), forKey: "user_street_number")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_street"] as! String), forKey: "user_street")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_city"] as! String), forKey: "user_city")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_state"] as! String), forKey: "user_state")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_country"] as! String), forKey: "user_country")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_postal_code"] as! String), forKey: "user_postal_code")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_lat"] as! String), forKey: "user_lat")
//        request.setPostValue(((self.profileDataDict["User"] as! String)["user_lng"] as! String), forKey: "user_lng")
//        var servicesStr = serviceTempStr.replacingOccurrencesOf(",", withString: "|")
//        request.setPostValue(servicesStr, forKey: "service_id")
//        print("Image Binary :\(self.encode(toBase64String: profileImage))")
//        request.setPostValue(self.encode(toBase64String: profileImage), forKey: "user_avatar")
//        request.setPostValue("png", forKey: "extension")
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = [AnyHashable: Any]()
//            returnDict = request.responseString().jsonValue()
//            print("Return Profile Data = \(returnDict)")
//            //        if (returnDict objev) {
//            //            <#statements#>
//            //        }
//            self.navigationController!.popViewController(animated: true)!
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Fail Project Data Dict = \(returnDict)")
//        var error = request.error
//        if error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }

}
//
//  ProfileEditViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 13/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
