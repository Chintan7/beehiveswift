//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CurrentLocationViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 09/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
import CoreLocation

class CurrentLocationViewController: BaseViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITextFieldDelegate, UIAlertViewDelegate {
    var currentLocationMapView: GMSMapView!
    var gmsCameraPosition: GMSCameraPosition!
    var currentLocation: CLLocation!
    var locationManager: CLLocationManager!
    var radiusCircle: GMSCircle!
    var pinDropButton: UIButton!
    var pinMarker: GMSMarker!
    var circleCenter = CLLocationCoordinate2D()
    var mapCoordinate = CLLocationCoordinate2D()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //    self.navigationController.interactivePopGestureRecognizer.enabled = NO;


        locationManager = CLLocationManager()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
//        if IS_OS_8_OR_LATER {
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestAlwaysAuthorization()
//        }
        //    requestWhenInUseAuthorization
        self.showCurrentLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func showCurrentLocation() {
            //    [GMSServices provideAPIKey:kGOOGLE_MAP_SERVER_API_KEY_19JUNE];
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(15), y: CGFloat(10), width: CGFloat(220), height: CGFloat(30)))
        titleLabel.text = "Project Location :"
        titleLabel.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(17.0))!
        //    Gothic725 Bd BT Century Gothic
        titleLabel.textAlignment = .left
        self.view.addSubview(titleLabel)
        pinDropButton = UIButton(type: .custom)
        pinDropButton.frame = CGRect(x: CGFloat(210), y: CGFloat(286), width: CGFloat(100), height: CGFloat(23))
        pinDropButton.setBackgroundImage(UIImage(named: "dropPinBtn.png")!, for: .normal)
        pinDropButton.addTarget(self, action: #selector(self.dropNewPin), for: .touchUpInside)
        pinDropButton.setTitleColor(UIColor.white, for: .normal)
        self.view.addSubview(pinDropButton)
        let continueButton = UIButton(type: .custom)
        continueButton.frame = CGRect(x: CGFloat(40), y: CGFloat(320), width: CGFloat(240), height: CGFloat(45))
        continueButton.setImage(UIImage(named: "continueBtn.png")!, for: .normal)
        continueButton.addTarget(self, action: #selector(self.continueBtnPressed), for: .touchUpInside)
        self.view.addSubview(continueButton)
        let changeLocation = UIButton(type: .custom)
        changeLocation.frame = CGRect(x: CGFloat(85), y: CGFloat(375), width: CGFloat(150), height: CGFloat(30))
        changeLocation.setImage(UIImage(named: "changeLocationBtn")!, for: .normal)
        changeLocation.addTarget(self, action: #selector(self.changeLocationBtnPressed), for: .touchUpInside)
        self.view.addSubview(changeLocation)
        currentLocationMapView = GMSMapView(frame: CGRect(x: CGFloat(0), y: CGFloat(50), width: CGFloat(320), height: CGFloat(230)))
        currentLocationMapView.mapType = kGMSTypeNormal
        currentLocationMapView.isMyLocationEnabled = true
        currentLocationMapView.isUserInteractionEnabled = false
        self.view.addSubview(currentLocationMapView)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        print("my location :\(locations.last!)")
        currentLocation = (locations.last! )
        //    NSLog(@"my longitude :%f,%f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
        gmsCameraPosition = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 9.0)
        //myLocation
        print("Lat : \(currentLocation.coordinate.latitude) \n Long : \(currentLocation.coordinate.longitude) ")
        currentLocationMapView.delegate = self
        currentLocationMapView.camera = gmsCameraPosition
        //    [locationManager stopUpdatingLocation];
    }

    func continueBtnPressed() {
        if mapCoordinate.latitude == 0.000000 && mapCoordinate.longitude == 0.000000 {
            let alertV = UIAlertView(title: "Warning", message: "Please select location first", delegate: self, cancelButtonTitle: "OK")
            alertV.show()
            return
        }
//        var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
        let newView = STORYBOARD.instantiateViewController(withIdentifier: "MapFieldsFormController") as! MapFieldsFormController

        newView.locationLatitude = mapCoordinate.latitude
        newView.locationLongitude = mapCoordinate.longitude
        self.navigationController!.pushViewController(newView, animated: true)
    }

    func changeLocationBtnPressed() {
        popTo()
    }

    func dropNewPin() {
        currentLocationMapView.isUserInteractionEnabled = true
        pinMarker = GMSMarker()
        pinMarker.icon = UIImage(named: "markerPin.png.png")!
        //    pinMarker.position = CLLocationCoordinate2DMake(43.680250,-79.630832);
        pinMarker.position = CLLocationCoordinate2DMake((currentLocationMapView.myLocation?.coordinate.latitude)!, (currentLocationMapView.myLocation?.coordinate.longitude)!)
        print("Current Location Cordinates : Longitude : \(currentLocation.coordinate.latitude) \n Latitude :\(currentLocation.coordinate.longitude)")
        //    mapCoordinate.latitude = 43.680250;
        //    mapCoordinate.longitude = -79.630832;
        mapCoordinate.latitude = currentLocation.coordinate.latitude
        mapCoordinate.longitude = currentLocation.coordinate.longitude
        pinMarker.map = currentLocationMapView
        //    NSLog(@"User's location: %@", currentLocationMapView.myLocation);
        //    NSLog(@"Current Location Cordinates : Longitude : %f \n Latitude :%f",mapCoordinate.latitude, mapCoordinate.longitude);
        //    circleCenter = CLLocationCoordinate2DMake(43.680250, -79.630832);
        circleCenter = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
        let radiusCircle2 = GMSCircle(position: circleCenter, radius: 100.0)
        radiusCircle2.fillColor = UIColor(red: CGFloat(1.0), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(0.10))
        radiusCircle2.strokeColor = UIColor.red
        radiusCircle2.strokeWidth = 0.5
        radiusCircle2.map = currentLocationMapView
        pinDropButton.isUserInteractionEnabled = false
        locationManager.stopUpdatingLocation()
    }
    // For Providing Project location based on User Tapped.

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {

        currentLocationMapView.clear()
        mapCoordinate.latitude = coordinate.latitude
        mapCoordinate.longitude = coordinate.longitude

        GMSGeocoder.init().reverseGeocodeCoordinate(CLLocationCoordinate2D.init(latitude: mapCoordinate.latitude, longitude: mapCoordinate.longitude)) { (resp:GMSReverseGeocodeResponse?, error:Error?) in
            self.pinMarker.title = NSLocalizedString("Project Location", comment: "")
            self.pinMarker.icon = UIImage(named: "markerPin.png")!
            self.pinMarker.position = CLLocationCoordinate2DMake(self.mapCoordinate.latitude, self.mapCoordinate.longitude)
            self.pinMarker.map = self.currentLocationMapView
        }

        print("New Cordinates : Longitude : \(mapCoordinate.latitude) \n Latitude :\(mapCoordinate.longitude)")
        circleCenter = CLLocationCoordinate2DMake(mapCoordinate.latitude, mapCoordinate.longitude)
        radiusCircle = GMSCircle(position: circleCenter, radius: 100.0)
        radiusCircle.fillColor = UIColor(red: CGFloat(1.0), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(0.10))
        radiusCircle.strokeColor = UIColor.red
        radiusCircle.strokeWidth = 0.5
        radiusCircle.map = currentLocationMapView
    }


}
