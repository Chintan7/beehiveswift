//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  AdjustRadiusViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 06/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit

class AdjustRadiusViewController: BaseViewController, UITextFieldDelegate, GMSMapViewDelegate {

    //    IBOutlet MKMapView *radiusAdjustMapview;
    @IBOutlet var adjustRadiusTextLabel: UILabel!
    var radiusMapView: GMSMapView!
    @IBOutlet var radiusCountLabel: UILabel!
    @IBOutlet var radiusSlider: UISlider!
    var circleCenter = CLLocationCoordinate2D()
    var radiusCircle: GMSCircle!
    var radiusValue = 0
    var completeProjectAddressData = [String:String]()
    var actualLocation: CLLocation!
    var locationLatitude = CLLocationDegrees()
    var locationLongitude = CLLocationDegrees()
    var addressString = ""

    @IBAction func backButtonClick(_ sender: Any) {
        popTo()
    }

    @IBAction func sliderValueChanged(_ sender: UISlider) {
        radiusValue = Int(sender.value)
        radiusCountLabel.text = "\(radiusValue) Mtr"
        radiusCircle.radius = CLLocationDistance(radiusValue)
    }

    @IBAction func continueBtnClicked(_ sender: Any) {
        self.createNewProject()
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        print("Location Coordinates \n Latitu : \(locationLatitude),\nLongit : \(locationLongitude)")
        //    NSLog(@"Address Data : %@",completeProjectAddressData);

//        if screenSize.height != 568 {
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg")!)
//        }
//        else {
//            self.view.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg-568h")!)
//        }
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        //    [self.navigationController.navigationBar addSubview:[Global checkedInModeButton]];
        adjustRadiusTextLabel.font = CenturyGothicBold15
        self.mapViewCustomization()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func mapViewCustomization() {
            //  [GMSServices provideAPIKey:kGOOGLE_MAP_API_KEY];
        let camera = GMSCameraPosition.camera(withLatitude: circleCenter.latitude, longitude: circleCenter.longitude, zoom: 15.0)
        radiusMapView = GMSMapView.map(withFrame: CGRect(x: CGFloat(0), y: CGFloat(45), width: CGFloat(320), height: CGFloat(205)), camera: camera)
        radiusMapView.mapType = kGMSTypeNormal
        radiusMapView.delegate = self
        radiusMapView.isMyLocationEnabled = true
        self.view.addSubview(radiusMapView)
            // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.icon = UIImage(named: "markerPin.png")!
        marker.position = CLLocationCoordinate2DMake(circleCenter.latitude, circleCenter.longitude)
        marker.map = radiusMapView
        circleCenter = CLLocationCoordinate2DMake(circleCenter.latitude, circleCenter.longitude)
        radiusCircle = GMSCircle(position: circleCenter, radius: 100)
        radiusValue = 100
        radiusCircle.fillColor = UIColor(red: CGFloat(1.0), green: CGFloat(0), blue: CGFloat(0), alpha: CGFloat(0.10))
        radiusCircle.strokeColor = UIColor.red
        radiusCircle.strokeWidth = 0.5
        radiusCircle.map = radiusMapView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func createNewProject() {



        let project = [
            "project_name":"\(completeProjectAddressData["project_name"]!)"
            ,"project_location":"\(completeProjectAddressData["project_location"]!)"
            ,"project_street_number":"\(completeProjectAddressData["project_street_number"]!)"
            ,"project_street":"\(completeProjectAddressData["project_street"]!)"
            ,"project_city":"\(completeProjectAddressData["project_city"]!)"
            ,"project_state":"\(completeProjectAddressData["project_state"]!)"
            ,"project_country":"\(completeProjectAddressData["project_country"]!)"
            ,"project_postal_code":"\(completeProjectAddressData["project_postal_code"]!)"
            ,"project_lat":"\(circleCenter.latitude)"
            ,"project_lng":"\(circleCenter.longitude)"
            ,"project_radius":"\(radiusValue)"
        ]

        print(project)


        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.createNewProject(proje: project) { (dict:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()

            if error == nil {
                
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
                newView.isIndicationFlag = true
                newView.isNewlyAddedProject = true

                print(dict![kResult]!["project"]!!)

                if let id = dict![kResult]!["project"]!! as? JSONDictionary {

                    newView.projectId = id["id"] as! String
                    self.pushToVC(controller: newView)
                } else {

                }


//                if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                    var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                    
//                    newView.projectId = (((returnDict["result"] as! String)["project"] as! String).value(forKey: "id") as! String)
//                    //            newView.projectLocation = actualLocation;
//
//                }

            }
        }

        //        request.setPostValue(projectName, forKey: "project_name")
        //        request.setPostValue(projectLocation, forKey: "project_location")
        //        request.setPostValue(projectStreetNumber, forKey: "project_street_number")
        //        request.setPostValue(projectStreetName, forKey: "project_street")
        //        request.setPostValue(projectCityName, forKey: "project_city")
        //        request.setPostValue(projectStateName, forKey: "project_state")
        //        request.setPostValue(projectCountryName, forKey: "project_country")
        //        request.setPostValue(projectPinCode, forKey: "project_postal_code")
        //        //    NSLog(@"Latitude = %f \nLongitude = %f",locationLatitude,locationLongitude);
        //        request.setPostValue("\(locationLatitude)", forKey: "project_lat")
        //        request.setPostValue("\(locationLongitude)", forKey: "project_lng")
        //        request.setPostValue("\(radiusValue)", forKey: "project_radius")

//        appDelegate.addChargementLoader()
//        var urlString = "\(BaseWebRequestUrl)\(CreateProjectUrl)"
//        var serviceURL = URL(string: urlString)!
//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        var projectName = (completeProjectAddressData["project_name"] as! String)
//        var projectLocation = (completeProjectAddressData["project_location"] as! String)
//        var projectStreetNumber = (completeProjectAddressData["project_street_number"] as! String)
//        var projectStreetName = (completeProjectAddressData["project_street"] as! String)
//        var projectCityName = (completeProjectAddressData["project_city"] as! String)
//        var projectStateName = (completeProjectAddressData["project_state"] as! String)
//        var projectCountryName = (completeProjectAddressData["project_country"] as! String)
//        var projectPinCode = (completeProjectAddressData["project_postal_code"] as! String)

//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()

    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Create Project = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                var newView = sb.instantiateViewController(withIdentifier: "ProjectPageViewController")
//                newView.projectId = (((returnDict["result"] as! String)["project"] as! String).value(forKey: "id") as! String)
//                //            newView.projectLocation = actualLocation;
//                newView.indicationFlag = true
//                newView.isNewlyAddedProject = true
//                self.navigationController!.pushViewController(newView, animated: true)
//            }
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        print("Error : \(.error)")
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }
}
