import UIKit

class MenuEventViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var sideMenuContentTbl: UITableView!
    let sideMenuArr = ["Home","Projects","Add Projects","Profile","Account","Reports","Settings"]

    override func viewDidLoad() {super.viewDidLoad()}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let footer = UIView.init(frame: CGRect.zero)

        self.sideMenuContentTbl.tableFooterView = footer
        self.sideMenuContentTbl.reloadData()

    }

    override func didReceiveMemoryWarning() { super.didReceiveMemoryWarning() }

    func numberOfSections(in tableView: UITableView) -> Int { return 3 }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
            return 1
        } else if section == 1 {
            return 2
        } else if section == 2 {
            return 4
        }
        return sideMenuArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }

        if indexPath.section == 0 {
            cell?.textLabel!.text = "Home"
            cell?.imageView!.image = UIImage(named: "homeIcon.png")!
        }
        else if indexPath.section == 1 {
            if indexPath.row == 0 {
                cell?.textLabel!.text = "Projects"
                cell?.imageView!.image = UIImage(named: "projectsIcon.png")!
            }
            else {
                cell?.textLabel!.text = "Add Project"
                cell?.imageView!.image = UIImage(named: "plusIcon.png")!
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                cell?.textLabel!.text = "Profile"
                cell?.imageView!.image = UIImage(named: "profileIcon.png")!
            }
            else if indexPath.row == 1 {
                cell?.textLabel!.text = "Account"
                cell?.imageView!.image = UIImage(named: "accntIcon.png")!
            }
            else if indexPath.row == 2 {
                cell?.textLabel!.text = "Reports"
                cell?.imageView!.image = UIImage(named: "reportIcon.png")!
            }
            else if indexPath.row == 3 {
                cell?.textLabel!.text = "Settings"
                cell?.imageView!.image = UIImage(named: "reportIcon.png")!
            }
        }else if indexPath.section == 3 {
            cell?.textLabel!.text = "Projects Synchronize"
            cell?.imageView!.image = UIImage(named: "projectsIcon.png")!
        }
        cell?.textLabel!.textColor = UIColor.white
        cell?.textLabel!.font = UIFont(name: FONTTYPE, size: CGFloat(16.0))!
        cell?.backgroundColor = UIColor.clear
        return cell!

    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 20
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var customView: UIView?
        if section == 1 {
            customView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 320, height: 20))
            let headerLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(0), width: CGFloat(100), height: CGFloat(20)))
            headerLabel.text = "Projects"
            headerLabel.textColor = UIColor.white
            headerLabel.textAlignment = .left
            
            headerLabel.font = UIFont(name: FONTTYPE, size: CGFloat(12.0))!
            customView!.addSubview(headerLabel)
        } else if section == 2 {
            customView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 320, height: 20))
            let headerLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(0), width: CGFloat(100), height: CGFloat(20)))
            headerLabel.text = "Manage"
            headerLabel.textColor = UIColor.white
            headerLabel.font = UIFont(name: FONTTYPE, size: CGFloat(12.0))!
            customView?.addSubview(headerLabel)
        } else if section == 3 {
            customView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 320, height: 20))
            let headerLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(0), width: CGFloat(100), height: CGFloat(20)))
            headerLabel.text = "Update"
            headerLabel.textColor = UIColor.white
            headerLabel.font = UIFont(name: FONTTYPE, size: CGFloat(12.0))!
            customView?.addSubview(headerLabel)
        }
        customView?.backgroundColor = UIColor.lightGray
        customView?.clipsToBounds = false
        customView?.layer.shadowColor! = UIColor.lightGray.cgColor
        customView?.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(5))
        customView?.layer.shadowOpacity = 0.5
        return customView
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
                let navigateController = UINavigationController(rootViewController: newView)
                self.menuContainerViewController.centerViewController = navigateController
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "AllProjectsViewController") as! AllProjectsViewController
                let navigateController = UINavigationController(rootViewController: newView)
                newView.wayIndicationFlag = true
//                newView.allProjectsArray = APPDELEGATE.allProjectArray
                self.menuContainerViewController.centerViewController = navigateController
            }
            else if indexPath.row == 1 {

                let createView = STORYBOARD.instantiateViewController(withIdentifier: "CreateProjectViewController") as! CreateProjectViewController
                let navigateController = UINavigationController(rootViewController: createView)
                createView.comingIndicationFlag = true
                self.menuContainerViewController.centerViewController = navigateController
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "UserProfileViewController")
                let navigateController = UINavigationController(rootViewController: newView)
                self.menuContainerViewController.centerViewController = navigateController
            } else if indexPath.row == 1 {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "AccountsViewController")
                let navigateController = UINavigationController(rootViewController: newView)
                self.menuContainerViewController.centerViewController = navigateController
            } else if indexPath.row == 2 {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "ReportsViewController")
                let navigateController = UINavigationController(rootViewController: newView)
                self.menuContainerViewController.centerViewController = navigateController
            } else if indexPath.row == 3 {
                let newView = STORYBOARD.instantiateViewController(withIdentifier: "SettingsViewController")
                let navigateController = UINavigationController(rootViewController: newView)
                self.menuContainerViewController.centerViewController = navigateController
            }

        } else if indexPath.section == 3 {
            APPDELEGATE.requestGetMyCurrentProject()
        }
        tableView.deselectRow(at: indexPath, animated: true)
        self.menuContainerViewController.menuState = MFSideMenuStateClosed
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }
    
}
