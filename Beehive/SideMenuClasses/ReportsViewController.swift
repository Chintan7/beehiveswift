//
//  ReportsViewController.swift
//  Beehive
//
//  Created by SoluLab on 02/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class ReportsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource,NotificationViewPrtotcol {

    @IBOutlet var reportsTableView: UITableView!
    @IBOutlet var bottomToolbar: UIToolbar!
    var topToolbar: UIToolbar!
    var segmentControllerCrewsProject: UISegmentedControl!
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected: Bool = false
    var comeFromProjectPage: Bool! = false
    var notificationView: NotificationView!
    var activeProjectsArray = [JSONDictionary]()
    var archivedProjectsArray = [JSONDictionary]()
    var activeCrewArray = [JSONDictionary]()
    var archivedCrewArray = [JSONDictionary]()
    var customView: UIView!
    var headerLabel: UILabel!
    var crewWebServiceCallingCount: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        crewWebServiceCallingCount = 0;
        self.viewCustomizaton()
        self.fetchAllProjectsReportList()
        // Do any additional setup after loading the view.
    }
    func viewCustomizaton() {

        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png"), style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        let items = ["Projects", "Crew Members"]
        segmentControllerCrewsProject = UISegmentedControl(items: items)
        segmentControllerCrewsProject.frame = CGRect(x: CGFloat(50), y: CGFloat(5), width: CGFloat(220), height: CGFloat(34))
        segmentControllerCrewsProject.selectedSegmentIndex = 0
        segmentControllerCrewsProject.addTarget(self, action: #selector(self.segmentControlSelection), for: .valueChanged)
        let normalBackgroundImage = UIImage(named: "BorderLongWhiteBtn.png")!
        segmentControllerCrewsProject.setBackgroundImage(normalBackgroundImage, for: .normal, barMetrics: .default)
        let selectedBackgroundImage = UIImage(named: "borderOrangeLongBtn.png")!
        segmentControllerCrewsProject.setBackgroundImage(selectedBackgroundImage, for: .selected, barMetrics: .default)
        bottomToolbar.addSubview(segmentControllerCrewsProject)
        UISegmentedControl.appearance().setDividerImage(UIImage(named: "divider.png")!, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        topToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        topToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(100), y: CGFloat(0), width: CGFloat(100), height: CGFloat(44)))
        titleLabel.text = "Reports"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold18
        var barItems = [UIBarButtonItem]()
        if self.comeFromProjectPage == true {
            let image = UIImage(named: "backBtn.png")!
            let button = UIButton(type: .custom)
            button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
            button.setImage(image, for: .normal)
            button.addTarget(self, action: #selector(self.toolBarBackBtnPressed), for: .touchUpInside)
            let backBtn = UIBarButtonItem(customView: button)
            barItems.append(backBtn)
        }
        topToolbar.setItems(barItems, animated: true)
        topToolbar.addSubview(titleLabel)
        self.view.addSubview(topToolbar)
        customView = UIView(frame: CGRect.init(x: 0, y: 0, width: 200, height: 100))
        headerLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(2), width: CGFloat(200), height: CGFloat(20)))

    }

    func showNotificationView(_ note: Notification) {
        //    NSLog(@"Received Notification - Someone seems to have logged in");
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }
    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func toolBarBackBtnPressed() {
        popTo()
    }

    func segmentControlSelection(_ sender: UISegmentedControl) {
        if segmentControllerCrewsProject.selectedSegmentIndex == 0 {
            reportsTableView.reloadData()
        }
        else {
            if crewWebServiceCallingCount == 0 {
                crewWebServiceCallingCount = 1
                self.fetchAllCrewReportList()
            }
            reportsTableView.reloadData()
        }
    }

    func fetchAllProjectsReportList() {
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.fetchAllProjectsReportList { (json:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            
            if error == nil {
                if let status = json?[kResult]?[kstatus] as? Bool {
                    
                    if  status == false  {
                        SLAlert.showAlert(str: json?["result"]!["message"]! as! String)
                    }
                }
                
                if let result = json!["project_report"] as? [JSONDictionary] {
                    print(result)
                    if result.count == 0 {
                        SLAlert.showAlert(str: "No Project Added Yet")
                    } else {
                        
                        for i in 0..<result.count {
                            
                            if let projectStatus = result[i]["Project"]?["project_status"] as? String {
                                if projectStatus == "1"{
                                    self.activeProjectsArray.append(result[i])
                                } else if projectStatus == "2" {
                                    self.archivedProjectsArray.append(result[i])
                                }
                            }
                        }
                        
                        self.reportsTableView.reloadData()
                    }
                } else {
                    
                }
            }
        }
    }

    func fetchAllCrewReportList() {
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.fetchAllCrewReportList { (dict:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            
            if error == nil {
                if let status = dict?[kResult]?[kstatus] as? Bool {
                    
                    if  status == false  {
                        SLAlert.showAlert(str: dict?["result"]!["message"]! as! String)
                    }
                }
                
                if let result = dict!["crew_member_report"] as? [JSONDictionary] {
                    print(result)
                    if result.count == 0 {
                        SLAlert.showAlert(str: "No Project Added Yet")
                    } else {
                        
                        for i in 0..<result.count {
                            
                            
                            if let projectCrew = result[i]["ProjectCrew"] as? JSONDictionary {
                                
                                if let projectStatus = projectCrew["status"] as? String {
                                    if projectStatus == "1"{
                                        self.activeCrewArray.append(result[i])
                                    } else if projectStatus == "2" {
                                        self.archivedCrewArray.append(result[i])
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        self.reportsTableView.reloadData()
                    }
                } else {
                    
                }

            }
            
        }
    }

    func tableView(_ tableView: UITableView, canCollapseSection section: Int) -> Bool {
        return true
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ReportsCell?

        if segmentControllerCrewsProject.selectedSegmentIndex == 0 {
            let CellIdentifier = "Project"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? ReportsCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("ReportsCell", owner: self, options: nil)
                cell = nib?[0] as? ReportsCell
            }
            
            if indexPath.section == 0 {
                //            cell.projectNameLabel.text = @"George Project";

                let dict = activeProjectsArray[indexPath.row]
//                print(project)

                if let checkinCheckoutTracker = dict["CheckinCheckoutTracker"] as? JSONDictionary {
                    
                    if let timeStr = checkinCheckoutTracker["total_hours"] as? String {
                        
                        if timeStr == "" {
                            cell!.projectworkTimeLabel.text = "00 Hrs"
                        } else {
                            cell!.projectworkTimeLabel.text = "\(timeStr) Hrs"
                        }
                    } else {
                        
                    }
                    
                } else {
                    cell!.projectworkTimeLabel.text = "00 Hrs"
                }
                
                if let project = dict["Project"] as? JSONDictionary{
                    cell!.projectNameLabel.text = project["project_name"] as! String?
                    cell!.projectLocationLabel.text = "\(project["project_city"]!)," + "\(project["project_state"]!)"

                }

//
            } else {

                let dict = archivedProjectsArray[indexPath.row]

                if let checkinCheckoutTracker = dict["CheckinCheckoutTracker"] as? JSONDictionary{
                    if let timeStr = checkinCheckoutTracker["total_hours"] as? String {
                        if timeStr == "" {
                            cell!.projectworkTimeLabel.text = "00 Hrs"
                        } else {
                            cell!.projectworkTimeLabel.text = "\(timeStr) Hrs"
                        }
                    }
                } else {
                    cell!.projectworkTimeLabel.text = "00 Hrs"
                }

                if let project = dict["Project"] as? JSONDictionary{
                    cell?.projectNameLabel.text = project["project_name"] as? String
                    cell!.projectLocationLabel.text = "\(project["project_city"]!)," + "\(project["project_state"]!)"

                }


//                    cell!.projectworkTimeLabel.text = (((archivedProjectsArray[indexPath.row] as AnyObject).value(forKey: "Project") as AnyObject).value(forKey: "project_name") as! String)
//                    //            cell.projectLocationLabel.text = @"Ontario, Canada";
//                    cell!.projectLocationLabel.text = "\(((archivedProjectsArray[indexPath.row] as AnyObject).value(forKey: "Project") as AnyObject).value(forKey: "project_city") as! String), \(((archivedProjectsArray[indexPath.row] as AnyObject).value(forKey: "Project") as AnyObject).value(forKey: "project_state") as! String)"
//                    let timeStr = (((archivedProjectsArray[indexPath.row] as AnyObject).value(forKey: "CheckinCheckoutTracker") as AnyObject).value(forKey: "total_hours") as! String)
//                    if (timeStr == "") {
//                        cell!.projectworkTimeLabel.text = "00 Hrs"
//                    }
//                    else {
//                        cell!.projectworkTimeLabel.text = "\(timeStr) Hrs"
//                    }


            }
            cell!.projectNameLabel.font = CenturyGothiCRegular18
            cell!.projectNameLabel.textColor = BLACKCOLOR
            cell!.projectLocationLabel.font = CenturyGothiCRegular12
            cell!.projectLocationLabel.textColor = GRAYCOLOR
            cell!.projectworkTimeLabel.font = CenturyGothicBold15

        }else {

                    let CellIdentifier = "Crew"
                    cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? ReportsCell
                    if cell == nil {
                        var nib = Bundle.main.loadNibNamed("ReportsCell", owner: self, options: nil)
                        cell = nib?[1] as? ReportsCell
                    }

                    if indexPath.section == 0 {


                        let dict = activeCrewArray[indexPath.row]

                        if let checkinCheckoutTracker = dict["CheckinCheckoutTracker"] as? JSONDictionary{
                            if let timeStr = checkinCheckoutTracker["total_hrs"] as? String {
                                if timeStr == "" {
                                    cell!.crewWorkTimeLabel.text = "00 Hrs This Week"
                                } else {
                                    cell!.crewWorkTimeLabel.text = "\(timeStr) Hrs This Week"
                                }
                            } else {
                                cell!.crewWorkTimeLabel.text = "00 Hrs This Week"
                            }
                        } else {
                            cell!.crewWorkTimeLabel.text = "00 Hrs This Week"
                        }

                        if let project = dict["Project"] as? JSONDictionary{
                            cell?.crewWorkTimeLabel.text = project["project_name"] as? String
                            cell!.projectLocationLabel.text = "\(project["project_city"]!)," + "\(project["project_state"]!)"
                            
                        }

                        if let name = dict["User"] as? JSONDictionary {
                            cell?.crewNameLabel.text = "\(name["first_name"]!)" + "\(name["last_name"]!)"
                        }

                        if let name = dict["User"] as? JSONDictionary {
                            let image = "\(name["user_avatar"]!)"
                            let url = URL.init(string: ImageBaseUrl + image)
                            cell?.crewImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
                        }


//
//                        let timeStr = (((activeCrewArray[indexPath.row] as AnyObject).value(forKey: "CheckinCheckoutTracker") as AnyObject).value(forKey: "total_hrs") as! String)
//                        if (timeStr == "") {
//                            cell?.crewWorkTimeLabel.text = "00 Hrs This Week"
//                        }
//                        else {
//                            cell?.crewWorkTimeLabel.text = "\(timeStr) Hrs This Week"
//                        }
//                        var url = URL(string: "\(ImageBaseUrl)\((activeCrewArray[indexPath.row]["User"] as! AnyObject)["user_avatar"] as! String)")!
//                        cell.crewImageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
                        cell?.crewImageView.layer.borderWidth = 0.5
                        cell?.crewImageView.layer.borderColor = UIColor.lightGray.cgColor
                    } else {

                        let dict = archivedCrewArray[indexPath.row]

                        if let checkinCheckoutTracker = dict["CheckinCheckoutTracker"] as? JSONDictionary{
                            if let timeStr = checkinCheckoutTracker["total_hours"] as? String {
                                if timeStr == "" {
                                    cell!.crewWorkTimeLabel.text = "00 Hrs This Week"
                                } else {
                                    cell!.crewWorkTimeLabel.text = "\(timeStr) Hrs This Week"
                                }
                            } else {
                                cell!.crewWorkTimeLabel.text = "00 Hrs This Week"
                            }
                        } else {
                            cell!.crewWorkTimeLabel.text = "00 Hrs This Week"
                        }

                        if let project = dict["Project"] as? JSONDictionary{
                            cell?.crewWorkTimeLabel.text = project["project_name"] as? String
                            cell!.projectLocationLabel.text = "\(project["project_city"]!)," + "\(project["project_state"]!)"

                        }

                        if let name = dict["User"] as? JSONDictionary {
                            cell?.crewNameLabel.text = "\(name["first_name"]!)" + "\(name["last_name"]!)"
                        }

                        if let name = dict["User"] as? JSONDictionary {
                            let image = "\(name["user_avatar"]!)"
                            let url = URL.init(string: image)
                            cell?.crewImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
                        }

//                        let timeStr = (((archivedCrewArray[indexPath.row] as AnyObject).value(forKey: "CheckinCheckoutTracker") as AnyObject).value(forKey: "total_hrs") as! String)
//                        if (timeStr == "") {
//                            cell?.crewWorkTimeLabel.text = "00 Hrs"
//                        }
//                        else {
//                            cell?.crewWorkTimeLabel.text = "\(timeStr) Hrs"
//                        }
//                        var url = URL(string: "\(ImageBaseUrl)\((archivedCrewArray[indexPath.row]["User"] as! String)["user_avatar"] as! String)")!
//                        cell.crewImageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
                    }
                    
                    cell?.crewNameLabel.font = CenturyGothiCRegular18
                    cell?.crewNameLabel.textColor = BLACKCOLOR
                    cell?.crewWorkTimeLabel.font = CenturyGothicBold12
                    cell?.crewWorkTimeLabel.textColor = GRAYCOLOR
            }
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.orange
        cell?.selectedBackgroundView = bgColorView
        return cell!
    }



    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControllerCrewsProject.selectedSegmentIndex == 0 {
            if section == 0 {
                return activeProjectsArray.count
            }
            else {
                return archivedProjectsArray.count
            }
        }
        else {
            if section == 0 {
                //            return 2;
                return activeCrewArray.count
            }
            else {
                return archivedCrewArray.count
                //            return 3;
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 25.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        customView = UIView(frame: CGRect.init(x: 0, y: 0, width: 200, height: 100))
        headerLabel = UILabel(frame: CGRect(x: CGFloat(20), y: CGFloat(2), width: CGFloat(200), height: CGFloat(20)))
        if segmentControllerCrewsProject.selectedSegmentIndex == 0 {
            if section == 0 {
                headerLabel.text = "Active Projects"
            }
            else {
                headerLabel.text = "Archived Projects"
            }
        }
        else {
            if section == 0 {
                headerLabel.text = "Active Crew Members"
            }
            else {
                headerLabel.text = "Inactive Crew Members"
            }
        }
        headerLabel.font = CenturyGothiCRegular13
        headerLabel.textColor = UIColor.black
        customView.backgroundColor = UIColor(red: CGFloat(210.0 / 255.0), green: CGFloat(210.0 / 255.0), blue: CGFloat(210.0 / 255.0), alpha: CGFloat(1.0))
        customView.addSubview(headerLabel)
        customView.isOpaque = false
        return customView
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if segmentControllerCrewsProject.selectedSegmentIndex == 0 {
            var reportView = STORYBOARD.instantiateViewController(withIdentifier: "ProjectReportsViewController") as! ProjectReportsViewController


            if indexPath.section == 0 {
                let dict = activeProjectsArray[indexPath.row]
                if let project = dict["Project"] as? JSONDictionary{

                    reportView.projectId = project["id"] as! String
                    reportView.projectName = "\(project["project_name"]!)"

                }
            } else {
                    let dict = archivedProjectsArray[indexPath.row]
                    if let project = dict["Project"] as? JSONDictionary{

                        reportView.projectId = (project["id"] as? String)!
                        reportView.projectName = "\(project["project_name"]!)"
                        
                    }
                }


//            if indexPath.section == 0 {
//
//
//                reportView.projectId = ((activeProjectsArray[indexPath.row].value(forKey: "Project") as! String).value(forKey: "id") as! String)
//                reportView.projectName = ((activeProjectsArray[indexPath.row].value(forKey: "Project") as! String).value(forKey: "project_name") as! String)
//            }
//            else {
//                reportView.projectId = ((archivedProjectsArray[indexPath.row].value(forKey: "Project") as! String).value(forKey: "id") as! String)
//                reportView.projectName = ((activeProjectsArray[indexPath.row].value(forKey: "Project") as! String).value(forKey: "project_name") as! String)
//            }
            self.navigationController!.pushViewController(reportView, animated: true)
            
        }
        else {
            print("Crew Selection")
            
            let crewReportDetail = STORYBOARD.instantiateViewController(withIdentifier: "CrewWeeklyReportViewController") as! CrewWeeklyReportViewController

                if indexPath.section == 0 {


                    let dict = activeCrewArray[indexPath.row]

                    if let name = dict["User"] as? JSONDictionary {
                        crewReportDetail.userInfoDict = name
                    }
                } else {

                    let dict = archivedCrewArray[indexPath.row]
                    if let name = dict["User"] as? JSONDictionary {
                        crewReportDetail.userInfoDict = name
                    }
                }
//            if indexPath.section == 0 {
//                crewReportDetail.userInfoDict = (activeCrewArray[indexPath.row].value(forKey: "User") as! String)
//            }
//            else {
//                crewReportDetail.userInfoDict = (activeCrewArray[indexPath.row].value(forKey: "User") as! String)
//            }
            self.navigationController!.pushViewController(crewReportDetail, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
