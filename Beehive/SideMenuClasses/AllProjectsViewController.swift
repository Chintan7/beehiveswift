//
//  AllProjectsViewController.swift
//  Beehive
//
//  Created by SoluLab on 02/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class AllProjectsViewController: BaseViewController, UITableViewDelegate,UITableViewDataSource,NotificationViewPrtotcol {

    //,MNMBottomPullToRefreshManagerClient


    @IBOutlet var crewManagerSegment: UISegmentedControl!
    @IBOutlet var bottomToolBar: UIToolbar!

    var viewSearch: UIView!
    @IBOutlet var projectsListTable: UITableView!
    var activeArchiveSegment: UISegmentedControl!
    var searchArray = NSMutableArray()
    var finalProjectArray = NSMutableArray()
    var tempFilterArray = NSMutableArray()
    var getProjectArray = NSMutableArray()
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton?
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    //var pullToRefreshManager_: MNMBottomPullToRefreshManager!
    var reloads_ = 0

    var wayIndicationFlag: Bool! = false
    var allProjectsArray = [JSONDictionary]()
    var screenSize = CGSize.zero
    var projectSearchBar: UISearchBar!
//    var appDelegate: BeehiveAppDelegate!
    var projectTypeString: String!
    var userTypeString: String!
    var varObj: AnyObject!
    var requestString: String!
    var pageNumber: Int!
    var geofences = NSMutableArray()

    var status: String?
    var projects = [ProjectList]()
    var count: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        pageNumber = 1
        userTypeString = "3"
        projectTypeString = "active"

        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    func updateNotificationCountValue(_ count: Any) {
        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        if notificationButton != nil {
            notificationButton?.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        }
        
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
        if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
        isNotificationButtonSelected = false

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

    func getMyCurrentProject() {
        let project = Projects()
        project.roalID = "3"
        project.pageNo = "\(pageNumber)"

        APIManager.sharedInstance.requestGetMyCurrentProject(project: project) { (response:ProjectsResponse?, error:NSError?) in

            if error == nil {
                self.count = response?.count
                self.status = response?.status

                if (response?.projects?.count)! > 0 {
                    self.projects = (response?.projects)!
                    self.projectsListTable.reloadData()
                }
            }
        }

    }

    func viewCustomization() {

        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton?.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton?.backgroundColor = UIColor.red
        notificationButton?.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)

//        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton?.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton?.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton!)
        self.navigationItem.rightBarButtonItem = notificationBarButton

        viewSearch = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(50)))
        viewSearch.layer.shadowColor! = UIColor.gray.cgColor
        viewSearch.layer.shadowOffset = CGSize(width: CGFloat(2), height: CGFloat(2))
        viewSearch.layer.shadowOpacity = 1
        viewSearch.layer.shadowRadius = 2.0
        viewSearch.clipsToBounds = false

//        if screenSize.height == 568 {
//            projectsListTable = UITableView.init(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(410)))
//        }
//        else {
//            projectsListTable = UITableView.init(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(325)))
//        }


        projectsListTable.dataSource = self
        projectsListTable.delegate = self
        self.view.addSubview(projectsListTable)


        reloads_ = -1
        //pullToRefreshManager_ = MNMBottomPullToRefreshManager(pullToRefreshViewHeight: 90.0, tableView: projectsListTable, with: self)
        //    Custom Active Archive Segment Controller
        let items = ["Active", "Archived"]
        activeArchiveSegment = UISegmentedControl(items: items)
        activeArchiveSegment.frame = CGRect(x: CGFloat(10), y: CGFloat(5), width: CGFloat(200), height: CGFloat(34))
        activeArchiveSegment.selectedSegmentIndex = 0

        let normalBackgroundImage = UIImage(named: "BorderLongWhiteBtn.png")!
        activeArchiveSegment.setBackgroundImage(normalBackgroundImage, for: .normal, barMetrics: .default)
        let selectedBackgroundImage = UIImage(named: "borderOrangeLongBtn.png")!
        activeArchiveSegment.setBackgroundImage(selectedBackgroundImage, for: .selected, barMetrics: .default)
        activeArchiveSegment.addTarget(self, action: #selector(self.segmentSelection), for: .valueChanged)
        activeArchiveSegment.tag = 1000
        bottomToolBar.addSubview(activeArchiveSegment)

        UISegmentedControl.appearance().setDividerImage(UIImage(named: "divider.png")!, forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
        //    Top Toolbar Creation
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        var backBtn: UIBarButtonItem!
        if wayIndicationFlag == true {
            let image = UIImage(named: "backBtn.png")!
            let button = UIButton(type: .custom)
            button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
            button.setImage(image, for: .normal)
            button.addTarget(self, action: #selector(self.backBtn), for: .touchUpInside)
            backBtn = UIBarButtonItem(customView: button)

        }
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)


        let titlelbl = UILabel()
        titlelbl.text = "Project"
        titlelbl.font = UIFont(name: FONTTYPE, size: CGFloat(17.0))!
        titlelbl.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(20))
        titlelbl.textAlignment = .left
        let titleLabel = UIBarButtonItem(customView: titlelbl)
        let rightFlexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)

        let prefImage = UIImage(named: "addBtn.png")!
        let prefbutton = UIButton(type: .custom)
        prefbutton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(70), height: CGFloat(30))
        prefbutton.setImage(prefImage, for: .normal)
        prefbutton.addTarget(self, action: #selector(self.addNewProject), for: .touchUpInside)
        let prefBarButton = UIBarButtonItem(customView: prefbutton)

        var barItems = [UIBarButtonItem]()
        if wayIndicationFlag == true {
            barItems.append(backBtn)
        }
        barItems.append(flexSpace)
        barItems.append(titleLabel)
        barItems.append(rightFlexSpace)
        barItems.append(prefBarButton)
        viewToolbar.setItems(barItems, animated: true)

        self.view.addSubview(viewToolbar)
        activeArchiveSegment.selectedSegmentIndex = 0
        crewManagerSegment.selectedSegmentIndex = 0

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.viewCustomization()
        }


    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        projectTypeString = "active"
        userTypeString = "3"
        pageNumber = 1
        requestString = "getMyProjects.json?role_id=\(userTypeString)&page=\(pageNumber)"
        self.getMyCurrentProject()
//        self.getMyProjects(requestString)

    }

    @IBAction func segmentSelection(_ sender: UISegmentedControl) {
        pageNumber = 1
        if sender == activeArchiveSegment {
            if activeArchiveSegment.selectedSegmentIndex == 0 {
                projectTypeString = "active"
            }
            else {
                projectTypeString = "archive"
            }
        } else if sender == crewManagerSegment {
            if crewManagerSegment.selectedSegmentIndex == 0 {
                //            NSLog(@"Manager");
                userTypeString = "3"
            }
            else {
                //            NSLog(@"Crew");
                userTypeString = "4"
            }
        }

        if (projectTypeString == "active") {

            let project = Projects()
            project.roalID = userTypeString
            project.pageNo = "\(pageNumber)"

            APPDELEGATE.addChargementLoader()
            APIManager.sharedInstance.requestGetMyCurrentProject(project: project) { (response:ProjectsResponse?, error:NSError?) in
                APPDELEGATE.removeChargementLoader()
                if error == nil {
                    self.count = response?.count
                    self.status = response?.status
                    if (response?.projects?.count)! > 0 {
                        self.projects = (response?.projects)!
                        self.projectsListTable.reloadData()
                    }
                }
            }
        }
        else {
            let project = Projects()
            project.roalID = userTypeString
            project.pageNo = "\(pageNumber)"
            APPDELEGATE.addChargementLoader()
            APIManager.sharedInstance.getAllArchivedProject(project: project, completion: { (response:ProjectsResponse?, error:NSError?) in
                APPDELEGATE.removeChargementLoader()
                if error == nil {
                    self.count = response?.count
                    self.status = response?.status
                    if (response?.projects?.count)! > 0 {
                        self.projects = (response?.projects)!
                        self.projectsListTable.reloadData()
                    }
                }
            })

//            requestString = "getAllMyArchivedProjects.json?role_id=\(userTypeString)&page=\(pageNumber)"
//            self.getMyProjects(requestString)
        }


    }

    func backBtn() {
//        if self.menuContainerViewController.menuState == MFSideMenuStateLeftMenuOpen {
//            self.menuContainerViewController.menuState = MFSideMenuStateClosed
//        }
//        else {
//            self.menuContainerViewController.menuState = MFSideMenuStateLeftMenuOpen
//        }
        popTo()
    }

    func addNewProject() {

        let createProjectVC = STORYBOARD.instantiateViewController(withIdentifier: "CreateProjectViewController") as! CreateProjectViewController
        pushToVC(controller: createProjectVC)

    }

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.projects.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let CellIdentifier = "ManagerCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? ProjectListCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("ProjectListCell", owner: self, options: nil)
            cell = nib![0] as? ProjectListCell
        }

        let project = self.projects[indexPath.row]

        let firstLabel = (project.project?.name)!
        let secondLabel = (project.project?.state)! + "," + (project.project?.country)!
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: 0, length: (firstLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: (firstLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: (firstLabel.characters.count) + 1, length: (secondLabel.characters.count)))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (firstLabel.characters.count) + 1, length: secondLabel.characters.count))
        cell?.namePlaceLabel.attributedText =  customTextLabelAttribute


        if (crewManagerSegment.selectedSegmentIndex == 0)
        {
            cell?.cellImage.image = #imageLiteral(resourceName: "brifcaseIcon.png")
        }else{
            cell?.cellImage.image = UIImage.init(named: "")
        }
        
        
        var checkInCount: NSInteger = 0
        var checkOutCount: NSInteger = 0
        
        if let total = project.checkedInCrew?.total {
            if total == "" {
                cell?.onlineCount.text = "0"
            } else {
                cell?.onlineCount.text = total
            }
            
        } else {
            cell?.onlineCount.text = "0"
        }
        
        if let checkOutCount = project.checkeOutCrew?.total {
            if checkOutCount == "" {
                cell?.offlineCount.text = "0"
            } else {
                cell?.offlineCount.text = checkOutCount
            }
            
        }
        if let unreacheble = project.countReview?.count_crews {
            
            let count = checkInCount + checkOutCount
            
            
            let c = Int32(unreacheble)! - Int32(count)
            
            cell?.notificationLabel.text = "\(c)"
        }
        
        
        /*
         NSInteger checkInCount;
         NSInteger checkOutCount;
         if ([[[[finalProjectArray objectAtIndex:indexPath.row] valueForKey:@"CheckedInCrew"] valueForKey:@"total"] isEqualToString:@""]) {
         checkInCount = 0;
         }else
         {
         checkInCount = [[[[finalProjectArray objectAtIndex:indexPath.row] valueForKey:@"CheckedInCrew"] valueForKey:@"total"] integerValue];
         }
         
         
         if ([[[[finalProjectArray objectAtIndex:indexPath.row] valueForKey:@"CheckeOutCrew"] valueForKey:@"total"] isEqualToString:@""]) {
         checkOutCount = 0;
         } else
         {
         checkOutCount = [[[[finalProjectArray objectAtIndex:indexPath.row] valueForKey:@"CheckeOutCrew"] valueForKey:@"total"] integerValue];
         }
         NSInteger unreachable = ([[[[finalProjectArray objectAtIndex:indexPath.row] valueForKey:@"0"] valueForKey:@"count_crews"] integerValue]-(checkInCount+checkOutCount));
         
         cell.onlineCount.text = [NSString stringWithFormat:@"%zd",checkInCount];
         cell.offlineCount.text = [NSString stringWithFormat:@"%zd",checkOutCount];
         cell.notificationLabel.text = [NSString stringWithFormat:@"%zd",unreachable];
         */
        

        return cell!

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


        if crewManagerSegment.selectedSegmentIndex == 0 {
            let newView = STORYBOARD.instantiateViewController(withIdentifier: "ProjectPageViewController") as? ProjectPageViewController

            let project = self.projects[indexPath.row]


            let locationCoordinates = CLLocation.init(latitude: Double((project.project?.lat)!)!, longitude: Double((project.project?.lng)!)!)

            newView?.projectLocation = locationCoordinates
            newView?.projectDataDict = self.projects[indexPath.row]
            newView?.projectId = (project.project?.id)!
            self.navigationController!.pushViewController(newView!, animated: true)
        }
        else {
            let project = self.projects[indexPath.row]

            let newView = STORYBOARD.instantiateViewController(withIdentifier: "CrewProjectDetailViewController") as! CrewProjectDetailViewController
            newView.projectId = project.project?.id
            self.navigationController!.pushViewController(newView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
