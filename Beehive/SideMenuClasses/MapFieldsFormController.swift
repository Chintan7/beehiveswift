//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  MapFieldsFormController.h
//  Beehive
//
//  Created by Deepak Dixit on 06/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class MapFieldsFormController: BaseViewController, UITextFieldDelegate {

    @IBOutlet var projectLocationLabel: UILabel!
    @IBOutlet var fieldsContainer: UIScrollView!
    @IBOutlet var projectNameField: UITextField!
    @IBOutlet var streetField: UITextField!
    @IBOutlet var streetNoField: UITextField!
    @IBOutlet var cityField: UITextField!
    @IBOutlet var stateField: UITextField!
    @IBOutlet var codeField: UITextField!
    @IBOutlet var countryField: UITextField!
    @IBOutlet var completeAddressTextField: UITextField!
    var completeAddressString = ""
    var completeAddressDict = [String: String]()
    var locationLatitude = CLLocationDegrees()
    var locationLongitude = CLLocationDegrees()
    var actualLocation: CLLocation!
    var addressString = ""
    var dataDict = JSONDictionary()

    @IBAction func continueClick(_ sender: Any) {
        if (projectNameField.text?.characters.count)! == 0 || streetNoField.text?.characters.count == 0 || streetField.text?.characters.count == 0 || cityField.text?.characters.count == 0 || countryField.text?.characters.count == 0 || stateField.text?.characters.count == 0 || codeField.text?.characters.count == 0 {
            var myAlert = UIAlertView(title: "Warning", message: "Please fill All the fields", delegate: nil, cancelButtonTitle: "OK")
            myAlert.show()

            return
        }

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "AdjustRadiusViewController") as! AdjustRadiusViewController
        completeAddressDict.removeAll()
        completeAddressDict["project_name"] = projectNameField.text
        completeAddressDict["project_location"] = completeAddressTextField.text
        completeAddressDict["project_street_number"] = streetNoField.text
        completeAddressDict["project_street"] = streetField.text
        completeAddressDict["project_city"] = cityField.text
        completeAddressDict["project_state"] = stateField.text
        completeAddressDict["project_country"] = countryField.text
        completeAddressDict["project_postal_code"] = codeField.text
        newView.completeProjectAddressData = completeAddressDict
        //    newView.actualLocation = actualLocation;
        newView.locationLatitude = locationLatitude
        newView.locationLongitude = locationLongitude
        self.navigationController!.pushViewController(newView, animated: true)
    }

    @IBAction func backButtonClick(_ sender: Any) {
        popTo()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("Coordinates = Latitude : \(locationLatitude), Longitude : \(locationLongitude)")
        self.getLocationCompleteAddress()

        if screenSize.height != 568 {
//            fieldsContainer.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg")!)
            fieldsContainer.contentSize = CGSize(width: CGFloat(320), height: CGFloat(640))
        }
        else {
//            fieldsContainer.backgroundColor = UIColor(patternImage: UIImage(named: "GreyBg-568h")!)
            //        [fieldsContainer setContentSize:CGSizeMake(320, 568)];
        }
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        projectLocationLabel.font = CenturyGothicBold15
        projectNameField.font = CenturyGothiCRegular15
        streetField.font = CenturyGothiCRegular15
        streetNoField.font = CenturyGothiCRegular15
        cityField.font = CenturyGothiCRegular15
        stateField.font = CenturyGothiCRegular15
        countryField.font = CenturyGothiCRegular15
        codeField.font = CenturyGothiCRegular15
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if screenSize.height != 568 {
            if textField == stateField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(65)), animated: true)
            }
            else if textField == countryField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(105)), animated: true)
            }
            else if textField == codeField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(150)), animated: true)
            }
        }
        else {
            if textField == countryField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(25)), animated: true)
            }
            else if textField == codeField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(65)), animated: true)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
        if screenSize.height != 568 {

        }
        else {
            if textField == countryField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
            }
            else if textField == codeField {
                fieldsContainer.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
            }
        }
        textField.resignFirstResponder()
        return true
    }

    func fillAllFields() {
        streetField.font = CenturyGothiCRegular16
        streetNoField.font = CenturyGothiCRegular16
        cityField.font = CenturyGothiCRegular16
        stateField.font = CenturyGothiCRegular16
        countryField.font = CenturyGothiCRegular16
        codeField.font = CenturyGothiCRegular16
        completeAddressTextField.font = CenturyGothiCRegular16

        completeAddressTextField.text = completeAddressString

        print(completeAddressDict)


        streetNoField.text = completeAddressDict["street_number"]
        streetField.text = completeAddressDict["locality"]
        cityField.text = completeAddressDict["administrative_area_level_2"]
        stateField.text = completeAddressDict["administrative_area_level_1"]
        countryField.text = completeAddressDict["country"]
        codeField.text = completeAddressDict["postal_code"]
    }


    func getLocationCompleteAddress() {
        APPDELEGATE.addChargementLoader()
        let strURL = "\(kGOOGLE_GEOCODE_URL)latlng=\(locationLatitude),\(locationLongitude)"

        APIManager.sharedInstance.getGoogleData(url: strURL) { (dict:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            
            
            if error == nil {
                if let result = dict!["results"] as? [JSONDictionary] {
                    
                    if let address = result[0]["address_components"] as? [JSONDictionary] {
                        for dictTemp in address {
                            if let arr = dictTemp["types"] as? [String] {
                                print(arr)
                                self.completeAddressDict["\(arr.first!)"] = "\(dictTemp["long_name"]!)"
                            }
                        }
                        
                        if let address = result[0]["formatted_address"] as? String {
                            self.completeAddressDict["formatted_address"] = address
                            self.completeAddressString = address
                        }
                        
                        self.fillAllFields()
                        
                        if let geometry = result[0]["geometry"] as? JSONDictionary {
                            
                            if let location = geometry["location"] as? JSONDictionary {
                                
                                self.actualLocation = CLLocation.init(latitude: Double(location["lat"]! as! NSNumber), longitude: Double(location["lng"]! as! NSNumber))
                            }
                            
                        }
                        
                        
                        
                        //actualLocation = [[CLLocation alloc] initWithLatitude:[[[[[[returnDict objectForKey:@"results"] objectAtIndex:0]objectForKey:@"geometry"]objectForKey:@"location"] objectForKey:@"lat"] doubleValue] longitude:[[[[[[returnDict objectForKey:@"results"]objectAtIndex:0]objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"] doubleValue]];
                    }
                }
            }
            
            
        }



//        var request = ASIFormDataRequest(URL: url)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            //        NSLog(@"Lat Long Dict Data = %@",returnDict);
//            if !((returnDict["status"] as! String) == "INVALID_REQUEST") {
//                var typeArr = (((returnDict.value(forKey: "results") as! String)[0].value(forKey: "address_components") as! String).value(forKey: "types") as! String)
//                completeAddressDict = [AnyHashable: Any]()
//                for i in 0..<typeArr.count {
//                    completeAddressDict[typeArr[i][0]] = (((returnDict.value(forKey: "results") as! String)[0].value(forKey: "address_components") as! String)[i].value(forKey: "long_name") as! String)
//                }
//                completeAddressString = ((returnDict.value(forKey: "results") as! String)[0].value(forKey: "formatted_address") as! String)
//                completeAddressDict["formatted_Address"] = completeAddressString
//                print("Dictionary Data : \(completeAddressDict)")
//                self.fillAllFields()
//                actualLocation = CLLocation(latitude: ((((returnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lat"] as! String).doubleValue, longitude: ((((returnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lng"] as! String).doubleValue)
//            }
//        }
//        appDelegate.removeChargementLoader()
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Error Dict = \(returnDict)")
//        var .error = request.error
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }



}
//
//  MapFieldsFormController.m
//  Beehive
//
//  Created by Deepak Dixit on 06/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
