//
//  UserProfileViewController.swift
//  Beehive
//
//  Created by SoluLab on 02/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class UserProfileViewController: BaseViewController, UITableViewDelegate,UITableViewDataSource,NotificationViewPrtotcol {


    var editButton: UIButton!
    @IBOutlet var customToolBar: UIToolbar!
    @IBOutlet var customTableView: UITableView!
    var notificationButton: UIButton!
    var notificationView: NotificationView!
    var notificationBarButton: UIBarButtonItem!
    var isNotificationButtonSelected = false
    var profileDataDict = NSMutableDictionary()

    var myProfileDataDict = NSMutableDictionary()
    var userProfileImage: UIImage!

    var cellRowHeight: CGFloat = 0.0
    var tempData = NSMutableArray()
    var dataDict = NSMutableDictionary()
    var bundlePath: String!
//    var screenSize = CGSize.zero
//    var appDelegate: BeehiveAppDelegate!
    var completeAddressString: String!
    var completeServicesString: String!
    var img: UIImage!

    var userDict:JSONDictionary = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)

        // Do any additional setup after loading the view.
        img = UIImage()
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        customToolBar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        //    UIBarButtonItem *flexSpace1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(150), y: CGFloat(0), width: CGFloat(120), height: CGFloat(44)))
        titleLabel.text = "Profile"
        titleLabel.textAlignment = .left
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold18
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        editButton = UIButton(type: .custom)
        editButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(50), height: CGFloat(20))
        editButton.backgroundColor = UIColor.lightGray
        editButton.layer.cornerRadius = 10.0
        editButton.setTitle("Edit", for: .normal)
        editButton.titleLabel!.font = CenturyGothiCRegular14
        editButton.addTarget(self, action: #selector(self.editProfile), for: .touchUpInside)
        let editBtn = UIBarButtonItem(customView: editButton)
        var barItems = [UIBarButtonItem]()
        barItems.append(flexSpace)
        barItems.append(toolbarTitleLbl)
        barItems.append(editBtn)
        customToolBar.setItems(barItems, animated: true)
        bundlePath = Bundle.main.path(forResource: "TempDataList", ofType: "plist")!



        dataDict = NSMutableDictionary(contentsOfFile: bundlePath)!
        //    NSMutableString *str; }

        customTableView.reloadData()
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    func updateNotificationCountValue(_ count: Any) {
        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
        if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
        isNotificationButtonSelected = false

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }



    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getProfileData()
    }

    func getProfileData() {
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.getUserProfileDetails { (dict:JSONDictionary?, error:NSError?) in
           
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                self.userDict = dict!
                self.customTableView.delegate = self
                self.customTableView.dataSource = self
                self.addressAndServiceString()
                self.customTableView.reloadData()
            }
            
        }
    }


    func editProfile() {

        let editProfile = STORYBOARD.instantiateViewController(withIdentifier: "ProfileEditViewController") as! ProfileEditViewController
        
        editProfile.profileDataDict = self.userDict
        editProfile.userProfileImage = userProfileImage
        editProfile.servicesString = completeServicesString
//        userProfileImage = nil
        self.navigationController!.pushViewController(editProfile, animated: true)
    }

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
             if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell: UserProfileCell!

//        let profilDict = ((profileDataDict.object(forKey: "data") as! NSDictionary).object(forKey: "User") as! NSDictionary)

        let profilDict = self.userDict["data"]!["User"]! as! [String:String]

        if indexPath.section == 0 {
            let CellIdentifier = "FirstCell"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[0] as! UserProfileCell
            }


            var firstLabel = profilDict["first_name"]!
            var secondLabel = profilDict["last_name"]!

            let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(18.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))

            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(14.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            cell.userName.attributedText = customTextLabelAttribute
            let url = URL(string: ImageBaseUrl + profilDict["user_avatar"]!)
            cell.profileImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
//            cell.profileImage.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)

            cell.companyName.text = profilDict["user_company"]!
            cell.companyName.textColor = UIColor.darkGray
            cell.companyName.font = CenturyGothiCRegular15

        } else if indexPath.section == 1 {
            let CellIdentifier = "contact"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! UserProfileCell!
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[1] as? UserProfileCell
            }
            var firstLabel = ""
            var secondLabel = ""
            if indexPath.row == 0 {
                firstLabel = "Work"
                secondLabel = profilDict["user_contact_no_work"]!
            }
            else if indexPath.row == 1 {
                firstLabel = "Mobile"
                secondLabel = profilDict["user_mobile"]!
            }
            else if indexPath.row == 2 {
                firstLabel = "Email"
                secondLabel = profilDict["email"]!
            }

            let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            cell.numberTextLbl.attributedText = customTextLabelAttribute

        } else if indexPath.section == 2 {
            let CellIdentifier = "serviceCell"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as! UserProfileCell!
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[2] as! UserProfileCell
            }
            var firstLabel = ""
            var secondLabel = ""
            if indexPath.row == 0 {
                firstLabel = "Address"
                secondLabel = completeAddressString
            }
            else if indexPath.row == 1 {
                firstLabel = "Service"
                secondLabel = completeServicesString
            }

            cell.descriptionTitleLabel.text = firstLabel
            cell.descriptionLabel.numberOfLines = 0
            cell.descriptionLabel.text = secondLabel
            cell.descriptionLabel.font = CenturyGothiCRegular15
            cell.descriptionTitleLabel.font = CenturyGothiCRegular12
            cell.descriptionTitleLabel.textColor = UIColor.darkGray
            cell.descriptionLabel.textColor = UIColor.darkGray
        }
        cell.selectionStyle = .none
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 3
        }
        else if section == 2 {
            return 2
        }
        
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            cellRowHeight = 110
        }
        else if indexPath.section == 1 {
            cellRowHeight = 56
        }
        else if indexPath.section == 2 {
            cellRowHeight = 121.0
        }
        
        return cellRowHeight
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func addressAndServiceString() {
        customTableView.delegate = self
        customTableView.dataSource = self
        var streetNo: String

        var arr = [String]()

        let profilDict = self.userDict["data"]!["User"]! as! [String:String]

        if (profilDict["user_street_number"] != "") {
            streetNo = profilDict["user_street_number"]!
            arr.append(streetNo)
        }
        else {
            streetNo = ""
            //        [arr addObject:streetNo];
        }
        var street: String! = ""
        if (profilDict["user_street"] != "") {
            street = profilDict["user_street"]!
            arr.append(street)
        }
        else {
            street = ""
            //        [arr addObject:street];
        }
        var city: String! = ""
        if (profilDict["user_city"] != "") {
            city = profilDict["user_city"]!

            arr.append(city)
        }
        else {
            city = ""
            //        [arr addObject:city];
        }
        var state: String! = ""
        if (profilDict["user_state"] != "") {
            state = profilDict["user_state"]!
            arr.append(state)
        }
        else {
            state = ""
        }
        var country: String! = ""
        if (profilDict["user_country"] != "") {
            country = profilDict["user_country"]!
            arr.append(country)
        }
        else {
            country = ""
            //        [arr addObject:country];
        }
        var pincode: String! = ""
        if (profilDict["user_postal_code"] != "") {
            pincode = profilDict["user_postal_code"]!
            arr.append(pincode)
        }
        else if pincode.characters.count > 0 {
            arr.append(pincode)
        } else {
            pincode = ""
        }

        //    [arr addObject:pincode];
        print("Array Data : \(arr)")
        completeAddressString = String()
        for i in 0..<arr.count {
            if i == 1 || i == 3 {
                completeAddressString! += ","
            }
            else if i == 2 || i == 4 {
                completeAddressString! += "\n"
            }

            completeAddressString! += "\(arr[i])"
        }
        print("Adrees String ....= \(completeAddressString)")
        completeServicesString = String()
        print(self.userDict)
//        let servicesArray = self.userDict["data"]!["UserService"]! as! [String:String]
//        print(servicesArray)
//        for i in 0..<servicesArray.count {
//            if i == 1 || i == 3 || i == 5 {
//                completeServicesString! += ","
//            }
//            else if i == 2 || i == 4 {
//                completeServicesString! += "\n"
//            }
//            completeServicesString! += "\(servicesArray["service"])"
//        }


        if let str = self.userDict["data"]!["UserService"]! as? [JSONDictionary] {

            if str.count > 1 {
                completeServicesString = str[0]["service"] as! String!
            }


        }
        customTableView.reloadData()
        print("services String ....= \(completeServicesString)")

    }

}
