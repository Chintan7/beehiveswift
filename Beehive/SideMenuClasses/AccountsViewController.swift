//
//  AccountsViewController.swift
//  Beehive
//
//  Created by SoluLab on 02/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class AccountsViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate,NotificationViewPrtotcol {

    var topToolbar: UIToolbar!
    var billingInfoButton: UIButton!
    var accountDetailsView: UIView!
    var tableViewBillingInfo: UITableView!
    var notificationView: NotificationView!
    var notificationBarButton: UIBarButtonItem!
    var isNotificationButtonSelected = false
    var notificationButton: UIButton!
    var billingInfoArray = NSMutableArray()
    var billindInfoCardDetail: JSONDictionary = [:]
    var billingInfoDict: JSONDictionary = [:]
    @IBOutlet var buttonChangeCard: UIButton!
    @IBOutlet var buttonCancel: UIButton!
    @IBOutlet var alertTitleLabel: UILabel!
    var viewChangeCardAlert: UIView!
    var viewBGChangeCardAlert: UIView!
    @IBOutlet var billingDetailView: UIView!
    @IBOutlet var numberOfActiveCrewLbl: UILabel!
    @IBOutlet var activeCrewAmountLabel: UILabel!
    @IBOutlet var taxLabel: UILabel!
    @IBOutlet var totalAmountLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
    var transactionId: String!
    var isSelectedBillingButton = false
    var billingInfoView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
        topToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        topToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(100), y: CGFloat(0), width: CGFloat(100), height: CGFloat(44)))
        titleLabel.text = "Account"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold18
        topToolbar.addSubview(titleLabel)
        self.view.addSubview(topToolbar)
        billingInfoButton = UIButton(type: .custom)
        billingInfoButton.frame = CGRect(x: CGFloat(20), y: CGFloat(50), width: CGFloat(280), height: CGFloat(35))
        billingInfoButton.setImage(UIImage(named: "plusBillingInfo.png")!, for: .normal)

        billingInfoButton.setImage(UIImage(named: "minusBillingInfo.png")!, for: .selected)
        billingInfoButton.addTarget(self, action: #selector(self.billingInfoButtonSelection), for: .touchUpInside)
        self.view.addSubview(billingInfoButton)
        billingInfoView = UIView(frame: CGRect(x: CGFloat(10), y: CGFloat(85), width: CGFloat(300), height: CGFloat(110)))
        billingInfoView.backgroundColor = UIColor(red: CGFloat(255 / 255.0), green: CGFloat(255 / 255.0), blue: CGFloat(224 / 255.0), alpha: CGFloat(1.0))
        self.view.addSubview(billingInfoView)
        billingInfoView.isHidden = true
        if screenSize.height == 568 {
            billingDetailView.frame = CGRect(x: CGFloat(billingDetailView.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(billingDetailView.frame.size.width), height: CGFloat(screenSize.height))
        }
        else {
            billingDetailView.frame = CGRect(x: CGFloat(billingDetailView.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(billingDetailView.frame.size.width), height: CGFloat(screenSize.height))
        }
        accountDetailsView = UIView()
        tableViewBillingInfo = UITableView()
        if screenSize.height == 568 {
            accountDetailsView.frame = CGRect(x: CGFloat(0), y: CGFloat(88), width: CGFloat(320), height: CGFloat(410))
            tableViewBillingInfo.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(320), height: CGFloat(390))
        }
        else {
            accountDetailsView.frame = CGRect(x: CGFloat(0), y: CGFloat(88), width: CGFloat(320), height: CGFloat(325))
            tableViewBillingInfo.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(320), height: CGFloat(305))
        }
        let historyLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(100), height: CGFloat(18)))
        historyLabel.text = "History"
        historyLabel.font = CenturyGothiCRegular15
        historyLabel.textColor = UIColor.darkGray
        accountDetailsView.addSubview(historyLabel)
        let invoiceButton = UIButton(type: .custom)
        invoiceButton.frame = CGRect(x: CGFloat(240), y: CGFloat(0), width: CGFloat(60), height: CGFloat(18))
        invoiceButton.setImage(UIImage(named: "invoiceBtn.png")!, for: .normal)
        invoiceButton.isUserInteractionEnabled = true
        accountDetailsView.addSubview(invoiceButton)
        let divideLine = UILabel(frame: CGRect(x: CGFloat(5), y: CGFloat(18), width: CGFloat(310), height: CGFloat(1)))
        divideLine.backgroundColor = UIColor.gray
        accountDetailsView.addSubview(divideLine)
        tableViewBillingInfo.dataSource = self
        tableViewBillingInfo.delegate = self
        accountDetailsView.addSubview(tableViewBillingInfo)
        self.view.addSubview(accountDetailsView)
        self.getAccountsBillingInfo()
    }

    func updateNotificationCountValue(_ count: Any) {
        //        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func getAccountsBillingInfo() {
        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.getAccountsBillingInfo { (returnDict:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {

//                self.billindInfoCardDetail = returnDict!
                
                if let arr = returnDict?["billing_information"] as? [String:String] {
                    
                    if arr.count > 0 {
                        self.billindInfoCardDetail = JSONDictionary()
                        self.billindInfoCardDetail = returnDict?["billing_information"] as! JSONDictionary
                        
                        print(self.billindInfoCardDetail)
                        self.billingInfoDict = JSONDictionary() /* capacity: 0 */
                        
                        if let dict = returnDict?["transaction"] as? JSONDictionary {
                            self.billingInfoDict = dict
                        }
                        
                        self.tableViewBillingInfo.reloadData()
                        self.viewCustomization()
                    }
                    
                    
                }

//                if (returnDict?["billing_information"] as! [String:String]).count == 0 {
//
//                } else {
//                   
//                }


            }
        }

    }
    
    func viewCustomization() {
        let nameLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(5), width: CGFloat(200), height: CGFloat(35)))
        nameLabel.numberOfLines = 2
        var firstLabel = "Name"
        var secondLabel = "\(billindInfoCardDetail["first_name"] as! String) \(billindInfoCardDetail["last_name"] as! String)"
        let customNameLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customNameLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular11, range: NSRange(location: 0, length: firstLabel.characters.count))
        customNameLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customNameLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular15, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customNameLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        nameLabel.attributedText = customNameLabelAttribute
        billingInfoView.addSubview(nameLabel)
        let cardLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(40), width: CGFloat(170), height: CGFloat(35)))
        cardLabel.numberOfLines = 2
        var cardName = "Card Name"
        var cardNumber = (billindInfoCardDetail["number"] as! String)
        let customCardLabelAttribute = NSMutableAttributedString(string: "\(cardName)\n\(cardNumber)")
        customCardLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular11, range: NSRange(location: 0, length: cardName.characters.count))
        customCardLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: cardName.characters.count))
        customCardLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular15, range: NSRange(location: cardName.characters.count + 1, length: cardNumber.characters.count))
        customCardLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: cardName.characters.count + 1, length: cardNumber.characters.count))
        cardLabel.attributedText = customCardLabelAttribute
        billingInfoView.addSubview(cardLabel)
        let cardExpiryLabel = UILabel(frame: CGRect(x: CGFloat(220), y: CGFloat(57), width: CGFloat(70), height: CGFloat(15)))
        cardExpiryLabel.text = "\(self.billindInfoCardDetail["expire_month"]!)/\(billindInfoCardDetail["expire_year"]!)"
        cardExpiryLabel.font = CenturyGothiCRegular15
        cardExpiryLabel.textColor = UIColor.darkGray
        billingInfoView.addSubview(cardExpiryLabel)
        let changeCardButton = UIButton(type: .custom)
        changeCardButton.frame = CGRect(x: CGFloat(210), y: CGFloat(80), width: CGFloat(90), height: CGFloat(25))
        changeCardButton.backgroundColor = UIColor.clear
        changeCardButton.setTitle("Change >", for: .normal)
        changeCardButton.titleLabel!.font = CenturyGothicBold18
        changeCardButton.addTarget(self, action: #selector(self.changeCardInfo), for: .touchUpInside)
        //    [changeCardButton setTintColor:[UIColor darkGrayColor]];
        changeCardButton.setTitleColor(UIColor.darkGray, for: .normal)
        billingInfoView.addSubview(changeCardButton)
        self.view.addSubview(billingInfoView)
        billingInfoView.isHidden = true
        viewBGChangeCardAlert = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(568)))
        viewBGChangeCardAlert.backgroundColor = UIColor.darkGray
        viewBGChangeCardAlert.alpha = 0.6
        self.view.addSubview(viewBGChangeCardAlert)
        viewChangeCardAlert = UIView(frame: CGRect(x: CGFloat(20), y: CGFloat(80), width: CGFloat(280), height: CGFloat(180)))
        viewChangeCardAlert.backgroundColor = UIColor.white
        viewChangeCardAlert.layer.borderColor = UIColor.gray.cgColor
        viewChangeCardAlert.layer.cornerRadius = 5.0
        let viewLabel = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(5), width: CGFloat(260), height: CGFloat(56)))
        viewLabel.text = "Remove existing credit card and add a new one ?"
        viewLabel.numberOfLines = 2
        viewLabel.textColor = UIColor.darkGray
        viewLabel.font = CenturyGothiCRegular15
        viewChangeCardAlert.addSubview(viewLabel)
        let changeCreditCardButton = UIButton(type: .custom)
        changeCreditCardButton.frame = CGRect(x: CGFloat(10), y: CGFloat(75), width: CGFloat(260), height: CGFloat(35))
        changeCreditCardButton.setImage(UIImage(named: "changecreditCard.png")!, for: .normal)
        changeCreditCardButton.addTarget(self, action: #selector(self.changeCardButtonClick), for: .touchUpInside)
        viewChangeCardAlert.addSubview(changeCreditCardButton)
        let changeCardCancelButton = UIButton(type: .custom)
        changeCardCancelButton.frame = CGRect(x: CGFloat(10), y: CGFloat(120), width: CGFloat(260), height: CGFloat(35))
        changeCardCancelButton.setImage(UIImage(named: "cancelBtn.png")!, for: .normal)
        changeCardCancelButton.titleLabel!.textColor = UIColor.darkGray
        changeCardCancelButton.titleLabel!.font = CenturyGothiCRegular15
        changeCardCancelButton.addTarget(self, action: #selector(self.cancelCardUpdateButtonClick), for: .touchUpInside)
        viewChangeCardAlert.addSubview(changeCardCancelButton)
        self.view.addSubview(viewChangeCardAlert)
        viewBGChangeCardAlert.isHidden = true
        viewChangeCardAlert.isHidden = true
    }

    func changeCardInfo() {
        viewBGChangeCardAlert.isHidden = false
        self.view.bringSubview(toFront: viewBGChangeCardAlert)
        viewChangeCardAlert.isHidden = false
        self.view.bringSubview(toFront: viewChangeCardAlert)
    }

    func changeCardButtonClick() {
        viewBGChangeCardAlert.isHidden = true
        viewChangeCardAlert.isHidden = true

        let cardViewControllert = STORYBOARD.instantiateViewController(withIdentifier: "CreditCardViewController") as! CreditCardViewController
        cardViewControllert.isPaymentIndicationFlag = true
        self.navigationController!.pushViewController(cardViewControllert, animated: true)
    }


    func cancelCardUpdateButtonClick() {
        viewBGChangeCardAlert.isHidden = true
        viewChangeCardAlert.isHidden = true
    }

    func billingInfoButtonSelection() {
        if !isSelectedBillingButton {
            billingInfoButton.isSelected = true
            isSelectedBillingButton = true
            print("Here we'll implement card info View...")
            billingInfoView.isHidden = false
            if screenSize.height == 568 {
                accountDetailsView.frame = CGRect(x: CGFloat(0), y: CGFloat(200), width: CGFloat(320), height: CGFloat(300))
                tableViewBillingInfo.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(320), height: CGFloat(280))
            }
            else {
                accountDetailsView.frame = CGRect(x: CGFloat(0), y: CGFloat(200), width: CGFloat(320), height: CGFloat(215))
                tableViewBillingInfo.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(320), height: CGFloat(195))
            }
        }
        else {
            print("Here we'll Remove card info View...")
            isSelectedBillingButton = false
            billingInfoButton.isSelected = false
            billingInfoView.isHidden = true
            if screenSize.height == 568 {
                accountDetailsView.frame = CGRect(x: CGFloat(0), y: CGFloat(88), width: CGFloat(320), height: CGFloat(410))
                tableViewBillingInfo.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(320), height: CGFloat(390))
            }
            else {
                accountDetailsView.frame = CGRect(x: CGFloat(0), y: CGFloat(88), width: CGFloat(320), height: CGFloat(325))
                tableViewBillingInfo.frame = CGRect(x: CGFloat(0), y: CGFloat(20), width: CGFloat(320), height: CGFloat(305))
            }
        }
    }

    @IBAction func doneButtonClick(_ sender: Any) {
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var frame = self.billingDetailView.frame
            frame.origin.y = screenSize.height
            self.billingDetailView.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
        })
    }

    @IBAction func emailButtonClick(_ sender: Any) {

    }

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return billingInfoDict.keys.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //[[billingInfoDict valueForKey:[[billingInfoDict allKeys] objectAtIndex:section]] count]

        let arr = Array(billingInfoDict.keys)

        let strArr = Array(arr.map { String($0) })
        if let str = strArr[section] {
            return ((billingInfoDict[str] as? [JSONDictionary])?.count)!
        }
        return 0

//        return (billingInfoDict[billingInfoDict.keys[section]] as [JSONDictionary]).count
//        let dic = billingInfoDict.keys
//        let sec = dic[section]
//        let ss = billingInfoDict.object(forKey: sec) as! NSArray
//        return ss.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MyCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier) as?
        AccountsTableViewCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("AccountsTableViewCell", owner: self, options: nil)
            cell = nib?[0] as? AccountsTableViewCell
        }


        let arr = Array(billingInfoDict.keys)
        let strArr = Array(arr.map { String($0) })

        let sec = strArr[indexPath.section]
        let ss = billingInfoDict[sec!] as! [JSONDictionary]


        let dateStr = ss[indexPath.row]["billing_start"]!
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "YYYY-MM-dd"
        let date = dateFormat.date(from: dateStr as! String)!
        dateFormat.dateFormat = "MMMM"
        let monthName = dateFormat.string(from: date)
        cell?.monthNameLabel.text = monthName
        cell?.monthNameLabel.font = CenturyGothiCRegular18
        cell?.amountLabel.text = ss[indexPath.row]["transaction_total"] as! String?
        cell?.amountLabel.font = CenturyGothiCRegular18
        return cell!

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showNotificationView(_ note: Notification) {
        //    NSLog(@"Received Notification - Someone seems to have logged in");
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }
    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {

        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")

        if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            invitaionView.notificationMessage = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in_declined") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "checked_out") {
            APPDELEGATE.notificationReadWebService(notificationID: ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String))
            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
            notificationView.notificationTable.reloadData()
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "user_profile") {
            APPDELEGATE.navigate(toProfileView: self)
        } else if (((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "type") as! String) == "tentative_checked_in") {

            let invitaionView = STORYBOARD.instantiateViewController(withIdentifier: "TentativeCheckInViewController") as! TentativeCheckInViewController
            invitaionView.notificationId = ((APPDELEGATE.allNotificationArray[indexPath.row] as! NSDictionary).value(forKey: "id") as! String)
            self.navigationController!.pushViewController(invitaionView, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
