//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CrewProfileViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 25/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
import MessageUI

class CrewProfileViewController: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet var alertPopupView: UIView!
    @IBOutlet var alertMessageLabel: UILabel!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var keepButton: UIButton!
    var notificationBarButton: UIBarButtonItem!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!
    var notificationButton: UIButton!
    var parentController: ProjectCrewListViewController!
    var crewDetailDict = JSONDictionary()
    var projectId: String!
    var indexPathOfCrew: IndexPath!
    var isCantRemove = false
    var isComeFromCrewList = false
    var cellRowHeight: CGFloat = 0.0
    var bundlePath = ""
    var dataDict = JSONDictionary()
    var menuButton: UIBarButtonItem!
    var backBtn: UIBarButtonItem!
    var removeButton: UIBarButtonItem!
    var contactNumbersArray = [JSONDictionary]()
    var addressServiceArray = JSONDictionary()
    var completeAddressString = ""
    var customLabelHt: CGFloat = 0.0
    @IBOutlet var table: UITableView!
    var user = JSONDictionary()

    @IBAction func deleteButtonPressed(_ sender: Any) {
        self.deleteCrewMemberWeservice()
        backgroundView.isHidden = true
        alertPopupView.isHidden = true
        menuButton.isEnabled = true
        backBtn.isEnabled = true
        removeButton.isEnabled = true
    }

    @IBAction func keepButtonPressed(_ sender: Any) {
        backgroundView.isHidden = true
        alertPopupView.isHidden = true
        menuButton.isEnabled = true
        backBtn.isEnabled = true
        removeButton.isEnabled = true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        print("Project Id ******* \(projectId)")
//        print("Crew Detail Dict : \(crewDetailDict)")
//        print("Index Path : row:\(self.indexPathOfCrew.row) section:\(self.indexPathOfCrew.section)")
//        self.addressAndServiceString()

        if let userTemp = crewDetailDict["User"] as? JSONDictionary {
            self.user = userTemp
            self.table.delegate = self
            self.table.dataSource = self
            self.table.reloadData()
        }

        self.viewCustomization()
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        self.navigationController!.isNavigationBarHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        let image = UIImage(named: "navbar.png")!
        UINavigationBar.appearance().setBackgroundImage(image, for: .default)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewCustomization() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        backgroundView.isHidden = true
        alertPopupView.isHidden = true
        menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //    Toolbar
        var barItems = [UIBarButtonItem]()
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        backBtn = UIBarButtonItem(customView: button)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(44)))
        titleLabel.text = "Crew"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold15
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        let flexSpace1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let rmveBtn = UIButton(type: .custom)
        rmveBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(60), height: CGFloat(20))
        rmveBtn.setTitle("Remove", for: .normal)
        rmveBtn.setTitleColor(UIColor.darkGray, for: .normal)
        rmveBtn.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(13.0))!
        rmveBtn.addTarget(self, action: #selector(self.removeButtonClick), for: .touchUpInside)
        barItems.append(backBtn)
        barItems.append(flexSpace)
        barItems.append(toolbarTitleLbl)
        barItems.append(flexSpace1)
        if self.isCantRemove == false {
            removeButton = UIBarButtonItem(customView: rmveBtn)
            barItems.append(removeButton)
        }
        else {

        }
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
            //    Remove Crew Alert View
        let firstLabel = "Are you sure you wish to delete this crew member from the project?"
        let secondLabel = "All associated reports will also be deleted."
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular18, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular14, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        alertMessageLabel.attributedText = customTextLabelAttribute
        deleteButton.setTitleColor(UIColor(red: CGFloat(253.0), green: CGFloat(211.0), blue: CGFloat(178.0), alpha: CGFloat(0.6)), for: .normal)
        keepButton.titleLabel!.font = CenturyGothiCRegular18
        alertPopupView.layer.borderColor = UIColor.darkGray.cgColor
        alertPopupView.layer.borderWidth = 1.0
        alertPopupView.layer.cornerRadius = 3.0
    }

    func backBtnClick() {
        popTo()
    }

    func removeButtonClick() {


        if let checkIn = self.crewDetailDict["CheckinCheckoutTracker"] as? JSONDictionary {

            if let statusTemp = checkIn["status"] as? String  {
                if statusTemp == "checked in" {

                    let myAlert = UIAlertView(title: "Beehive", message: "Sorry, Crew is currently Checked In to your Project", delegate: nil, cancelButtonTitle: "OK")
                    myAlert.show()
                    return
                }
            }
        }

        backgroundView.isHidden = false
        alertPopupView.isHidden = false
        menuButton.isEnabled = false
        backBtn.isEnabled = false
        removeButton.isEnabled = false
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }
// MARK: TableView Datasource Delegate

    func numberOfSections(inTableView tableView: UITableView) -> Int {
        return 6
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UserProfileCell?
        //    NSLog(@"Section : %d",indexPath.section);
        if indexPath.section == 0 {
            let CellIdentifier = "FirstCell"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[0] as? UserProfileCell
            }
            
            let firstLabel = "\(self.user["first_name"]!) \(self.user["last_name"]!)"
            let secondLabel = "\(self.user["user_city"]!) \(self.user["user_country"]!)"
            let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular18, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(14.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            cell!.userName.attributedText = customTextLabelAttribute
            cell!.companyName.text = (self.user["user_company"]!) as? String
            let url = URL(string: "\(ImageBaseUrl)\(self.user["user_avatar"]!)")!
            cell!.profileImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
        }
        else if indexPath.section == 1 {
            let
            CellIdentifier = "contact"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[1] as? UserProfileCell
            }
            let firstLabel = "Mobile"
            let secondLabel = self.user["user_mobile"]! as! String
                //        NSString *secondLabel = @"+5462485546";
            let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: firstLabel.characters.count, length: secondLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            cell!.numberTextLbl.attributedText = customTextLabelAttribute
            let callButton = UIButton(frame: CGRect(x: CGFloat(275), y: CGFloat(15), width: CGFloat(15), height: CGFloat(21)))
            callButton.setImage(UIImage(named: "mobileIcon.png")!, for: .normal)
            callButton.addTarget(self, action: #selector(self.callButtonPressed), for: .touchUpInside)
            cell!.contentView.addSubview(callButton)
            let smsButton = UIButton(frame: CGRect(x: CGFloat(230), y: CGFloat(15), width: CGFloat(25), height: CGFloat(25)))
            smsButton.setImage(UIImage(named: "chatIcon.png")!, for: .normal)
            smsButton.addTarget(self, action: #selector(self.smsButtonPressed), for: .touchUpInside)
            cell!.contentView.addSubview(smsButton)
        }
        else if indexPath.section == 2 {
            let CellIdentifier = "contact"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[1] as? UserProfileCell
            }
            let firstLabel = "Work"
            let secondLabel = self.user["user_contact_no_work"]! as! String
            let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: firstLabel.characters.count, length: (secondLabel.characters.count)))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: (secondLabel.characters.count)))
            cell!.numberTextLbl.attributedText = customTextLabelAttribute
            let callButton = UIButton(frame: CGRect(x: CGFloat(275), y: CGFloat(10), width: CGFloat(15), height: CGFloat(21)))
            callButton.setImage(UIImage(named: "mobileIcon.png")!, for: .normal)
            callButton.addTarget(self, action: #selector(self.callWorkPhoneButtonPressed), for: .touchUpInside)
            cell!.contentView.addSubview(callButton)
        }
        else if indexPath.section == 3 {
            let CellIdentifier = "contact"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[1] as? UserProfileCell
            }
            let firstLabel = "Email"
            let secondLabel = self.user["email"]! as! String
            let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel)")
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: 0, length: firstLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(16.0))!, range: NSRange(location: firstLabel.characters.count, length: secondLabel.characters.count))
            customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
            cell!.numberTextLbl.attributedText = customTextLabelAttribute
            let mailButton = UIButton(frame: CGRect(x: CGFloat(275), y: CGFloat(20), width: CGFloat(20), height: CGFloat(15)))
            mailButton.setImage(UIImage(named: "emailIcon.png")!, for: .normal)
            mailButton.addTarget(self, action: #selector(self.sendMailButtonPressed), for: .touchUpInside)
            cell!.contentView.addSubview(mailButton)
        }
        else if indexPath.section == 4 {
            let CellIdentifier = "serviceCell"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[2] as? UserProfileCell
            }
            let str = "Address"
            cell!.descriptionTitleLabel.text = str
            cell!.descriptionTitleLabel.textColor = UIColor.gray
            cell!.descriptionTitleLabel.font = CenturyGothiCRegular12
            let addressStr = self.addressAndServiceString()
            let descriptionLabel = UILabel()
            let ht1: Int = 22 * ((str.characters.count / 25) + 1)
            let ht2: Int = 22 * ((addressStr.characters.count / 25) + 1)
            customLabelHt = CGFloat(ht1) + CGFloat(ht2)
            if customLabelHt < 24 {
                customLabelHt = 21.0
            }
            descriptionLabel.frame = CGRect(x: CGFloat(10), y: CGFloat(15), width: CGFloat(291), height: CGFloat(customLabelHt))
            descriptionLabel.numberOfLines = 0
            descriptionLabel.lineBreakMode = .byWordWrapping
            descriptionLabel.font = CenturyGothiCRegular15
            descriptionLabel.textColor = UIColor.darkGray
            descriptionLabel.text = completeAddressString
            cell!.contentView.addSubview(descriptionLabel)
            let locationButton = UIButton(frame: CGRect(x: CGFloat(275), y: CGFloat(10), width: CGFloat(25), height: CGFloat(25)))
            locationButton.setImage(UIImage(named: "markerPinGrey.png")!, for: .normal)
            locationButton.addTarget(self, action: #selector(self.locationButtonPressed), for: .touchUpInside)
            cell!.contentView.addSubview(locationButton)
        }
        else if indexPath.section == 5 {
            let CellIdentifier = "serviceCell"
            cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? UserProfileCell
            if cell == nil {
                var nib = Bundle.main.loadNibNamed("UserProfileCell", owner: self, options: nil)
                cell = nib?[2] as? UserProfileCell
            }
            //        NSString *firstLabel = @"Services";
        }

        cell!.selectionStyle = .none
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {


        if indexPath.section == 0 {
            return 115.0
        }
        else if indexPath.section == 1 || indexPath.section == 2 || indexPath.section == 3 {
            return 56.0
        }
        else if indexPath.section == 4 {
            print("4th Row Height = \(customLabelHt)")
            return customLabelHt + 25
        }
        else if indexPath.section == 5 {
            return 121
        }
        else {
            return 44
        }

    }

    func callButtonPressed() {
        UIApplication.shared.openURL(URL(string: "telprompt://\(self.user["user_mobile"] as! String)")!)
        //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",@"+919765968568"]]];
    }

    func callWorkPhoneButtonPressed() {
        UIApplication.shared.openURL(URL(string: "telprompt://\(self.user["user_contact_no_work"] as! String)")!)
    }

    func smsButtonPressed() {
        UINavigationBar.appearance().setBackgroundImage(nil, for: .default)
        print("Send SMS")

            //set receipients
        let recipients = [(self.user["user_mobile"] as! String)]
            //set message text
        let message = ""
        let messageController = MFMessageComposeViewController()
        messageController.messageComposeDelegate = self
        messageController.recipients = recipients
        messageController.body = message
        // Present message view controller on screen
        self.present(messageController, animated: true, completion: { _ in })
    }

    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: { _ in })
    }

    func sendMailButtonPressed() {
        UINavigationBar.appearance().setBackgroundImage(nil, for: .default)
        let emailViewController = MFMailComposeViewController()
        //    [emailViewController setMessageBody:@"<p>Send me a message is you like this tutorial :)<p>" isHTML:YES];
        // adding recipients
        emailViewController.setToRecipients([(self.user["email"] as! String)])
        emailViewController.mailComposeDelegate = self
        //    [navView setHidden:YES];
        self.present(emailViewController, animated: true, completion: { _ in })
    }



    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: { _ in })
    }

    func locationButtonPressed() {
        print("Location Pressed")
//        var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
        let mapView = STORYBOARD.instantiateViewController(withIdentifier: "CrewLocationMapViewController") as! CrewLocationMapViewController

        if let lat = self.user["user_lat"] as? String {
            mapView.locationLatitude = Double(lat)!
        }

        if let lat = self.user["user_lng"] as? String {
            mapView.locationLongitude = Double(lat)!
        }

//        mapView.locationLatitude = Double(self.user["user_lat"])
//        mapView.locationLongitude = Double(self.user["user_lng"] as! NSNumber)
        self.navigationController!.pushViewController(mapView, animated: true)
    }

    func addressAndServiceString() -> String {
        print("Crew Detail : \(crewDetailDict)")
        let arr = NSMutableArray()
        var streetNo: String
        if !((self.user["user_street_number"] as! String) == "") {
            streetNo = (self.user["user_street_number"] as! String)
            arr.add(streetNo)
        }
        else {
            streetNo = ""
            //        [arr addObject:streetNo];
        }
        var street: String
        if !((self.user["user_street"] as! String) == "") {
            street = (self.user["user_street"] as! String)
            arr.add(street)
        }
        else {
            street = ""
            //        [arr addObject:street];
        }
        var city: String
        if !((self.user["user_city"] as! String) == "") {
            city = (self.user["user_city"] as! String)
            arr.add(city)
        }
        else {
            city = ""
            //        [arr addObject:city];
        }
        var state: String
        if !((self.user["user_state"] as! String) == "") {
            state = (self.user["user_state"] as! String)
            arr.add(state)
        }
        else {
            state = ""
        }
        var country: String
        if !((self.user["user_country"] as! String) == "") {
            country = (self.user["user_country"] as! String)
            arr.add(country)
        }
        else {
            country = ""
            //        [arr addObject:country];
        }
        var pincode = String()
        if !((self.user["user_postal_code"] as! String) == "") {
            pincode = (self.user["user_postal_code"] as! String)
        }
        else if pincode.characters.count > 0 {
            arr.add(pincode)
        }
        else {
            pincode = ""
        }

        //    [arr addObject:pincode];
        print("Array Data : \(arr)")
        completeAddressString = String()
        for i in 0..<arr.count {
            if i == 1 || i == 3 {
                completeAddressString += ","
            }
            else if i == 2 || i == 4 {
                completeAddressString += "\n"
            }

            completeAddressString += "\(arr[i])"
        }
        print("Adrees String ....= \(completeAddressString)")
        return completeAddressString
    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {

        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        //    NSLog(@"Received Notification - Someone seems to have logged in");
//        notificationView = NotificationView()
//        notificationView.customView()
//        if screenSize.height == 568 {
//            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
//        }
//        else {
//            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
//        }
//        notificationView.delegate = self
//        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

//    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray[indexPath.row])")
//        if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            APPDELEGATE.notificationReadWebService((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            APPDELEGATE.navigate(toProfileView: self)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        //    else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
// MARK: - ASIHTTP Requests

    func deleteCrewMemberWeservice() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(DeleteCrewMemberURL)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!

        var dict: JSONDictionary = [:]
        dict["project_id"] = "\(projectId!)" as AnyObject?
        dict["crew_id"] = "\(self.user["id"]!)" as AnyObject?

        print(dict)

        APIManager.sharedInstance.deleteCrewMemberWeservice(dict: dict) { (result:JSONDictionary?, error:NSError?) in

            if error == nil {

                if let err = result?["status"] as? String {

                    if err == "ERROR" {
                        let myAlert = UIAlertView(title: "Beehive", message: result?["message"] as! String?, delegate: nil, cancelButtonTitle: "OK")
                        myAlert.show()
                    } else {
                        if self.isComeFromCrewList == true {
                            self.parentController.projectCrewArray .remove(at: self.indexPathOfCrew.row)
                        }
                        self.popTo()
                    }

                }

            }
            

        }

            //    NSLog(@"Login URL- %@",url);
//        var request = ASIFormDataRequest(URL: url)
//        request.setPostValue(projectId, forKey: "project_id")
//        request.setPostValue(((crewDetailDict["User"] as! String).value(forKey: "id") as! String), forKey: "crew_id")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = [AnyHashable: Any]()
//            returnDict = request.responseString().jsonValue()
//            print("Return Delete Crew Response = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "ERROR") {
//                var myAlert = UIAlertView(title: "Beehive", message: ((returnDict["result"] as! String)["message"] as! String), delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
//                myAlert.show()
//            }
//            else {
//                if self.isComeFromCrewList {
//                    parentController.projectCrewArray.remove(at: self.indexPathOfCrew.row)
//                }
//                self.navigationController!.popViewController(animated: true)!
//            }
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        APPDELEGATE.removeChargementLoader()
//        var returnDict = request.responseString().jsonValue()
//        print("Return Fail Project Data Dict = \(returnDict)")
//        var error = request.error
//        if error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }

}
//
//  CrewProfileViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 25/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
