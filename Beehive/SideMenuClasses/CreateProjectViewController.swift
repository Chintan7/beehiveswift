//
//  CreateProjectViewController.swift
//  Beehive
//
//  Created by SoluLab on 02/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit
import GooglePlaces

class CreateProjectViewController: BaseViewController, UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet var fieldsContainer: UIScrollView!
    @IBOutlet var serachField: UITextField!
    @IBOutlet var projectNameField: UITextField!
    @IBOutlet var streetField: UITextField!
    @IBOutlet var streetNoField: UITextField!
    @IBOutlet var cityField: UITextField!
    @IBOutlet var stateField: UITextField!
    @IBOutlet var codeField: UITextField!
    @IBOutlet var countryField: UITextField!
    @IBOutlet var projectNameTextField: UITextField!
    @IBOutlet var projectNameLabel: UILabel!
    var addressString: String!
    var actualLocationCoordinates: CLLocationCoordinate2D!
    var completeAddressDict = NSMutableDictionary()
    var searchDataList = [GMSAutocompletePrediction]()
    var isCheckInSelected: Bool! = false
    var comingIndicationFlag: Bool! = false
    var completeAddressReturnDict = JSONDictionary()
    @IBOutlet var autocompleteDataTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewCustomization()
        serachField.delegate = self
        autocompleteDataTable.estimatedRowHeight = 44
        autocompleteDataTable.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewCustomization() {
        autocompleteDataTable.isHidden = true
        fieldsContainer.contentSize = CGSize(width: CGFloat(320), height: CGFloat(620))
        if screenSize.height != 568 {

            autocompleteDataTable.frame = CGRect(x: CGFloat(0), y: CGFloat(74), width: CGFloat(320), height: CGFloat(131))
        }
        else {

        }
        fieldsContainer.backgroundColor = UIColor.clear
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelProject))
        cancelButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = cancelButton
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        projectNameField.font = CenturyGothiCRegular15
        streetField.font = CenturyGothiCRegular15
        streetNoField.font = CenturyGothiCRegular15
        cityField.font = CenturyGothiCRegular15
        stateField.font = CenturyGothiCRegular15
        countryField.font = CenturyGothiCRegular15
        codeField.font = CenturyGothiCRegular15
        projectNameLabel.font = CenturyGothicBold15

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDataList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
//        cell?.textLabel?.text = ((searchDataList[indexPath.row] as NSDictionary).value(forKey: "description") as! String)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = self.searchDataList[indexPath.row].attributedFullText.string
        
        //    NSLog(@"Cell COntent = %@",[[searchDataList objectAtIndex:indexPath.row] objectForKey:@"description"]);
        cell.tag = indexPath.row
        return cell
    }

    @IBAction func addButtonClicked(_ sender: UIButton) {

        if (projectNameField.text?.characters.count)! == 0 || streetNoField.text?.characters.count == 0 || streetField.text?.characters.count == 0 || cityField.text?.characters.count == 0 || countryField.text?.characters.count == 0 || stateField.text?.characters.count == 0 || codeField.text?.characters.count == 0 {
            let myAlert = UIAlertView(title: "Warning", message: "Please fill all the fields", delegate: nil, cancelButtonTitle: "OK")
            myAlert.show()
            return
        }

        let adjustRadius = STORYBOARD.instantiateViewController(withIdentifier: "AdjustRadiusViewController") as! AdjustRadiusViewController

        let dict = ["project_name":"\(projectNameField.text!)",
            "project_location":"\(serachField.text!)",
            "project_street_number":"\(streetNoField.text!)",
            "project_street":"\(streetField.text!)",
            "project_city":"\(cityField.text!)",
            "project_state":"\(stateField.text!)",
            "project_country":"\(countryField.text!)",
            "project_postal_code":"\(codeField.text!)"
        ]

        adjustRadius.circleCenter = actualLocationCoordinates
        adjustRadius.locationLatitude = actualLocationCoordinates.latitude
        adjustRadius.locationLongitude = actualLocationCoordinates.longitude
//        adjustRadius.actualLocation = actualLocationCoordinates
        adjustRadius.completeProjectAddressData = dict;
        adjustRadius.locationLatitude = 23.00
        adjustRadius.locationLongitude = 72.00
        pushToVC(controller: adjustRadius)

        /*
         UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
         AdjustRadiusViewController *newView = [sb instantiateViewControllerWithIdentifier:@"AdjustRadiusViewController"];
         newView.actualLocation = actualLocationCoordinates;
         [completeAddressDict removeAllObjects];
         [completeAddressDict setValue:projectNameField.text forKey:@"project_name"];
         [completeAddressDict setValue:serachField.text forKey:@"project_location"];
         [completeAddressDict setValue:streetNoField.text forKey:@"project_street_number"];
         [completeAddressDict setValue:streetField.text forKey:@"project_street"];
         [completeAddressDict setValue:cityField.text forKey:@"project_city"];
         [completeAddressDict setValue:stateField.text forKey:@"project_state"];
         [completeAddressDict setValue:countryField.text forKey:@"project_country"];
         [completeAddressDict setValue:codeField.text forKey:@"project_postal_code"];
         newView.completeProjectAddressData = completeAddressDict;
         newView.locationLatitude = [[[[[[completeAddressReturnDict objectForKey:@"results"]objectAtIndex:0]objectForKey:@"geometry"]objectForKey:@"location"]objectForKey:@"lat"] doubleValue];
         newView.locationLongitude = [[[[[[completeAddressReturnDict objectForKey:@"results"]objectAtIndex:0]objectForKey:@"geometry"]objectForKey:@"location"]objectForKey:@"lng"] doubleValue];
         [self.navigationController pushViewController:newView animated:YES];

         */
    }

    @IBAction func currentLocationClicked(_ sender: UIButton) {

        autocompleteDataTable.isHidden = true
        let currentVC = STORYBOARD.instantiateViewController(withIdentifier: "CurrentLocationViewController") as! CurrentLocationViewController

        pushToVC(controller: currentVC)
    }



    func cancelProject() {
        
        if self.comingIndicationFlag == true {
            //self.comingIndicationFlag = false
            if self.menuContainerViewController.menuState == MFSideMenuStateLeftMenuOpen {
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }
            else {
                self.menuContainerViewController.menuState = MFSideMenuStateLeftMenuOpen
            }
        }
        else {
            popTo()
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == serachField {
            
            let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            
            if (text.trim().characters.count > 0) {
//                self.requestGoogleWebServices()
                
            
                print(text)
                
                let place = GMSPlacesClient()
                place.autocompleteQuery(serachField.text!, bounds: nil, filter: nil, callback: { (data:[GMSAutocompletePrediction]?, error:Error?) in
                    
                    if error == nil {
                        
                        self.searchDataList.removeAll()
                        self.searchDataList = data!
                        if self.searchDataList.count > 0 {
                            self.autocompleteDataTable.isHidden = false
                        } else {
                            self.autocompleteDataTable.isHidden = true
                        }
                        self.autocompleteDataTable.reloadData()
                    } else {
                        print(error!)
                    }
                })
                
                return true
            }  else {
                self.autocompleteDataTable.isHidden = true
            }
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            self.autocompleteDataTable.isHidden = true
        }
        
        
        let place = GMSPlacesClient()
        print(self.searchDataList[indexPath.row].attributedSecondaryText!)
        print(self.searchDataList[indexPath.row].attributedPrimaryText)
        self.serachField.text = self.searchDataList[indexPath.row].attributedFullText.string
        APPDELEGATE.addChargementLoader()
        place.lookUpPlaceID(self.searchDataList[indexPath.row].placeID!) { (place:GMSPlace?, error:Error?) in
        APPDELEGATE.removeChargementLoader()
            
            self.actualLocationCoordinates = place?.coordinate
            
            
            self.streetField.text = ""
            self.streetNoField.text = ""
            self.cityField.text = ""
            self.stateField.text = ""
            self.countryField.text = ""
            self.codeField.text = ""
            
            if let addressLines = place?.addressComponents {
                // Populate all of the address fields we can find.
                for field in addressLines {
                    switch field.type {
                    case kGMSPlaceTypeStreetNumber:
                         print(field.name)
                        self.streetNoField.text = field.name
                    case kGMSPlaceTypeRoute:
                        print(field.name)
                        self.streetField.text = field.name
                    case kGMSPlaceTypeNeighborhood:
                        print(field.name)
                    case kGMSPlaceTypeLocality:
                        print(field.name)
                        self.cityField.text = field.name
                    case kGMSPlaceTypeAdministrativeAreaLevel1:
                        self.stateField.text = field.name
                        print(field.name)
                    case kGMSPlaceTypeCountry:
                        print(field.name)
                        self.countryField.text = field.name
                    case kGMSPlaceTypePostalCode:
                        print(field.name)
                        self.codeField.text = field.name
                    case kGMSPlaceTypePostalCodeSuffix:
                        print(field.name)
                    // Print the items we aren't using.
                    default:
//                        print("Type: \(field.type), Name: \(field.name)")
                        print(field.name)
                    }
                }
            }
            DispatchQueue.main.async {
                self.view.endEditing(true)
            }
        
        }
        
    }

    func requestGoogleWebServices() {
//        APIManager.sharedInstance.fetchgoogleData(text:serachField.text!) { (dict:JSONDictionary?, error:NSError?) in
//
//            if error == nil {
//
//                if let req = dict?["status"] as? String {
//                    if req != "INVALID_REQUEST" {
//                        if let arr = dict?["predictions"] as? [JSONDictionary] {
//                            self.searchDataList = arr
//                        }
//                        if self.searchDataList.count > 0 {
//                            self.autocompleteDataTable.isHidden = false
//                        } else {
//                            self.autocompleteDataTable.isHidden = true
//                        }
//                        
//                        self.autocompleteDataTable.reloadData()
//                    }
//                }
//            } else {
//                print(error)
//            }
//
//        }
    }

    func getCompleteData()  {

        APIManager.sharedInstance.getCompleteData(text: addressString) { (dic:JSONDictionary?, error:NSError?) in

            if let status = dic?["status"] as? String{

                if status == "OK" {
                    self.splitReturnData()
                }

            }


        }

    }

    func splitReturnData() {


        if let results = completeAddressReturnDict["results"] as? [JSONDictionary] {

            if let address = results[0]["address_components"] as? [JSONDictionary] {

                if let typeArr = address[0]["types"] as? [JSONDictionary] {
                    print(typeArr)
//                    for i in 0..<typeArr.count {
//                        //[[[[[completeAddressReturnDict valueForKey:@"results"]objectAtIndex:0] valueForKey:@"address_components"] objectAtIndex:i] valueForKey:@"long_name"]
//
//                        if let address = results[0]["address_components"] as? JSONDictionary {
//
//                        }
//                    }
                }
            }

        }

//        var typeArr = (((completeAddressReturnDict.value(forKey: "results") as! String)[0].value(forKey: "address_components") as! String).value(forKey: "types") as! String)
//        completeAddressDict = [AnyHashable: Any]()
//        if typeArr.count > 0 {
//            for i in 0..<typeArr.count {
//                completeAddressDict[typeArr[i][0]] = (((completeAddressReturnDict.value(forKey: "results") as! String)[0].value(forKey: "address_components") as! String)[i].value(forKey: "long_name") as! String)
//            }
//        }
//        completeAddressDict["formatted_address"] = ((completeAddressDict.value(forKey: "results") as! String)[0].value(forKey: "formatted_address") as! String)
//        print("dict.. = \(completeAddressDict)")
//        self.fillAllFields()
//        actualLocationCoordinates = CLLocation(latitude: ((((completeAddressReturnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lat"] as! String).doubleValue, longitude: ((((completeAddressReturnDict["results"] as! String)[0]["geometry"] as! String)["location"] as! String)["lng"] as! String).doubleValue)
        //    NSLog(@"Lat & Long: %@", actualLocationCoordinates);
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}
