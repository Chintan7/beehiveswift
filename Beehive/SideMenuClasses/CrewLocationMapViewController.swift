//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CrewLocationMapViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 23/09/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit

import CoreLocation

class CrewLocationMapViewController: UIViewController {
    var crewLocationMapView: GMSMapView!
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!

    var locationLatitude: Double = 0.0
    var locationLongitude: Double = 0.0

    var screenSize = CGSize.zero

    var backBtn: UIBarButtonItem!
    var menuButton: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.


        self.navigationItem.setHidesBackButton(true, animated: false)
        menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //    Toolbar
        var barItems = [UIBarButtonItem]()
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        backBtn = UIBarButtonItem(customView: button)
        barItems.append(backBtn)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        let camera = GMSCameraPosition.camera(withLatitude: locationLatitude, longitude: locationLongitude, zoom: 12.0)
        crewLocationMapView = GMSMapView.map(withFrame: CGRect(x: CGFloat(-44), y: CGFloat(45), width: CGFloat(320), height: CGFloat(screenSize.height)), camera: camera)
        crewLocationMapView.isMyLocationEnabled = true
        self.view.addSubview(crewLocationMapView)
            // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.icon = UIImage(named: "markerPin.png")!
        marker.position = CLLocationCoordinate2DMake(locationLatitude, locationLongitude)
        marker.map = crewLocationMapView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateNotificationCountValue), name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "NotificationCountValue"), object: nil)
    }

    func updateNotificationCountValue(_ count: Any) {
        print("Counter Value : \(APPDELEGATE.unreadNotificationCount)")
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
    }
    /*
    #pragma mark - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    func backBtnClick() {
        popTo()
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
//        notificationView = NotificationView()
//        notificationView.customView()
//        if screenSize.height == 568 {
//            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
//        }
//        else {
//            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
//        }
//        notificationView.delegate = self
//        self.view.addSubview(notificationView)
//        isNotificationButtonSelected = true
    }

//    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(APPDELEGATE.allNotificationArray?[indexPath.row])")
//        if (((APPDELEGATE.allNotificationArray?[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController") as! InvitationViewController
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            APPDELEGATE.notificationReadWebService((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            APPDELEGATE.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            APPDELEGATE.navigate(toProfileView: self)
//        }
//        else if ((APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (APPDELEGATE.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        APPDELEGATE
//        //    else if ([[[APPDELEGATE.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
//    }


}
