//
//  ODURLSession.m
//  Unister
//
//  Created by OlDor on 2/27/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import "ODURLSession.h"

#import "ODKit.h"

#define keyODURLSessionBackgroundIdentifier @"keyODURLSessionBackgroundIdentifier"

@interface ODURLSession () <NSURLSessionDelegate>

@property (nonatomic, strong, nonnull) NSBlockOperation *theMainBlockOperation;
@property (nonatomic, strong, nullable) NSURLSession *theMainURLSession;
@property (nonatomic, strong, nullable) NSMutableData *theMainData;
@property (nonatomic, assign) double theTotalReceivedPercentage;
@property (nonatomic, copy, nullable) void (^theProgressBlock)(NSData * _Nullable theData, double theTotalReceivedPercentage);
@property (nonatomic, copy, nullable) void (^theCompletionBlock)(NSData * _Nullable theData, NSURLSessionTask * _Nonnull theURLSessionTask, NSError * _Nullable theError);

@end

@implementation ODURLSession

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self methodInitODURLSession];
    }
    return self;
}

- (void)methodInitODURLSession
{
    _shouldPassDataOnCompletion = YES;
    _theTotalReceivedPercentage = 0;
    _theTimeoutIntervalForResource = NSNotFound;
    _isBackgroundBlock = NO;
}

#pragma mark - Setters (Public)

- (void)setShouldPassDataOnCompletion:(BOOL)shouldPassDataOnCompletion
{
    if (self.theMainURLSession)
    {
        NSLog(@"set this BEFORE calling the methods");
        methodAssert(self.theMainURLSession)
    }
    if (_shouldPassDataOnCompletion == shouldPassDataOnCompletion)
    {
        return;
    }
    _shouldPassDataOnCompletion = shouldPassDataOnCompletion;
}

- (void)setTheDelay:(double)theDelay
{
    if (self.theMainURLSession)
    {
        NSLog(@"set this BEFORE calling the methods");
        methodAssert(self.theMainURLSession)
    }
    if (_theDelay == theDelay)
    {
        return;
    }
    _theDelay = theDelay;
}

- (void)setIsBackgroundBlock:(BOOL)isBackgroundBlock
{
    if (self.theMainURLSession)
    {
        NSLog(@"set this BEFORE calling the methods");
        methodAssert(self.theMainURLSession)
    }
    if (_isBackgroundBlock == isBackgroundBlock)
    {
        return;
    }
    _isBackgroundBlock = isBackgroundBlock;
}

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates (NSURLSessionDelegate)

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveData:(NSData *)data
{
    @synchronized(self)
    {
        if (session != self.theMainURLSession)
        {
            return;
        }
        
        self.theTotalReceivedPercentage += (double)data.length/dataTask.response.expectedContentLength;
        if (self.shouldPassDataOnCompletion)
        {
            [self.theMainData appendData:data];
        }
        if (self.theProgressBlock)
        {
            if (self.isBackgroundBlock)
            {
                self.theProgressBlock(data, self.theTotalReceivedPercentage);
            }
            else
            {
                [ODKit methodSyncMain:^
                 {
                     self.theProgressBlock(data, self.theTotalReceivedPercentage);
                 }];
            }
        }
    }
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(nullable NSError *)error
{
    @synchronized(self)
    {
        if (session != self.theMainURLSession)
        {
            return;
        }
        
        if (self.theCompletionBlock)
        {
            NSData *theData;
            if (self.shouldPassDataOnCompletion)
            {
                theData = self.theMainData.copy;
            }
            if (self.isBackgroundBlock)
            {
                self.theCompletionBlock(theData.length ? theData : nil, task, error);
            }
            else
            {
                [ODKit methodSyncMain:^
                 {
                     self.theCompletionBlock(theData.length ? theData : nil, task, error);
                 }];
            }
            [self methodStop];
        }
    }
}

#pragma mark - Methods (Public)

- (void)methodLoadWithURLString:(NSString * _Nonnull)theURLString
                  progressBlock:(void (^ _Nullable)(NSData * _Nullable theData, double theTotalReceivedPercentage))theProgressBlock
                     completion:(void (^ _Nullable)(NSData * _Nullable theData, NSURLSessionTask * _Nonnull theURLSessionTask, NSError * _Nullable theError))theCompletionBlock
{
    if (!theURLString)
    {
        abort();
    }
    [self methodStop];
    self.theProgressBlock = theProgressBlock;
    self.theCompletionBlock = theCompletionBlock;
    
    [self.theMainBlockOperation cancel];
    
    self.theMainBlockOperation = [NSBlockOperation new];
    NSBlockOperation *theMainBlockOperation = self.theMainBlockOperation;
    weakify(self);
    weakify(theMainBlockOperation);
    [theMainBlockOperation addExecutionBlock:^
     {
         strongify(self);
         [ODKit methodDispatchAfterSeconds:self.theDelay
                                     block:^
          {
              strongify(theMainBlockOperation);
              if (!theMainBlockOperation)
              {
                  return;
              }
              
              NSURLSessionConfiguration *theURLSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
              theURLSessionConfiguration.timeoutIntervalForResource = self.theTimeoutIntervalForResource;
              theURLSessionConfiguration.URLCache = nil;
              self.theMainURLSession = [NSURLSession sessionWithConfiguration:theURLSessionConfiguration
                                                                     delegate:self
                                                                delegateQueue:nil];
              NSURLSessionDataTask *theURLSessionDataTask = [self.theMainURLSession dataTaskWithURL:[NSURL URLWithString:theURLString]];
              [theURLSessionDataTask resume];
          }];
     }];
    [theMainBlockOperation start];
}

- (void)methodStop
{
    self.theTotalReceivedPercentage = 0;
    [self.theMainURLSession invalidateAndCancel];
    self.theMainData = [NSMutableData new];
    self.theProgressBlock = nil;
    self.theCompletionBlock = nil;
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























