//
//  ODURLSession.h
//  Unister
//
//  Created by OlDor on 2/27/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import <Foundation/Foundation.h>

#define keyODURLSessionErrorCodeRequestTimedOut -1001

@interface ODURLSession : NSObject
/// defaults to NSNotFound
@property (nonatomic, assign) double theTimeoutIntervalForResource;
/// defaults to YES.
@property (nonatomic, assign) BOOL shouldPassDataOnCompletion;
/// defaults to 0
@property (nonatomic, assign) double theDelay;
/// defaults to NO. determines whether `progressBlock` and `completionBlock` are executed on background thread.
@property (nonatomic, assign) BOOL isBackgroundBlock;

- (void)methodLoadWithURLString:(NSString * _Nonnull)theURLString
                  progressBlock:(void (^ _Nullable)(NSData * _Nullable theData, double theTotalReceivedPercentage))theProgressBlock
                     completion:(void (^ _Nullable)(NSData * _Nullable theData, NSURLSessionTask * _Nonnull theURLSessionTask, NSError * _Nullable theError))theCompletionBlock;

- (void)methodStop;

@end






























