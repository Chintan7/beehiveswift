//
//  TransparentHoleView.h
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 7/24/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransparentHoleView : UIView

/// pass CGRects as NSValue
@property (nonatomic, strong) NSArray *theTransparentRectsArray;

/// use this instead of self.backgroundColor
@property (nonatomic, strong) UIColor *theBackgroundColor;

@property (nonatomic, strong) UIColor *theHoleBorderColor;

/// default is 0
@property (nonatomic, assign) double theHoleBorderWidth;

@end






























