//
//  UIViewController+Extensions.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 6/30/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "UIViewController+ODExtensions.h"

#import "ODKit.h"

#import <objc/runtime.h>

@implementation UIViewController (ODExtensions)

#pragma mark - Class Methods (Public)

+ (void)load
{
    static dispatch_once_t theOnceToken;
    dispatch_once(&theOnceToken, ^
                  {
                      Class class = [self class];
                      SEL theOriginalSelector = nil;
                      SEL theSwizzledSelector = nil;
                      for (int i = 0; i < 2; i++)
                      {
                          switch (i)
                          {
                              case 0:
                              {
                                  theOriginalSelector = @selector(viewWillTransitionToSize:withTransitionCoordinator:);
                                  theSwizzledSelector = @selector(swizzled_viewWillTransitionToSize:withTransitionCoordinator:);
                              }
                                  break;
                              case 1:
                              {
                                  theOriginalSelector = @selector(viewWillAppear:);
                                  theSwizzledSelector = @selector(swizzled_viewWillAppear:);
                              }
                                  break;
                          }
                          
                          if (!theOriginalSelector || !theSwizzledSelector)
                          {
                              abort();
                          }
                          
                          Method theOriginalMethod = class_getInstanceMethod(class, theOriginalSelector);
                          Method theSwizzledMethod = class_getInstanceMethod(class, theSwizzledSelector);
                          
                          BOOL didAddMethod = class_addMethod(class,
                                                              theOriginalSelector,
                                                              method_getImplementation(theSwizzledMethod),
                                                              method_getTypeEncoding(theSwizzledMethod));
                          
                          if (didAddMethod)
                          {
                              class_replaceMethod(class,
                                                  theSwizzledSelector,
                                                  method_getImplementation(theOriginalMethod),
                                                  method_getTypeEncoding(theOriginalMethod));
                          }
                          else
                          {
                              method_exchangeImplementations(theOriginalMethod, theSwizzledMethod);
                          }
                      }
                  });
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

- (UIViewController * __nullable)theBackViewController
{
    NSUInteger theCount = self.navigationController.viewControllers.count;
    if (theCount < 2)
    {
        return nil;
    }
    else
    {
        return self.navigationController.viewControllers[theCount - 2];
    }
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodPopToRootVC
{
    if (self.navigationController)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if ([self isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *theNavigationVC = (UINavigationController *)self;
        [theNavigationVC popToRootViewControllerAnimated:YES];
    }
    else
    {
        __weak id thePresentingVC = (UIViewController *)self.presentingViewController;
        [self dismissViewControllerAnimated:NO completion:^
         {
             [thePresentingVC methodPopToRootVC];
         }];
    }
}

#pragma mark - Methods (Private)

- (void)swizzled_viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self swizzled_viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    UIInterfaceOrientation theNewOrientation = (UIInterfaceOrientation)[UIDevice currentDevice].orientation;
    if (theNewOrientation == UIInterfaceOrientationLandscapeRight)
    {
        theNewOrientation = UIInterfaceOrientationLandscapeLeft;
    }
    else if (theNewOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        theNewOrientation = UIInterfaceOrientationLandscapeRight;
    }
    
    UIInterfaceOrientation theOldOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if (theNewOrientation == theOldOrientation)
    {
        return;
    }
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context)
     {
         if (theNewOrientation == UIInterfaceOrientationLandscapeLeft || theNewOrientation == UIInterfaceOrientationLandscapeRight)
         {
             [self.view methodAdjustToOrientation:UIViewOrientationLandscape];
         }
         else
         {
             [self.view methodAdjustToOrientation:UIViewOrientationPortrait];
         }
         [ODKit methodSetStatusBarHidden:NO];
     }
                                 completion:nil];
}

- (void)swizzled_viewWillAppear:(BOOL)animated
{
    if (self.isFirstLoad)
    {
        if (self.view.theWidth < self.view.theHeight)
        {
            [self.view methodSetCurrentFrameForOrientation:UIViewOrientationPortrait];
            double theWidth = self.view.theWidth;
            self.view.theWidth = self.view.theHeight;
            self.view.theHeight = theWidth;
            [self.view methodSetCurrentFrameForOrientation:UIViewOrientationLandscape];
            [self.view methodAdjustToOrientation:UIViewOrientationPortrait];
        }
        else
        {
            [self.view methodSetCurrentFrameForOrientation:UIViewOrientationLandscape];
            double theWidth = self.view.theWidth;
            self.view.theWidth = self.view.theHeight;
            self.view.theHeight = theWidth;
            [self.view methodSetCurrentFrameForOrientation:UIViewOrientationPortrait];
            [self.view methodAdjustToOrientation:UIViewOrientationLandscape];
        }
    }
}

#pragma mark - Standard Methods

@end






























