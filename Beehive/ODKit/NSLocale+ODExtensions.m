//
//  NSLocale+ODExtensions.m
//  ODTableView
//
//  Created by OlDor on 1/30/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import "NSLocale+ODExtensions.h"

@implementation NSLocale (ODExtensions)

#pragma mark - Class Methods (Public)

+ (instancetype __nonnull)getDefaultLocale
{
    return [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























