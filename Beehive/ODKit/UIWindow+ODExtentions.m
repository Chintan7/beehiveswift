//
//  UIWindow+ODExtentions.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 1/20/16.
//  Copyright © 2016 Mobiwolf. All rights reserved.
//

#import "UIWindow+ODExtentions.h"

@implementation UIWindow (ODExtentions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

- (UIViewController * __nullable)theVisibleVC
{
    return [self getVisibleVCFromVC:self.rootViewController];
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

- (UIViewController *)getVisibleVCFromVC:(UIViewController *)theVC
{
    if ([theVC isKindOfClass:[UINavigationController class]])
    {
        return [self getVisibleVCFromVC:((UINavigationController *)theVC).visibleViewController];
    }
    else if ([theVC isKindOfClass:[UITabBarController class]])
    {
        return [self getVisibleVCFromVC:((UITabBarController *)theVC).selectedViewController];
    }
    else
    {
        if (theVC.presentedViewController)
        {
            return [self getVisibleVCFromVC:theVC.presentedViewController];
        }
        else
        {
            return theVC;
        }
    }
}

#pragma mark - Standard Methods

@end






























