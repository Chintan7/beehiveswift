//
//  UIResponder+ODExtensions.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 1/4/16.
//  Copyright © 2016 Mobiwolf. All rights reserved.
//

#import "UIResponder+ODExtensions.h"

@implementation UIResponder (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

- (UIViewController * __nullable)theVC
{
    if ([self.nextResponder isKindOfClass:[UIViewController class]])
    {
        return (id)self.nextResponder;
    }
    return self.nextResponder.theVC;
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























