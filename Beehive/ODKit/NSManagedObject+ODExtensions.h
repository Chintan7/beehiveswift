//
//  NSManagedObject+ODExtensions.h
//  ODTableView
//
//  Created by OlDor on 1/31/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (ODExtensions)

- (id __nullable)getPrimitiveValueForSelector:(SEL __nonnull)theSelector;
- (void)methodSetPrimitiveValue:(id __nullable)theValue forSelector:(SEL __nonnull)theSelector;

@end






























