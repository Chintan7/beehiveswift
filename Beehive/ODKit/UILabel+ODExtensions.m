//
//  UILabel+ODExtensions.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 12/18/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import "UILabel+ODExtensions.h"

#import "ODKit.h"

@implementation UILabel (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

- (NSUInteger)theCurrentNumberOfLines
{
    CGRect theFrame = self.frame;
    [self sizeToFit];
    NSUInteger theCurrentNumberOfLines = self.theHeight/self.font.lineHeight;
    self.frame = theFrame;
    return theCurrentNumberOfLines;
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodAddAttributedTitle:(NSAttributedString * __nonnull)theAttributedString
               animationDuration:(double)theAnimationDuration
                      completion:(void (^ __nullable)())theBlock;
{
    if (!theAttributedString)
    {
        abort();
    }
    
    if (theAttributedString.string.length)
    {
        NSMutableAttributedString *theString = [NSMutableAttributedString new];
        [theString appendAttributedString:self.attributedText];
        [theString appendAttributedString:[theAttributedString attributedSubstringFromRange:NSMakeRange(0, 1)]];
        self.attributedText = theString;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(theAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
                       {
                           [self methodAddAttributedTitle:[theAttributedString attributedSubstringFromRange:NSMakeRange(1, theAttributedString.string.length - 1)]
                                        animationDuration:theAnimationDuration
                                               completion:theBlock];
                       });
    }
    else
    {
        if (theBlock)
        {
            theBlock();
        }
    }
}

- (double)getHeightWithNumberOfLines:(NSUInteger)theNumberOfLines
{
    if (!theNumberOfLines)
    {
        return 0;
    }
    
    NSString *theOldText = self.text;
    CGRect theOldFrame = self.frame;
    NSInteger theOldNumberOfLines = self.numberOfLines;
    
    self.numberOfLines = 0;
    NSMutableString *theTestString = [NSMutableString new];
    [theTestString appendString:@"W"];
    for (NSUInteger theIndex = 1; theIndex < theNumberOfLines; theIndex++)
    {
        [theTestString appendString:@"\nW"];
    }
    self.text = theTestString;
    [self sizeToFit];
    double theHeight = self.theHeight;
    
    self.numberOfLines = theOldNumberOfLines;
    self.text = theOldText;
    self.frame = theOldFrame;
    return theHeight;
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























