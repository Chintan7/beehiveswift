//
//  UIImage+Extensions.h
//  Imperium
//
//  Created by od1914@my.bristol.ac.uk on 20/02/2015.
//  Copyright (c) 2015 OlDor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ODExtensions)

+ (UIImage * __nullable)getImageNamed:(NSString * __nonnull)theImageName;
+ (NSArray<NSString *> * __nonnull)getImagePathsWithImageName:(NSString * __nonnull)theImageName;
+ (NSArray<UIImage *> * __nonnull)getImagesFromGifWithFileName:(NSString * __nonnull)theFileName;
+ (NSArray<UIImage *> * __nonnull)getImagesWithImageName:(NSString * __nonnull)theImageName;
+ (UIImage * __nullable)getImageWithName:(NSString * __nonnull)theImageName consideringScale:(BOOL)isScale;
+ (UIImage * __nonnull)getScreenshotFromRect:(CGRect)theRect fromView:(UIView * __nonnull)theView;

- (UIImage * __nonnull)getImageWithBlurRadius:(CGFloat)theBlurRadius
                        saturationDeltaFactor:(CGFloat)theSaturationDeltaFactor
                                    maskImage:(UIImage * __nullable)theMaskImage;

- (UIImage * __nonnull)getImageWithSize:(CGSize)theSize;

@end






























