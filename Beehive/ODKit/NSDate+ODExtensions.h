//
//  NSDate+Extensions.h
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 9/10/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSDate (ODExtensions)

+ (BOOL)isDate:(NSDate * __nonnull)theDate earlierThanDate:(NSDate * __nonnull)theOtherDate;
+ (BOOL)isDate:(NSDate * __nonnull)theDate laterThanDate:(NSDate * __nonnull)theOtherDate;

@end






























