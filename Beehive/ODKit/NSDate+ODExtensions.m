//
//  NSDate+Extensions.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 9/10/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import "NSDate+ODExtensions.h"

@implementation NSDate (ODExtensions)

#pragma mark - Class Methods (Public)

+ (BOOL)isDate:(NSDate * __nonnull)theDate earlierThanDate:(NSDate * __nonnull)theOtherDate
{
    if (!theDate || !theOtherDate)
    {
        abort();
    }
    return [theDate timeIntervalSinceDate:theOtherDate] < 0;
}
+ (BOOL)isDate:(NSDate * __nonnull)theDate laterThanDate:(NSDate * __nonnull)theOtherDate
{
    if (!theDate || !theOtherDate)
    {
        abort();
    }
    return [theDate timeIntervalSinceDate:theOtherDate] > 0;
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end




























