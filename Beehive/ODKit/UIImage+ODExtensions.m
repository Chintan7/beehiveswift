//
//  UIImage+Extensions.m
//  Imperium
//
//  Created by od1914@my.bristol.ac.uk on 20/02/2015.
//  Copyright (c) 2015 OlDor. All rights reserved.
//

#import "UIImage+ODExtensions.h"

#import "ODKit.h"

#import <Accelerate/Accelerate.h>
#import <ImageIO/ImageIO.h>

@implementation UIImage (ODExtensions)

#pragma mark - Class Methods (Public)

+ (UIImage * __nullable)getImageNamed:(NSString * __nonnull)theImageName
{
    if (!theImageName)
    {
        abort();
    }
    switch ([ODKit getDevice])
    {
        case DeviceiPhone4:
            theImageName = [theImageName stringByAppendingString:@"_4"];
            break;
        case DeviceiPhone5:
            theImageName = [theImageName stringByAppendingString:@"_5"];
            break;
        case DeviceiPhone6:
            theImageName = [theImageName stringByAppendingString:@"_6"];
            break;
        case DeviceiPhone6Plus:
            break;
        case DeviceiPad:
            break;
    }
    
    UIImage *theImage = [UIImage getImageWithName:theImageName consideringScale:YES];
    if (!theImage)
    {
        if (theImageName.length > 2)
        {
            theImageName = [theImageName stringByReplacingCharactersInRange:NSMakeRange(theImageName.length - 2, 2) withString:@""];
            theImage = [UIImage getImageWithName:theImageName consideringScale:YES];
        }
    }
    return theImage;
}

+ (NSArray<NSString *> * __nonnull)getImagePathsWithImageName:(NSString * __nonnull)theImageName
{
    if (!theImageName)
    {
        abort();
    }
    NSMutableArray *theArray = [NSMutableArray new];
    theImageName = [theImageName stringByAppendingString:@"_"];
    
    int theCounter = 0;
    while ([[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@%i", theImageName, theCounter] ofType:@"png"])
    {
        [theArray addObject:[NSString stringWithFormat:@"%@%i", theImageName, theCounter++]];
    }
    return theArray;
}

+ (NSArray<UIImage *> * __nonnull)getImagesFromGifWithFileName:(NSString * __nonnull)theFileName
{
    if (!theFileName)
    {
        abort();
    }
    NSData *theData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:theFileName ofType:@"gif"]];
    if (!theData)
    {
        return nil;
    }
    
    CGImageSourceRef theImageSource = CGImageSourceCreateWithData((CFDataRef)theData, nil);
    NSMutableArray *theFinalArray = [NSMutableArray new];
    for (int i = 0; i < CGImageSourceGetCount(theImageSource); i++)
    {
        CGImageRef theImageRef = CGImageSourceCreateImageAtIndex(theImageSource, i, nil);
        if (theImageRef)
        {
            [theFinalArray addObject:[UIImage imageWithCGImage:theImageRef]];
            CFRelease(theImageRef);
        }
    }
    CFRelease(theImageSource);
    return theFinalArray;
}

+ (NSArray<UIImage *> * __nonnull)getImagesWithImageName:(NSString * __nonnull)theImageName
{
    if (!theImageName)
    {
        abort();
    }
    NSMutableArray *theArray = [NSMutableArray new];
    if ([ODKit getDevice] == DeviceiPhone6)
    {
        theImageName = [theImageName stringByAppendingString:@"_6"];
    }
    else if ([ODKit getDevice] == DeviceiPhone5)
    {
        theImageName = [theImageName stringByAppendingString:@"_5"];
    }
    else if ([ODKit getDevice] == DeviceiPhone4)
    {
        theImageName = [theImageName stringByAppendingString:@"_4"];
    }
    theImageName = [theImageName stringByAppendingString:@"_"];
    
    int theCounter = 0;
    UIImage *theImage;
    while ((theImage = [self getImageWithName:[NSString stringWithFormat:@"%@%i", theImageName, theCounter++] consideringScale:YES]))
    {
        [theArray addObject:theImage];
    }
    return theArray;
}

+ (UIImage * __nullable)getImageWithName:(NSString * __nonnull)theImageName consideringScale:(BOOL)isScale
{
    if (!theImageName)
    {
        abort();
    }
    
    static int theScale;
    if (!theScale)
    {
        theScale = [UIScreen mainScreen].scale;
    }
    
    NSString *thePath;
    if (isScale)
    {
        thePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@@%ix", theImageName, theScale] ofType:@"png"];
    }
    else
    {
        thePath = [[NSBundle mainBundle] pathForResource:theImageName ofType:@"png"];
    }
    return [UIImage imageWithContentsOfFile:thePath];
}

+ (UIImage * __nonnull)getScreenshotFromRect:(CGRect)theRect fromView:(UIView * __nonnull)theView;
{
    if (!theView)
    {
        abort();
    }
    if (theRect.size.height < 1 || theRect.size.width < 1)
    {
        abort();
    }
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)])
    {
        UIGraphicsBeginImageContextWithOptions(theRect.size, YES, [UIScreen mainScreen].scale);
    }
    else
    {
        UIGraphicsBeginImageContext(theRect.size);
    }
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -theRect.origin.x, -theRect.origin.y);
    [theView.layer renderInContext:ctx];
    
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (UIImage * __nonnull)getImageWithBlurRadius:(CGFloat)theBlurRadius
                        saturationDeltaFactor:(CGFloat)theSaturationDeltaFactor
                                    maskImage:(UIImage * __nullable)theMaskImage
{
    // Check pre-conditions.
    if (self.size.width < 1 || self.size.height < 1) {
        NSLog (@"*** error: invalid size: (%.2f x %.2f). Both dimensions must be >= 1: %@", self.size.width, self.size.height, self);
        return nil;
    }
    if (!self.CGImage) {
        NSLog (@"*** error: image must be backed by a CGImage: %@", self);
        return nil;
    }
    if (theMaskImage && !theMaskImage.CGImage) {
        NSLog (@"*** error: maskImage must be backed by a CGImage: %@", theMaskImage);
        return nil;
    }
    
    CGRect imageRect = { CGPointZero, self.size };
    UIImage *effectImage = self;
    
    BOOL hasBlur = theBlurRadius > __FLT_EPSILON__;
    BOOL hasSaturationChange = fabs(theSaturationDeltaFactor - 1.) > __FLT_EPSILON__;
    if (hasBlur || hasSaturationChange) {
        UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectInContext = UIGraphicsGetCurrentContext();
        CGContextScaleCTM(effectInContext, 1.0, -1.0);
        CGContextTranslateCTM(effectInContext, 0, -self.size.height);
        CGContextDrawImage(effectInContext, imageRect, self.CGImage);
        
        vImage_Buffer effectInBuffer;
        effectInBuffer.data     = CGBitmapContextGetData(effectInContext);
        effectInBuffer.width    = CGBitmapContextGetWidth(effectInContext);
        effectInBuffer.height   = CGBitmapContextGetHeight(effectInContext);
        effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);
        
        UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
        vImage_Buffer effectOutBuffer;
        effectOutBuffer.data     = CGBitmapContextGetData(effectOutContext);
        effectOutBuffer.width    = CGBitmapContextGetWidth(effectOutContext);
        effectOutBuffer.height   = CGBitmapContextGetHeight(effectOutContext);
        effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);
        
        if (hasBlur) {
            // A description of how to compute the box kernel width from the Gaussian
            // radius (aka standard deviation) appears in the SVG spec:
            // http://www.w3.org/TR/SVG/filters.html#feGaussianBlurElement
            //
            // For larger values of 's' (s >= 2.0), an approximation can be used: Three
            // successive box-blurs build a piece-wise quadratic convolution kernel, which
            // approximates the Gaussian kernel to within roughly 3%.
            //
            // let d = floor(s * 3*sqrt(2*pi)/4 + 0.5)
            //
            // ... if d is odd, use three box-blurs of size 'd', centered on the output pixel.
            //
            CGFloat inputRadius = theBlurRadius * [[UIScreen mainScreen] scale];
            uint32_t radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
            if (radius % 2 != 1) {
                radius += 1; // force radius to be odd so that the three box-blur methodology works.
            }
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
        }
        BOOL effectImageBuffersAreSwapped = NO;
        if (hasSaturationChange) {
            CGFloat s = theSaturationDeltaFactor;
            CGFloat floatingPointSaturationMatrix[] = {
                0.0722 + 0.9278 * s,  0.0722 - 0.0722 * s,  0.0722 - 0.0722 * s,  0,
                0.7152 - 0.7152 * s,  0.7152 + 0.2848 * s,  0.7152 - 0.7152 * s,  0,
                0.2126 - 0.2126 * s,  0.2126 - 0.2126 * s,  0.2126 + 0.7873 * s,  0,
                0,                    0,                    0,  1,
            };
            const int32_t divisor = 256;
            NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix)/sizeof(floatingPointSaturationMatrix[0]);
            int16_t saturationMatrix[matrixSize];
            for (NSUInteger i = 0; i < matrixSize; ++i) {
                saturationMatrix[i] = (int16_t)roundf(floatingPointSaturationMatrix[i] * divisor);
            }
            if (hasBlur) {
                vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                effectImageBuffersAreSwapped = YES;
            }
            else {
                vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
            }
        }
        if (!effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    // Set up output context.
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, imageRect, self.CGImage);
    
    // Draw effect image.
    if (hasBlur) {
        CGContextSaveGState(outputContext);
        if (theMaskImage) {
            CGContextClipToMask(outputContext, imageRect, theMaskImage.CGImage);
        }
        CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
        CGContextRestoreGState(outputContext);
    }
    
    // Add in color tint.
//    if (tintColor) {
//        CGContextSaveGState(outputContext);
//        CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
//        CGContextFillRect(outputContext, imageRect);
//        CGContextRestoreGState(outputContext);
//    }
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

- (UIImage * __nonnull)getImageWithSize:(CGSize)theSize;
{
    size_t theWidth = theSize.width * [UIScreen mainScreen].scale;
    size_t theHeight = theSize.height * [UIScreen mainScreen].scale;
    
    CGColorSpaceRef theColorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGContextRef theContextRef =  CGBitmapContextCreate(NULL, theWidth, theHeight, 8, theWidth*4, theColorSpaceRef, kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little);
    CGColorSpaceRelease(theColorSpaceRef);
    
    CGContextDrawImage(theContextRef, CGRectMake(0, 0, theWidth, theHeight), self.CGImage);
    CGImageRef theOutputImageRef = CGBitmapContextCreateImage(theContextRef);
    UIImage *theImage = [UIImage imageWithCGImage:theOutputImageRef];
    
    CGImageRelease(theOutputImageRef);
    CGContextRelease(theContextRef);
    return theImage;
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























