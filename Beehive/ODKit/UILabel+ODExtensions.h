//
//  UILabel+ODExtensions.h
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 12/18/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (ODExtensions)

@property (nonatomic, assign, readonly) NSUInteger theCurrentNumberOfLines;

- (void)methodAddAttributedTitle:(NSAttributedString * __nonnull)theAttributedString
               animationDuration:(double)theAnimationDuration
                      completion:(void (^ __nullable)())theBlock;

- (double)getHeightWithNumberOfLines:(NSUInteger)theNumberOfLines;

@end






























