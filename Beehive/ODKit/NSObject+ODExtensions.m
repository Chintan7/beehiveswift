//
//  NSObject+Extensions.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 9/17/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "NSObject+ODExtensions.h"

#import "ODKit.h"

#import <objc/runtime.h>

@implementation NSObject (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

- (void)setIsFirstLoad:(BOOL)isFirstLoad
{
    if (self.isFirstLoad == isFirstLoad)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(isFirstLoad), isFirstLoad ? nil : [NSNull null], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheStrongObject:(id __nullable)theStrongObject
{
    if (self.theStrongObject == theStrongObject)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theStrongObject), theStrongObject, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheStrongObject2:(id __nullable)theStrongObject2
{
    if (self.theStrongObject2 == theStrongObject2)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theStrongObject2), theStrongObject2, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setThePairedObject:(id __nullable)thePairedObject
{
    if (self.thePairedObject == thePairedObject)
    {
        return;
    }
    NSObject *theNewPairedObject = thePairedObject;
    NSObject *theOldPairedObject = self.thePairedObject;
    if (!theNewPairedObject)
    {
        objc_setAssociatedObject(self, @selector(thePairedObject), theNewPairedObject, OBJC_ASSOCIATION_ASSIGN);
    }
    
    if (theOldPairedObject)
    {
        theOldPairedObject.thePairedObject = nil;
    }
    
    if (theNewPairedObject)
    {
        objc_setAssociatedObject(self, @selector(thePairedObject), theNewPairedObject, OBJC_ASSOCIATION_ASSIGN);
        if (theNewPairedObject.thePairedObject != self)
        {
            theNewPairedObject.thePairedObject = self;
        }
        
    }
}

- (void)setTheWeakObject:(id __nullable)theWeakObject
{
    if (self.theWeakObject == theWeakObject)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theWeakObject), theWeakObject, OBJC_ASSOCIATION_ASSIGN);
}

#pragma mark - Getters (Public)

- (BOOL)isFirstLoad
{
    NSNull *theNull = objc_getAssociatedObject(self, @selector(isFirstLoad));
    return theNull ? NO : YES;
}

- (NSInteger)theRetainCount
{
    return CFGetRetainCount((__bridge CFTypeRef)self);
}

- (id __nullable)theStrongObject
{
    return objc_getAssociatedObject(self, @selector(theStrongObject));
}

- (id __nullable)theStrongObject2
{
    return objc_getAssociatedObject(self, @selector(theStrongObject2));
}

- (id __nullable)thePairedObject
{
    return objc_getAssociatedObject(self, @selector(thePairedObject));
}

- (id __nullable)theWeakObject
{
    return objc_getAssociatedObject(self, @selector(theWeakObject));
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























