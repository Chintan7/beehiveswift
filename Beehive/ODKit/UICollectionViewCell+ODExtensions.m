//
//  UICollectionViewCell+ODExtensions.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 1/15/16.
//  Copyright © 2016 Mobiwolf. All rights reserved.
//

#import "UICollectionViewCell+ODExtensions.h"

#import "ODKit.h"

#import <objc/runtime.h>

@implementation UICollectionViewCell (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

- (void)setTheIndexPath:(NSIndexPath * __nullable)theIndexPath
{
    if (self.theIndexPath == theIndexPath)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theIndexPath), theIndexPath, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Getters (Public)

- (NSIndexPath * __nullable)theIndexPath
{
    return objc_getAssociatedObject(self, @selector(theIndexPath));
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























