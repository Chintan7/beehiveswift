//
//  ODAlertView.h
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 8/21/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ODAlertView : UIView

@property (nonatomic, assign, readonly) BOOL isVisible;
/// defaults to 0.5
@property (nonatomic, assign) double theAnimationDuration;
/// defaults to 0.7
@property (nonatomic, assign) double theBackgroundViewAlpha;
/// defaults to [UIColor blackColor]
@property (nonatomic, strong, nullable) UIColor *theBackgroundViewColor;
@property (nonatomic, weak, nullable) UIView *theMainView;

/// pass nil to 'inView:' in order to present on [UIApplication sharedApplication].keyWindow
- (void)methodSetVisible:(BOOL)isVisible
                  inView:(UIView * __nullable)theView
              completion:(void (^ __nullable)())theBlock;

@end






























