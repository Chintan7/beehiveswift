//
//  UIResponder+ODExtensions.h
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 1/4/16.
//  Copyright © 2016 Mobiwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIResponder (ODExtensions)

@property (nonatomic, weak, nullable, readonly) UIViewController *theVC;

@end






























