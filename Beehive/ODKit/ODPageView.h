//
//  ODPageView.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 10/16/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ODPageViewDelegate;

typedef enum : NSUInteger
{
    ODPageViewMovementDirectionLeftToRight = 1,
    ODPageViewMovementDirectionTopToBottom,
    ODPageViewMovementDirectionEnumCount = ODPageViewMovementDirectionTopToBottom
} ODPageViewMovementDirection;

@interface ODPageView : UIView

/// defaults to YES
@property (nonatomic, assign) BOOL isAllowedToScroll;
/// defaults to NO
@property (nonatomic, assign) BOOL isInfinite;
@property (nonatomic, assign, readonly) BOOL isTouchesFinished;
/// defaults to 0.4
@property (nonatomic, assign) double theAnimationDuration;
@property (nonatomic, assign, readonly) double theCurrentOffset;
@property (nonatomic, assign, readonly) NSUInteger theCurrentPage;
@property (nonatomic, weak, nullable) id <ODPageViewDelegate> theDelegate;
@property (nonatomic, assign, readonly) CGRect theMainViewFrame;
/// defaults to ODPageViewMovementDirectionLeftToRight
@property (nonatomic, assign) ODPageViewMovementDirection theMovementDirection;
@property (nonatomic, strong, nonnull, readonly) NSArray<UIView *> *thePageViewsArray;
/// defaults to 0.6
@property (nonatomic, assign) double theSpringDamping;

- (void)methodAddPageView:(UIView * __nonnull)theView;
- (void)methodRemoveAllPageViews;
- (void)methodScrollToPageView:(UIView * __nonnull)theView
                      animated:(BOOL)isAnimated
                    completion:(void (^ __nullable)())theBlock;

@end

@protocol ODPageViewDelegate <NSObject>

@optional

- (void)pageViewDidFinishTouches:(ODPageView * __nonnull)theODPageView;
- (void)pageViewDidScroll:(ODPageView * __nonnull)theODPageView;
- (void)pageViewDidStartScrolling:(ODPageView * __nonnull)theODPageView;
- (void)pageViewDidStopScrolling:(ODPageView * __nonnull)theODPageView;

@end






























