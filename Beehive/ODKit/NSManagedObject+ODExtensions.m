//
//  NSManagedObject+ODExtensions.m
//  ODTableView
//
//  Created by OlDor on 1/31/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import "NSManagedObject+ODExtensions.h"

#import "ODKit.h"

@implementation NSManagedObject (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

- (void)methodSetPrimitiveValue:(id __nullable)theValue forSelector:(SEL __nonnull)theSelector
{
    if (!theSelector)
    {
        abort();
    }
    if ([self getPrimitiveValueForSelector:theSelector] == theValue)
    {
        return;
    }
    [self willChangeValueForKey:sfs(theSelector)];
    [self setPrimitiveValue:theValue forKey:sfs(theSelector)];
    [self didChangeValueForKey:sfs(theSelector)];
}

#pragma mark - Getters (Public)

- (id __nullable)getPrimitiveValueForSelector:(SEL __nonnull)theSelector
{
    if (!theSelector)
    {
        abort();
    }
    [self willAccessValueForKey:sfs(theSelector)];
    id theValue = [self primitiveValueForKey:sfs(theSelector)];
    [self didAccessValueForKey:sfs(theSelector)];
    return theValue;
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























