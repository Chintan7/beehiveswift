//
//  ODSyncBackground.m
//  VKMusicPlayer
//
//  Created by OlDor on 3/31/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import "ODSyncBackground.h"

@interface ODSyncBackground ()

@property (nonatomic, strong, nonnull) NSOperationQueue *theMainOperationQueue;

@end

@implementation ODSyncBackground

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self methodInitODSyncBackground];
    }
    return self;
}

- (void)methodInitODSyncBackground
{
    _theDelay = 0;
    _theMainOperationQueue = [NSOperationQueue new];
    _theMainOperationQueue.maxConcurrentOperationCount = 1;
}

#pragma mark - Setters (Public)

- (void)setTheDelay:(double)theDelay
{
    if (theDelay < 0)
    {
        abort();
    }
    if (_theDelay == theDelay)
    {
        return;
    }
    _theDelay = theDelay;
}

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodSynchronizedBackgroundWithBlock:(void (^ _Nonnull)())theBlock
{
    if (!theBlock)
    {
        abort();
    }
    NSDate *theDate = [NSDate date];
    double theDelay = self.theDelay;
    
    NSBlockOperation *theBlockOperation = [NSBlockOperation new];
    theBlockOperation.queuePriority = NSOperationQueuePriorityVeryHigh;
    [theBlockOperation addExecutionBlock:^
     {
         if (theDelay)
         {
             [NSThread sleepForTimeInterval:theDelay - [[NSDate date] timeIntervalSinceDate:theDate]];
         }
         theBlock();
     }];
    [self.theMainOperationQueue addOperation:theBlockOperation];
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























