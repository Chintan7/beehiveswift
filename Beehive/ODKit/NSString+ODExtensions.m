//
//  NSString+Extensions.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 9/25/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "NSString+ODExtensions.h"

#import "ODKit.h"

@implementation NSString (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

- (BOOL)areEmailsValid
{
    NSString *theString = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    for (NSString *theText in [theString componentsSeparatedByString:@","])
    {
        if (![theText isEmailValid])
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)isEmailValid
{
    NSString *theRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *thePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", theRegex];
    return [thePredicate evaluateWithObject:self];
}

- (BOOL)isEmpty
{
    NSString *theString = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [theString isEqual:@""];
}

- (double)theDeviceValue
{
    NSArray<NSString *> *theArray = [self componentsSeparatedByString:@" "];
    if (theArray.count != DeviceEnumCount)
    {
        abort();
    }
    return theArray[[ODKit getDevice] - 1].doubleValue;
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























