//
//  ODAlertView.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 8/21/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import "ODAlertView.h"

#import "ODKit.h"

@interface ODAlertView ()

@property (nonatomic, strong, nonnull) UIView *theBackgroundView;

@end

@implementation ODAlertView

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self methodInitODAlertView];
    }
    return self;
}

- (void)methodInitODAlertView
{
    _theAnimationDuration = 0.5;
    _isVisible = NO;
    _theBackgroundViewColor = [UIColor blackColor];
    _theBackgroundViewAlpha = 0.7;
}

#pragma mark - Setters (Public)

- (void)setIsVisible:(BOOL)isVisible
{
    if (_isVisible == isVisible)
    {
        return;
    }
    _isVisible = isVisible;
    [self createAllViews];
}


- (void)setTheAnimationDuration:(double)theAnimationDuration
{
    if (_theAnimationDuration == theAnimationDuration)
    {
        return;
    }
    _theAnimationDuration = theAnimationDuration;
    [self createAllViews];
}

- (void)setTheBackgroundViewAlpha:(double)theBackgroundViewAlpha
{
    if (_theBackgroundViewAlpha == theBackgroundViewAlpha)
    {
        return;
    }
    _theBackgroundViewAlpha = theBackgroundViewAlpha;
    [self createAllViews];
    self.theBackgroundView.alpha = self.theBackgroundViewAlpha;
}

- (void)setTheBackgroundViewColor:(UIColor * __nullable)theBackgroundViewColor
{
    if (_theBackgroundViewColor == theBackgroundViewColor)
    {
        return;
    }
    _theBackgroundViewColor = theBackgroundViewColor;
    [self createAllViews];
    self.theBackgroundView.backgroundColor = self.theBackgroundViewColor;
}

- (void)setTheMainView:(UIView * __nullable)theMainView
{
    if (_theMainView == theMainView)
    {
        return;
    }
    if (!theMainView)
    {
        abort();
    }
    _theMainView = theMainView;
    [self createAllViews];
}

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

- (void)createAllViews
{
    if (!self.isFirstLoad)
    {
        return;
    }
    self.isFirstLoad = NO;
    
    self.theWidth = [UIScreen mainScreen].bounds.size.width;
    self.theHeight = [UIScreen mainScreen].bounds.size.height;
    
    UIView *theBackgroundView = [UIView new];
    self.theBackgroundView = theBackgroundView;
    [self addSubview:theBackgroundView];
    theBackgroundView.theWidth = self.theWidth;
    theBackgroundView.theHeight = self.theHeight;
    theBackgroundView.backgroundColor = self.theBackgroundViewColor;
    theBackgroundView.alpha = self.theBackgroundViewAlpha;
}

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

- (void)methodSetVisible:(BOOL)isVisible
                  inView:(UIView * __nullable)theView
              completion:(void (^ __nullable)())theBlock
{
    [self createAllViews];
    
    self.isVisible = isVisible;
    if (isVisible)
    {
        if (!self.theMainView)
        {
            abort();
        }
        self.theMainView.center = CGPointMake(self.theWidth/2, self.theHeight/2);
        if (self.theMainView.superview != self)
        {
            [self addSubview:self.theMainView];
        }
        
        if (!theView)
        {
            theView = [UIApplication sharedApplication].keyWindow;
        }
        [theView addSubview:self];
        
        self.alpha = 0;
        [UIView animateWithDuration:self.theAnimationDuration
                         animations:^
         {
             self.alpha = 1;
         }
                         completion:^(BOOL finished)
         {
             if (theBlock)
             {
                 theBlock();
             }
         }];
    }
    else
    {
        [UIView animateWithDuration:self.theAnimationDuration
                         animations:^
         {
             self.alpha = 0;
         }
                         completion:^(BOOL finished)
         {
             [self.theMainView removeFromSuperview];
             [self removeFromSuperview];
             if (theBlock)
             {
                 theBlock();
             }
         }];
    }
}

#pragma mark - Standard Methods

@end






























