//
//  UtilitiesManager.m
//  TubeTravel
//
//  Created by od1914@my.bristol.ac.uk on 05/04/2015.
//  Copyright (c) 2015 OlDor. All rights reserved.
//

#import "ODKit.h"
#import <Beehive-Swift.h>
#import <objc/runtime.h>

@implementation ODKit
// objc_msgSend(objc_msgSend(NSClassFromString(@"NSObject"), @selector(alloc)), @selector(init));
// from http://stackoverflow.com/questions/17263354/why-shouldnt-you-use-objc-msgsend-in-objective-c

int getDigitFromNumberAtIndex(int theNumber, int theIndex)
{
    return (theNumber / (int)pow(10, floor(log10(theNumber)) - theIndex)) % 10;
}

BOOL getTrueWithProbability(int theProbability)
{
    int theNumber = 1000000;
    return arc4random() % theNumber < (float)theProbability / 100 * theNumber;
}

BOOL isEqual(id __nullable theFirstObject, id __nullable theSecondObject)
{
    if (!theFirstObject && !theSecondObject)
    {
        return YES;
    }
    return [theFirstObject isEqual:theSecondObject];
}

BOOL isSystemAtLeastVersion(int theBasicVersion, int theSubVersion, int theSubSubVersion)
{
    return [[NSProcessInfo processInfo] isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){theBasicVersion, theSubVersion, theSubSubVersion}];
}

NSString * __nonnull sfc(Class __nonnull theClass)
{
    if (!theClass)
    {
        abort();
    }
    return NSStringFromClass(theClass);
}

NSString * __nonnull sfs(SEL __nonnull theSelector)
{
    if (!theSelector)
    {
        abort();
    }
    return NSStringFromSelector(theSelector);
}

#pragma mark - Class Methods (Public)

+ (Device)getDevice
{
    static Device theDevice = 0;
    if (!theDevice)
    {
        int theHeight = [UIScreen mainScreen].bounds.size.height;
        int theWidth = [UIScreen mainScreen].bounds.size.width;
        switch (theHeight > theWidth ? theHeight : theWidth)
        {
            case 480:
                theDevice = DeviceiPhone4;
                break;
            case 568:
                theDevice = DeviceiPhone5;
                break;
            case 667:
                theDevice = DeviceiPhone6;
                break;
            case 736:
                theDevice = DeviceiPhone6Plus;
                break;
            case 1024:
                theDevice = DeviceiPad;
                break;
            default:
                NSLog(@"error");
                abort();
        }
    }
    return theDevice;
}

+ (double)getStatusBarHeight
{
    return [UIApplication sharedApplication].statusBarFrame.size.height;
}

+ (id <NSObject> __nonnull)methodAddObserverForName:(NSString * __nonnull)theNotificationName
                                       withWeakSelf:(__weak id __nonnull)theWeakSelf
                                         usingBlock:(void (^ __nonnull)(NSNotification * __nonnull theNotification))theBlock
{
    if (!theNotificationName || !theWeakSelf || !theBlock)
    {
        abort();
    }
    __block id <NSObject> theObserver = [[NSNotificationCenter defaultCenter] addObserverForName:theNotificationName
                                                                                          object:nil
                                                                                           queue:nil
                                                                                      usingBlock:^(NSNotification *note)
                                         {
                                             if (theWeakSelf)
                                             {
                                                 [self methodAsyncMain:^
                                                  {
                                                      theBlock(note);
                                                  }];
                                             }
                                             else
                                             {
                                                 [[NSNotificationCenter defaultCenter] removeObserver:theObserver];
                                             }
                                         }];
    return theObserver;
}

+ (void)methodAsyncBackground:(void (^ __nonnull)())theBlock
{
    if (!theBlock)
    {
        abort();
    }
    
    ODSyncBackground *theODSyncBackground = [ODSyncBackground new];
    [theODSyncBackground methodSynchronizedBackgroundWithBlock:^
     {
         theBlock();
     }];
}

+ (void)methodAsyncMain:(void (^ __nonnull)())theBlock
{
    if (!theBlock)
    {
        abort();
    }
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       theBlock();
                   });
}

+ (void)methodSyncMain:(void (^ __nonnull)())theBlock
{
    if (!theBlock)
    {
        abort();
    }
    dispatch_sync(dispatch_get_main_queue(), ^
                   {
                       theBlock();
                   });
}

+ (void)methodDispatchAfterSeconds:(double)theSeconds
                             block:(void (^ __nonnull)())theBlock
{
    if (theSeconds < 0 || !theBlock)
    {
        abort();
    }
    
    if ([NSThread isMainThread])
    {
        [self performSelector:@selector(methodDispatchAfterWithBlock:) withObject:theBlock afterDelay:theSeconds];
    }
    else
    {
        ODSyncBackground *theODSyncBackground = [ODSyncBackground new];
        theODSyncBackground.theDelay = theSeconds;
        [theODSyncBackground methodSynchronizedBackgroundWithBlock:^
         {
             theBlock();
         }];
    }
}

+ (void)methodOpenSettings
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

+ (void)methodPrintAllFonts
{
    for (NSString *theFamily in [UIFont familyNames])
    {
        NSLog(@"%@", theFamily);
        for (NSString *theName in [UIFont fontNamesForFamilyName:theFamily])
        {
            NSLog(@"    %@", theName);
        }
    }
}

+ (void)methodPrintIvarsOfObject:(id __nonnull)theObject
{
    if (!theObject)
    {
        abort();
    }
    uint32_t theIvarsCount;
    Ivar *theIvars = class_copyIvarList([theObject class], &theIvarsCount);
    if (theIvars)
    {
        for (uint32_t i = 0; i < theIvarsCount; i++)
        {
            Ivar ivar = theIvars[i];
            id thePointer = object_getIvar(theObject, ivar);
            if (thePointer)
            {
                NSLog(@"%@", thePointer);
            }
        }
    }
    free(theIvars);
}

+ (void)methodTurnBluetoothON:(BOOL)shouldTurnON
                   completion:(void (^ __nullable)())theBlock
{
    Class BluetoothManager = objc_getClass("BluetoothManager");
    id btCont = [BluetoothManager sharedInstance];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
                   {
                       [btCont setValue:@(shouldTurnON) forKey:@"powered"];
                       [btCont setEnabled:shouldTurnON];
                       if (theBlock)
                       {
                           theBlock();
                       }
                   });
}

+ (void)methodSetStatusBarColor:(UIColor *)theColor
{
    id theStatusBar = [[UIApplication sharedApplication] valueForKey:@"statusBar"];
    [theStatusBar setValue:theColor forKey:@"foregroundColor"];
}

+ (void)methodSetStatusBarHidden:(BOOL)isHidden
{
    [[UIApplication sharedApplication] setStatusBarHidden:isHidden withAnimation:UIStatusBarAnimationSlide];
}

+ (void)methodSetStatusBarStyle:(UIStatusBarStyle)theStyle animated:(BOOL)isAnimated
{
    [[UIApplication sharedApplication] setStatusBarStyle:theStyle animated:isAnimated];
}

+ (NSArray<NSArray *> * _Nonnull)getCombinatoricsArrayFromObjectsArray:(NSArray * _Nonnull)theObjectsArray choose:(NSUInteger)theChoose
{
    methodAssert(theObjectsArray.count);
    methodAssert(theObjectsArray.count >= theChoose);
    if (!theChoose || theChoose == theObjectsArray.count)
    {
        return @[theObjectsArray];
    }
    
    NSUInteger theNumberOfCombinations = 0;
    {// n! / r! / (n - r)!
        double theNFactorial = 1;
        for (NSUInteger i = 1; i <= theObjectsArray.count; i++)
        {
            theNFactorial *= i;
        }
        double theRFactorial = 1;
        for (NSUInteger i = 1; i <= theChoose; i++)
        {
            theRFactorial *= i;
        }
        double theNMinusRFactorial = 1;
        for (NSUInteger i = 1; i <= theObjectsArray.count - theChoose; i++)
        {
            theNMinusRFactorial *= i;
        }
        theNumberOfCombinations = theNFactorial/theRFactorial/theNMinusRFactorial;
    }
    NSMutableArray<NSNumber *> *theCurrentMatrixArray = [NSMutableArray new];
    for (NSUInteger j = 0; j < theChoose; j++)
    {
        [theCurrentMatrixArray addObject:@(0)];
    }
    
    NSUInteger theMaximumNumber = theObjectsArray.count - theChoose;
    NSMutableArray *theFinalArray = [NSMutableArray new];
    for (NSUInteger i = 0; i < theNumberOfCombinations; i++)
    {
        NSMutableArray *theArray = [NSMutableArray new];
        for (NSUInteger j = 0; j < theChoose; j++)
        {
            [theArray addObject:theObjectsArray[j + theCurrentMatrixArray[j].unsignedIntegerValue]];
        }
        [theFinalArray addObject:theArray];
        
        for (NSUInteger k = theChoose - 1; (int)k >= 0; k--)
        {
            theCurrentMatrixArray[k] = @(theCurrentMatrixArray[k].unsignedIntegerValue + 1);
            if (theCurrentMatrixArray[k].unsignedIntegerValue <= theMaximumNumber)
            {
                break;
            }
            else
            {
                for (NSUInteger l = k; l < theChoose; l++)
                {
                    if (!k)
                    {
                        continue;
                    }
                    theCurrentMatrixArray[l] = @(theCurrentMatrixArray[k - 1].unsignedIntegerValue + 1);
                }
            }
        }
    }
    return theFinalArray;
}

#pragma mark - Class Methods (Private)

+ (id)sharedInstance
{
    NSLog(@"this method is needed only for 'methodTurnBluetoothON:completion:' to compile without errors");
    abort();
}

+ (void)methodDispatchAfterWithBlock:(void (^ _Nonnull)())theBlock
{
    theBlock();
}

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























