//
//  NSObject+Extensions.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 9/17/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSObject (ODExtensions)

/// defaults to YES
@property (nonatomic, assign) BOOL isFirstLoad;
@property (nonatomic, assign, readonly) NSInteger theRetainCount;
@property (nonatomic, strong, nullable) id theStrongObject;
@property (nonatomic, strong, nullable) id theStrongObject2;
@property (nonatomic, weak, nullable) id thePairedObject;
@property (nonatomic, weak, nullable) id theWeakObject;

@end






























