//
//  TransparentHoleView.m
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 7/24/15.
//  Copyright (c) 2015 Mobiwolf. All rights reserved.
//

#import "TransparentHoleView.h"

@implementation TransparentHoleView

#pragma mark - Class Methods

#pragma mark - Init & Dealloc

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self methodInitTransparentHoleView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self methodInitTransparentHoleView];
    }
    return self;
}

#pragma mark - Setters & Getters

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Notifications

#pragma mark - Delegates ()

#pragma mark - Methods

- (void)methodInitTransparentHoleView
{
    self.opaque = NO;
    self.theHoleBorderWidth = 0;
}

#pragma mark - Standard Methods

- (void)drawRect:(CGRect)rect
{
    self.backgroundColor = nil;
    [self.theBackgroundColor setFill];
    UIRectFill(rect);
    for (NSValue *theRectValue in self.theTransparentRectsArray)
    {
        if ([theRectValue isKindOfClass:[NSValue class]])
        {
            CGRect theHoleRect = [theRectValue CGRectValue];
            CGRect theHoleIntersectionRect = CGRectIntersection(theHoleRect, rect);
            CGContextRef theContextRef = UIGraphicsGetCurrentContext();
            if (CGRectIntersectsRect(theHoleIntersectionRect, rect))
            {
                CGContextAddEllipseInRect(theContextRef, theHoleIntersectionRect);
                CGContextClip(theContextRef);
                CGContextClearRect(theContextRef, theHoleIntersectionRect);
                CGContextSetFillColorWithColor(theContextRef, [UIColor clearColor].CGColor);
                CGContextSetLineWidth(theContextRef, self.theHoleBorderWidth);
                CGContextSetStrokeColorWithColor(theContextRef, self.theHoleBorderColor.CGColor);
                CGContextAddArc(theContextRef, CGRectGetMidX(theHoleRect), CGRectGetMidY(theHoleRect), theHoleRect.size.width/2, 0.0, 360*M_PI/180, YES);
                CGContextStrokePath(theContextRef);
                CGContextFillRect(theContextRef, theHoleIntersectionRect);
            }
        }
    }
}

@end






























