//
//  UtilitiesManager.h
//  TubeTravel
//
//  Created by od1914@my.bristol.ac.uk on 05/04/2015.
//  Copyright (c) 2015 OlDor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSArray+ODExtensions.h"
#import "NSDate+ODExtensions.h"
#import "NSLocale+ODExtensions.h"
#import "NSManagedObject+ODExtensions.h"
#import "NSMutableAttributedString+ODExtensions.h"
#import "NSObject+ODExtensions.h"
#import "NSString+ODExtensions.h"
#import "ODAlertView.h"
#import "ODPageView.h"
#import "ODURLSession.h"
#import "UIButton+ODExtensions.h"
#import "UICollectionViewCell+ODExtensions.h"
#import "UIImage+ODExtensions.h"
#import "UILabel+ODExtensions.h"
#import "UIResponder+ODExtensions.h"
#import "UIViewController+ODExtensions.h"
#import "UIView+ODExtensions.h"
#import "UIWindow+ODExtentions.h"
#import "Reachability.h"
#import "ODTableView.h"
#import "ODSyncBackground.h"
#import "UISlider+ODExtensions.h"

#define DELEGATE ((AppDelegate*)[[UIApplication sharedApplication]delegate])
#define theMOC (DELEGATE.managedObjectContext)

#define COLOR(r,g,b) [UIColor colorWithRed:(float)(r)/255 green:(float)(g)/255 blue:(float)(b)/255 alpha:1]
#define ColorFromHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define keyDefaultAnimationDuration 0.5
#define keyDefaultCornerRadius 5
#define keyDefaultReuseIdentifier @"defaultReuseIdentifier"

#define strongify(var) \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wshadow\"") \
__strong typeof(var) var = AHKWeak_##var; \
_Pragma("clang diagnostic pop")

#define weakify(var) __weak typeof(var) AHKWeak_##var = var;
#define weakifyBlock(var); __weak __block typeof(var) AHKWeak_##var =

#define startObservingTime(var) NSDate *AHKWeak_##var = [NSDate date]
#define finishObservingTime(var) NSLog(@"observed period: %f", [[NSDate date] timeIntervalSinceDate:AHKWeak_##var])

#ifdef DEBUG
#define methodAssert(var) \
if (!(var)) \
{ \
abort(); \
}
#else
#define methodAssert(var)
#endif

typedef enum : NSUInteger
{
    DeviceiPhone6Plus = 1,
    DeviceiPhone6,
    DeviceiPhone5,
    DeviceiPhone4,
    DeviceiPad,
    DeviceEnumCount = DeviceiPhone4
} Device;

@interface ODKit : NSObject

int getDigitFromNumberAtIndex(int theNumber, int theIndex);
BOOL getTrueWithProbability(int theProbability);
BOOL isEqual(id _Nullable theFirstObject, id _Nullable theSecondObject);
BOOL isSystemAtLeastVersion(int theBasicVersion, int theSubVersion, int theSubSubVersion);
NSString * __nonnull sfc(Class _Nonnull theClass);
NSString * __nonnull sfs(SEL _Nonnull theSelector);

+ (Device)getDevice;
+ (double)getStatusBarHeight;
/// use [[NSNotificationCenter defaultCenter] removeObserver:theObserver] to remove observer manually
+ (id <NSObject> __nonnull)methodAddObserverForName:(NSString * _Nonnull)theNotificationName
                                       withWeakSelf:(id _Nonnull)theWeakSelf
                                         usingBlock:(void (^ _Nonnull)(NSNotification * _Nonnull theNotification))theBlock;

+ (void)methodAsyncBackground:(void (^ _Nonnull)())theBlock;
+ (void)methodAsyncMain:(void (^ _Nonnull)())theBlock;
+ (void)methodSyncMain:(void (^ _Nonnull)())theBlock;
+ (void)methodDispatchAfterSeconds:(double)theSeconds
                             block:(void (^ _Nonnull)())theBlock;

+ (void)methodOpenSettings;
+ (void)methodPrintAllFonts;
+ (void)methodPrintIvarsOfObject:(id _Nonnull)theObject;
+ (void)methodTurnBluetoothON:(BOOL)shouldTurnON
                   completion:(void (^ _Nullable)())theBlock;

+ (void)methodSetStatusBarColor:(UIColor * _Nullable)theColor;
+ (void)methodSetStatusBarHidden:(BOOL)isHidden;
+ (void)methodSetStatusBarStyle:(UIStatusBarStyle)theStyle animated:(BOOL)isAnimated;
+ (NSArray<NSArray *> * _Nonnull)getCombinatoricsArrayFromObjectsArray:(NSArray * _Nonnull)theObjectsArray choose:(NSUInteger)theChoose;

@end






























