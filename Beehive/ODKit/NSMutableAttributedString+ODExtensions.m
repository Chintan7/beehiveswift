//
//  NSAttributedString+Extensions.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 9/21/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "NSMutableAttributedString+ODExtensions.h"

@implementation NSMutableAttributedString (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodAddText:(NSString * __nonnull)theText
             withFont:(UIFont * __nonnull)theFont
            withColor:(UIColor * __nonnull)theColor;
{
    if (!theText || !theFont || !theColor)
    {
        abort();
    }
    NSDictionary *theAttributesDictionary = @{NSFontAttributeName : theFont,
                                              NSForegroundColorAttributeName : theColor};
    NSAttributedString *theAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", theText]
                                                                              attributes:theAttributesDictionary];
    [self appendAttributedString:theAttributedString];
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























