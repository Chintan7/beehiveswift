//
//  UIView+Extensions.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 30/04/2015.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger
{
    UIViewOrientationPortrait = 1,
    UIViewOrientationLandscape,
    UIViewOrientationEnumCount = UIViewOrientationLandscape
} UIViewOrientation;

@interface UIView (ODExtensions)

@property (nonatomic, assign, readonly) BOOL isBottomSeparatorView;
@property (nonatomic, assign, readonly) BOOL isLeftSeparatorView;
@property (nonatomic, assign, readonly) BOOL isRightSeparatorView;
@property (nonatomic, assign, readonly) BOOL isTopSeparatorView;
@property (nonatomic, strong, nonnull, readonly) UIView *theBottomSeparatorView;
@property (nonatomic, assign) double theCenterX;
@property (nonatomic, assign) double theCenterY;
@property (nonatomic, assign) double theHeight;
@property (nonatomic, strong, nonnull, readonly) UIView *theLeftSeparatorView;
@property (nonatomic, assign) double theMaxX;
@property (nonatomic, assign) double theMaxY;
@property (nonatomic, assign) double theMinX;
@property (nonatomic, assign) double theMinY;
@property (nonatomic, assign) double theWidth;
@property (nonatomic, strong, nonnull, readonly) UIView *theRightSeparatorView;
@property (nonatomic, strong, nonnull, readonly) UIView *theTopSeparatorView;
@property (nonatomic, assign, readonly) UIViewOrientation theOrientation;
@property (nonatomic, assign) CGRect theSwappedFrame;

- (void)methodSetCurrentFrameForOrientation:(UIViewOrientation)theOrientation;
- (void)methodAdjustToOrientation:(UIViewOrientation)theOrientation;

- (void)methodSetCornerRadius:(double)theCornerRadius toRectCorner:(UIRectCorner)theRectCorner;
- (void)methodSwapFrames;

@end






























