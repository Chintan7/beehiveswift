//
//  UIViewController+Extensions.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 6/30/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ODExtensions)

@property (nonatomic, weak, nullable, readonly) UIViewController *theBackViewController;

- (void)methodPopToRootVC;

@end






























