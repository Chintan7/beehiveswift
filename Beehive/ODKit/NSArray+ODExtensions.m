//
//  NSArray+ODExtensions.m
//  ODTableView
//
//  Created by OlDor on 1/31/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import "NSArray+ODExtensions.h"

@implementation NSArray (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

#pragma mark - Getters (Public)

- (NSArray * __nonnull)theReversedArray
{
    NSMutableArray *theReversedArray = [NSMutableArray new];
    NSEnumerator *enumerator = [self reverseObjectEnumerator];
    for (id element in enumerator)
    {
        [theReversedArray addObject:element];
    }
    return [NSArray arrayWithArray:theReversedArray];
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)forWithBlock:(void (^ __nullable)(id __nonnull theObject, NSUInteger theIndex, NSArray * __nonnull theArray))theBlock
{
    NSUInteger theCounter = 0;
    for (id theObject in self)
    {
        theBlock(theObject, ++theCounter, self);
    }
//    for (NSUInteger i = 0; i < self.count; i++)
//    {
//        theBlock(self[i], i, self);
//    }
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end
