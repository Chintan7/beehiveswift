//
//  UIWindow+ODExtentions.h
//  SNSales
//
//  Created by od1914@my.bristol.ac.uk on 1/20/16.
//  Copyright © 2016 Mobiwolf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (ODExtentions)

@property (nonatomic, assign, nullable, readonly) UIViewController *theVisibleVC;

@end






























