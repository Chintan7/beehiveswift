//
//  NSString+Extensions.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 9/25/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (ODExtensions)

@property (nonatomic, assign, readonly) BOOL areEmailsValid;
@property (nonatomic, assign, readonly) BOOL isEmailValid;
@property (nonatomic, assign, readonly) BOOL isEmpty;
/// pass values in order of enum 'Device' (i.e. [ODUtilitiesManager getDevice])
@property (nonatomic, assign, readonly) double theDeviceValue;

@end






























