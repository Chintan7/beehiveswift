//
//  NSAttributedString+Extensions.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 9/21/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSMutableAttributedString (ODExtensions)

- (void)methodAddText:(NSString * __nonnull)theText
             withFont:(UIFont * __nonnull)theFont
            withColor:(UIColor * __nonnull)theColor;

@end






























