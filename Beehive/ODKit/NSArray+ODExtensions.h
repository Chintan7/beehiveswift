//
//  NSArray+ODExtensions.h
//  ODTableView
//
//  Created by OlDor on 1/31/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSArray (ODExtensions)

@property (nonatomic, strong, nonnull, readonly) NSArray *theReversedArray;

- (void)forWithBlock:(void (^ __nullable)(id __nonnull theObject, NSUInteger theIndex, NSArray * __nonnull theArray))theBlock;

@end






























