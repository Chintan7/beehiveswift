//
//  ODPageView.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 10/16/15.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "ODPageView.h"

#import "ODKit.h"

@interface ODPageView () <UIGestureRecognizerDelegate>

@property (nonatomic, assign) double theStartPosition;
@property (nonatomic, assign) BOOL shouldRemoveFirst;
@property (nonatomic, assign) BOOL shouldRemoveLast;

@property (nonatomic, strong, nonnull) NSMutableArray<UIView *> *thePageViewsMutableArray;
@property (nonatomic, strong, nonnull) UIView *theMainView;

@end

@implementation ODPageView

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self methodInitODPageView];
    }
    return self;
}

- (void)methodInitODPageView
{
    self.clipsToBounds = YES;
    _isAllowedToScroll = YES;
    _isInfinite = NO;
    _isTouchesFinished = YES;
    _shouldRemoveFirst = NO;
    _shouldRemoveLast = NO;
    _theAnimationDuration = 0.4;
    _theMovementDirection = ODPageViewMovementDirectionLeftToRight;
    _thePageViewsMutableArray = [NSMutableArray new];
    _theSpringDamping = 0.6;
    _theStartPosition = 0;
    
    [self createAllViews];
}

#pragma mark - Setters (Public)

- (void)setIsInfinite:(BOOL)isInfinite
{
    if (_isInfinite == isInfinite)
    {
        return;
    }
    _isInfinite = isInfinite;
}

- (void)setTheMovementDirection:(ODPageViewMovementDirection)theMovementDirection
{
    if (_theMovementDirection == theMovementDirection)
    {
        return;
    }
    _theMovementDirection = theMovementDirection;
    
    if (!self.thePageViewsMutableArray.count)
    {
        return;
    }
    NSArray *theArray = [NSArray arrayWithArray:self.thePageViewsMutableArray];
    [self methodRemoveAllPageViews];
    for (UIView *theView in theArray)
    {
        [self methodAddPageView:theView];
    }
}

#pragma mark - Getters (Public)

- (double)theCurrentOffset
{
    double theCurrentOffset;
    switch (self.theMovementDirection)
    {
        case ODPageViewMovementDirectionLeftToRight:
        {
            theCurrentOffset = self.theMainView.theWidth/2 - self.theMainView.theCenterX;
        }
            break;
        case ODPageViewMovementDirectionTopToBottom:
        {
            theCurrentOffset = self.theMainView.theHeight/2 - self.theMainView.theCenterY;
        }
            break;
    }
    return theCurrentOffset;
}

- (NSUInteger)theCurrentPage
{
    UIView *theView;
    switch (self.theMovementDirection)
    {
        case ODPageViewMovementDirectionLeftToRight:
        {
            theView = [self.theMainView hitTest:CGPointMake(self.theCurrentOffset, self.theMainView.theHeight/2) withEvent:nil];
        }
            break;
        case ODPageViewMovementDirectionTopToBottom:
        {
            theView = [self.theMainView hitTest:CGPointMake(self.theMainView.theWidth/2, self.theCurrentOffset) withEvent:nil];
        }
            break;
    }
    while (theView.superview != self.theMainView)
    {
        theView = theView.superview;
        if (!theView.superview)
        {
            NSLog(@"error");
            abort();
        }
    }
    return [self.thePageViewsMutableArray indexOfObject:theView];
}

- (CGRect)theMainViewFrame
{
    return self.theMainView.frame;
}

- (NSArray<UIView *> * __nonnull)thePageViewsArray
{
    return self.thePageViewsMutableArray;
}

#pragma mark - Setters (Private)

- (void)setIsTouchesFinished:(BOOL)isTouchesFinished
{
    if (_isTouchesFinished == isTouchesFinished)
    {
        return;
    }
    _isTouchesFinished = isTouchesFinished;
}

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

- (void)createAllViews
{
    if (!self.isFirstLoad)
    {
        return;
    }
    self.isFirstLoad = NO;
    
    self.theMainView = [UIView new];
    [self addSubview:self.theMainView];
    {
        UIPanGestureRecognizer *thePanGesture = [UIPanGestureRecognizer new];
        thePanGesture.delegate = self;
        [thePanGesture addTarget:self action:@selector(handleMainPanGesture:)];
        [self.theMainView addGestureRecognizer:thePanGesture];
    }
}

#pragma mark - Actions

#pragma mark - Gestures

- (void)handleMainPanGesture:(UIPanGestureRecognizer *)thePanGesture
{
    if (!self.isAllowedToScroll)
    {
        return;
    }
    
    UIView *theGestureView = thePanGesture.view;
    
    double theVelocity = 0;
    switch (self.theMovementDirection)
    {
        case ODPageViewMovementDirectionLeftToRight:
        {
            theVelocity = [thePanGesture velocityInView:theGestureView].x/7.5;
        }
            break;
        case ODPageViewMovementDirectionTopToBottom:
        {
            theVelocity = [thePanGesture velocityInView:theGestureView].y/7.5;
        }
            break;
    }
    
    if (thePanGesture.state == UIGestureRecognizerStateBegan)
    {
        self.isTouchesFinished = NO;
        switch (self.theMovementDirection)
        {
            case ODPageViewMovementDirectionLeftToRight:
            {
                self.theStartPosition = [thePanGesture locationInView:theGestureView].x;
                if (self.isInfinite)
                {
                    if (theVelocity > 0)
                    {
                        if (self.theStartPosition < self.thePageViewsArray.firstObject.theMaxX)
                        {
                            self.shouldRemoveLast = YES;
                        }
                    }
                    else
                    {
                        if (self.theStartPosition > self.thePageViewsArray.firstObject.theMaxX)
                        {
                            self.shouldRemoveFirst = YES;
                        }
                    }
                }
            }
                break;
            case ODPageViewMovementDirectionTopToBottom:
            {
                self.theStartPosition = [thePanGesture locationInView:theGestureView].y;
            }
                break;
        }
        if ([self.theDelegate respondsToSelector:@selector(pageViewDidStartScrolling:)])
        {
            [self.theDelegate pageViewDidStartScrolling:self];
        }
    }
    else if (thePanGesture.state == UIGestureRecognizerStateEnded ||
             thePanGesture.state == UIGestureRecognizerStateCancelled ||
             thePanGesture.state == UIGestureRecognizerStateFailed)
    {
        self.shouldRemoveFirst = NO;
        self.shouldRemoveLast = NO;
        self.isTouchesFinished = YES;
        double theFuturePosition;
        switch (self.theMovementDirection)
        {
            case ODPageViewMovementDirectionLeftToRight:
            {
                theFuturePosition = self.theStartPosition - theVelocity + (theVelocity > 0 ? -self.theWidth/4 : self.theWidth/4);
            }
                break;
            case ODPageViewMovementDirectionTopToBottom:
            {
                theFuturePosition = self.theStartPosition - theVelocity + (theVelocity > 0 ? -self.theHeight/4 : self.theHeight/4);
            }
                break;
        }
        
        UIView *theTargetView;
        switch (self.theMovementDirection)
        {
            case ODPageViewMovementDirectionLeftToRight:
            {
                theTargetView = [theGestureView hitTest:CGPointMake(theFuturePosition, theGestureView.theHeight/2) withEvent:nil];
            }
                break;
            case ODPageViewMovementDirectionTopToBottom:
            {
                theTargetView = [theGestureView hitTest:CGPointMake(theGestureView.theWidth/2, theFuturePosition) withEvent:nil];
            }
                break;
        }
        if (!theTargetView)
        {
            UIView *thePageView;
            switch (self.theMovementDirection)
            {
                case ODPageViewMovementDirectionLeftToRight:
                {
                    if (theFuturePosition > theGestureView.theWidth)
                    {
                        thePageView = self.thePageViewsMutableArray.lastObject;
                    }
                    if (theFuturePosition < theGestureView.theWidth/2)
                    {
                        thePageView = self.thePageViewsMutableArray.firstObject;
                    }
                    theTargetView = [theGestureView hitTest:CGPointMake(thePageView.theCenterX, theGestureView.theHeight/2) withEvent:nil];
                }
                    break;
                case ODPageViewMovementDirectionTopToBottom:
                {
                    if (theFuturePosition > theGestureView.theHeight)
                    {
                        thePageView = self.thePageViewsMutableArray.lastObject;
                    }
                    if (theFuturePosition < theGestureView.theHeight/2)
                    {
                        thePageView = self.thePageViewsMutableArray.firstObject;
                    }
                    theTargetView = [theGestureView hitTest:CGPointMake(theGestureView.theWidth/2, thePageView.theCenterY) withEvent:nil];
                }
                    break;
            }
        }
        
        if (self.isInfinite)
        {
            while (theTargetView.superview != self.theMainView)
            {
                theTargetView = theTargetView.superview;
                if (!theTargetView.superview)
                {
                    if (theVelocity > 0)
                    {
                        theTargetView = self.thePageViewsMutableArray.firstObject;
                    }
                    else
                    {
                        theTargetView = self.thePageViewsMutableArray.lastObject;
                    }
                }
            }
        }
        [self methodScrollToPageView:theTargetView animated:YES completion:nil];
        if ([self.theDelegate respondsToSelector:@selector(pageViewDidFinishTouches:)])
        {
            [self.theDelegate pageViewDidFinishTouches:self];
        }
    }
    else
    {
        double theFuturePosition;
        switch (self.theMovementDirection)
        {
            case ODPageViewMovementDirectionLeftToRight:
            {
                theFuturePosition = [thePanGesture locationInView:theGestureView.superview].x - self.theStartPosition + theGestureView.theWidth/2;
                theGestureView.theCenterX = theFuturePosition;
                if (self.shouldRemoveFirst)
                {
                    self.shouldRemoveFirst = NO;
                    
                    UIView *theView = self.thePageViewsMutableArray.firstObject;
                    self.theStartPosition -= theView.theWidth;
                    [self.thePageViewsMutableArray removeObjectAtIndex:0];
                    [self methodAddPageView:theView];
                }
                if (self.shouldRemoveLast)
                {
                    self.shouldRemoveLast = NO;
                    UIView *theView = self.thePageViewsMutableArray.lastObject;
                    self.theStartPosition += theView.theWidth;
                    
                    NSMutableArray *theArray = self.thePageViewsMutableArray.mutableCopy;
                    [theArray removeLastObject];
                    
                    NSUInteger theCounter = self.thePageViewsMutableArray.count;
                    for (int i = 0; i < theCounter - 1; i++)
                    {
                        [self.thePageViewsMutableArray removeObjectAtIndex:0];
                    }
                    for (UIView *theView in theArray)
                    {
                        [self methodAddPageView:theView];
                    }
                }
            }
                break;
            case ODPageViewMovementDirectionTopToBottom:
            {
                theFuturePosition = [thePanGesture locationInView:theGestureView.superview].y - self.theStartPosition + theGestureView.theHeight/2;
                theGestureView.theCenterY = theFuturePosition;
            }
                break;
        }
        if ([self.theDelegate respondsToSelector:@selector(pageViewDidScroll:)])
        {
            [self.theDelegate pageViewDidScroll:self];
        }
    }
}

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodAddPageView:(UIView * __nonnull)theView
{
    if (!theView)
    {
        abort();
    }
    [self.thePageViewsMutableArray addObject:theView];
    
    double theWidth = 0;
    double theHeight = 0;
    for (UIView *thePageView in self.thePageViewsMutableArray)
    {
        switch (self.theMovementDirection)
        {
            case ODPageViewMovementDirectionLeftToRight:
            {
                theWidth += thePageView.theWidth;
                if (thePageView.theHeight > theHeight)
                {
                    theHeight = thePageView.theHeight;
                }
            }
                break;
            case ODPageViewMovementDirectionTopToBottom:
            {
                theHeight += thePageView.theHeight;
                if (thePageView.theWidth > theWidth)
                {
                    theWidth = thePageView.theWidth;
                }
            }
                break;
        }
        
    }
    self.theMainView.theHeight = theHeight;
    self.theMainView.theWidth = theWidth;
    switch (self.theMovementDirection)
    {
        case ODPageViewMovementDirectionLeftToRight:
        {
            self.theMainView.theCenterY = self.theHeight/2;
        }
            break;
        case ODPageViewMovementDirectionTopToBottom:
        {
            self.theMainView.theCenterX = self.theWidth/2;
        }
            break;
    }
    
    double theCurrentOffset = 0;
    switch (self.theMovementDirection)
    {
        case ODPageViewMovementDirectionLeftToRight:
        {
            for (UIView *thePageView in self.thePageViewsMutableArray)
            {
                thePageView.theMinX = theCurrentOffset;
                thePageView.theCenterY = theHeight/2;
                theCurrentOffset += thePageView.theWidth;
            }
        }
            break;
        case ODPageViewMovementDirectionTopToBottom:
        {
            for (UIView *thePageView in self.thePageViewsMutableArray)
            {
                thePageView.theMinY = theCurrentOffset;
                thePageView.theCenterX = theWidth/2;
                theCurrentOffset += thePageView.theHeight;
            }
        }
            break;
    }
    if (theView.superview)
    {
        if (theView.superview != self.theMainView)
        {
            [theView removeFromSuperview];
            [self.theMainView addSubview:theView];
        }
    }
    else
    {
        [self.theMainView addSubview:theView];
    }
    
    if (self.thePageViewsMutableArray.count == 1)
    {
        [self methodScrollToPageView:self.thePageViewsMutableArray.firstObject animated:NO completion:nil];
    }
    if (self.isInfinite)
    {
        if (self.theMainView.theWidth == theWidth)
        {
            self.theMainView.theWidth += self.thePageViewsArray.firstObject.theWidth;
        }
    }
}

- (void)methodRemoveAllPageViews
{
    for (UIView *theView in self.thePageViewsMutableArray)
    {
        [theView removeFromSuperview];
    }
    [self.thePageViewsMutableArray removeAllObjects];
}

- (void)methodScrollToPageView:(UIView * __nonnull)theView
                      animated:(BOOL)isAnimated
                    completion:(void (^ __nullable)())theBlock
{
    if (!theView)
    {
        abort();
    }
    {// to properly pass coordinates, we need to get theTargetView as subview of theMainView
        while (theView.superview != self.theMainView)
        {
            theView = theView.superview;
            if (!theView.superview)
            {
                NSLog(@"error");
                abort();
            }
        }
    }
    
    [UIView animateWithDuration:isAnimated ? self.theAnimationDuration : 0
                          delay:0
         usingSpringWithDamping:self.theSpringDamping
          initialSpringVelocity:0
                        options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction
                     animations:^
     {
         switch (self.theMovementDirection)
         {
             case ODPageViewMovementDirectionLeftToRight:
             {
                 self.theMainView.theCenterX = self.theMainView.superview.theWidth/2 - theView.theCenterX + self.theMainView.theWidth/2;
             }
                 break;
             case ODPageViewMovementDirectionTopToBottom:
             {
                 self.theMainView.theCenterY = self.theMainView.superview.theHeight/2 - theView.theCenterY + self.theMainView.theHeight/2;
             }
                 break;
         }
         if ([self.theDelegate respondsToSelector:@selector(pageViewDidScroll:)])
         {
             [ODKit methodAsyncMain:^
              {
                  [self.theDelegate pageViewDidScroll:self];
              }];
         }
     }
                     completion:^(BOOL finished)
     {
         if (theBlock)
         {
             theBlock();
         }
         if ([self.theDelegate respondsToSelector:@selector(pageViewDidStopScrolling:)])
         {
             [self.theDelegate pageViewDidStopScrolling:self];
         }
     }];
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























