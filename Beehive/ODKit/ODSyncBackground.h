//
//  ODSyncBackground.h
//  VKMusicPlayer
//
//  Created by OlDor on 3/31/16.
//  Copyright © 2016 OlDor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ODSyncBackground : NSObject
/// defaults to 0
@property (nonatomic, assign) double theDelay;

- (void)methodSynchronizedBackgroundWithBlock:(void (^ _Nonnull)())theBlock;

@end






























