//
//  UIView+Extensions.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 30/04/2015.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "UIView+ODExtensions.h"

#import "ODKit.h"

#import <objc/runtime.h>

#define keyUIViewOrientationPortrait @"keyUIViewOrientationPortrait"
#define keyUIViewOrientationLandscape @"keyUIViewOrientationLandscape"

@interface UIView (ODExtentions)

@property (nonatomic, assign) CGRect theSwappedOrientationFrame;

@end

@implementation UIView (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

+ (void)load
{
    static dispatch_once_t theOnceToken;
    dispatch_once(&theOnceToken, ^
                  {
                      Class class = [self class];
                      SEL theOriginalSelector = nil;
                      SEL theSwizzledSelector = nil;
                      for (int i = 0; i < 1; i++)
                      {
                          switch (i)
                          {
                              case 0:
                              {
                                  theOriginalSelector = @selector(setFrame:);
                                  theSwizzledSelector = @selector(swizzled_setFrame:);
                              }
                                  break;
                          }
                      }
                      if (!theOriginalSelector || !theSwizzledSelector)
                      {
                          abort();
                      }
                      
                      Method theOriginalMethod = class_getInstanceMethod(class, theOriginalSelector);
                      Method theSwizzledMethod = class_getInstanceMethod(class, theSwizzledSelector);
                      
                      BOOL didAddMethod = class_addMethod(class,
                                                          theOriginalSelector,
                                                          method_getImplementation(theSwizzledMethod),
                                                          method_getTypeEncoding(theSwizzledMethod));
                      
                      if (didAddMethod)
                      {
                          class_replaceMethod(class,
                                              theSwizzledSelector,
                                              method_getImplementation(theOriginalMethod),
                                              method_getTypeEncoding(theOriginalMethod));
                      }
                      else
                      {
                          method_exchangeImplementations(theOriginalMethod, theSwizzledMethod);
                      }
                  });
}

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

- (void)setDidAdjustToLandscape:(BOOL)didAdjustToLandscape
{
    objc_setAssociatedObject(self, @selector(didAdjustToLandscape), @(didAdjustToLandscape), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setIsBottomSeparatorView:(BOOL)isBottomSeparatorView
{
    objc_setAssociatedObject(self, @selector(isBottomSeparatorView), isBottomSeparatorView ? @(isBottomSeparatorView) : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setIsLeftSeparatorView:(BOOL)isLeftSeparatorView
{
    objc_setAssociatedObject(self, @selector(isLeftSeparatorView), isLeftSeparatorView ? @(isLeftSeparatorView) : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setIsRightSeparatorView:(BOOL)isRightSeparatorView
{
    objc_setAssociatedObject(self, @selector(isRightSeparatorView), isRightSeparatorView ? @(isRightSeparatorView) : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setIsTopSeparatorView:(BOOL)isTopSeparatorView
{
    objc_setAssociatedObject(self, @selector(isTopSeparatorView), isTopSeparatorView ? @(isTopSeparatorView) : nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheBottomSeparatorView:(UIView *)theBottomSeparatorView
{
    theBottomSeparatorView.isBottomSeparatorView = YES;
    objc_setAssociatedObject(self, @selector(theBottomSeparatorView), theBottomSeparatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheCenterX:(double)theCenterX
{
    CGPoint thePoint = self.center;
    thePoint.x = theCenterX;
    self.center = thePoint;
}

- (void)setTheCenterY:(double)theCenterY
{
    CGPoint thePoint = self.center;
    thePoint.y = theCenterY;
    self.center = thePoint;
}

- (void)setTheHeight:(double)theHeight
{
    CGRect theFrame = self.frame;
    theFrame.size.height = theHeight;
    self.frame = theFrame;
}

- (void)setTheLeftSeparatorView:(UIView *)theLeftSeparatorView
{
    theLeftSeparatorView.isLeftSeparatorView = YES;
    objc_setAssociatedObject(self, @selector(theLeftSeparatorView), theLeftSeparatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheMaxX:(double)theMaxX
{
    CGRect theFrame = self.frame;
    theFrame.origin.x = theMaxX - theFrame.size.width;
    self.frame = theFrame;
}

- (void)setTheMaxY:(double)theMaxY
{
    CGRect theFrame = self.frame;
    theFrame.origin.y = theMaxY - theFrame.size.height;
    self.frame = theFrame;
}

- (void)setTheMinX:(double)theMinX
{
    CGRect theFrame = self.frame;
    theFrame.origin.x = theMinX;
    self.frame = theFrame;
}

- (void)setTheMinY:(double)theMinY
{
    CGRect theFrame = self.frame;
    theFrame.origin.y = theMinY;
    self.frame = theFrame;
}

- (void)setTheWidth:(double)theWidth
{
    CGRect theFrame = self.frame;
    theFrame.size.width = theWidth;
    self.frame = theFrame;
}

- (void)setTheRightSeparatorView:(UIView *)theRightSeparatorView
{
    theRightSeparatorView.isRightSeparatorView = YES;
    objc_setAssociatedObject(self, @selector(theRightSeparatorView), theRightSeparatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheTopSeparatorView:(UIView *)theTopSeparatorView
{
    theTopSeparatorView.isTopSeparatorView = YES;
    objc_setAssociatedObject(self, @selector(theTopSeparatorView), theTopSeparatorView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheOrientation:(UIViewOrientation)theOrientation
{
    if (!theOrientation || theOrientation > UIViewOrientationEnumCount)
    {
        abort();
    }
    if (self.theOrientation == theOrientation)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theOrientation), @(theOrientation), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setTheSwappedFrame:(CGRect)theSwappedFrame
{
    NSValue *theSwappedFrameValue = [NSValue valueWithCGRect:theSwappedFrame];
    if (objc_getAssociatedObject(self, @selector(theSwappedFrame)) == theSwappedFrameValue)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theSwappedFrame), theSwappedFrameValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Getters (Public)

- (BOOL)didAdjustToLandscape
{
    NSNumber *theNumber = objc_getAssociatedObject(self, @selector(didAdjustToLandscape));
    if (theNumber)
    {
        return theNumber.boolValue;
    }
    return YES;
}

- (BOOL)isBottomSeparatorView
{
    return objc_getAssociatedObject(self, @selector(isBottomSeparatorView)) ? YES : NO;
}

- (BOOL)isLeftSeparatorView
{
    return objc_getAssociatedObject(self, @selector(isLeftSeparatorView)) ? YES : NO;
}

- (BOOL)isRightSeparatorView
{
    return objc_getAssociatedObject(self, @selector(isRightSeparatorView)) ? YES : NO;
}

- (BOOL)isTopSeparatorView
{
    return objc_getAssociatedObject(self, @selector(isTopSeparatorView)) ? YES : NO;
}

- (UIView *)theBottomSeparatorView
{
    UIView *theView = objc_getAssociatedObject(self, @selector(theBottomSeparatorView));
    if (!theView)
    {
        theView = [UIView new];
        [self addSubview:theView];
        self.theBottomSeparatorView = theView;
        theView.theWidth = self.theWidth;
        theView.theHeight = 1;
        theView.theMaxY = self.theHeight;
        theView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        theView.userInteractionEnabled = NO;
    }
    return theView;
}

- (double)theCenterX
{
    return self.center.x;
}

- (double)theCenterY
{
    return self.center.y;
}

- (double)theHeight
{
    return self.frame.size.height;
}

- (UIView *)theLeftSeparatorView
{
    UIView *theView = objc_getAssociatedObject(self, @selector(theLeftSeparatorView));
    if (!theView)
    {
        theView = [UIView new];
        [self addSubview:theView];
        self.theLeftSeparatorView = theView;
        theView.theHeight = self.theHeight;
        theView.theWidth = 1;
        theView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        theView.userInteractionEnabled = NO;
    }
    return theView;
}

- (double)theMaxX
{
    return self.frame.origin.x + self.frame.size.width;
}

- (double)theMaxY
{
    return self.frame.origin.y + self.frame.size.height;
}

- (double)theMinX
{
    return self.frame.origin.x;
}

- (double)theMinY
{
    return self.frame.origin.y;
}

- (double)theWidth
{
    return self.frame.size.width;
}

- (UIView *)theRightSeparatorView
{
    UIView *theView = objc_getAssociatedObject(self, @selector(theRightSeparatorView));
    if (!theView)
    {
        theView = [UIView new];
        [self addSubview:theView];
        self.theRightSeparatorView = theView;
        theView.theHeight = self.theHeight;
        theView.theWidth = 1;
        theView.theMaxX = self.theWidth;
        theView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
        theView.userInteractionEnabled = NO;
    }
    return theView;
}

- (UIView *)theTopSeparatorView
{
    UIView *theView = objc_getAssociatedObject(self, @selector(theTopSeparatorView));
    if (!theView)
    {
        theView = [UIView new];
        [self addSubview:theView];
        self.theTopSeparatorView = theView;
        theView.theWidth = self.theWidth;
        theView.theHeight = 1;
        theView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        theView.userInteractionEnabled = NO;
    }
    return theView;
}

- (UIViewOrientation)theOrientation
{
    NSNumber *theNumber = objc_getAssociatedObject(self, @selector(theOrientation));
    if (!theNumber)
    {
        return UIViewOrientationPortrait;
    }
    return theNumber.unsignedIntegerValue;
}

- (CGRect)theSwappedFrame
{
    NSValue *theSwappedFrameValue = objc_getAssociatedObject(self, @selector(theSwappedFrame));
    if (!theSwappedFrameValue)
    {
        return self.frame;
    }
    return theSwappedFrameValue.CGRectValue;
}

#pragma mark - Setters (Private)

- (void)swizzled_setFrame:(CGRect)frame
{
    if (frame.size.width < 0)
    {
        frame.size.width = 0;
    }
    if (frame.size.height < 0)
    {
        frame.size.height = 0;
    }
    
    if (self.isTopSeparatorView)
    {
        frame.origin.y = 0;
    }
    else if (self.isBottomSeparatorView)
    {
        frame.origin.y = self.superview.theHeight - frame.size.height;
    }
    else if (self.isLeftSeparatorView)
    {
        frame.origin.x = 0;
    }
    else if (self.isRightSeparatorView)
    {
        frame.origin.x = self.superview.theWidth - frame.size.width;
    }
    [self swizzled_setFrame:frame];
}

- (void)setTheSwappedOrientationFrame:(CGRect)theSwappedOrientationFrame
{
    NSValue *theSwappedOrientationFrameValue = [NSValue valueWithCGRect:theSwappedOrientationFrame];
    if (objc_getAssociatedObject(self, @selector(theSwappedOrientationFrame)) == theSwappedOrientationFrameValue)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theSwappedOrientationFrame), theSwappedOrientationFrameValue, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Getters (Private)

- (CGRect)theSwappedOrientationFrame
{
    NSValue *theSwappedOrientationFrameValue = objc_getAssociatedObject(self, @selector(theSwappedOrientationFrame));
    if (!theSwappedOrientationFrameValue)
    {
        return self.frame;
    }
    return theSwappedOrientationFrameValue.CGRectValue;
}

#pragma mark - Create Views & Variables

#pragma mark - Actions

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodSetCurrentFrameForOrientation:(UIViewOrientation)theOrientation
{
    if (!theOrientation || theOrientation > UIViewOrientationEnumCount)
    {
        abort();
    }
    NSString *theKey;
    switch (theOrientation)
    {
        case UIViewOrientationPortrait:
        {
            theKey = keyUIViewOrientationPortrait;
        }
            break;
        case UIViewOrientationLandscape:
        {
            theKey = keyUIViewOrientationLandscape;
        }
            break;
    }
    if (!theKey)
    {
        abort();
    }
    objc_setAssociatedObject(self, (__bridge const void *)(theKey), [NSValue valueWithCGRect:self.frame], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)methodAdjustToOrientation:(UIViewOrientation)theOrientation
{
    if (!theOrientation || theOrientation > UIViewOrientationEnumCount)
    {
        abort();
    }
    NSValue *theValue;
    self.theOrientation = theOrientation;
    switch (theOrientation)
    {
        case UIViewOrientationPortrait:
        {
            theValue = objc_getAssociatedObject(self, keyUIViewOrientationPortrait);
        }
            break;
        case UIViewOrientationLandscape:
        {
            theValue = objc_getAssociatedObject(self, keyUIViewOrientationLandscape);;
        }
            break;
    }
    if (theValue)
    {
        self.frame = theValue.CGRectValue;
    }
    for (NSUInteger theIndex = 0; theIndex < self.subviews.count; theIndex++)
    {
        UIView *theSubview = self.subviews[theIndex];
        [theSubview methodAdjustToOrientation:theOrientation];
    }
}

- (void)methodSetCornerRadius:(double)theCornerRadius toRectCorner:(UIRectCorner)theRectCorner
{
    CAShapeLayer *theShapeLayer = [[CAShapeLayer alloc] init];
    theShapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                               byRoundingCorners:theRectCorner
                                                     cornerRadii:CGSizeMake(theCornerRadius, theCornerRadius)].CGPath;
    self.layer.mask = theShapeLayer;
}

- (void)methodSwapFrames
{
    CGRect theFrame = self.frame;
    self.frame = self.theSwappedFrame;
    self.theSwappedFrame = theFrame;
}

#pragma mark - Methods (Private)

- (void)methodSwapOrientationFrames
{
    self.didAdjustToLandscape = !self.didAdjustToLandscape;
    CGRect theFrame = self.frame;
    self.frame = self.theSwappedOrientationFrame;
    self.theSwappedOrientationFrame = theFrame;
}

#pragma mark - Standard Methods

@end






























