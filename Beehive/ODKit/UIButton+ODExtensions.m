//
//  UIButton+Extensions.m
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 13/05/2015.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import "UIButton+ODExtensions.h"

#import "ODKit.h"

#import <objc/runtime.h>

@implementation UIButton (ODExtensions)

#pragma mark - Class Methods (Public)

#pragma mark - Class Methods (Private)

#pragma mark - Init & Dealloc

#pragma mark - Setters (Public)

- (void)setTheTitleAndImageSpacing:(double)theTitleAndImageSpacing
{
    if (self.theTitleAndImageSpacing == theTitleAndImageSpacing)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(theTitleAndImageSpacing), @(theTitleAndImageSpacing), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    switch (self.contentHorizontalAlignment)
    {
        case UIControlContentHorizontalAlignmentCenter:
        {
            self.imageEdgeInsets = UIEdgeInsetsMake(self.imageEdgeInsets.top, -theTitleAndImageSpacing/2, self.imageEdgeInsets.bottom, theTitleAndImageSpacing/2);
            self.titleEdgeInsets = UIEdgeInsetsMake(self.titleEdgeInsets.top, theTitleAndImageSpacing/2, self.titleEdgeInsets.bottom, -theTitleAndImageSpacing/2);
        }
            break;
        case UIControlContentHorizontalAlignmentLeft:
        {
            self.titleEdgeInsets = UIEdgeInsetsMake(self.titleEdgeInsets.top, theTitleAndImageSpacing, self.titleEdgeInsets.bottom, -theTitleAndImageSpacing);
        }
            break;
        case UIControlContentHorizontalAlignmentRight:
        {
            self.imageEdgeInsets = UIEdgeInsetsMake(self.imageEdgeInsets.top, -theTitleAndImageSpacing, self.imageEdgeInsets.bottom, theTitleAndImageSpacing);
        }
            break;
        case UIControlContentHorizontalAlignmentFill:
        {
            NSLog(@"need to finish");
            abort();
        }
            break;
    }
}

#pragma mark - Getters (Public)

- (double)theTitleAndImageSpacing
{
    NSNumber *theTitleAndImageSpacing = objc_getAssociatedObject(self, @selector(theTitleAndImageSpacing));
    return theTitleAndImageSpacing.doubleValue;
}

#pragma mark - Setters (Private)

#pragma mark - Getters (Private)

#pragma mark - Create Views & Variables

#pragma mark - Actions

- (void)actionHandleBlock:(UIButton *)theButton
{
    void (^theBlock)() = objc_getAssociatedObject(self, @selector(methodHandleActionWithBlock:));
    if (theBlock)
    {
        theBlock();
    }
}

#pragma mark - Gestures

#pragma mark - Delegates ()

#pragma mark - Methods (Public)

- (void)methodHandleActionBlock
{
    [self actionHandleBlock:self];
}

- (void)methodHandleActionWithBlock:(void (^ __nullable)())theBlock
{
    if (objc_getAssociatedObject(self, @selector(methodHandleActionWithBlock:)) == (id)theBlock)
    {
        return;
    }
    objc_setAssociatedObject(self, @selector(methodHandleActionWithBlock:), theBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(actionHandleBlock:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)methodSwapTitleAndImage
{
    if (!self.currentTitle || (!self.imageView.frame.size.width && !self.imageView.frame.size.height))
    {
        abort();
    }
    else
    {
        self.titleEdgeInsets = UIEdgeInsetsMake(self.titleEdgeInsets.top, -self.imageView.frame.size.width, self.titleEdgeInsets.bottom, self.imageView.frame.size.width);
        self.imageEdgeInsets = UIEdgeInsetsMake(self.imageEdgeInsets.top, self.titleLabel.frame.size.width, self.imageEdgeInsets.bottom, -self.titleLabel.frame.size.width);
    }
}

#pragma mark - Methods (Private)

#pragma mark - Standard Methods

@end






























