//
//  UIButton+Extensions.h
//  Digitsole
//
//  Created by od1914@my.bristol.ac.uk on 13/05/2015.
//  Copyright (c) 2015 Glaglashoes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ODExtensions)

@property (nonatomic, assign) double theTitleAndImageSpacing;

- (void)methodHandleActionBlock;
- (void)methodHandleActionWithBlock:(void (^ __nullable)())theBlock;
- (void)methodSwapTitleAndImage;

@end






























