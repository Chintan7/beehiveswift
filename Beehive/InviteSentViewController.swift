//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  InviteSentViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 02/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class InviteSentViewController: BaseViewController {

    @IBOutlet var viewMessageLabel: UILabel!

    @IBAction func inviteAnotherBtnClick(_ sender: Any) {
        popTo()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.customizeView()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func customizeView() {

        
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
            //    Top Toolbar Create
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(125), height: CGFloat(44)))
        titleLabel.text = "Send an Invite"
        titleLabel.textAlignment = .right
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold15
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        barItems.append(toolbarTitleLbl)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
            //    [invitationButton setTitle:@"Send Invite" forState:UIControlStateNormal];
            //    invitationButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20.0f];
            //    [invitationButton setTitleColor:[UIColor colorWithRed:255.0 green:255.0 blue:255.0 alpha:0.7] forState:UIControlStateNormal];
        let firstLabel = "Invitation sent!"
        let secondLabel = "Please wait a few minutes for it to show up. If your friend doesn't see it, ask them to check their spam folder."
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\n\(secondLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular18, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular14, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        viewMessageLabel.attributedText = customTextLabelAttribute
    }


    func backBtnClick() {
        popTo()
    }


}
//
//  InviteSentViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 02/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
