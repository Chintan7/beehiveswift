//
//  LoginViewController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController, UIAlertViewDelegate {

    // Variable Declaration

    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet var emailTextfieldBG: UIImageView!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var forgotPasswordButton: UIButton!
    var keyboardHideTap: UITapGestureRecognizer!

    var responseTokenString: String!
    var responseEmailString: String!
    var alertLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loginViewModification()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == emailTextfield {

            if alertLabel != nil {
                alertLabel.removeFromSuperview()
            }

            emailTextfieldBG.image = UIImage(named: "LoginTf.png")!
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextfield {
            passwordTextfield.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextfield {
            if textField.text?.validateEmail() == false {
                alertLabel = UILabel(frame: CGRect(x: CGFloat(emailTextfield.frame.origin.x), y: CGFloat(emailTextfield.frame.origin.y + 40), width: CGFloat(emailTextfield.frame.size.width), height: CGFloat(15)))
                alertLabel.text = "Please enter a valid email format"
                alertLabel.textColor = UIColor.red
                alertLabel.textAlignment = .left
                alertLabel.font = UIFont(name: FONTTYPE, size: CGFloat(12.0))!
                self.view.addSubview(alertLabel)
                emailTextfieldBG.image = UIImage(named: "AlertTf.png")!
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loginViewModification() {
        keyboardHideTap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyBoard))
        self.view.addGestureRecognizer(keyboardHideTap)
        let prefs = UserDefaults.standard
        
        emailTextfield.text = prefs.object(forKey: "UserId") as! String?
        passwordTextfield.text = prefs.object(forKey: "Password") as! String?
//        emailTextfield.text = "chintan@solulab.com"
//        passwordTextfield.text = "BeFocus7"
    }

    func hideKeyBoard()
    {

        self.view.endEditing(true)
    }

    @IBAction func forgotButtonPressed(_ sender: Any) {

        pushTo(string: "ForgotPasswordViewController")
        self.navigationController!.isNavigationBarHidden = false
    }

    @IBAction func signUpButtonPressed(_ sender: Any) {
        pushTo(string: "SignUpViewController")
    }

    @IBAction func loginButtonPressed(_ sender: Any) {

        if self.validateAllData() == true {

            let login = Login()
            login.deviceToken = "DeviceToken"
            login.email = emailTextfield.text!
            login.password = passwordTextfield.text!

            APPDELEGATE.addChargementLoader()
            APIManager.sharedInstance.LoginUser(login: login, completion: { (error:NSError?) in

                APPDELEGATE.removeChargementLoader()
                if error == nil {

                    UserDefaults.standard.set(self.emailTextfield.text!, forKey: "UserId")
                    UserDefaults.standard.set(self.passwordTextfield.text!, forKey: "Password")

                    APIManager.sharedInstance.getNotificationCountValue(completion: { (result:JSONDictionary?, error:NSError?) in

                        if error == nil {
            
                            if let badge = result?["unread_push_notification"] as? String {
                                APPDELEGATE.unreadNotificationCount = badge
                                APPDELEGATE.requestGetMyCurrentProject()
                            }
                            
                        }
                        APPDELEGATE.addStatButton()
                        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
                        let menuView = MenuEventViewController(nibName: "MenuEventViewController", bundle: nil)
                        APPDELEGATE.navController = UINavigationController(rootViewController: homeScreen)
                        let container = MFSideMenuContainerViewController.container(withCenter: APPDELEGATE.navController, leftMenuViewController: menuView, rightMenuViewController: nil)
                        APPDELEGATE.window!.rootViewController! = container!
                    })
                }
            })
        }
    }

    func validateAllData() -> Bool {
        if emailTextfield.text?.characters.count == 0 {
            let alert = UIAlertView(title: " Alert", message: "All field are mandatory", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        else if passwordTextfield.text?.characters.count == 0 {
            let alert = UIAlertView(title: " Alert", message: "All field are mandatory", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        else {
            emailTextfield.resignFirstResponder()
            passwordTextfield.resignFirstResponder()
            self.getLoginWebService()
        }
        
        return true
    }

    func passwordIsValid(_ password: String) -> Bool {
        
        if password.characters.count < 6 { return false }
        let specialCharacters = "@!#€%&/()[]=?$§*'"
        if password.components(separatedBy: CharacterSet.init(charactersIn: "\(specialCharacters)")).count < 2 { return true }
        if password.components(separatedBy: CharacterSet.init(charactersIn: "0123456789")).count < 2 { return true }
        return false
        
    }

    func getLoginWebService() {

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
