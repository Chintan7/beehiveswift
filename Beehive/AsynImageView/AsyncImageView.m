//
//  SendGiftToOtherViewController.m
//  PlansDujour
//
//  Created by sdnmacmini2 on 20/03/14.
//  Copyright (c) 2014 sdnmacmini4. All rights reserved.
//

#import "AsyncImageView.h"
#import "ImageCacheObject.h"
#import "ImageCache.h"

//
// Key's are URL strings.
// Value's are ImageCacheObject's
//
static ImageCache *imageCache = nil;

@implementation AsyncImageView
@synthesize imageView;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
}




-(void)loadImageFromURL:(NSURL*)url withFrame:(CGRect)frame {
    imageFrame=frame;
    
    if (connection != nil) {
        [connection cancel];
       
        connection = nil;
    }
    if (data != nil) {
        
        data = nil;
    }
    
    if (imageCache == nil) // lazily create image cache
        imageCache = [[ImageCache alloc] initWithMaxSize:2*1024*1024];  // 2 MB Image cache
    
    
    urlString = [[url absoluteString] copy];
    UIImage *cachedImage = [imageCache imageForKey:urlString];
    if (cachedImage != nil) {
        if ([[self subviews] count] > 0) {
            [[[self subviews] objectAtIndex:0] removeFromSuperview];
        }
        imageView = [[UIImageView alloc] initWithImage:cachedImage];
        //imageView.contentMode = UIViewContentModeScaleAspectFit;
        /*imageView.autoresizingMask =
            UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;*/
        
        [self addSubview:imageView];
        imageView.frame = frame;
        
       // [imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
       // [self setNeedsLayout];
       
        return;
    }
    
#define SPINNY_TAG 5555
    
    if(![urlString isEqualToString:@""])
    {
        UIActivityIndicatorView *spinny = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        spinny.tag = SPINNY_TAG;
        spinny.center = self.center;
        [spinny startAnimating];
        [self addSubview:spinny];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy 
                                         timeoutInterval:60.0];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (void)connection:(NSURLConnection *)connection 
    didReceiveData:(NSData *)incrementalData {
    if (data==nil) {
        data = [[NSMutableData alloc] initWithCapacity:2048];
    }
    [data appendData:incrementalData];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)aConnection {
        connection = nil;
    
    UIView *spinny = [self viewWithTag:SPINNY_TAG];
    [spinny removeFromSuperview];
    
    if ([[self subviews] count] > 0) {
        [[[self subviews] objectAtIndex:0] removeFromSuperview];
    }
    
    UIImage *image = [UIImage imageWithData:data];
    
    [imageCache insertImage:image withSize:[data length] forKey:urlString];
    
     imageView = [[UIImageView alloc]
                               initWithImage:image];
   // imageView.contentMode = UIViewContentModeScaleAspectFit;
   /* imageView.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;*/
    [self addSubview:imageView];
    //imageView.frame = self.bounds;
    //imageView.frame = imageFrame;
    //[imageView setNeedsLayout]; // is this necessary if superview gets setNeedsLayout?
    //[self setNeedsLayout];
    imageView.frame = imageFrame;
    NSLog(@"B_Frame=%f %f",imageFrame.size.width, imageFrame.size.height);
    data = nil;
}

@end
