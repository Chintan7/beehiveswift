//
//  SendGiftToOtherViewController.m
//  PlansDujour
//
//  Created by sdnmacmini2 on 20/03/14.
//  Copyright (c) 2014 sdnmacmini4. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface AsyncImageView : UIView {
    NSURLConnection *connection;
    NSMutableData *data;
    NSString *urlString; // key for image cache dictionary
    CGRect imageFrame;
    UIImageView *imageView;
}
@property(nonatomic,strong)UIImageView *imageView;
-(void)loadImageFromURL:(NSURL*)url withFrame:(CGRect)frame;

@end
