//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CreditCardViewController.h
//  Beehive
//
//  Created by sdnmacmini5 on 31/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class CreditCardViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    var cardDetailsDictionary = [AnyHashable: Any]()
    var completeAddressString = ""
    var segmentControllerCardDetails: UISegmentedControl!
    var normalBackgroundImage: UIImage!
    var selectedBackgroundImage: UIImage!
    var topToolbar: UIToolbar!
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var notificationView: NotificationView!
    var isNotificationButtonSelected = false
    @IBOutlet var creditCardContainerView: UIView!
    @IBOutlet var cardScrollView: UIScrollView!
    @IBOutlet var cardTypeButton: UIButton!
    @IBOutlet var cardTypeTxtFld: UITextField!
    @IBOutlet var cardNameTxtFld: UITextField!
    @IBOutlet var cardNumberTxtFld: UITextField!
    @IBOutlet var cardExpiryTxtFld: UITextField!
    @IBOutlet var cardCVVTxtFld: UITextField!
    @IBOutlet var addressContainerView: UIView!
    @IBOutlet var countryButton: UIButton!
    @IBOutlet var addressScrollView: UIScrollView!
    @IBOutlet var firstNameTxtFld: UITextField!
    @IBOutlet var lastNameTxtFld: UITextField!
    @IBOutlet var streetNumberTxtFld: UITextField!
    @IBOutlet var streetTxtFld: UITextField!
    @IBOutlet var appartmentTxtFld: UITextField!
    @IBOutlet var cityTxtFld: UITextField!
    @IBOutlet var countryTxtFld: UITextField!
    @IBOutlet var postaltxtFld: UITextField!
    @IBOutlet var stateTxtFld: UITextField!
    @IBOutlet var detailViewContainerView: UIView!
    @IBOutlet var cardNameLbl: UILabel!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var cardNumberLbl: UILabel!
    @IBOutlet var cardExpiryLbl: UILabel!
    var countryArray = [JSONDictionary]()
    var cardTypeArray = [JSONDictionary]()
    var pickerIdentifier: String? = ""
    var countryType: String? = ""
    var cardType: String? = ""
    var customPickerView: UIPickerView!
    var pickerToolbar: UIToolbar!
    var isPaymentIndicationFlag = false
    var selectedRow: Int! = 0

    @IBAction func cardtypeButtonClick(_ sender: Any) {
        customPickerView.reloadAllComponents()
        //    customPickerView.tag = sender;
        //    [self customPickerView:sender];
        dismissKeyboard()
    }

    @IBAction func countryButtonClick(_ sender: Any) {
        customPickerView.reloadAllComponents()
        dismissKeyboard()
        //    [self customPickerView:sender];
    }

    @IBAction func cardViewContinueBtnClick(_ sender: Any) {
        segmentControllerCardDetails.selectedSegmentIndex = 1
        creditCardContainerView.isHidden = true
        addressContainerView.isHidden = false
        detailViewContainerView.isHidden = true
        dismissKeyboard()
    }

    @IBAction func addressViewContinueBtnClick(_ sender: Any) {
        self.fillDataInDictionary()
        self.addressString()
        //    NSLog(@"Name %@",[cardDetailsDictionary objectForKey:@"Card_Name"]);
        //    NSLog(@"Expiry %@",[cardDetailsDictionary objectForKey:@"Card_Expiry"]);
        //    NSLog(@"ExpiryNumber %@",[cardDetailsDictionary objectForKey:@"Card_Number"]);
        //    NSLog(@"Address %@",completeAddressString);
        addressLbl.text = completeAddressString
        cardNameLbl.text = (cardDetailsDictionary["Card_Name"] as! String)
        cardExpiryLbl.text = (cardDetailsDictionary["Card_Expiry"] as! String)
        var str = cardNumberTxtFld.text!
        if str.characters.count == 19 {
            cardNumberLbl.text = "xxxx xxxx xxxx \(str.substring(from: (str.index((str.startIndex), offsetBy: 15))))"
        }
        segmentControllerCardDetails.selectedSegmentIndex = 2
        creditCardContainerView.isHidden = true
        addressContainerView.isHidden = true
        detailViewContainerView.isHidden = false
        segmentControllerCardDetails.setTitle("Address", forSegmentAt: 1)
    }

    @IBAction func reviewEditContinueBtnClick(_ sender: Any) {
        segmentControllerCardDetails.selectedSegmentIndex = 1
        creditCardContainerView.isHidden = true
        addressContainerView.isHidden = false
        detailViewContainerView.isHidden = true
    }

    @IBAction func cardSubmitBtnClick(_ sender: Any) {
        if cardNameTxtFld.text?.characters.count == 0 || cardNumberTxtFld.text?.characters.count == 0 || cardCVVTxtFld.text?.characters.count == 0 || cardExpiryTxtFld.text?.characters.count == 0 || streetNumberTxtFld.text?.characters.count == 0 || streetTxtFld.text?.characters.count == 0 || appartmentTxtFld.text?.characters.count == 0 || cityTxtFld.text?.characters.count == 0 || stateTxtFld.text?.characters.count == 0 || countryTxtFld.text?.characters.count == 0 || postaltxtFld.text?.characters.count == 0 || firstNameTxtFld.text?.characters.count == 0 || lastNameTxtFld.text?.characters.count == 0 {
            var myAlert = UIAlertView(title: "Warning", message: "Please Provide Complete Detail", delegate: self, cancelButtonTitle: "OK")
            myAlert.show()
            return
        }
        self.storeCreditCard()
        dismissKeyboard()
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.getPayPalRequiredAttributes()
        cardTypeButton.tag = CARD_BUTTON_TAG
        countryButton.tag = COUNTRY_BUTTON_TAG
        cardDetailsDictionary = [AnyHashable: Any]()
        cardDetailsDictionary["Card_Name"] = ""
        cardDetailsDictionary["Card_Number"] = ""
        cardDetailsDictionary["Card_CVV"] = ""
        cardDetailsDictionary["Card_Expiry"] = ""
        cardDetailsDictionary["Street_No"] = ""
        cardDetailsDictionary["Street_Name"] = ""
        cardDetailsDictionary["Address_Apt"] = ""
        cardDetailsDictionary["Address_City"] = ""
        cardDetailsDictionary["Address_State"] = ""
        cardDetailsDictionary["Address_Country"] = ""
        cardDetailsDictionary["Address_Postal"] = ""
        cardDetailsDictionary["Address_Apartment"] = ""
        self.viewCustomizaton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        cardNameTxtFld.text = (cardDetailsDictionary["Card_Name"] as! String)
        cardNumberTxtFld.text = (cardDetailsDictionary["Card_Number"] as! String)
        cardCVVTxtFld.text = (cardDetailsDictionary["Card_CVV"] as! String)
        cardExpiryTxtFld.text = (cardDetailsDictionary["Card_Expiry"] as! String)
        streetNumberTxtFld.text = (cardDetailsDictionary["Street_No"] as! String)
        appartmentTxtFld.text = (cardDetailsDictionary["Address_Apartment"] as! String)
        streetTxtFld.text = (cardDetailsDictionary["Street_Name"] as! String)
        cityTxtFld.text = (cardDetailsDictionary["Address_City"] as! String)
        stateTxtFld.text = (cardDetailsDictionary["Address_State"] as! String)
        countryTxtFld.text = (cardDetailsDictionary["Address_Country"] as! String)
        postaltxtFld.text = (cardDetailsDictionary["Address_Postal"] as! String)
        self.addressString()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

    func viewCustomizaton() {
        creditCardContainerView.isHidden = false
        addressContainerView.isHidden = true
        detailViewContainerView.isHidden = true
        if screenSize.height != 568 {
            cardScrollView.contentSize = CGSize(width: CGFloat(320), height: CGFloat(400))
            addressScrollView.contentSize = CGSize(width: CGFloat(320), height: CGFloat(640))
        }
        else {
            cardScrollView.contentSize = CGSize(width: CGFloat(320), height: CGFloat(400))
            addressScrollView.contentSize = CGSize(width: CGFloat(320), height: CGFloat(590))
        }
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //    Custom Segment Controller
        let items = ["Credit Card", "Address", "Review & Submit"]
        segmentControllerCardDetails = UISegmentedControl(items: items)
        segmentControllerCardDetails.frame = CGRect(x: CGFloat(20), y: CGFloat(50), width: CGFloat(280), height: CGFloat(35))
        segmentControllerCardDetails.selectedSegmentIndex = 0
        segmentControllerCardDetails.addTarget(self, action: #selector(self.segmentedControlSelection), for: .valueChanged)
        segmentControllerCardDetails.layer.borderWidth = 1.0
        segmentControllerCardDetails.layer.borderColor = UIColor.darkGray.cgColor
        segmentControllerCardDetails.layer.cornerRadius = 5.0
        segmentControllerCardDetails.tintColor = UIColor(red: CGFloat(211 / 255.0), green: CGFloat(123 / 255.0), blue: CGFloat(38 / 255.0), alpha: CGFloat(1.0))
        segmentControllerCardDetails.layer.masksToBounds = true
        self.view.addSubview(segmentControllerCardDetails)
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.darkGray, NSFontAttributeName: CenturyGothicBold10], for: .normal)
        //    Top Toolbar Creation
        topToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        if isPaymentIndicationFlag {
            topToolbar.setBackgroundImage(UIImage(named: "editCardToolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        }
        else {
            topToolbar.setBackgroundImage(UIImage(named: "addCardToolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        }
        self.view.addSubview(topToolbar)
        UITextField.appearance().font = CenturyGothiCRegular15
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
        cardNameLbl.font = CenturyGothiCRegular15
        addressLbl.font = CenturyGothiCRegular15
        cardExpiryLbl.font = CenturyGothiCRegular15
        cardNumberLbl.font = CenturyGothiCRegular15
        customPickerView = UIPickerView()
        customPickerView.tag = 1
        if screenSize.height != 568 {
            customPickerView = UIPickerView.init()
            pickerToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(220), width: CGFloat(320), height: CGFloat(40)))
        }
        else {
            customPickerView = UIPickerView.init()
            pickerToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(308), width: CGFloat(320), height: CGFloat(40)))
        }
        pickerToolbar.backgroundColor = UIColor.white
        //    [pickerToolbar setBackgroundColor:[UIColor colorWithRed:211/255.0f green:123/255.0f blue:38/255.0f alpha:1.0]];
        //    [pickerToolbar set];
        pickerToolbar.tintColor = UIColor(red: CGFloat(211 / 255.0), green: CGFloat(123 / 255.0), blue: CGFloat(38 / 255.0), alpha: CGFloat(1.0))
        customPickerView.backgroundColor = UIColor.white
        customPickerView.delegate = self
        customPickerView.dataSource = self
        
        let button1 = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneSelection))
        let flex = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let button2 = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.cancelSelection))
        pickerToolbar.items = [button2, flex, button1]
        
        let picker = UIPickerView.init()
        picker.delegate = self
        picker.dataSource = self
        picker.tag = 2
        countryTxtFld.inputView = customPickerView
        countryTxtFld.inputAccessoryView = pickerToolbar
        cardTypeTxtFld.inputView = customPickerView
        cardTypeTxtFld.inputAccessoryView = pickerToolbar
        cardTypeTxtFld.leftView = UIView.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(5), height: CGFloat(20)))
        cardTypeTxtFld.leftViewMode = .always
        countryTxtFld.leftView = UIView.init(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(5), height: CGFloat(20)))
        countryTxtFld.leftViewMode = .always
    }

    func dismissKeyboard() {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        


//        addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
//        cardScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(0)), animated: true)
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in
        })
        dismissKeyboard()
    }
// MARK: - Text Field Delegates & Datasource

    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if screenSize.height != 568 {
//            if textField == cardNumberTxtFld {
//                cardScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(65)), animated: true)
//            }
//            else if textField == cardCVVTxtFld {
//                cardScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(120)), animated: true)
//            }
//            else if textField == cardExpiryTxtFld {
//                cardScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(180)), animated: true)
//            }
//            else if textField == appartmentTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(65)), animated: true)
//            }
//            else if textField == cityTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(120)), animated: true)
//            }
//            else if textField == stateTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(175)), animated: true)
//            }
//            else if textField == postaltxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(235)), animated: true)
//            }
//            else if textField == countryTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(290)), animated: true)
//            }
//        }
//        else {
//            if textField == cardCVVTxtFld {
//                cardScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(35)), animated: true)
//            }
//            else if textField == cardExpiryTxtFld {
//                cardScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(90)), animated: true)
//            }
//            else if textField == cityTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(30)), animated: true)
//            }
//            else if textField == stateTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(90)), animated: true)
//            }
//            else if textField == postaltxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(150)), animated: true)
//            }
//            else if textField == countryTxtFld {
//                addressScrollView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(210)), animated: true)
//            }
//        }
        if textField == countryTxtFld {
            pickerIdentifier = "Country"
            customPickerView.reloadAllComponents()
        }
        else if textField == cardTypeTxtFld {
            pickerIdentifier = "Card"
            customPickerView.reloadAllComponents()
        }

    }




    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {


        if textField == cardNumberTxtFld {
            // Only the 16 digits + 3 spaces
            if range.location == 19 {
                return false
            }
            // Backspace
            if string.characters.count == 0 {
                return true
            }

            if (range.location == 4) || (range.location == 9) || (range.location == 14) {
                let str = "\(textField.text!) "
                cardNumberTxtFld.text = str
            }
            return true
        }
        else if textField == cardCVVTxtFld {
            let str = cardCVVTxtFld.text as NSString?
            var newString = str!.replacingCharacters(in: range, with: string)
            return !(newString.characters.count > 3)
        }
        else if textField == postaltxtFld {
            let str = postaltxtFld.text as NSString?
            var newString = str?.replacingCharacters(in: range, with: string)
            return !(newString!.characters.count > 6)
//            var newString = postaltxtFld.text?.replacingCharacters(in: range, with: string)
//            return !(newString.characters.count > 6)
//            return true
        }
        else if textField == cardExpiryTxtFld {
            // Only the 16 digits + 3 spaces
            if range.location == 7 {
                return false
            }
            // Backspace
            if string.characters.count == 0 {
                return true
            }
            if range.location == 2 {
                let i = Int("\(cardExpiryTxtFld.text!)")
                if i! > 12 {
                    return false
                }
                else {
                    let str = "\(textField.text!)/"
                    cardExpiryTxtFld.text = str
                }
            }
            return true
        }

        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        dismissKeyboard()
        return true
    }
// MARK: - PickerView Allocation, Delegate & Data Source

    func customPickerView(_ sender: Any) {
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        if (pickerIdentifier == "Country") {
            return countryArray.count
        }
        else if (pickerIdentifier == "Card") {
            return cardTypeArray.count
        }
        else {
            return 0
        }

    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerIdentifier == "Country") {
            return countryArray[row]["value"] as! String?
        }
        else if (pickerIdentifier == "Card") {
            return cardTypeArray[row]["value"] as! String?
        }
        else {
            return nil
        }

    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
    }

    func doneSelection() {
        if (pickerIdentifier == "Country") {
            if let str = countryArray[selectedRow]["value"] as? String {
                countryTxtFld.text = str
            }

            print(selectedRow)
            print(countryArray[selectedRow])

            if let ids = countryArray[selectedRow]["id"] as? NSNumber {
                countryType = "\(ids)"
            }


        }else if (pickerIdentifier == "Card") {
            
            
            if let str =  cardTypeArray[selectedRow]["value"] as? String {
                cardTypeTxtFld.text = str
                if let ids = cardTypeArray[selectedRow]["id"] as? String {
                    cardType = "\(ids)"
                }
            }
//            cardType = cardTypeArray[selectedRow]["id"] as! String?
        }
        dismissKeyboard()
    }

    func cancelSelection() {
        dismissKeyboard()
    }
// MARK: -  Button Events

    func segmentedControlSelection(_ sender: Any) {
        self.dismissKeyboard()
        
        if segmentControllerCardDetails.selectedSegmentIndex == 0 {
            creditCardContainerView.isHidden = false
            addressContainerView.isHidden = true
            detailViewContainerView.isHidden = true
            pickerToolbar.isHidden = true
        }
        else if segmentControllerCardDetails.selectedSegmentIndex == 1 {
            creditCardContainerView.isHidden = true
            addressContainerView.isHidden = false
            detailViewContainerView.isHidden = true
        }
        else {
            self.fillDataInDictionary()
            self.addressString()
            addressLbl.text = completeAddressString
            cardNameLbl.text = (cardDetailsDictionary["Card_Name"] as! String)
            cardExpiryLbl.text = (cardDetailsDictionary["Card_Expiry"] as! String)
            var str = cardNumberTxtFld.text
            if str?.characters.count == 19 {
                cardNumberLbl.text = "xxxx xxxx xxxx \(str?.substring(from: (str?.index((str?.startIndex)!, offsetBy: 15))!))"
            }
            creditCardContainerView.isHidden = true
            addressContainerView.isHidden = true
            detailViewContainerView.isHidden = false
        }

    }

    func fillDataInDictionary() {
        cardDetailsDictionary["First_Name"] = firstNameTxtFld.text
        cardDetailsDictionary["Last_Name"] = lastNameTxtFld.text
        cardDetailsDictionary["Card_Name"] = cardNameTxtFld.text
        cardDetailsDictionary["Card_Number"] = cardNumberTxtFld.text
        cardDetailsDictionary["Card_CVV"] = cardCVVTxtFld.text
        cardDetailsDictionary["Card_Expiry"] = cardExpiryTxtFld.text
        cardDetailsDictionary["Street_No"] = streetNumberTxtFld.text
        cardDetailsDictionary["Street_Name"] = streetTxtFld.text
        cardDetailsDictionary["Address_Apt"] = appartmentTxtFld.text
        cardDetailsDictionary["Address_City"] = cityTxtFld.text
        cardDetailsDictionary["Address_State"] = stateTxtFld.text
        cardDetailsDictionary["Address_Country"] = countryTxtFld.text
        cardDetailsDictionary["Address_Postal"] = postaltxtFld.text
        cardDetailsDictionary["Address_Apartment"] = appartmentTxtFld.text
    }

    func addressString() {
        var arr = [Any]()
        var streetNo: String
        if !((cardDetailsDictionary["Street_No"] as! String) == "") {
            streetNo = (cardDetailsDictionary["Street_No"] as! String)
            arr.append(streetNo)
        }
        else {
            streetNo = ""
            //        [arr addObject:streetNo];
        }
        var street: String
        if !((cardDetailsDictionary["Street_Name"] as! String) == "") {
            street = "\(cardDetailsDictionary["Street_Name"] as! String)"
            arr.append(street)
        }
        else {
            street = ""
            //        [arr addObject:street];
        }
        var apartment: String
        if !((cardDetailsDictionary["Address_Apt"] as! String) == "") {
            apartment = "\(cardDetailsDictionary["Address_Apt"] as! String)"
            arr.append(apartment)
        }
        else {
            apartment = ""
            //        [arr addObject:apartment];
        }
        var city: String
        if !((cardDetailsDictionary["Address_City"] as! String) == "") {
            city = (cardDetailsDictionary["Address_City"] as! String)
            arr.append(city)
        }
        else {
            city = ""
            //        [arr addObject:city];
        }
        var state: String
        if !((cardDetailsDictionary["Address_State"] as! String) == "") {
            state = "\(cardDetailsDictionary["Address_State"] as! String)"
            // state = @"California";
            arr.append(state)
        }
        else {
            state = ""
        }
        var country: String
        if !((cardDetailsDictionary["Address_Country"] as! String) == "") {
            country = (cardDetailsDictionary["Address_Country"] as! String)
            arr.append(country)
        }
        else {
            country = ""
            //        [arr addObject:country];
        }
        var pincode: String
        if !((cardDetailsDictionary["Address_Postal"] as! String) == "") {
            pincode = "\(cardDetailsDictionary["Address_Postal"] as! String)"
            arr.append(pincode)
        }
        print("Array Data : \(arr)")
        completeAddressString = String()
        for i in 0..<arr.count {
            if i == 1 || i == 2 || i == 4 || i == 6 {
                completeAddressString += ","
            }
            else if i == 3 || i == 5 {
                completeAddressString += "\n"
            }

            completeAddressString += "\(arr[i])"
        }
        //    NSLog(@"Adrees String ....= %@",completeAddressString);
    }
    /*
    #pragma mark - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
// MARK: - Web Service Calling & Delagates

    func storeCreditCard() {
        APPDELEGATE.addChargementLoader()
        var urlString = "\(BaseWebRequestUrl)\(StoreCreditCard)"
        urlString = urlString.replacingOccurrences(of: " ", with: "")
        _ = URL(string: urlString)!


        var cardDetailDict: JSONDictionary = [:]
        //        request.setPostValue(cardType, forKey: "type")


        cardDetailDict["type"] = cardType! as AnyObject?
        var cardNumber = cardNumberTxtFld.text!
        cardNumber = cardNumber.replacingOccurrences(of: " ", with: "")
        //        request.setPostValue(cardNumber, forKey: "number")
        cardDetailDict["number"] = cardNumber as AnyObject?
        let str = cardExpiryTxtFld.text!

        if str != "" {
            let expiryMonth = str.substring(to: str.index(str.startIndex, offsetBy: 2))
            let expiryYear = str.substring(from: str.index(str.startIndex, offsetBy: 3))
            //        request.setPostValue(expiryMonth, forKey: "expire_month")
            cardDetailDict["expire_month"] = expiryMonth as AnyObject?
            //        request.setPostValue(expiryYear, forKey: "expire_year")
            cardDetailDict["expire_year"] = expiryYear as AnyObject?
            //        request.setPostValue(cardCVVTxtFld.text, forKey: "ccv")
        }


        cardDetailDict["ccv"] = cardCVVTxtFld.text! as AnyObject?
        //        request.setPostValue(firstNameTxtFld.text, forKey: "first_name")
        cardDetailDict["first_name"] = firstNameTxtFld.text! as AnyObject?
        //        request.setPostValue(lastNameTxtFld.text, forKey: "last_name")
        cardDetailDict["last_name"] = lastNameTxtFld.text! as AnyObject?
        //        request.setPostValue(streetNumberTxtFld.text, forKey: "street_no")
        cardDetailDict["street_no"] = streetNumberTxtFld.text! as AnyObject?
        //        request.setPostValue(streetTxtFld.text, forKey: "street")
        cardDetailDict["street"] = streetTxtFld.text! as AnyObject?
        //        request.setPostValue(appartmentTxtFld.text, forKey: "apt")
        cardDetailDict["apt"] = appartmentTxtFld.text! as AnyObject?
        //        request.setPostValue(cityTxtFld.text, forKey: "city")
        cardDetailDict["city"] = cityTxtFld.text! as AnyObject?
        //        request.setPostValue(postaltxtFld.text, forKey: "postal")
        cardDetailDict["postal"] = postaltxtFld.text! as AnyObject?
        //        request.setPostValue(stateTxtFld.text, forKey: "state")
        cardDetailDict["state"] = stateTxtFld.text! as AnyObject?
        //        request.setPostValue(countryType, forKey: "country")
        cardDetailDict["country"] = countryType as AnyObject?


        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.storeCreditCard(cardDict: cardDetailDict) { (result:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {
                if self.isPaymentIndicationFlag {
                    let accountScreen = STORYBOARD.instantiateViewController(withIdentifier: "AccountsViewController")
                    self.navigationController!.pushViewController(accountScreen, animated: true)
                }
                else {
                    let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
                    self.navigationController!.pushViewController(homeScreen, animated: true)
                }

            } else {

            }

        }

//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        var str = cardExpiryTxtFld.text
//        var expiryMonth = str.substring(to: str.index(str.startIndex, offsetBy: 2))
//        var expiryYear = str.substring(from: str.index(str.startIndex, offsetBy: 3))
//        var cardNumber = cardNumberTxtFld.text
//        cardNumber = cardNumber.replacingOccurrencesOf(" ", withString: "")
//        request.setPostValue(cardType, forKey: "type")
//        request.setPostValue(cardNumber, forKey: "number")
//        request.setPostValue(expiryMonth, forKey: "expire_month")
//        request.setPostValue(expiryYear, forKey: "expire_year")
//        request.setPostValue(cardCVVTxtFld.text, forKey: "ccv")
//        request.setPostValue(firstNameTxtFld.text, forKey: "first_name")
//        request.setPostValue(lastNameTxtFld.text, forKey: "last_name")
//        request.setPostValue(streetNumberTxtFld.text, forKey: "street_no")
//        request.setPostValue(streetTxtFld.text, forKey: "street")
//        request.setPostValue(appartmentTxtFld.text, forKey: "apt")
//        request.setPostValue(cityTxtFld.text, forKey: "city")
//        request.setPostValue(postaltxtFld.text, forKey: "postal")
//        request.setPostValue(stateTxtFld.text, forKey: "state")
//        request.setPostValue(countryType, forKey: "country")
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

    func getPayPalRequiredAttributes() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(GetPayPalAttributes)"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        _ = URL(string: strURL)!

        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.getPayPalRequiredAttributes { (result:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            if error == nil {

                if let dict = result?["data"] as? JSONDictionary {
                    if let contryArr = dict["countries"] as? [JSONDictionary] {
                        self.countryArray = contryArr
                    }
                    if let contryArr = dict["cc_type"] as? [JSONDictionary] {
                        self.cardTypeArray = contryArr
                    }
                }

            }

        }


            //    NSLog(@"Get Project URL- %@",url);
//        var request = ASIFormDataRequest(URL: url)
//        var pref = UserDefaults.standard
//        var emailString = pref.object(forKey: "UserId")!
//        var userTokenString = pref.object(forKey: "User_Token")!
//        var authStr = "\(emailString):\(userTokenString)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 2
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            print("Return Card Detail Response : \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                if isPaymentIndicationFlag {
//                    var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                    var accountScreen = sb.instantiateViewController(withIdentifier: "AccountsViewController")
//                    self.navigationController!.pushViewController(accountScreen, animated: true)
//                }
//                else {
//                    var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                    var homeScreen = sb.instantiateViewController(withIdentifier: "HomeScreenController")
//                    self.navigationController!.pushViewController(homeScreen, animated: true)
//                }
//            }
//            else {
//                var errorAlert = UIAlertView(title: "Error", message: ((returnDict["result"] as! String)["message"] as! String), delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//                errorAlert.show()
//            }
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            //        NSLog(@"Return PayPal Attributes Response : %@",returnDict);
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                countryArray = [Any]()
//                countryArray = (((returnDict["result"] as! String)["data"] as! String)["countries"] as! String)
//                cardTypeArray = [Any]()
//                cardTypeArray = (((returnDict["result"] as! String)["data"] as! String)["cc_type"] as! String)
//            }
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        print("Error : \(.error)")
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
//        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

//    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(appDelegate.allNotificationArray[indexPath.row])")
//        if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            appDelegate.notificationReadWebService((appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            appDelegate.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            appDelegate.navigate(toProfileView: self)
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        //    else if ([[[appDelegate.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
//    }


}

let CARD_BUTTON_TAG = 1000
let COUNTRY_BUTTON_TAG = 1001
