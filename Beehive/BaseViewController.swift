//
//  BaseViewController.swift
//  Beehive
//
//  Created by SoluLab on 01/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let imgView = UIImageView.init(image: #imageLiteral(resourceName: "GreyBg"))
        imgView.frame = UIScreen.main.bounds
        self.view.addSubview(imgView)
        self.view.insertSubview(imgView, at: 0)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func sideMenuBar() {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({() -> Void in })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
