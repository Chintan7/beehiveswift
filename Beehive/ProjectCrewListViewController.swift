//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  ProjectCrewListViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 30/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class ProjectCrewListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, CrewCustomCellDelegate, NotificationViewPrtotcol {
    var screenSize = CGSize.zero
    var crewListTable: UITableView!
    var crewSearchBar: UISearchBar!
    var searchView: UIView!
    var allCrewArray = [JSONDictionary]()
    var searchCrewsArray = [JSONDictionary]()
    var notificationBarButton: UIBarButtonItem!
    var notificationButton: UIButton!
    var isNotificationButtonSelected = false
    var notificationView: NotificationView!

    var projectID = ""
    var projectCrewArray = [JSONDictionary]()



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //    NSLog(@"Crew  List : %@",projectCrewArray);
        //    NSString *path = [[NSBundle mainBundle] pathForResource:@"TempDataList" ofType:@"plist"];
        //    allCrewArray = [[[NSDictionary alloc] initWithContentsOfFile:path] objectForKey:@"Crew"];
        //    NSLog(@"All Crew : %@",allCrewArray);
        //    searchCrewsArray = allCrewArray;
        //    searchCrewsArray = [[NSMutableArray alloc] init];
        //    searchCrewsArray = projectCrew;
        self.customisedView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.showNotificationView), name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
        crewListTable.reloadData()
    }

    func customisedView() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
        notificationButton = UIButton(type: .custom)
        notificationButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(30), height: CGFloat(30))
        notificationButton.backgroundColor = UIColor.red
        notificationButton.setTitle(APPDELEGATE.unreadNotificationCount, for: .normal)
        notificationButton.titleLabel!.font = CenturyGothicBold15
        notificationButton.addTarget(self, action: #selector(self.notificationButtonClick), for: .touchUpInside)
        notificationButton.layer.cornerRadius = 15.0
        notificationBarButton = UIBarButtonItem(customView: notificationButton)
        self.navigationItem.rightBarButtonItem = notificationBarButton
            //    Toolbar
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let button = UIButton(type: .custom)
        button.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        button.setImage(image, for: .normal)
        button.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let backBtn = UIBarButtonItem(customView: button)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let addCrewBtnImage = UIImage(named: "addBtn.png")!
        let addCrewButton = UIButton(type: .custom)
        addCrewButton.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(60), height: CGFloat(25))
        addCrewButton.setImage(addCrewBtnImage, for: .normal)
        addCrewButton.addTarget(self, action: #selector(self.addCrewButtonClick), for: .touchUpInside)
        let prefBarButton = UIBarButtonItem(customView: addCrewButton)
        let titleLabel = UILabel(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(80), height: CGFloat(44)))
        titleLabel.text = "Crew"
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        titleLabel.font = CenturyGothicBold15
        let toolbarTitleLbl = UIBarButtonItem(customView: titleLabel)
        let flexSpace1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        var barItems = [UIBarButtonItem]()
        barItems.append(backBtn)
        barItems.append(flexSpace)
        barItems.append(toolbarTitleLbl)
        barItems.append(flexSpace1)
        barItems.append(prefBarButton)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        screenSize = UIScreen.main.bounds.size
        if screenSize.height == 568 {
            crewListTable = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(460)))
        }
        else {
            crewListTable = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(44), width: CGFloat(320), height: CGFloat(372)))
        }
        crewListTable.dataSource = self
        crewListTable.delegate = self
        self.view.addSubview(crewListTable)
        /*
            searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 44, 320, 50)];
            searchView.layer.shadowColor = [UIColor grayColor].CGColor;
            searchView.layer.shadowOffset = CGSizeMake(2, 2);
            searchView.layer.shadowOpacity = 1;
            searchView.layer.shadowRadius = 2.0;
            searchView.clipsToBounds = NO;
             */
    }


    func backBtnClick() {
        popTo()
    }

    func addCrewButtonClick() {

        let addCrewView = STORYBOARD.instantiateViewController(withIdentifier: "AddCrewListViewController") as! AddCrewListViewController
        addCrewView.projectId = projectID
        self.navigationController!.pushViewController(addCrewView, animated: true)
    }
// MARK: Table Datasource & Delegate

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //    return [searchCrewsArray count];
        return projectCrewArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "MyCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier) as? CrewTableCell
        
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("CrewTableCell", owner: self, options: nil)
            cell = nib![0] as? CrewTableCell
        }
        cell?.statusImage.layer.cornerRadius = (cell?.statusImage.frame.size.height)! / 2
        cell?.statusImage.layer.masksToBounds = true
        cell?.delegate = self
        //    cell.nameLabel.font = [UIFont fontWithName:@"Century Gothic" size:18.0f];

        var url = URL.init(string: "")

        if let user = projectCrewArray[indexPath.row]["User"] as? JSONDictionary {

            cell?.nameLabel.text = user["first_name"] as! String?
            let urlTemp = ImageBaseUrl + "\(user["user_avatar"]!)"
            url = URL(string: "\(urlTemp)")
        }
        
        var statusStr = ""
        if let user = projectCrewArray[indexPath.row]["CheckinCheckoutTracker"] as? JSONDictionary {
            statusStr = user["status"] as! String
        }

//        cell?.nameLabel.text = ((projectCrewArray[indexPath.row]["User"] as! String)["first_name"] as! String)
        cell?.nameLabel.font = UIFont(name: "Gothic725 Bd BT", size: CGFloat(14.0))!
//        var statusStr = ((projectCrewArray[indexPath.row]["CheckinCheckoutTracker"] as! String)["status"] as! String)
//        var url = URL(string: "\(ImageBaseUrl)\((projectCrewArray[indexPath.row].value(forKey: "User") as! String).value(forKey: "user_avatar") as! String)")!

        if (statusStr == "checked in") {
            cell?.statusImage.backgroundColor = UIColor.green
        }
        else if (statusStr == "checked out") {
            cell?.statusImage.backgroundColor = UIColor.red
        }
        else {
            cell?.statusImage.backgroundColor = UIColor.gray
        }


        var imageView = (cell?.viewWithTag(100)! as! UIImageView)
        imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
//        imageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
        cell?.selectionStyle = .none
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }


    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {


        //    NSLog(@"Commit Editing");
        if editingStyle == .delete {
            print("Selected Index Crew : \(projectCrewArray[indexPath.row])")
            //        [_objects removeObjectAtIndex:indexPath.row];
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        else {
            print("Unhandled editing style! \(editingStyle)")
        }
    }



    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Remove"
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        /*
            CGFloat contentYoffset = scrollView.contentOffset.y;
            
            if(contentYoffset < -55)
            {
                if (screenSize.height == 568) {
                    [crewListTable setFrame:CGRectMake(0, 88, 320, 410)];
                }else{
                    [crewListTable setFrame:CGRectMake(0, 88, 320, 319)];
                }
                [self.view addSubview:searchView];
                
                crewSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
                [crewSearchBar setBackgroundColor:[UIColor clearColor]];
                crewSearchBar.delegate = self;
                [searchView addSubview:crewSearchBar];
            }
            else if(contentYoffset>50)
            {
                if (screenSize.height == 568) {
                    [crewListTable setFrame:CGRectMake(0, 44, 320, 460)];
                }else{
                    [crewListTable setFrame:CGRectMake(0, 44, 320, 372)];
                }
                [searchView removeFromSuperview];
            }
             */
    }
// MARK: Custom Cell Delegates

    func contactButtonTappedOnCell(_ sender: Any) {
        let callButton = (sender as! UIButton)
        let buttonFrame = callButton.convert(callButton.bounds, to: crewListTable)
        _ = crewListTable.indexPathForRow(at: buttonFrame.origin)!
        //    NSLog(@"sectionIndexNO is%d rowIndexNO is %d",indexPath.section,indexPath.row);
//        if callButton.tag == 1 {
//            if (projectCrewArray[indexPath.row]["user_contact_no_work"] as! String) == nil {
//                var alert = UIAlertView(title: "Sorry", message: "No Contact Number Found", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
//                alert.show()
//            }
//            else {
//                UIApplication.shared.openURL(URL(string: "telprompt://\(projectCrewArray[indexPath.row]["user_contact_no_work"] as! String)")!)
//            }
//        }
    }

    func crewDetailButtonTappedOnCell(_ sender: Any) {
        let callButton = (sender as! UIButton)
        let buttonFrame = callButton.convert(callButton.bounds, to: crewListTable)
        let indexPath = crewListTable.indexPathForRow(at: buttonFrame.origin)!
        print("Index Path  = \(indexPath)")

        let newView = STORYBOARD.instantiateViewController(withIdentifier: "CrewProfileViewController") as? CrewProfileViewController
        //    NSLog(@"Data = %@",[[projectCrewArray objectAtIndex:indexPath.row] valueForKey:@"User"]);

        var dict: JSONDictionary = [:]

        dict["User"] = projectCrewArray[indexPath.row]["User"]
        dict["CheckinCheckoutTracker"] = projectCrewArray[indexPath.row]["CheckinCheckoutTracker"]

        newView?.crewDetailDict = dict
//        newView.crewDetailDict["User"] = (projectCrewArray[indexPath.row].value(forKey: "User") as! String)
//        newView.crewDetailDict["CheckinCheckoutTracker"] = (projectCrewArray[indexPath.row].value(forKey: "CheckinCheckoutTracker") as! String)
        newView?.indexPathOfCrew = indexPath
        newView?.isComeFromCrewList = true
        newView?.projectId = projectID
        newView?.parentController = self
        self.navigationController!.pushViewController(newView!, animated: true)
    }
    /*
    #pragma mark - SearchBar Delegates
    -(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
    {
        NSString *dictionaryKey = [[projectCrewArray valueForKey:@"User"] valueForKey:@"first_name"];
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"K contains[c] %@",dictionaryKey,searchText];
        searchCrewsArray = (NSMutableArray *)[projectCrewArray filteredArrayUsingPredicate:pred];
        
        [crewListTable reloadData];
        
    }
    
    -(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
    {
        [searchBar setShowsCancelButton:YES animated: YES];
    }
    
    - (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
    {
        [crewSearchBar resignFirstResponder];
        [crewListTable reloadData];
    }
    
    - (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
    {
        if ([searchBar.text isEqualToString:@""])
        {
            if ([searchCrewsArray count] == 0)
            {
                searchCrewsArray = projectCrewArray ;
            }
        }
        [crewListTable reloadData];
        [searchBar resignFirstResponder];
        [searchBar setShowsCancelButton:NO animated: YES];
    }
     */
// MARK: - NotificationViewPrtotcol Methods.

    func notificationButtonClick() {
        if isNotificationButtonSelected == false {
            APPDELEGATE.getAllNotificationData()
        }
        else {
            if ((notificationView) != nil) {
                notificationView.removeFromSuperview()
            }
            isNotificationButtonSelected = false
        }
    }

    func showNotificationView(_ note: Notification) {
        notificationView = NotificationView()
        notificationView.customView()
        if screenSize.height == 568 {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            notificationView.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
        notificationView.delegate = self
        self.view.addSubview(notificationView)
        isNotificationButtonSelected = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetAllNotificationData"), object: nil)
    }

    func didselectRow(_ tableView: UITableView, indexPath: IndexPath) {
//        print("SelectedRow Data :\(appDelegate.allNotificationArray[indexPath.row])")
//        if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "InvitationViewController")
//            invitaionView.notificationId = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            invitaionView.notificationMessage = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "message") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_rejected") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "invitation_accepted") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in_declined") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_in") || ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "checked_out") {
//            appDelegate.notificationReadWebService((appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String))
//            appDelegate.allNotificationArray.remove(at: indexPath.row)
//            notificationView.notificationTable.reloadData()
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "user_profile") {
//            appDelegate.navigate(toProfileView: self)
//        }
//        else if ((appDelegate.allNotificationArray[indexPath.row].value(forKey: "type") as! String) == "tentative_checked_in") {
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var invitaionView = sb.instantiateViewController(withIdentifier: "TentativeCheckInViewController")
//            invitaionView.notificationId = (appDelegate.allNotificationArray[indexPath.row].value(forKey: "id") as! String)
//            self.navigationController!.pushViewController(invitaionView, animated: true)
//        }
//
//        //    else if ([[[appDelegate.allNotificationArray objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"checked_out"])
//        //    {
//        //        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
//        //        CheckoutViewController *newView = [sb instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
//        //        [self.navigationController pushViewController:newView animated:YES];
//        //    }
//        tableView.deselectRow(at: indexPath, animated: true)
    }
}
