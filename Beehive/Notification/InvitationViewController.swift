//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  InvitationViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 08/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit


class InvitationViewController: UIViewController, GMSMapViewDelegate, UIAlertViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var navigationView: UIView!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var invitationText: UILabel!
    @IBOutlet var projectNameLabel: UILabel!
    @IBOutlet var projectLocationLabel: UILabel!
    @IBOutlet var markerAddressLabel: UILabel!
    @IBOutlet var markerImageView: UIImageView!
    @IBOutlet var acceptProjectButton: UIButton!
    @IBOutlet var declineButton: UIButton!
    @IBOutlet var phoneCallButton: UIButton!
    @IBOutlet var detailDisclosourButton: UIButton!
    var returnDataDict = JSONDictionary()
    var returnDataDictUser = JSONDictionary()
    var projectDataDict = JSONDictionary()
    var projectLocationMapView: GMSMapView!
    var checkInProjectId: String!
    var notificationId: String!
    var notificationMessage: String!

    var didStartMonitoringRegion = false
    var location: CLLocation!
    var dataDict = NSMutableDictionary()


    //@property (strong, nonatomic) CLLocationManager *locationManager;
    var geofences = [Any]()


    @IBAction func backButtonClick(_ sender: Any) {
        popTo()
    }

    @IBAction func phoneCallButtonClick(_ sender: Any) {
        var strURL = String()
        let phoneNum = (returnDataDictUser["user_mobile"] as! String)
        if (phoneNum == "") {
            self.showAlert(withMessage: "No Contact Number Found")
        }
        else {
            strURL = "telprompt://\(phoneNum)"
        }

        let doNotWant = CharacterSet.init(charactersIn: "()- ")


        strURL = (strURL.components(separatedBy: doNotWant) as NSArray).componentsJoined(by: "")
        UIApplication.shared.openURL(URL(string: strURL)!)
    }

    @IBAction func detailButtonClick(_ sender: Any) {
        print("Detail Button Click ")
//        var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//        var newView = sb.instantiateViewController(withIdentifier: "CrewProfileViewController")
//        newView.crewDetailDict = returnDataDict
//        newView.cantRemove = true
//        self.navigationController!.pushViewController(newView, animated: true)
    }

    @IBAction func acceptButtonClick(_ sender: Any) {
        self.acceptProjectWebService()
    }

    @IBAction func declineButtonClick(_ sender: Any) {
        self.declineProjectWebService()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.fetchNotificationData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func customizeView() {
        profileImageView.alpha = 1.0
        nameLabel.alpha = 1.0
        invitationText.alpha = 1.0
        projectNameLabel.alpha = 1.0
        projectLocationLabel.alpha = 1.0
        markerAddressLabel.alpha = 1.0
        acceptProjectButton.alpha = 1.0
        declineButton.alpha = 1.0
        detailDisclosourButton.alpha = 1.0
        phoneCallButton.alpha = 1.0
        markerImageView.alpha = 1.0
//        navigationView.backgroundColor = UIColor(patternImage: UIImage(named: "greytoolbar.png"))
        //    GMSCameraPosition *cameraPositon = [GMSCameraPosition cameraWithLatitude:43.680250 longitude:-79.630832 zoom:12.0f] ;
        projectLocationMapView = GMSMapView(frame: CGRect(x: CGFloat(20), y: CGFloat(167), width: CGFloat(280), height: CGFloat(82)))


//        var cameraPositon = GMSCameraPosition.camera(withLatitude: ((returnDataDict["Project"] as AnyObject)["project_lat"] as! String).floatValue, longitude: ((returnDataDict["Project"] as AnyObject)["project_lng"] as! String).floatValue, zoom: 10.0)

//        let dict = returnDataDict.object(forKey: "Project") as? NSDictionary

        let lati = Double(returnDataDict["project_lat"] as! String)
        let logi = Double(returnDataDict["project_lat"] as! String)


        
        let cameraPositon = GMSCameraPosition.camera(withLatitude: lati!, longitude: logi!, zoom: 10.0)

        projectLocationMapView.camera = cameraPositon
        //    NSLog(@"Map Lat : %f, Map Long : %f",location.coordinate.latitude,location.coordinate.longitude);
        projectLocationMapView.isMyLocationEnabled = true
        projectLocationMapView.mapType = kGMSTypeNormal
        projectLocationMapView.accessibilityElementsHidden = true
        projectLocationMapView.delegate = self
        self.view.addSubview(projectLocationMapView)
        let fullMapButton = UIButton(type: .custom)
        fullMapButton.frame = CGRect(x: CGFloat(150), y: CGFloat(260), width: CGFloat(100), height: CGFloat(20))
        fullMapButton.backgroundColor = UIColor.darkGray
        fullMapButton.setTitle("View", for: .normal)
        fullMapButton.setTitleColor(UIColor.white, for: .normal)
        fullMapButton.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(9.0))!
        fullMapButton.addTarget(self, action: #selector(self.fullMapButtonClick), for: .touchUpInside)
        projectLocationMapView.addSubview(fullMapButton)
        nameLabel.text! = "\(returnDataDictUser["first_name"] as! String) \(returnDataDictUser["last_name"] as! String)"
        let url = URL(string: "\(ImageBaseUrl)\(returnDataDictUser["user_avatar"] as! String)")!
        let request = URLRequest(url: url)

        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response:URLResponse?, data:Data?, error:Error?) in
            if error == nil {
                let img = UIImage(data: data!)
                if img != nil {
                    self.profileImageView.image = img
                }
            } else {

            }
        }

//        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main, completionHandler: {(_ response: URLResponse, _ data: Data, _ error: Error) -> Void in
//            if !.error {
//                var img = UIImage(data: data)!
//                if img != nil {
//                    profileImageView.image = img
//                }
//            }
//        })
        projectNameLabel.text = (returnDataDict["project_name"] as! String)
        projectNameLabel.font = CenturyGothiCRegular15
        //invitationText.text = self.notificationMessage;
        invitationText.text! = "Has invited you to the following project:"
        invitationText.font = CenturyGothiCRegular11
        //    projectLocationLabel.text = @"Toronto, Canada";
        projectLocationLabel.text! = "\(returnDataDict["project_city"] as! String), \(returnDataDict["project_country"] as! String)"
        projectLocationLabel.font = CenturyGothiCRegular11
        var firstLabel = "\(returnDataDict["project_street_number"] as! String), \(returnDataDict["project_street"] as! String)"
        var secondLabel = (returnDataDict["project_city"] as! String)
        var thirdLabel = (returnDataDict["project_state"] as! String)
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel)\n\(secondLabel),\(thirdLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular15, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular11, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: CenturyGothiCRegular11, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count), length: thirdLabel.characters.count + 1))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGray, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count), length: thirdLabel.characters.count + 1))
        markerAddressLabel.attributedText = customTextLabelAttribute
        acceptProjectButton.setTitle("Accept Project", for: .normal)
        acceptProjectButton.titleLabel!.font = UIFont(name: "Helvetica-Bold", size: CGFloat(20.0))!
        acceptProjectButton.setTitleColor(UIColor(red: CGFloat(255.0), green: CGFloat(255.0), blue: CGFloat(255.0), alpha: CGFloat(0.7)), for: .normal)
        declineButton.setTitle("Decline", for: .normal)
        declineButton.titleLabel!.font = CenturyGothicBold15
        declineButton.titleLabel!.textColor = UIColor.darkGray
    }

    func fullMapButtonClick() {
    }
// MARK: - Geofence Creation

    func createGeoFence(forDevice projectData: NSMutableDictionary) {
        dataDict = NSMutableDictionary()
        dataDict = projectData
        var centre =  CLLocationCoordinate2D()
        centre.latitude = CLLocationDegrees(((dataDict["Project"] as! NSDictionary)["project_lat"] as! NSString).floatValue)
        centre.longitude = CLLocationDegrees(((dataDict["Project"] as! NSDictionary)["project_lng"] as! NSString).floatValue)
        APPDELEGATE.circularRegion = CLCircularRegion(center: centre, radius: CLLocationDistance(((dataDict["Project"] as! NSDictionary)["project_radius"] as! NSString).floatValue), identifier: ((dataDict["Project"] as! NSDictionary)["id"] as! String))
        // Typecast the instance for use with your CLRegion instance

        APPDELEGATE.locationManager?.startMonitoring(for: APPDELEGATE.circularRegion!)
//        APPDELEGATE.locationManager?.startMonitoring(for: (APPDELEGATE.circularRegion as! CLRegion))

        var homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
        self.navigationController!.pushViewController(homeScreen, animated: true)
        //    [locationManager startMonitoringForRegion:(CLRegion *)region];
    }

    func showAlert(withMessage message: String) {
        let tempAlert = UIAlertView(title: "Beehive", message: message, delegate: nil, cancelButtonTitle: "Dismiss", otherButtonTitles: "")
        tempAlert.show()
    }

    func saveDataInDocumentDirectory() {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as? NSArray
        let basePath = ((paths?.count)! > 0) ? paths?.object(at: 0) : nil
        let plistPath = URL(fileURLWithPath: basePath as! String).appendingPathComponent("BeehiveLocalDB.plist").absoluteString
        var projectDictionary = NSMutableDictionary(contentsOfFile: plistPath)
        print(projectDictionary)
        let storeProjectDetailArray = NSMutableArray()
        if projectDictionary?.allKeys.count == 0 {
            projectDictionary = NSMutableDictionary() /* capacity: 0 */
        }
        else {

            if let projectdict = projectDictionary?.object(forKey: "ProjectDetails") as? NSArray {

                for i in 0..<projectdict.count {
                    storeProjectDetailArray.add("\(projectdict.object(at: i))")
                }
                storeProjectDetailArray.insert((returnDataDict["Project"] as! String), at: 0)
                projectDictionary?["ProjectDetails"] = storeProjectDetailArray
                projectDictionary?.write(toFile: plistPath, atomically: true)
                let saveStatus = projectDictionary?.write(toFile: plistPath, atomically: true)
                print(" Status : \(saveStatus)")
                APPDELEGATE.getNotificationCountValue()
            }
        }
        //    projectDataDict = [[NSMutableDictionary alloc]init];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"id"] forKey:@"Project_Id"];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"project_name"] forKey:@"Project_Name"];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"project_city"] forKey:@"Project_City"];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"project_state"] forKey:@"Project_State"];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"project_lat"] forKey:@"Project_Lat"];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"project_lng"] forKey:@"Project_Long"];
        //    [projectDataDict setObject:[[returnDataDict objectForKey:@"Project"] objectForKey:@"project_radius"] forKey:@"Project_Radius"];

//        print("Update Geofence From Invitation View Controller")
        //    Before 11 Dec
        //    [appDelegate updateGeofences];
        //            After 11 Dec
        //    [appDelegate createNewGeofence:[returnDataDict objectForKey:@"Project"]];
        //    [appDelegate createGeoFenceForDevice:[returnDataDict objectForKey:@"Project"]];
        //APPDELEGATE.createGeofenceOfProject((returnDataDict["Project"] as! String), isWebAcceptedProject: false)
//        let sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
        self.navigationController!.pushViewController(homeScreen, animated: true)
        //        [self createGeoFenceForDevice:returnDataDict];
    }
// MARK: - ASIHTTP Requests & Delegates

    func fetchNotificationData() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)getInvitationData/\(self.notificationId!).json"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        let url = URL(string: strURL)!
        print("Login URL- \(url)")


        APIManager.sharedInstance.callAcceptTentativeCheckInWebService(url: strURL) { (response:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()

            if let result = response?[kResult] as? JSONDictionary {
                
                if "\(result[kstatus]!)" == kOK {

                    print(result)

                    if let projectArr = result["projectsArray"] as? JSONDictionary {
                        if let project = projectArr["Project"] as? JSONDictionary {
                            self.returnDataDict = project

                        }

                        if let project = projectArr["User"] as? JSONDictionary {
                            self.returnDataDictUser = project

                        }
                        self.customizeView()
                    }

                } else if "\(result[kstatus]!)" == "INVALID" {
                    let myAlert = UIAlertView(title: "Warning", message: (result["message"] as! String), delegate: self, cancelButtonTitle: "OK")
                    myAlert.show()
                }

            } else {

            }

        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData?.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

    func acceptProjectWebService() {
        APPDELEGATE.addChargementLoader()
            //    NSLog(@"project ID =%@",[[returnDataDict objectForKey:@"Project"] objectForKey:@"id"]);
            //    NSLog(@"Notification ID =%@",self.notificationId);
        let urlString = "\(BaseWebRequestUrl)\(AcceptProjectURL)"
        _ = URL(string: urlString)!

        var tempDict: JSONDictionary = [:]

        tempDict["push_notification_id"] = "\(self.notificationId!)" as AnyObject?
        tempDict["project_id"] = "\(returnDataDict["id"]!)" as AnyObject?
        APIManager.sharedInstance.acceptProjectWebService(dict: tempDict) { (response:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            if let result = response?[kResult] as? JSONDictionary {

                if "\(result[kstatus]!)" == kOK {
                    
                    self.saveDataInDocumentDirectory()

                } else if "\(result[kstatus]!)" == "INVALID" {
                    let myAlert = UIAlertView(title: "Warning", message: (result["message"] as! String), delegate: self, cancelButtonTitle: "OK")
                    myAlert.show()
                }


            } else {
                
            }


        }


//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.setPostValue(((returnDataDict["Project"] as! String)["id"] as! String), forKey: "project_id")
//        request.setPostValue(self.notificationId, forKey: "push_notification_id")
//        request.requestMethod = "POST"
//        request.tag = 2
//        request.startAsynchronous()
    }

    func declineProjectWebService() {
        APPDELEGATE.addChargementLoader()
            //    NSLog(@"project ID =%@",[[returnDataDict objectForKey:@"Project"] objectForKey:@"id"]);
            //    NSLog(@"Notification ID =%@",self.notificationId);
        let urlString = "\(BaseWebRequestUrl)\(DeclineProjectURL)"
        _ = URL(string: urlString)!
//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.setPostValue(((returnDataDict["Project"] as! String)["id"] as! String), forKey: "project_id")
//        request.setPostValue(self.notificationId, forKey: "push_notification_id")
//        request.requestMethod = "POST"
//        request.tag = 3
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            //    NSLog(@"Return Notification Detail Dict = %@",returnDict);
//            if (((returnDict["result"] as! String)["status"] as! String) == "INVALID") {
//                var myAlert = UIAlertView(title: "Warning", message: ((returnDict["result"] as! String)["message"] as! String), delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
//                myAlert.tag = 100
//                myAlert.show()
//                return
//            }
//            else if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                returnDataDict = ((returnDict["result"] as! String)["projectsArray"] as! String)
//                self.customizeView()
//            }
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Accept Project Response = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                //            Before 11 Dec
//                self.saveDataInDocumentDirectory()
//            }
//        }
//        else if request.tag == 3 {
//            var returnDict = request.responseString().jsonValue()
//            print("Decline Project Response = \(returnDict)")
//            //        [self.navigationController popViewControllerAnimated:YES];
//            appDelegate.getNotificationCountValue()
//            var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//            var homeScreen = sb.instantiateViewController(withIdentifier: "HomeScreenController")
//            self.navigationController!.pushViewController(homeScreen, animated: true)
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        var alert: UIAlertView?
//        if .error.code == 1 {
//            alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//
//    }

    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {

        if alertView.tag == 100 {
            popTo()
        }
    }
}
//
//  InvitationViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 08/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
