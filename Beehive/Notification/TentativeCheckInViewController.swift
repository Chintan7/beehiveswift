//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  TentativeCheckInViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 01/10/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class TentativeCheckInViewController: UIViewController {

    @IBOutlet var inviteeImageView: UIImageView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var inviteeNameLabel: UILabel!
    var notificationId: String!
    var returnDataDict = JSONDictionary()

    @IBAction func acceptButtonPressed(_ sender: Any) {
        self.callAcceptTentativeCheckInWebService()
    }

    @IBAction func declineButtonPressed(_ sender: Any) {
        self.callDeclineTentativeCheckInWebService()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchNotificationData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func viewCustomization() {


        if let user = returnDataDict["sender"] as? JSONDictionary {
            inviteeNameLabel.text = "\(user["first_name"]!) " + "\(user["last_name"]!)"
            let url = URL(string: "\(ImageBaseUrl)\(user["user_avatar"] as! String)")!
            inviteeImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ImageIcon.png"))
        }
        inviteeImageView.layer.borderWidth = 0.1
        inviteeImageView.layer.borderColor = DARKGRAYCOLOR.cgColor

        //inviteeImageView.setImageWith(url, placeholderImage: UIImage(named: "ImageIcon.png")!)
        messageLabel.text = "has manually checked into SDN Gardening at 03:30pm."
        messageLabel.font
            = CenturyGothiCRegular13
    }
    /*
    #pragma mark - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
// MARK: - ASIHTTP Requests & Delegates

    func fetchNotificationData() {
        APPDELEGATE.addChargementLoader()
        var strURL = "\(BaseWebRequestUrl)\(TentetativeCheckInNotificationUrl)/\(notificationId!).json"
        strURL = strURL.replacingOccurrences(of: " ", with: "")
        let url = URL(string: strURL)!
        print("Tentative URL- \(url)")


        APIManager.sharedInstance.fetchNotificationData(id: notificationId!) { (dict:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()
            if error == nil {

                if let result = dict?[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {

                        if let dictObj = dict {
                            self.returnDataDict =  dictObj
                            self.viewCustomization()
                        }


                    } else if "\(result[kstatus]!)" == "ERROR"  {
                        var myAlert = UIAlertView(title: "Beehive", message: (result["message"])! as! String, delegate: nil, cancelButtonTitle: "OK")
                        myAlert.tag = 100
                        myAlert.show()
                        return
                    } else if "\(result[kstatus]!)" == kINVALID  {

                    }


                } else {

                }
            }

        }

//        var request = ASIFormDataRequest(URL: url)
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.delegate = self
//        request.tag = 1
//        request.requestMethod = "GET"
//        request.startAsynchronous()
    }

    func callAcceptTentativeCheckInWebService() {
        APPDELEGATE.addChargementLoader()
            //    NSLog(@"project ID =%@",[[returnDataDict objectForKey:@"Project"] objectForKey:@"id"]);
            //    NSLog(@"Notification ID =%@",self.notificationId);
        let urlString = "\(BaseWebRequestUrl)acceptTentativeCheckin/\(self.notificationId!).json"
        _ = URL(string: urlString)!



        APIManager.sharedInstance.callAcceptTentativeCheckInWebService(url: urlString) { (response:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {

                if let result = response?[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        APPDELEGATE.getNotificationCountValue()
                        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
                        self.navigationController!.pushViewController(homeScreen, animated: true)

                    }
                    
                } else {
                    
                }
            }

        }

//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "GET"
//        request.tag = 2
//        request.startAsynchronous()
    }

    func callDeclineTentativeCheckInWebService() {
        APPDELEGATE.addChargementLoader()
        let urlString = "\(BaseWebRequestUrl)declineTentativeCheckin/\(self.notificationId!).json"
//        print("Decline Url >>>>>>>>: \(urlString)")
//        _ = URL(string: urlString)!



        APIManager.sharedInstance.callAcceptTentativeCheckInWebService(url: urlString) { (response:JSONDictionary?, error:NSError?) in

            APPDELEGATE.removeChargementLoader()

            if error == nil {

                if let result = response?[kResult] as? JSONDictionary {

                    if "\(result[kstatus]!)" == kOK {
                        APPDELEGATE.getNotificationCountValue()
                        let homeScreen = STORYBOARD.instantiateViewController(withIdentifier: "HomeScreenController")
                        self.navigationController!.pushViewController(homeScreen, animated: true)
                    } else if "\(result[kstatus]!)" == "ERROR" {
                        SLAlert.showAlert(str: "\(result["message"]!)")
                    }
                } else {
                    
                }
            }
            
        }


//        var request = ASIFormDataRequest(URL: serviceURL)
//        request.delegate = self
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "GET"
//        request.tag = 3
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            //    NSLog(@"Return Notification Detail Dict = %@",returnDict);
//            if (((returnDict["result"] as! String)["status"] as! String) == "ERROR") {
//                var myAlert = UIAlertView(title: "Beehive", message: ((returnDict["result"] as! String)["message"] as! String), delegate: self, cancelButtonTitle: "OK", otherButtonTitles: "")
//                myAlert.tag = 100
//                myAlert.show()
//                return
//            }
//            else if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                returnDataDict = returnDict
//                self.viewCustomization()
//            }
//        }
//        else if request.tag == 2 {
//            var returnDict = request.responseString().jsonValue()
//            print("Accept Project Response = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                appDelegate.getNotificationCountValue()
//                var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                var homeScreen = sb.instantiateViewController(withIdentifier: "HomeScreenController")
//                self.navigationController!.pushViewController(homeScreen, animated: true)
//                //            [self.navigationController popViewControllerAnimated:YES];
//            }
//        }
//        else if request.tag == 3 {
//            var returnDict = request.responseString().jsonValue()
//            print("Decline Project Response = \(returnDict)")
//            if (((returnDict["result"] as! String)["status"] as! String) == "OK") {
//                appDelegate.getNotificationCountValue()
//                var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                var homeScreen = sb.instantiateViewController(withIdentifier: "HomeScreenController")
//                self.navigationController!.pushViewController(homeScreen, animated: true)
//            }
//            //        [self.navigationController popViewControllerAnimated:YES];
//        }
//
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        var alert: UIAlertView?
//        if .error.code == 1 {
//            alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404

//            alert!.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert!.tag = 404
//            alert!.show()
//            alert = nil
//        }
//
//    }

}
//
//  TentativeCheckInViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 01/10/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
