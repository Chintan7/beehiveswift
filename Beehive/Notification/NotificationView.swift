//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  NotificationView.h
//  Beehive
//
//  Created by Deepak Dixit on 22/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
protocol NotificationViewPrtotcol: NSObjectProtocol {
    func didselectRow(_ tableView: UITableView, indexPath: IndexPath)
}
class NotificationView: UIView, UITableViewDelegate, UITableViewDataSource {
    var notificationCustomView: UIView!


    var delegate: NotificationViewPrtotcol!
    var notificationTable: UITableView!

    func customView() {
        notificationCustomView = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504)))
        notificationCustomView.backgroundColor = UIColor.darkGray
        if IS_IPHONE5 {
            notificationTable = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504)), style: .plain)
        }
        else {
            notificationTable = UITableView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(415)), style: .plain)
        }
        notificationTable.delegate = self
        notificationTable.dataSource = self
        notificationTable.backgroundColor = UIColor.lightGray
        notificationCustomView.addSubview(notificationTable)
        self.addSubview(notificationCustomView)
        print("Notifications : \(APPDELEGATE.allNotificationArray)")
        print("Content Count : \(APPDELEGATE.allNotificationArray.count)")
    }


    override init(frame: CGRect) {
        super.init(frame: frame)

        if IS_IPHONE5 {
            self.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(504))
        }
        else {
            self.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(424))
        }
    
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
// MARK: NotificationTableView Delegate/Datasource

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APPDELEGATE.allNotificationArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? NotificationCell
        if cell == nil {
            var nib = Bundle.main.loadNibNamed("NotificationCell", owner: self, options: nil)
            cell = nib?[0] as? NotificationCell
        }
        let markReadLabel = UILabel(frame: CGRect(x: CGFloat(230), y: CGFloat(50), width: CGFloat(90), height: CGFloat(10)))
        markReadLabel.text = "Mark As Read"
        markReadLabel.textAlignment = .right
        markReadLabel.font = CenturyGothiCRegular11
        markReadLabel.textColor = UIColor.white
        cell?.contentView.addSubview(markReadLabel)
        if (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "checked_out") {
            cell?.imageView!.image = UIImage(named: "waraningIcon.png")!
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "user_profile") {
            cell?.imageView!.image = UIImage(named: "profileIcon.png")!
        }
        else if (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "invitation_accepted") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "invitation_rejected") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "invitation") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "tentative_checked_in") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "tentative_checked_in_confirmed") || (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "type") as! String) == "tentative_checked_in_declined") {
            cell?.imageView!.image = UIImage(named: "projectsIcon.png")!
        }
        else {

        }

        cell?.textLabel!.text = "\((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "message") as! String)"
        if (((APPDELEGATE.allNotificationArray[indexPath.row] as AnyObject).value(forKey: "status") as! String) == "unread") {
            cell?.backgroundColor = UIColor.gray
        }
        else {
            cell?.backgroundColor = UIColor.clear
        }
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let result: CGFloat = 66.0
        return result
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didselectRow(tableView, indexPath: indexPath)
    }
}
//
//  NotificationView.m
//  Beehive
//
//  Created by Deepak Dixit on 22/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
