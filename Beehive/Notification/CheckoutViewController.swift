//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CheckoutViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 10/06/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
class CheckoutViewController: UIViewController {

    var screenSize = CGSize.zero
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var settingButton: UIButton!
    @IBOutlet var phoneButton: UIButton!
    @IBOutlet var keepButton: UIButton!
    @IBOutlet var ignoreButton: UIButton!
    @IBOutlet var viewProjectButton: UIButton!
    @IBOutlet var resetRadiusButton: UIButton!

    @IBAction func contactButtonClick(_ sender: Any) {
    }

    @IBAction func settingButtonClick(_ sender: Any) {
    }

    @IBAction func keepButtonClick(_ sender: Any) {
    }

    @IBAction func ignoreButtonClick(_ sender: Any) {
    }

    @IBAction func viewProjectClick(_ sender: Any) {
    }

    @IBAction func radiusAdjustClick(_ sender: Any) {
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        nameLabel.font = CenturyGothicBold18
        descriptionLabel.font = CenturyGothiCRegular15
        ignoreButton.titleLabel!.font = CenturyGothiCRegular15
        viewProjectButton.titleLabel!.font = CenturyGothiCRegular15
        resetRadiusButton.titleLabel!.font = CenturyGothiCRegular15
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
