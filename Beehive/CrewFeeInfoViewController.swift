//  Converted with Swiftify v1.0.6171 - https://objectivec2swift.com/
//
//  CrewFeeInfoViewController.h
//  Beehive
//
//  Created by Deepak Dixit on 02/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
import UIKit
//#import "NSString+SBJSON.h"
class CrewFeeInfoViewController: BaseViewController {

    @IBOutlet var viewMessageLabel: UILabel!
    @IBOutlet var addMemberButton: UIButton!
    @IBOutlet var notThanksButton: UIButton!
    @IBOutlet var termConditionButton: UIButton!
    @IBOutlet var screenBGView: UIView!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var termConditionBGView: UIView!
    @IBOutlet var termConditionTextview: UITextView!
    var crewCount = ""
    var projectId = ""
    var selectedCrewArray = [[String: String]]()

    @IBAction func addCrewBtnPressed(_ sender: Any) {
        var addCrewString = String()
            
        if selectedCrewArray.count > 0 {

            addCrewString = selectedCrewArray[0]["Crew_Id"]!

            if selectedCrewArray.count > 1 {

                for dict in selectedCrewArray {
                    addCrewString =  addCrewString + "|\(dict["Crew_Id"]!)"
                }
            }
        }

//        if selectedCrewArray.count > 1 {
//            addCrewString = ((selectedCrewArray[0] as AnyObject).value(forKey: "Crew_Id") as! String)
//            for j in 1..<selectedCrewArray.count {
//                addCrewString = "\(addCrewString)|\((selectedCrewArray[j] as AnyObject).value(forKey: "Crew_Id") as! String)"
//            }
//        }
//        else if selectedCrewArray.count == 1 {
//            addCrewString = ((selectedCrewArray[0] as AnyObject).value(forKey: "Crew_Id") as! String)
//        }
//
//        print("String \(addCrewString)")
        if (addCrewString == "") {
            let myAlert = UIAlertView(title: "Error", message: "Please select atleast one crew", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
            myAlert.show()
            return
        }
        self.addCrew(intoProject: addCrewString)
    }

    @IBAction func noThanksBtnPressed(_ sender: Any) {
        popTo()
    }

    @IBAction func termConditionBtnPressed(_ sender: Any) {
        screenBGView.isHidden = false
        if screenSize.height == 568 {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.termConditionBGView.frame = CGRect(x: CGFloat(self.termConditionBGView.frame.origin.x), y: CGFloat(218), width: CGFloat(self.termConditionBGView.frame.size.width), height: CGFloat(350))
                var frame = self.termConditionTextview.frame
                frame.size.height = 260
                self.termConditionTextview.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
            })
        }
        else {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.termConditionBGView.frame = CGRect(x: CGFloat(self.termConditionBGView.frame.origin.x), y: CGFloat(130), width: CGFloat(self.termConditionBGView.frame.size.width), height: CGFloat(300))
                var frame = self.termConditionTextview.frame
                frame.size.height = 260
                self.termConditionTextview.frame = frame
            }, completion: {(_ finished: Bool) -> Void in
            })
        }
    }

    @IBAction func closeTermCondition(_ sender: Any) {
        screenBGView.isHidden = true
        UIView.animate(withDuration: 0.4, animations: {() -> Void in
            var frame = self.termConditionBGView.frame
            frame.origin.y = screenSize.height
            self.termConditionBGView.frame = frame
        }, completion: {(_ finished: Bool) -> Void in
            //        NSLog(@"Closed");
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        //    UIAlertView  *welcomeAlert = [[UIAlertView alloc] initWithTitle:@"Thank You" message:[NSString stringWithFormat:@"You are added %@ Members",self.crewCount] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        //    [welcomeAlert show];
        print("Selected Crew Array : \(self.selectedCrewArray)")
        print("Project Id : \(self.projectId)")
        self.customizeView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController!.navigationBar.addSubview(Global.checkedInModeButton())
    }

    func customizeView() {

        let menuButton = UIBarButtonItem(image: UIImage(named: "MenuBtn.png")!, style: .plain, target: self, action: #selector(self.sideMenuBar))
        menuButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = menuButton
            //    Top Toolbar Create
        let viewToolbar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(320), height: CGFloat(44)))
        viewToolbar.setBackgroundImage(UIImage(named: "greytoolbar.png")!, forToolbarPosition: .any, barMetrics: .default)
        let image = UIImage(named: "backBtn.png")!
        let backBtn = UIButton(type: .custom)
        backBtn.bounds = CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(image.size.width), height: CGFloat(image.size.height))
        backBtn.setImage(image, for: .normal)
        backBtn.addTarget(self, action: #selector(self.backBtnClick), for: .touchUpInside)
        let bckBarButton = UIBarButtonItem(customView: backBtn)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        var barItems = [UIBarButtonItem]()
        barItems.append(bckBarButton)
        barItems.append(flexSpace)
        viewToolbar.setItems(barItems, animated: true)
        self.view.addSubview(viewToolbar)
        var firstLabel = "You've added"
        var secondLabel = self.crewCount
        var thirdLabel = "crew members to a project."
        var fourthLabel = "You will be billed $5 per crew member each month they are actively checking into projects."
        let fifthLabel = "You may add these crew members to additional projects at no extra charge"
        let customTextLabelAttribute = NSMutableAttributedString(string: "\(firstLabel) \(secondLabel) \(thirdLabel)\n\n\(fourthLabel)\n\n\(fifthLabel)")
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: 0, length: firstLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica-Bold", size: CGFloat(15.0))!, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: firstLabel.characters.count + 1, length: secondLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1), length: thirdLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1), length: thirdLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1) + (thirdLabel.characters.count + 1), length: fourthLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1) + (thirdLabel.characters.count + 1), length: fourthLabel.characters.count))
        customTextLabelAttribute.addAttribute(NSFontAttributeName, value: UIFont(name: "Century Gothic", size: CGFloat(12.0))!, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1) + (thirdLabel.characters.count + 1) + (fourthLabel.characters.count + 1), length: fifthLabel.characters.count + 2))
        customTextLabelAttribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.gray, range: NSRange(location: (firstLabel.characters.count + 1) + (secondLabel.characters.count + 1) + (thirdLabel.characters.count + 1) + (fourthLabel.characters.count + 1), length: fifthLabel.characters.count + 2))
        viewMessageLabel.attributedText = customTextLabelAttribute
        addMemberButton.setTitle("Add Crew Members", for: .normal)
        addMemberButton.titleLabel!.font = UIFont(name: "Helvetica Bold", size: CGFloat(18.0))!
        addMemberButton.setTitleColor(UIColor(red: CGFloat(255.0), green: CGFloat(255.0), blue: CGFloat(255.0), alpha: CGFloat(0.6)), for: .normal)
        let termButtonLabel = "Legal | Terms & Conditions"
        termConditionButton.titleLabel!.font = UIFont(name: "Century Gothic", size: CGFloat(16.0))!
        termConditionButton.setTitle(termButtonLabel, for: .normal)
        if screenSize.height == 568 {
            termConditionBGView.frame = CGRect(x: CGFloat(termConditionBGView.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(termConditionBGView.frame.size.width), height: CGFloat(350))
        }
        else {
            termConditionBGView.frame = CGRect(x: CGFloat(termConditionBGView.frame.origin.x), y: CGFloat(screenSize.height), width: CGFloat(termConditionBGView.frame.size.width), height: CGFloat(300))
        }
        termConditionBGView.layer.cornerRadius = 5.0
        termConditionBGView.layer.borderWidth = 1.0
        termConditionBGView.layer.borderColor = UIColor.darkGray.cgColor
        termConditionTextview.font = UIFont(name: "Century Gothic", size: CGFloat(14.0))!
        screenBGView.isHidden = true
    }



    func backBtnClick() {
        popTo()
    }

    func addCrew(intoProject crewIdString: String) {
        APPDELEGATE.addChargementLoader()
        let urlString = "\(BaseWebRequestUrl)\(AddCrewBySelection)"
        _ = URL(string: urlString)!

        var dict = [String: String]()
        dict["project_id"] = projectId
        dict["user_id"] = crewIdString

        APPDELEGATE.addChargementLoader()
        APIManager.sharedInstance.addCrew(dict: dict) { (result:JSONDictionary?, error:NSError?) in
            APPDELEGATE.removeChargementLoader()
            if error == nil {

                if let paymentStauts = result?["payment_status"] as? Bool {

                    if paymentStauts == true {

                        let newView = STORYBOARD.instantiateViewController(withIdentifier: "ProjectPageViewController") as! ProjectPageViewController
                        newView.projectId = self.projectId
                        newView.isNewlyAddedProject = true
                        self.navigationController!.pushViewController(newView, animated: true)
                        SLAlert.showAlert(str: "Added crew members")
                    } else {
                        let newView = STORYBOARD.instantiateViewController(withIdentifier: "CrewMemberPaywallViewController")
                        self.navigationController!.pushViewController(newView, animated: true)
                    }

                } else {

                }

            }

        }

        


//        var request = ASIFormDataRequest.init(url: serviceURL)
//        request.delegate = self
//        request.setPostValue(projectId, forKey: "project_id")
//        request.setPostValue(crewIdString, forKey: "user_id")
//        var prefs = UserDefaults.standard
//        var emailId = prefs.object(forKey: "UserId")!
//        var userToken = prefs.object(forKey: "User_Token")!
//        var authStr = "\(emailId):\(userToken)"
//        var authData = authStr.data(using: String.Encoding.utf8)
//        var authValue = "Basic \(authData.base64EncodedString(withOptions: .endLineWithCarriageReturn))"
//        request.addHeader("Authorization", value: authValue)
//        request.requestMethod = "POST"
//        request.tag = 1
//        request.startAsynchronous()
    }

//    func requestFinished(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        if request.tag == 1 {
//            var returnDict = request.responseString().jsonValue()
//            print("Add Crew Return Data = \(returnDict)")
//            if ((returnDict["result"] as! String)["payment_status"] as! String).boolValue == true {
//                var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                var newView = sb.instantiateViewController(withIdentifier: "ProjectPageViewController")
//                newView.projectId = self.projectId
//                newView.isNewlyAddedProject = true
//                self.navigationController!.pushViewController(newView, animated: true)
//            }
//            else {
//                var sb = UIStoryboard(name: "Main_iPhone", bundle: nil)
//                var newView = sb.instantiateViewController(withIdentifier: "CrewMemberPaywallViewController")
//                self.navigationController!.pushViewController(newView, animated: true)
//            }
//        }
//    }

//    func requestFailed(_ request: ASIHTTPRequest) {
//        appDelegate.removeChargementLoader()
//        var .error = request.error
//        print("Error : \(.error)")
//        if .error.code == 1 {
//            var alert = UIAlertView(title: "Network Error", message: "Internet connection is required", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//        else if .error.code != 4 {
//            var alert = UIAlertView(title: "Error Occurred", message: "It seems the Server network is not responding.", delegate: self, cancelButtonTitle: "Ok", otherButtonTitles: "")
//            alert.show()
//            alert = nil
//        }
//
//    }

}
//
//  CrewFeeInfoViewController.m
//  Beehive
//
//  Created by Deepak Dixit on 02/07/14.
//  Copyright (c) 2014 Deepak Dixit. All rights reserved.
//
