//
//  SLAlert.swift
//  Beehive
//
//  Created by SoluLab on 07/12/16.
//  Copyright © 2016 SoluLab. All rights reserved.
//

import Foundation

class SLAlert {

    class func showAlert(str: String)  {
        let alert = UIAlertController.init(title: kAPPNAME, message: str, preferredStyle: .alert)
        let alertAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        APPDELEGATE.window?.rootViewController!.present(alert, animated: true, completion: nil)
    }
}
